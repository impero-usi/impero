IMPERO!

Is a web strategy game project for the Software Atelier IV class at USI (Lugano, Switzerland). It is mainly built in java with a tomcat server and mongo DB.

To build the project you need to use maven:
1) cd to the /impero/ folder in the repository
2) make sure Java 7 is used (on mac: export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.<VERSION>.jdk/Contents/Home)
3) check that java 7 is used by issuing 'mvn -version'. Java version should be 1.7.*.
4) mvn tomcat:run (to start the server)
5) mvn verify (to run all tests)
