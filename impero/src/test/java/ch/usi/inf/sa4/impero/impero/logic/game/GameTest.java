package ch.usi.inf.sa4.impero.impero.logic.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.logic.action.MoveAction;
import ch.usi.inf.sa4.impero.impero.logic.changes.AbstractChange;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class GameTest {
	private Game game;
	
	@Before
	public void setUp() throws Exception {
		List<String> playersList = new ArrayList<String>();
		playersList.add("p1Test");
		playersList.add("p2Test");
		game = new Game(40, 40, 5000, 10000, playersList, "Test", 10, 50, 10, 5, false, false);
	}
	
	@Test
	public void testSpawnHQ() {
		List<AbstractProductionBuilding> buildings = game.getProductionBuildingList();
		assertEquals(2, buildings.size());
		boolean p1Ok = false;
		boolean p2Ok = false;
		for (AbstractBuilding abstractBuilding : buildings) {
			if(abstractBuilding.getOwner().getPlayerID().equals("p1Test")) {
				p1Ok = true;
			} else if(abstractBuilding.getOwner().getPlayerID().equals("p2Test")) {
				p2Ok = true;
			}
			assertTrue(abstractBuilding.getHexagon().getType() != TerrainType.SHALLOW_WATER &&
					abstractBuilding.getHexagon().getType() != TerrainType.DEEP_WATER);
		}
		assertTrue(p1Ok && p2Ok);
	}
	
	@Test
	public void testTurnExecuted() {
		game.turnExecuted("p1Test");
		game.turnExecuted("p2Test");
		assertEquals(2, (int) game.getTurnNumber());
	}
	
	@Test
	public void testmovingUnit(){
		assertEquals(2, game.getPlayerList().size());
		AbstractProductionBuilding b = null;
		for(AbstractProductionBuilding building : game.getProductionBuildingList()){
			if(building.getOwner().equals(game.getPlayerList().get(0))){
				b = building;
			}
		}
		game.getVerifier().verifySpawnUnitAction(game.getPlayerList().get(0).getPlayerID(),b.getHexagon().getRow(), b.getHexagon().getCol(), Bomber.class.getSimpleName());
		assertEquals(1, game.getActionList().get(game.getPlayerList().get(0)).size());
		for(Player p : game.getPlayerList()){
			game.turnExecuted(p.getPlayerID());
		}
		assertEquals(1, game.getChangeLog().size());
		assertEquals(4, game.getLastTurnChanges(game.getPlayerList().get(0)).size());
		assertEquals(0, game.getActionList().get(game.getPlayerList().get(0)).size());
		assertEquals(2,(int) game.getTurnNumber());
		assertEquals(1, game.getUnitList().size());
		AbstractUnit u = game.getUnitList().get(0);
		int initialRow = u.getHexagon().getRow();
		int initialCol = u.getHexagon().getCol();
		Map<Hexagon, Integer> map = game.getVerifier().getMovementCells(initialRow, initialCol, u.getOwner().getPlayerID());
		MoveAction action = game.getVerifier().verifyMoveAction(initialRow, initialCol, initialRow + 2, initialCol + 3, u.getOwner().getPlayerID());
		if(action == null){
			fail();
		}
		assertEquals(1, game.getActionList().get(game.getPlayerList().get(0)).size());
		for(Player p : game.getPlayerList()){
			game.turnExecuted(p.getPlayerID());
		}
		assertEquals(0, game.getActionList().get(game.getPlayerList().get(0)).size());
		assertEquals(3,(int) game.getTurnNumber());
		assertEquals(2, game.getChangeLog().size());
		List<AbstractChange> changes = game.getLastTurnChanges(game.getPlayerList().get(0));
		if((changes.size() != 5) && (changes.size() != 6)){
			fail();
		}
		int finalRow = game.getUnitList().get(0).getHexagon().getRow();
		int finalCol = game.getUnitList().get(0).getHexagon().getCol();
		assertEquals(initialRow + 2, u.getHexagon().getRow());
		assertEquals(initialCol + 3, u.getHexagon().getCol());
		
		assertEquals(initialRow + 2, finalRow);
		assertEquals(initialCol + 3, finalCol);
	}
	
	@Test
	public void testCheckGameOver(){
		if(game.checkGameOver()){
			fail();
		}
		game.getProductionBuildingList().remove(0);
		game.turnEnd();
		if(!game.checkGameOver()){
			fail();
		}
		if(!game.getGameOver()){
			fail();
		}
	}
	
}
