package ch.usi.inf.sa4.impero.impero.db.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.db.MongoGames;


public class MongoGamesTest {

	@Test
	public void testGetGameListByPlayer() throws Exception {
		Map<String,String> a = MongoGames.getGameListByPlayerID("5345b930036405f8f43ade4a");
		assertThat(a.size(),is(not(0)));
	}
	
	
	@Test
	public void testGetGameList() throws Exception {
		List<String> a = MongoGames.getGameList();
		assertThat(a.size(),is(not(0)));
		
	}

}
