package ch.usi.inf.sa4.impero.impero.logic.map.strategy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.util.Hash;

import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.map.Coordinate;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.mapgenerator.MapGenerator;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * 
 * @author Luca
 * 
 */
public class HexagonalStrategyTest {

	private HexagonalMap map;
	private BoardStrategy strategy;

	/**
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.map = new MapGenerator().generateMap(100, 100, 50, 60, 10, 3,
				false, false);
		this.strategy = new HexagonalStrategy();
	}

	/**
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.map = null;
	}

	/**
	 * 
	 */
	@Test
	public void testNeighborsInRange() {
		List<Hexagon> test1 = new ArrayList<Hexagon>();
		test1.add(new Hexagon(TerrainType.SAND, 1, 1));
		test1.add(new Hexagon(TerrainType.SAND, 1, 2));
		test1.add(new Hexagon(TerrainType.SAND, 1, 3));
		test1.add(new Hexagon(TerrainType.SAND, 2, 1));
		test1.add(new Hexagon(TerrainType.SAND, 2, 3));
		test1.add(new Hexagon(TerrainType.SAND, 3, 2));

		List<Hexagon> neighbour = this.strategy.getNeighboursInRange(this.map,
				2, 2, 1);
		for (Hexagon hexagon : neighbour) {
			assertTrue(test1.contains(hexagon));
		}

		List<Hexagon> test2 = new ArrayList<Hexagon>();
		test2.add(new Hexagon(TerrainType.SAND, 1, 1));
		test2.add(new Hexagon(TerrainType.SAND, 1, 2));
		test2.add(new Hexagon(TerrainType.SAND, 1, 3));
		test2.add(new Hexagon(TerrainType.SAND, 2, 1));
		test2.add(new Hexagon(TerrainType.SAND, 2, 3));
		test2.add(new Hexagon(TerrainType.SAND, 3, 2));
		test2.add(new Hexagon(TerrainType.SAND, 0, 1));
		test2.add(new Hexagon(TerrainType.SAND, 0, 2));
		test2.add(new Hexagon(TerrainType.SAND, 0, 3));
		test2.add(new Hexagon(TerrainType.SAND, 1, 0));
		test2.add(new Hexagon(TerrainType.SAND, 1, 4));
		test2.add(new Hexagon(TerrainType.SAND, 2, 0));
		test2.add(new Hexagon(TerrainType.SAND, 2, 4));
		test2.add(new Hexagon(TerrainType.SAND, 3, 0));
		test2.add(new Hexagon(TerrainType.SAND, 3, 1));
		test2.add(new Hexagon(TerrainType.SAND, 3, 3));
		test2.add(new Hexagon(TerrainType.SAND, 3, 4));
		test2.add(new Hexagon(TerrainType.SAND, 4, 2));

		List<Hexagon> neighbour2 = this.strategy.getNeighboursInRange(this.map,
				2, 2, 2);
		for (Hexagon hexagon : neighbour2) {
			assertTrue(test2.contains(hexagon));
		}

		List<Hexagon> test3 = new ArrayList<Hexagon>();
		test3.add(new Hexagon(TerrainType.SAND, 1, 1));
		test3.add(new Hexagon(TerrainType.SAND, 1, 2));
		test3.add(new Hexagon(TerrainType.SAND, 1, 3));
		test3.add(new Hexagon(TerrainType.SAND, 2, 1));
		test3.add(new Hexagon(TerrainType.SAND, 2, 3));
		test3.add(new Hexagon(TerrainType.SAND, 3, 2));
		test3.add(new Hexagon(TerrainType.SAND, 0, 1));
		test3.add(new Hexagon(TerrainType.SAND, 0, 2));
		test3.add(new Hexagon(TerrainType.SAND, 0, 3));
		test3.add(new Hexagon(TerrainType.SAND, 1, 0));
		test3.add(new Hexagon(TerrainType.SAND, 1, 4));
		test3.add(new Hexagon(TerrainType.SAND, 2, 0));
		test3.add(new Hexagon(TerrainType.SAND, 2, 4));
		test3.add(new Hexagon(TerrainType.SAND, 3, 0));
		test3.add(new Hexagon(TerrainType.SAND, 3, 1));
		test3.add(new Hexagon(TerrainType.SAND, 3, 3));
		test3.add(new Hexagon(TerrainType.SAND, 3, 4));
		test3.add(new Hexagon(TerrainType.SAND, 4, 2));
		test3.add(new Hexagon(TerrainType.SAND, 0, 0));
		test3.add(new Hexagon(TerrainType.SAND, 0, 4));
		test3.add(new Hexagon(TerrainType.SAND, 0, 5));
		test3.add(new Hexagon(TerrainType.SAND, 1, 5));
		test3.add(new Hexagon(TerrainType.SAND, 2, 5));
		test3.add(new Hexagon(TerrainType.SAND, 3, 5));
		test3.add(new Hexagon(TerrainType.SAND, 4, 0));
		test3.add(new Hexagon(TerrainType.SAND, 4, 1));
		test3.add(new Hexagon(TerrainType.SAND, 4, 4));
		test3.add(new Hexagon(TerrainType.SAND, 4, 3));
		test3.add(new Hexagon(TerrainType.SAND, 5, 2));

		List<Hexagon> neighbour3 = this.strategy.getNeighboursInRange(this.map,
				2, 2, 3);
		for (Hexagon hexagon : neighbour3) {
			assertTrue(test3.contains(hexagon));
		}
		// Works like a charm
	}

	/**
	 * 
	 */
	@Test
	public void testNeighborsHexagon() {
		List<Hexagon> test1 = new ArrayList<Hexagon>();
		test1.add(new Hexagon(TerrainType.SAND, 0, 1));
		test1.add(new Hexagon(TerrainType.SAND, 1, 0));

		Map<Integer, Hexagon> neighbors = this.strategy.getNeighbours(this.map,
				0, 0);
		for (Integer integer : neighbors.keySet()) {
			assertTrue(test1.contains(neighbors.get(integer)));
		}

		List<Hexagon> test2 = new ArrayList<Hexagon>();
		test2.add(new Hexagon(TerrainType.SAND, 0, 1));
		test2.add(new Hexagon(TerrainType.SAND, 1, 0));
		test2.add(new Hexagon(TerrainType.SAND, 1, 2));
		test2.add(new Hexagon(TerrainType.SAND, 2, 2));
		test2.add(new Hexagon(TerrainType.SAND, 2, 1));
		test2.add(new Hexagon(TerrainType.SAND, 2, 0));

		Map<Integer, Hexagon> neighbors2 = this.strategy.getNeighbours(
				this.map, 1, 1);
		for (Integer integer : neighbors2.keySet()) {
			assertTrue(test2.contains(neighbors2.get(integer)));
		}

		List<Hexagon> test3 = new ArrayList<Hexagon>();
		test3.add(new Hexagon(TerrainType.SAND, 0, 1));
		test3.add(new Hexagon(TerrainType.SAND, 1, 1));
		test3.add(new Hexagon(TerrainType.SAND, 0, 2));
		test3.add(new Hexagon(TerrainType.SAND, 2, 2));
		test3.add(new Hexagon(TerrainType.SAND, 1, 3));
		test3.add(new Hexagon(TerrainType.SAND, 0, 3));

		Map<Integer, Hexagon> neighbors3 = this.strategy.getNeighbours(
				this.map, 1, 2);
		for (Integer integer : neighbors3.keySet()) {
			assertTrue(test3.contains(neighbors3.get(integer)));
		}
	}

	/**
	 * 
	 */
	@Test
	public void testNeighborsCoordinates() {
		List<Coordinate> test1 = new ArrayList<Coordinate>();
		test1.add(new Coordinate(0, 1));
		test1.add(new Coordinate(1, 0));

		Map<Integer, Coordinate> neighbors = this.strategy
				.getNeighboursCoordinates(this.map, 0, 0);
		for (Integer integer : neighbors.keySet()) {
			assertTrue(test1.contains(neighbors.get(integer)));
		}

		List<Coordinate> test2 = new ArrayList<Coordinate>();
		test2.add(new Coordinate(1, 0));
		test2.add(new Coordinate(0, 1));
		test2.add(new Coordinate(1, 2));
		test2.add(new Coordinate(2, 2));
		test2.add(new Coordinate(2, 1));
		test2.add(new Coordinate(2, 0));

		Map<Integer, Coordinate> neighbors2 = this.strategy
				.getNeighboursCoordinates(this.map, 1, 1);
		for (Integer integer : neighbors2.keySet()) {
			assertTrue(test2.contains(neighbors2.get(integer)));
		}

		List<Coordinate> test3 = new ArrayList<Coordinate>();
		test3.add(new Coordinate(0, 1));
		test3.add(new Coordinate(1, 1));
		test3.add(new Coordinate(0, 2));
		test3.add(new Coordinate(2, 2));
		test3.add(new Coordinate(1, 3));
		test3.add(new Coordinate(0, 3));

		Map<Integer, Coordinate> neighbors3 = this.strategy
				.getNeighboursCoordinates(this.map, 1, 2);
		for (Integer integer : neighbors3.keySet()) {
			assertTrue(test3.contains(neighbors3.get(integer)));
		}
	}

	/**
	 * 
	 */
	@Test
	public void testNeighborsCellInput() {
		List<Hexagon> test1 = new ArrayList<Hexagon>();
		test1.add(new Hexagon(TerrainType.SAND, 0, 1));
		test1.add(new Hexagon(TerrainType.SAND, 1, 0));

		Map<Integer, Hexagon> neighbors = this.strategy.getNeighbour(this.map,
				new Hexagon(TerrainType.SAND, 0, 0));
		for (Integer integer : neighbors.keySet()) {
			assertTrue(test1.contains(neighbors.get(integer)));
		}

		List<Hexagon> test2 = new ArrayList<Hexagon>();
		test2.add(new Hexagon(TerrainType.SAND, 0, 1));
		test2.add(new Hexagon(TerrainType.SAND, 1, 0));
		test2.add(new Hexagon(TerrainType.SAND, 1, 2));
		test2.add(new Hexagon(TerrainType.SAND, 2, 2));
		test2.add(new Hexagon(TerrainType.SAND, 2, 1));
		test2.add(new Hexagon(TerrainType.SAND, 2, 0));

		Map<Integer, Hexagon> neighbors2 = this.strategy.getNeighbour(this.map,
				new Hexagon(TerrainType.SAND, 1, 1));
		for (Integer integer : neighbors2.keySet()) {
			assertTrue(test2.contains(neighbors2.get(integer)));
		}

		List<Hexagon> test3 = new ArrayList<Hexagon>();
		test3.add(new Hexagon(TerrainType.SAND, 0, 1));
		test3.add(new Hexagon(TerrainType.SAND, 1, 1));
		test3.add(new Hexagon(TerrainType.SAND, 0, 2));
		test3.add(new Hexagon(TerrainType.SAND, 2, 2));
		test3.add(new Hexagon(TerrainType.SAND, 1, 3));
		test3.add(new Hexagon(TerrainType.SAND, 0, 3));

		Map<Integer, Hexagon> neighbors3 = this.strategy.getNeighbour(this.map,
				new Hexagon(TerrainType.SAND, 1, 2));
		for (Integer integer : neighbors3.keySet()) {
			assertTrue(test3.contains(neighbors3.get(integer)));
		}
	}

	/**
	 * 
	 */
	@Test
	public void testFindAllCostsAndShortestPath() {
		// this map contains all the possible reachable cells from cell (3, 4)
		// with 60 actionpoints
		Map<Hexagon, Integer> costsAtCell34Points60 = new HashMap<Hexagon, Integer>();
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 4, 3), 60);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 4, 2), 60);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 3, 3), 30);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 3, 2), 50);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 2, 3), 30);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 2, 8), 60);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 2, 2), 50);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 2, 4), 30);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 1, 3), 50);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 1, 4), 50);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 2, 5), 30);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 2, 8), 60);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 1, 5), 50);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 2, 8), 60);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 2, 6), 60);
		costsAtCell34Points60.put(new Hexagon(TerrainType.SAND, 3, 6), 60);

		// create an ad-hoc map
		List<List<TerrainType>> mapT = new ArrayList<List<TerrainType>>();
		List<TerrainType> ca1 = new ArrayList<TerrainType>();
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		ca1.add(TerrainType.DEEP_WATER);
		mapT.add(ca1);
		List<TerrainType> ca2 = new ArrayList<TerrainType>();
		ca2.add(TerrainType.DEEP_WATER);
		ca2.add(TerrainType.FOREST);
		ca2.add(TerrainType.FOREST);
		ca2.add(TerrainType.SAND);
		ca2.add(TerrainType.SAND);
		ca2.add(TerrainType.SAND);
		ca2.add(TerrainType.SAND);
		ca2.add(TerrainType.SAND);
		ca2.add(TerrainType.FOREST);
		ca2.add(TerrainType.DEEP_WATER);
		mapT.add(ca2);
		List<TerrainType> ca3 = new ArrayList<TerrainType>();
		ca3.add(TerrainType.DEEP_WATER);
		ca3.add(TerrainType.FOREST);
		ca3.add(TerrainType.SAND);
		ca3.add(TerrainType.FOREST);
		ca3.add(TerrainType.FOREST);
		ca3.add(TerrainType.FOREST);
		ca3.add(TerrainType.FOREST);
		ca3.add(TerrainType.FOREST);
		ca3.add(TerrainType.SAND);
		ca3.add(TerrainType.DEEP_WATER);
		mapT.add(ca3);
		List<TerrainType> ca4 = new ArrayList<TerrainType>();
		ca4.add(TerrainType.DEEP_WATER);
		ca4.add(TerrainType.SAND);
		ca4.add(TerrainType.SAND);
		ca4.add(TerrainType.FOREST);
		ca4.add(TerrainType.FOREST);
		ca4.add(TerrainType.MOUNTAIN);
		ca4.add(TerrainType.FOREST);
		ca4.add(TerrainType.FOREST);
		ca4.add(TerrainType.FOREST);
		ca4.add(TerrainType.DEEP_WATER);
		mapT.add(ca4);
		List<TerrainType> ca5 = new ArrayList<TerrainType>();
		ca5.add(TerrainType.DEEP_WATER);
		ca5.add(TerrainType.SAND);
		ca5.add(TerrainType.FOREST);
		ca5.add(TerrainType.FOREST);
		ca5.add(TerrainType.MOUNTAIN);
		ca5.add(TerrainType.MOUNTAIN);
		ca5.add(TerrainType.MOUNTAIN);
		ca5.add(TerrainType.FOREST);
		ca5.add(TerrainType.FOREST);
		ca5.add(TerrainType.DEEP_WATER);
		mapT.add(ca5);
		List<TerrainType> ca6 = new ArrayList<TerrainType>();
		ca6.add(TerrainType.DEEP_WATER);
		ca6.add(TerrainType.SAND);
		ca6.add(TerrainType.FOREST);
		ca6.add(TerrainType.FOREST);
		ca6.add(TerrainType.FOREST);
		ca6.add(TerrainType.FOREST);
		ca6.add(TerrainType.MOUNTAIN);
		ca6.add(TerrainType.FOREST);
		ca6.add(TerrainType.SAND);
		ca6.add(TerrainType.DEEP_WATER);
		mapT.add(ca6);
		List<TerrainType> ca7 = new ArrayList<TerrainType>();
		ca7.add(TerrainType.DEEP_WATER);
		ca7.add(TerrainType.SAND);
		ca7.add(TerrainType.SAND);
		ca7.add(TerrainType.SAND);
		ca7.add(TerrainType.FOREST);
		ca7.add(TerrainType.MOUNTAIN);
		ca7.add(TerrainType.MOUNTAIN);
		ca7.add(TerrainType.FOREST);
		ca7.add(TerrainType.SAND);
		ca7.add(TerrainType.DEEP_WATER);
		mapT.add(ca7);
		List<TerrainType> ca8 = new ArrayList<TerrainType>();
		ca8.add(TerrainType.DEEP_WATER);
		ca8.add(TerrainType.SAND);
		ca8.add(TerrainType.SAND);
		ca8.add(TerrainType.SAND);
		ca8.add(TerrainType.FOREST);
		ca8.add(TerrainType.FOREST);
		ca8.add(TerrainType.FOREST);
		ca8.add(TerrainType.SAND);
		ca8.add(TerrainType.SAND);
		ca8.add(TerrainType.DEEP_WATER);
		mapT.add(ca8);
		List<TerrainType> ca9 = new ArrayList<TerrainType>();
		ca9.add(TerrainType.DEEP_WATER);
		ca9.add(TerrainType.SAND);
		ca9.add(TerrainType.SAND);
		ca9.add(TerrainType.SAND);
		ca9.add(TerrainType.SAND);
		ca9.add(TerrainType.SAND);
		ca9.add(TerrainType.FOREST);
		ca9.add(TerrainType.FOREST);
		ca9.add(TerrainType.FOREST);
		ca9.add(TerrainType.DEEP_WATER);
		mapT.add(ca9);
		List<TerrainType> ca10 = new ArrayList<TerrainType>();
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		ca10.add(TerrainType.DEEP_WATER);
		mapT.add(ca10);
		this.map = new HexagonalMap(mapT);

		// test for the possible cells
		Map<Hexagon, Integer> costs = this.strategy.getAllCosts(
				this.map,
				3,
				4,
				60,
				0,
				new Tank(new Player("1", 2, 6, 1), this.map.getCellAt(
						3, 4)), null, null, null, false);
		Iterator<Hexagon> it = costs.keySet().iterator();
		while (it.hasNext()) {
			Hexagon hexagon = (Hexagon) it.next();
			assertTrue(costsAtCell34Points60.containsKey(hexagon));
			assertEquals(costs.get(hexagon), costsAtCell34Points60.get(hexagon));

		}

		// test for path from (3, 4) to (5, 1)
		this.strategy.getAllCosts(this.map, 3, 4, 100, 0, new Tank(
				new Player("1", 2, 6, 1), this.map.getCellAt(3, 4)), null,
				null, null, false);
		List<Hexagon> testPath34To51 = new ArrayList<Hexagon>();
		testPath34To51.add(new Hexagon(TerrainType.SAND, 3, 3));
		testPath34To51.add(new Hexagon(TerrainType.SAND, 4, 2));
		testPath34To51.add(new Hexagon(TerrainType.SAND, 4, 1));
		testPath34To51.add(new Hexagon(TerrainType.SAND, 5, 1));
		List<Hexagon> path34To51 = strategy.getShortestPath(
				map.getCellAt(3, 4), map.getCellAt(5, 1));
		for (int i = 0; i < path34To51.size(); i++) {
			assertEquals(path34To51.get(i), testPath34To51.get(i));
		}

		// test for path from (3, 4) to (1, 1)
		List<Hexagon> testPath34To11 = new ArrayList<Hexagon>();
		testPath34To11.add(new Hexagon(TerrainType.SAND, 2, 3));
		testPath34To11.add(new Hexagon(TerrainType.SAND, 2, 2));
		testPath34To11.add(new Hexagon(TerrainType.SAND, 1, 1));
		List<Hexagon> path34To11 = strategy.getShortestPath(
				map.getCellAt(3, 4), map.getCellAt(1, 1));
		for (int i = 0; i < path34To11.size(); i++) {
			assertEquals(path34To11.get(i), testPath34To11.get(i));
		}

		// test for path from (3, 4) to (5, 4)
		List<Hexagon> testPath34To54 = new ArrayList<Hexagon>();
		testPath34To54.add(new Hexagon(TerrainType.SAND, 3, 3));
		testPath34To54.add(new Hexagon(TerrainType.SAND, 4, 3));
		testPath34To54.add(new Hexagon(TerrainType.SAND, 5, 4));
		List<Hexagon> path34To54 = strategy.getShortestPath(
				map.getCellAt(3, 4), map.getCellAt(5, 4));
		for (int i = 0; i < path34To54.size(); i++) {
			assertEquals(path34To54.get(i), testPath34To54.get(i));
		}

		// test for path from (3, 4) to (1, 6)
		List<Hexagon> testPath34To16 = new ArrayList<Hexagon>();
		testPath34To16.add(new Hexagon(TerrainType.SAND, 2, 5));
		testPath34To16.add(new Hexagon(TerrainType.SAND, 1, 5));
		testPath34To16.add(new Hexagon(TerrainType.SAND, 1, 6));
		List<Hexagon> path34To16 = strategy.getShortestPath(
				map.getCellAt(3, 4), map.getCellAt(1, 6));
		for (int i = 0; i < path34To16.size(); i++) {
			assertEquals(path34To16.get(i), testPath34To16.get(i));
		}
	}
}
