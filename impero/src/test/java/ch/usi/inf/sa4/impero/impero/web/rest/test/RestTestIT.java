/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.test;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import com.google.gson.Gson;
import com.jayway.restassured.path.json.JsonPath;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.EEF;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinateEntryPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.EntityPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.createBuilding.CreateBuildingAnsPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.createBuilding.CreateBuildingReqPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.createUnit.CreateUnitAnsPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.createUnit.CreateUnitReqPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit.MoveActionAnsPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit.MoveActionReqPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit.PathRequestAnsPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit.PathRequestReqPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.swap.SwapReqPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.actions.turnEnd.TurnEndPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.game.GameAnsPOJO;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

/**
 * @author jesper
 *
 */
public class RestTestIT {
	
	private String basePath;
	private String unitType = Bomber.class.getSimpleName();
	
	private String gameID;
	// start game fields
	private List<String> players;
	private String owner;
	private int width;
	private int height;
	private int actionpoints;
	private int energy;
	private int water;
	private String name;
	private JSONObject game; // start game as json
	
	// game fields
	
	private GameAnsPOJO gamePOJO;
	private String gameJSON;
	private CreateUnitAnsPOJO createUnitPOJO;
	private CreateBuildingAnsPOJO createBuildingPOJO;
	private boolean turnEnd;
	private PathRequestAnsPOJO pathAnswer;
	private MoveActionAnsPOJO moveAnswer;
	
	
	@Before
    public void setUp(){
        basePath = "http://localhost:8888/webresources";
        
        this.players = new ArrayList<>();
		this.players.add("5347ef21036418bff47345ca");
		this.players.add("5346a7ed0364b9a1436f6f1f");
		this.players.add("5347dd08036447df22597cbc");
        
		this.owner = "5347ef21036418bff47345ca";
		
		this.width = 30;
		this.height = 23;
		this.actionpoints = 80;
		this.energy = 44;
		this.water = 32;
		this.name = "TEST GAME";
		
		this.game = new JSONObject();

    }
	
	/**
	 * check that a new game can be created and that the ID is not null
	 */
	@Test
	public void StartGameRestTest() {
		System.out.println("REST TEST GAME REQUEST");
		
		startGame();		
		// check that gameID is not null
		assertThat(this.gameID, notNullValue());

	}
	
	
	/**
	 * starts a new game
	 * 
	 * @return	a string with the gameID
	 */
	public void startGame() {
		
		try {
			this.game.put("players", this.players);
			this.game.put("width", this.width);
			this.game.put("height", this.height);
			this.game.put("actionpoints", this.actionpoints);
			this.game.put("energy", this.energy);
			this.game.put("water", this.water);
			this.game.put("name", this.name);
			this.game.put("owner", this.owner);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		this.gameID = given().contentType("Application/JSON").body(this.game.toString()).post(basePath + "/startgame/").asString();
		
	}

	/**
	 * check that the game created can be retrieved and follows the specified schema
	 */
	@Test
	public void getGameTest() {
				
		startGame();
		
		System.out.println("TEST TO GET GAME " + this.gameID);
		
		getGame();
				
//		String gameID = JsonPath.from(json).getString("gameID");
//		assertThat(this.gameID, is(gameID));
		
		// check that width and height are the same as sent
		int width = JsonPath.from(this.gameJSON).getInt("width");
		assertThat(this.gamePOJO.getWidth(), is(width));
		int height = JsonPath.from(this.gameJSON).getInt("height");
		assertThat(this.gamePOJO.getHeight(), is(height));
		
		assertThat(this.gamePOJO.getMap(), notNullValue());
		assertThat(this.gamePOJO.getEntities(), notNullValue());
//		assertThat(entitiesActions, notNullValue());
		
	}
	
	/**
	 * gets the game with the given ID. Assigns map, entities, entities actions to fields.
	 * Stores JSON string in field gameJSON.
	 * 
	 */
	public void getGame() {
		
		this.gameJSON = get(basePath + "/game/" + this.gameID).
				then().contentType("application/json").extract().asString();
				
		Gson gson = new Gson();
		
		this.gamePOJO = gson.fromJson(this.gameJSON, GameAnsPOJO.class);
				
	}

	@Test
	public void createUnitTest() {
		
		System.out.println("TEST TO CREATE UNIT");
		
		startGame();
		getGame();
		createUnit();
		
		assertThat(createUnitPOJO.getCost(), not(-1));
		assertThat(createUnitPOJO.isValid(), is(true));
		
	}
	
	/**
	 * Creates a unit for the owner of the game. Requires that the game has started and has been received. 
	 */
	public void createUnit() {
		
		createUnit(this.owner);
		
	}
	
	/**
	 * create a unit for the given player.
	 * 
	 * @param playerID
	 */
	public void createUnit(String playerID) {
		
		EntityPOJO hq = null;
		
		for (EntityPOJO entity : this.gamePOJO.getEntities()) {
			if(entity.getType().equals(HQ.class.getSimpleName()) && entity.getOwner().equals(playerID)) {
				hq = entity;
				break;
			}
		}
		
		CreateUnitReqPOJO request = new CreateUnitReqPOJO();
		request.setRow(hq.getRow());
		request.setCol(hq.getCol());
		request.setType(Tank.class.getSimpleName());
		request.setPlayerID(hq.getOwner());
		
		Gson gson = new Gson();
		
		String requestJSON = gson.toJson(request, CreateUnitReqPOJO.class);
		
		String json = given().contentType("Application/JSON").body(requestJSON).
				post(basePath + "/action/createUnit/" + this.gameID).
				then().contentType("application/json").extract().asString();
				
		this.createUnitPOJO = gson.fromJson(json, CreateUnitAnsPOJO.class);
		
	}

	@Test
	public void createBuildingRestTest() {
		
		System.out.println("TEST TO CREATE BUILDING");
		
		startGame();
		getGame();

		createEnergyResource();
		
		assertThat(createBuildingPOJO.getCost(), not(-1));
		assertThat(createBuildingPOJO.isValid(), is(true));
		
	}
	
	/**
	 * Create an EFF for the owner of the game on an energy resource.
	 */
	public void createEnergyResource() {
		
		createEnergyResource(this.owner);
		
	}
	
	public void createEnergyResource(String playerID) {
		
		CreateBuildingReqPOJO request = new CreateBuildingReqPOJO();
		
		// get the first field hexagon in map and build a HQ on it
		for (CoordinateEntryPOJO hexagon : this.gamePOJO.getMap()) {
			
			if ( hexagon.getType() == TerrainType.ENERGY_RESOURCE.ordinal()) {
				request.setCol(hexagon.getCol());
				request.setRow(hexagon.getRow());
				request.setPlayerID(playerID);
				request.setType(EEF.class.getSimpleName());
				break;
			}
			
		}
		
		Gson gson = new Gson();
		
		String requestJSON = gson.toJson(request, CreateBuildingReqPOJO.class);
		
		String json = given().contentType("Application/JSON").body(requestJSON).
				post(basePath + "/action/createBuilding/" + this.gameID).
				then().contentType("application/json").extract().asString();
				
		this.createBuildingPOJO = gson.fromJson(json, CreateBuildingAnsPOJO.class);
		
	}
	
	@Test
	public void turnEndTest() {
		
		System.out.println("TURN END TEST");
		
		startGame();
		getGame();
		
		int numEntities = this.gamePOJO.getEntities().size();
		
		for (String player : this.players) {
			createUnit(player);
			turnEnd(player);
		}
		
		assertThat(this.turnEnd, is(true));
		
		getGame();
		assertThat(this.gamePOJO.getEntities().size(), is(numEntities + this.players.size()));
		
//		System.out.println("NEW GAME STATUS");
//		for (EntityPOJO entity : this.gamePOJO.getEntities()) {
//			System.out.println(entity.getType());
//		}
	}
	
	/**
	 * Ends the turn for the given player
	 * 
	 * @param playerID
	 */
	public void turnEnd(String playerID) {
		
		TurnEndPOJO request = new TurnEndPOJO();
		request.setPlayerID(playerID);
		
		Gson gson = new Gson();
		
		String requestJSON = gson.toJson(request, TurnEndPOJO.class);
		
		String json = given().contentType("Application/JSON").body(requestJSON).
				post(basePath + "/action/turnend/" + this.gameID).
				then().contentType("application/json").extract().asString();
		
		this.turnEnd = Boolean.valueOf(json);
	}
	
	@Test
	public void moveUnitTest() {
		
		System.out.println("MOVE UNIT TEST");
		
		startGame();
		getGame();
				
		for (String player : this.players) {
			createUnit(player);
			turnEnd(player);
		}
		
		// get the game with created units
		
		getGame();
		
		// move the first unit found
		
		EntityPOJO entity = null;
		int entityIndex = 0;
		for (EntityPOJO ent : this.gamePOJO.getEntities()) {
			if (ent.getType().equals(Tank.class.getSimpleName())) {
				System.out.println("UNIT AT col: " + ent.getCol() + " row: " + ent.getRow());
				entity = ent;
				getPath(entity);
				break;
			}
			entityIndex++;
		}
		
		assertThat(this.pathAnswer.getPath().size(), greaterThanOrEqualTo(1));
		
		// select where to move unit
		
		int index = (int) (0.5 * this.pathAnswer.getPath().size() * Math.random());
		
		CoordinatePOJO coordinateTo = this.pathAnswer.getPath().get(index);
		
		System.out.println("MOVING UNIT TO col: " + coordinateTo.getCol() + " row: " + coordinateTo.getRow());
		
		moveUnit(entity.getCol(), entity.getRow(), coordinateTo.getCol(), coordinateTo.getRow(), entity.getOwner());
		
		assertThat(this.moveAnswer.isValid(), is(true));
		assertThat(this.moveAnswer.getCost(), greaterThan(0));
		
		// end turn
		for (String player : this.players) {
			createUnit(player);
			turnEnd(player);
		}
		
		// check if unit moved
		getGame();
		
		entity = this.gamePOJO.getEntities().get(entityIndex);
		
		System.out.println("UNIT MOVED TO col: " + entity.getCol() + " row: " + entity.getRow());
		
		assertThat(entity.getCol(), is(coordinateTo.getCol()));
		assertThat(entity.getRow(), is(coordinateTo.getRow()));
		
	}
	
	public void getPath(EntityPOJO entity) {
		
		PathRequestReqPOJO request = new PathRequestReqPOJO();
		
		request.setCol(entity.getCol());
		request.setRow(entity.getRow());
		request.setPlayerID(entity.getOwner());
		request.setType(entity.getType());
		
		Gson gson = new Gson();
		
		String requestJSON = gson.toJson(request, PathRequestReqPOJO.class);
		
		String json = given().contentType("Application/JSON").body(requestJSON).
				post(basePath + "/action/pathRequest/" + this.gameID).
				then().contentType("application/json").extract().asString();
		
		this.pathAnswer = gson.fromJson(json, PathRequestAnsPOJO.class);
		
	}
	
	public void moveUnit(int colFrom, int rowFrom, int colTo, int rowTo, String playerID) {
		
		MoveActionReqPOJO request = new MoveActionReqPOJO();
		
		request.setColFrom(colFrom);
		request.setRowFrom(rowFrom);
		request.setColTo(colTo);
		request.setRowTo(rowTo);
		request.setPlayerID(playerID);
		
		Gson gson = new Gson();
		
		String requestJSON = gson.toJson(request, MoveActionReqPOJO.class);
		
		String json = given().contentType("Application/JSON").body(requestJSON).
				post(basePath + "/action/move/" + this.gameID).
				then().contentType("application/json").extract().asString();
		
		this.moveAnswer = gson.fromJson(json, MoveActionAnsPOJO.class);

	}
	
	@Test
	public void swapActionTest() {
		
		System.out.println("TEST TO SWAP ACTION");
		
		startGame();
		getGame();
				
		createUnit(this.owner);
		createEnergyResource(this.owner);
		
		Boolean answer = swapAction(0, 1, this.owner);
		
		assertThat(answer, is(true));
		
	}
	
	public boolean swapAction(int fromIndex, int toIndex, String playerID) {
		
		SwapReqPOJO request = new SwapReqPOJO();
		request.setFrom(fromIndex);
		request.setTo(toIndex);
		request.setPlayerID(playerID);
		
		Gson gson = new Gson();
		
		String requestJSON = gson.toJson(request, SwapReqPOJO.class);
		
		String answer = given().contentType("Application/JSON").body(requestJSON).
				post(basePath + "/action/swap/" + this.gameID).asString();
		
		return Boolean.parseBoolean(answer);
	}
	
	@Test
	public void endTurnTest() {
		
	}

}
