package ch.usi.inf.sa4.impero.impero.web.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.web.server.Login;

public class LoginTest {

	@Test
	public void testInsUser() throws Exception {
		//Login.insUser("jack","the ripper", "jack@theripper.com");
		String tbf =Login.verify("jesper", "jesper");
		if(tbf==null)
			tbf="";
		assertThat(tbf, is(not("")));
		assertEquals(null,Login.verify("dummy", "the ripper"));
	}

}
