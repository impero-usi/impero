package ch.usi.inf.sa4.impero.impero.logic;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.mappers.MovementCostMapper;

public class MovementCostMapperTest {

	@Test
	public void test() {
		if(MovementCostMapper.isValidTerrainAndCost(Tank.class, TerrainType.SHALLOW_WATER) != -1){
			fail();
		}
	}

}
