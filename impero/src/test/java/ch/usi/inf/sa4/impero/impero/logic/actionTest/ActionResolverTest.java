package ch.usi.inf.sa4.impero.impero.logic.actionTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.logic.action.AttackAction;
import ch.usi.inf.sa4.impero.impero.logic.action.MoveAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnUnitAction;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class ActionResolverTest {

	private Game g;
	
	
	@Before
	public void setUp() throws Exception {
		List<String> players = new ArrayList<>();
		players.add("testplayer");
		players.add("testplayer2");
		g = new Game(30, 30, 1000,1000, players, "000", 10, 50, 10, 5, false, false);
	}

	@After
	public void tearDown() throws Exception {
		g = null;
	}
	@Test
	public void testMaxLength() {
		Player play;
		Player play2;
		for(Player p : g.getPlayerList()){
			if(p.getPlayerID().equals("testplayer")){
				play = p;
				Hexagon hex = g.getMap().getCellAt(10, 10);
				g.getResolver().getActionList().get(play).add(new SpawnUnitAction(play, 5, hex, null, null, 5));
				g.getResolver().getActionList().get(play).add(new SpawnUnitAction(play, 5, hex, null, null, 5));
				g.getResolver().getActionList().get(play).add(new SpawnUnitAction(play, 5, hex, null, null, 5));
				g.getResolver().getActionList().get(play).add(new SpawnUnitAction(play, 5, hex, null, null, 5));
			}
		}
		
		for(Player p : g.getPlayerList()){
			if(p.getPlayerID().equals("testplayer2")){
				play2 = p;
				Hexagon hex = g.getMap().getCellAt(10, 10);
				g.getResolver().getActionList().get(play2).add(new SpawnUnitAction(play2, 5, hex, null, null, 5));
				g.getResolver().getActionList().get(play2).add(new SpawnUnitAction(play2, 5, hex, null, null, 5));
				g.getResolver().getActionList().get(play2).add(new SpawnUnitAction(play2, 5, hex, null, null, 5));
				g.getResolver().getActionList().get(play2).add(new SpawnUnitAction(play2, 5, hex, null, null, 5));
				g.getResolver().getActionList().get(play2).add(new SpawnUnitAction(play2, 5, hex, null, null, 5));
				g.getResolver().getActionList().get(play2).add(new SpawnUnitAction(play2, 5, hex, null, null, 5));
			}
		}
		if(g.getResolver().getLongestActionListLength() != 6){
			fail();
		}

	}
	
	@Test
	public void testSpawnUnit(){
		Player play;
		Hexagon hex = g.getMap().getCellAt(10, 10);
		Hexagon hex2 = g.getMap().getCellAt(10, 11);
		for(Player p : g.getPlayerList()){
			if(p.getPlayerID().equals("testplayer")){
				play = p;
				g.getResolver().resolveSpawnUnitAction(new SpawnUnitAction(play, 1, hex, new HQ(play, hex2), Bomber.class, 1));
			}
		}
		if(!g.getUnitList().get(0).getHexagon().equals(hex)){
		fail();
		}
	}
	
	@Test
	public void testMoveUnit(){
		Player play;
		Hexagon hex3 = g.getMap().getCellAt(10, 9);
		Hexagon hex = g.getMap().getCellAt(10, 10);
		Hexagon hex2 = g.getMap().getCellAt(13, 13);
		List<Hexagon> bla = new ArrayList<>();
		bla.add(hex2);
		for(Player p : g.getPlayerList()){
			if(p.getPlayerID().equals("testplayer")){
				play = p;
				g.getResolver().resolveSpawnUnitAction(new SpawnUnitAction(play, 1, hex, new HQ(play, hex3), Bomber.class, 1));
				g.getResolver().resolveMoveAction(new MoveAction(play, 1, hex2 , g.getUnitList().get(0), bla));
			}
		}
		if(!g.getUnitList().get(0).getHexagon().equals(hex2)){
			fail();
		}
	}
	
	@Test
	public void testAttack(){
		Player play;
		Player play2;
		Hexagon hex3 = g.getMap().getCellAt(10, 9);
		Hexagon hex = g.getMap().getCellAt(10, 10);
		Hexagon hex2 = g.getMap().getCellAt(10, 11);
		Hexagon hex4 = g.getMap().getCellAt(10, 12);
		List<Hexagon> bla = new ArrayList<>();
		bla.add(hex);
		for(Player p : g.getPlayerList()){
			if(p.getPlayerID().equals("testplayer")){
				play = p;
				play2 = g.getPlayerList().get(1);
				g.getResolver().resolveSpawnUnitAction(new SpawnUnitAction(play, 1, hex, new HQ(play, hex3), Bomber.class, 1));
				g.getResolver().resolveSpawnUnitAction(new SpawnUnitAction(play2, 1, hex2, new HQ(play2, hex4), Bomber.class, 1));
				g.getResolver().resolveAttackAction(new AttackAction(play, 1, hex2, g.getUnitList().get(0)));
			}
		}
		if(g.getUnitList().size() != 2){
			fail();
		}
		if(g.getUnitList().get(1).getHealth() == g.getUnitList().get(0).getHealth()){
			fail();
		}
		g.getUnitList().clear();
		g.getResolver().resolveSpawnUnitAction(new SpawnUnitAction(g.getPlayerList().get(0), 1, g.getMap().getCellAt(29, 29), new HQ(g.getPlayerList().get(0), g.getMap().getCellAt(28, 29)), Bomber.class, 1));
		g.getMap().getCellAt(29, 28).setType(TerrainType.ROAD);
		assertEquals(g.getMap().getCellAt(29, 28).getType(), TerrainType.ROAD);
		g.getResolver().resolveAttackAction(new AttackAction(g.getPlayerList().get(0), 1, g.getMap().getCellAt(29, 28), g.getUnitList().get(0)));
		assertEquals(g.getMap().getCellAt(29, 28).getType(), g.getMap().getCellAt(29, 28).getDefaultType());
		
	}
	
	
	
	
	
	
	
	/**
	 * test for battle resolution
	 */
	@Test
	public void battleResolutionTest(){
		/**
		 * initialisation for the test
		 */
		Player play = g.getPlayerList().get(0);
		Player play2 = g.getPlayerList().get(1);
		Hexagon hex = g.getMap().getCellAt(10, 9);
		Hexagon hex2 = g.getMap().getCellAt(10, 10);
		Hexagon hex3 = g.getMap().getCellAt(10, 11);
		Hexagon hex4 = g.getMap().getCellAt(10, 12);
		Hexagon hex5 = g.getMap().getCellAt(10, 13);
		List<Hexagon> bla = new ArrayList<>();
		bla.add(hex3);
		bla.add(hex4);
		/**
		 * spawn 2 Units
		 */
		g.getResolver().resolveSpawnUnitAction(new SpawnUnitAction(play, 1, hex2, new HQ(play, hex), Tank.class, 1));
		g.getResolver().resolveSpawnUnitAction(new SpawnUnitAction(play2, 1, hex4, new HQ(play2, hex5), Tank.class, 1));
		/**
		 * check that 2 unit are in the unit list of the game
		 */
		assertEquals("Check that 2 unit are in the list", g.getUnitList().size(), 2);
		/**
		 * try to move on the cell occupied by enemy unit and trigger a battle
		 */
		g.getResolver().resolveMoveAction(new MoveAction(play, 1, hex4, g.getUnitList().get(0), bla));
		/**
		 * check that there is only one unit remaining in the unit list and that is on the right cell
		 */
		assertEquals("Check that one of the Unit has been removed", g.getUnitList().size(), 1);
		assertEquals("Check that the remaining unit is on the right Row", g.getUnitList().get(0).getHexagon().getRow(), 10);
		assertEquals("Check that the remaining unit is on the right ",g.getUnitList().get(0).getHexagon().getCol(), 12);
	}

}
