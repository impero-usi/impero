package ch.usi.inf.sa4.impero.impero.logic.actionTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.action.AttackAction;
import ch.usi.inf.sa4.impero.impero.logic.action.ConquerAction;
import ch.usi.inf.sa4.impero.impero.logic.action.MoveAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnBuildingAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnUnitAction;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class ActionVerifierTest {

	private Game g;
	
	@Before
	public void setUp() throws Exception {
		List<String> players = new ArrayList<>();
		players.add("testplayer");
		players.add("testplayer2");
		g = new Game(30, 30, 1000,1000, players, "000", 10, 50, 10, 5, false, false);
		g.getUnitList().add(new Bomber(g.getPlayerList().get(0), g.getMap().getCellAt(0, 0)));
		g.getUnitList().add(new Bomber(g.getPlayerList().get(1), g.getMap().getCellAt(1, 1)));
		g.getUnitList().add(new Bomber(g.getPlayerList().get(1), g.getMap().getCellAt(0, 29)));
		g.getProductionBuildingList().add(new HQ(g.getPlayerList().get(1), g.getMap().getCellAt(0, 1)));
		g.getProductionBuildingList().get(0).setHexagon(g.getMap().getCellAt(29, 3));
		g.getProductionBuildingList().get(0).setHexagon(g.getMap().getCellAt(29, 4));
		g.getUtility().updateVisibleMap(g.getPlayerList().get(0));
		g.getUtility().updateVisibleMap(g.getPlayerList().get(1));
	}

	@After
	public void tearDown() throws Exception {
		g = null;
	}
	
	@Test
	public void testSpawnUnitAction() {
		for(AbstractProductionBuilding building : g.getProductionBuildingList()){
			SpawnUnitAction a = g.getVerifier().verifySpawnUnitAction(building.getOwner().getPlayerID(), building.getHexagon().getRow(), building.getHexagon().getCol(), Bomber.class.getSimpleName());
			assertNotEquals(a, null);
		}
		for(AbstractProductionBuilding building : g.getProductionBuildingList()){
			SpawnUnitAction a = g.getVerifier().verifySpawnUnitAction("test", building.getHexagon().getRow(), building.getHexagon().getCol(), Bomber.class.getSimpleName());
			assertEquals(a, null);
		}
		for(AbstractProductionBuilding building : g.getProductionBuildingList()){
			SpawnUnitAction a = g.getVerifier().verifySpawnUnitAction(building.getOwner().getPlayerID(), building.getHexagon().getRow(), building.getHexagon().getCol(), "ciao");
			assertEquals(a, null);
		}
		for(AbstractProductionBuilding building : g.getProductionBuildingList()){
			for(Integer i: g.getMap().getNeigbour(building.getHexagon().getRow(),building.getHexagon().getCol()).keySet()){
				g.getMap().getNeigbour(building.getHexagon().getRow(),building.getHexagon().getCol()).get(i).setType(TerrainType.DEEP_WATER);
			}
			SpawnUnitAction a = g.getVerifier().verifySpawnUnitAction(building.getOwner().getPlayerID(), building.getHexagon().getRow(), building.getHexagon().getCol(), Tank.class.getSimpleName());
			assertEquals(a, null);
		}
		for(AbstractProductionBuilding building : g.getProductionBuildingList()){
			for(Integer i: g.getMap().getNeigbour(building.getHexagon().getRow(),building.getHexagon().getCol()).keySet()){
				g.getMap().getNeigbour(building.getHexagon().getRow(),building.getHexagon().getCol()).get(i).setType(TerrainType.FIELD);
			}
			SpawnUnitAction a = g.getVerifier().verifySpawnUnitAction(building.getOwner().getPlayerID(), building.getHexagon().getRow(), building.getHexagon().getCol(), Tank.class.getSimpleName());
			assertNotEquals(a, null);
		}
	}
	
	@Test
	public void testverifyMoveAction() {
		for (Player p : g.getPlayerList()) {
			p.setEnergy(99999);
			p.setActionPoint(9999);
		}
		Map<Hexagon, Integer> map = g.getVerifier().getMovementCells(0, 0, "testplayer");
		assertNotEquals(map, null);
		Iterator<Hexagon> j = map.keySet().iterator();
		Hexagon cell = j.next();
		MoveAction a = g.getVerifier().verifyMoveAction(0,0,cell.getRow(), cell.getCol(),"testplayer");
		assertNotEquals(a, null);
		MoveAction b = g.getVerifier().verifyMoveAction(0,0,29, 29,"testplayer");
		assertEquals(b, null);
		for (Player p : g.getPlayerList()) {
			p.setEnergy(0);
			p.setActionPoint(0);
		}
		Hexagon cell2 = j.next();
		if(!cell.equals(cell2)){
			MoveAction c = g.getVerifier().verifyMoveAction(0,0,cell2.getRow(), cell2.getCol(),"testplayer");
			assertEquals(c, null);
		}
	}
	
	@Test
	public void testgetAttackCells(){
		List<Hexagon> cells = g.getVerifier().getAttackCells(0, 0, "testplayer");
		assertNotEquals(cells, null);
		assertNotEquals(cells.size(), 0);
		List<Hexagon> cells2 = g.getVerifier().getAttackCells(3, 0, "testplayer");
		assertEquals(cells2, null);
		List<Hexagon> cells3 = g.getVerifier().getAttackCells(0, 29, "testplayer2");
		assertNotEquals(cells3, null);
		assertEquals(cells3.size(), 0);
		g.getMap().getCellAt(0, 28).setType(TerrainType.ROAD);
		assertEquals(TerrainType.ROAD ,g.getMap().getCellAt(0, 28).getType());
		List<Hexagon> cells4 = g.getVerifier().getAttackCells(0, 29, "testplayer2");
		assertNotEquals(cells4, null);
		assertEquals(cells4.size(), 1);
	}
	
	@Test
	public void testverifyAttackAction(){
		for (Player p : g.getPlayerList()) {
			p.setEnergy(99999);
			p.setActionPoint(9999);
		}
		List<Hexagon> cells = g.getVerifier().getAttackCells(0, 0, "testplayer");
		assertNotEquals(cells, null);
		if(cells.size()>0){
			Hexagon cell = cells.get(0);
			AttackAction a = g.getVerifier().verifyAttackAction("testplayer", cell.getRow(), cell.getCol(), 0, 0);
			assertNotEquals(a, null);
		}
	}
	
	@Test
	public void testgetConquerCells(){
//		List<Hexagon> cells = g.getVerifier().getConquerCells(0, 0, "testplayer");
//		assertNotEquals(cells, null);
//		assertNotEquals(cells.size(), 0);
//		List<Hexagon> cells2 = g.getVerifier().getConquerCells(3, 0, "testplayer");
//		assertEquals(cells2, null);
//		List<Hexagon> cells3 = g.getVerifier().getConquerCells(1, 1, "testplayer2");
//		assertNotEquals(cells3, null);
//		assertEquals(cells3.size(), 0);
	}
	
	@Test
	public void testverifyConquerAction(){
		for (Player p : g.getPlayerList()) {
			p.setEnergy(99999);
			p.setActionPoint(9999);
		}
		List<Hexagon> cells = g.getVerifier().getConquerCells(0, 0, "testplayer");
		assertNotEquals(cells, null);
		if(cells.size()>0){
			ConquerAction a = g.getVerifier().verifyConquerAction("testplayer", 0, 1, 0, 0);
			assertNotEquals(a, null);
		}
	}
	
	@Test
	public void testSpawnBuildingAction(){
		for (Player p : g.getPlayerList()) {
			p.setEnergy(99999);
			p.setActionPoint(9999);
		}
		g.getMap().getCellAt(5, 5).setType(TerrainType.ENERGY_RESOURCE);
		g.getUnitList().add(new Bomber(g.getPlayerList().get(0), g.getMap().getCellAt(5, 4)));
		g.getUtility().updateVisibleMap(g.getPlayerList().get(0));
		SpawnBuildingAction a = g.getVerifier().verifySpawnBuildingAction(5, 5, "EEF", "testplayer");
		assertNotEquals(a, null);
		SpawnBuildingAction b = g.getVerifier().verifySpawnBuildingAction(5, 5, "EEF", "testplayer");
		assertEquals(b,null);
		g.getMap().getCellAt(6, 6).setType(TerrainType.DEEP_WATER);
		SpawnBuildingAction c = g.getVerifier().verifySpawnBuildingAction(6, 6, "EEF", "testplayer");
		assertEquals(c,null);
		g.getMap().getCellAt(29,5).setType(TerrainType.FIELD);
		SpawnBuildingAction d = g.getVerifier().verifySpawnBuildingAction(29, 5, "Road", "testplayer");
		assertNotEquals(d,null);
		SpawnBuildingAction x = g.getVerifier().verifySpawnBuildingAction(29, 5, "Road", "testplayer");
		assertEquals(x,null);
		
		g.getMap().getCellAt(25,5).setType(TerrainType.SHALLOW_WATER);
		SpawnBuildingAction i = g.getVerifier().verifySpawnBuildingAction(25, 5, "Harbour", "testplayer");
		assertNotEquals(i,null);

		
	}
	
	@Test
	public void testdeleteAction(){
		for (Player p : g.getPlayerList()) {
			p.setEnergy(0);
			p.setActionPoint(0);
		}
		Player p = g.getPlayerList().get(0);
		List<AbstractAction> actions = g.getActionList().get(p);
		actions.add(new SpawnBuildingAction(p, 4, null, 5, null));
		assertEquals(g.getVerifier().deleteAction(1, "testplayer"), null);
		assertEquals(g.getVerifier().deleteAction(0, "testplayer3"), null);
		assertNotEquals(g.getVerifier().deleteAction(0, "testplayer"), null);
		assertEquals(p.getActionPoint().intValue(), 4);
		assertEquals(p.getEnergy().intValue(), 5);
		assertEquals(actions.size(), 0);
		assertEquals(g.getVerifier().deleteAction(0, "testplayer"), null);
	}
	@Test
	public void testswapAction(){
		Player p = g.getPlayerList().get(0);
		List<AbstractAction> actions = g.getActionList().get(p);
		actions.add(new SpawnBuildingAction(p, 4, null, 4, null));
		actions.add(new SpawnBuildingAction(p, 8, null, 8, null));
		assertTrue(g.getVerifier().popInstert(0, 1, "testplayer"));
		assertEquals(actions.get(0).getActionPointCost().intValue(), 8);
		assertEquals(actions.get(1).getActionPointCost().intValue(), 4);
		assertFalse(g.getVerifier().popInstert(1, 1, "testplayer"));
		assertFalse(g.getVerifier().popInstert(-2, 1, "testplayer"));
		
	}
}
