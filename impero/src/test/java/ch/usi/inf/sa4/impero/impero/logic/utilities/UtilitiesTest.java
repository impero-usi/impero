package ch.usi.inf.sa4.impero.impero.logic.utilities;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.action.MoveAction;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.EmptyEntity;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class UtilitiesTest {
	private Game g;
	private List<String> players;
	private Utilities utilities;

	@Before
	public void setUp() throws Exception {
		players = new ArrayList<>();
		players.add("testplayer");
		players.add("testplayer2");
		g = new Game(30, 30, 1000,1000, players, "000", 10, 50, 10, 5, false, false);
		utilities = g.getUtility();
		g.getUnitList().add(new Bomber(g.getPlayerList().get(0), g.getMap().getCellAt(0, 0)));
		g.getUnitList().add(new Bomber(g.getPlayerList().get(1), g.getMap().getCellAt(1, 1)));
		g.getUtility().updateVisibleMap(g.getPlayerList().get(0));
		g.getUtility().updateVisibleMap(g.getPlayerList().get(1));
	}

	@After
	public void tearDown() throws Exception {
		players = null;
		g.getUnitList().clear();
		g = null;
	}

	@Test
	public void testGetClassFromRowCol() {
		assertEquals(utilities.getClassFromRowCol(0, 0, g.getPlayerList().get(0) ), Bomber.class);
		assertEquals(utilities.getClassFromRowCol(0, 1, g.getPlayerList().get(1)), EmptyEntity.class);
	}
	
	@Test
	public void testGetEntityAt() {
		assertEquals(utilities.getEntityAt(0, 0), g.getUnitList().get(0));
		assertEquals(utilities.getEntityAt(0, 1), null);
	}
	@Test
	public void testIsEnemyEntity() {
		assertTrue(utilities.isEnemyEntity(g.getMap().getCellAt(1, 1), g.getPlayerList().get(0)));
		assertFalse(utilities.isEnemyEntity(g.getMap().getCellAt(1, 1), g.getPlayerList().get(1)));
		assertFalse(utilities.isEnemyEntity(g.getMap().getCellAt(1, 5), g.getPlayerList().get(1)));
	}
	
	@Test
	public void testGetPlayer() {
		assertEquals(utilities.getPlayer("tets"), null);
		assertEquals(utilities.getPlayer("testplayer"), g.getPlayerList().get(0));
	}
	
	@Test
	public void testCellOccupied() {
		assertTrue(utilities.cellOccupied(1, 1, g.getPlayerList().get(0), null));
		assertFalse(utilities.cellOccupied(1, 0, g.getPlayerList().get(0), null));
		Map<Player, List<AbstractAction>> actionList = new HashMap<Player, List<AbstractAction>>();
		actionList.put(g.getPlayerList().get(0), new ArrayList<AbstractAction>());
		actionList.get(g.getPlayerList().get(0)).add(new MoveAction(g.getPlayerList().get(0), 1, g.getMap().getCellAt(3, 3), null, null));
		assertTrue(utilities.cellOccupied(3, 3, g.getPlayerList().get(0), actionList));
	}
	
	@Test
	public void testIsSubClassOf() {
		assertTrue(Utilities.isSubClassOf(Integer.class, Object.class));
		assertFalse(Utilities.isSubClassOf(Object.class, Integer.class));
	}
	
	@Test
	public void testUpdateVisibleMap() {
		utilities.updateVisibleMap(g.getPlayerList().get(0));
		System.out.println("The size is : " + g.getPlayerList().get(0).getVisibleHexagons().size());
	}


}
