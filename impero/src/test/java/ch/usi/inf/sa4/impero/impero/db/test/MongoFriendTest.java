package ch.usi.inf.sa4.impero.impero.db.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.db.MongoFriend;



public class MongoFriendTest {

	@Test
	public void testMakeFriends() throws Exception {
		boolean ret = MongoFriend.makeFriends("5342c5a26c820ba02a212394", "5345b930036405f8f43ade4a");
		assertEquals(ret,true);
	}

}
