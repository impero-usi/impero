//package ch.usi.inf.sa4.impero.impero.logic;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Test;
//
//import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
//import ch.usi.inf.sa4.impero.impero.logic.entities.MovementCostMapper;
//import ch.usi.inf.sa4.impero.impero.logic.game.Game;
//import ch.usi.inf.sa4.impero.impero.logic.game.HQSpawner;
//import ch.usi.inf.sa4.impero.impero.logic.map.Coordinate;
//import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
//
//public class HQPlacement {
//
//	@Test
//	public void test() {
//		List<String> playerList = new ArrayList<String>();
//		playerList.add("Orestis1");
//		playerList.add("Orestis2");
//		playerList.add("Orestis3");
//		playerList.add("Orestis4");
//		playerList.add("Orestis5");
//		playerList.add("Orestis6");
//		playerList.add("Orestis7");
//		playerList.add("Orestis8");
//		playerList.add("Orestis9");
//		playerList.add("Orestis10");
//		playerList.add("Orestis11");
//		playerList.add("Orestis12");
//		Game g = new Game(50, 50, 0, 0, playerList, "MyGame", 60);
//		HQSpawner hqsp = new HQSpawner(g.getPlayerList().size(), g.getMap());
//		List<Coordinate> hqcoords = hqsp.setHeadquartersCoordinates();
//		/*for (Coordinate c : hqcoords)
//			System.out.println(c.toString());*/
//		for (int i = 0; i < g.getMap().getWidth(); i++) {
//			System.out.printf("\n");
//			for (int j = 0; j < g.getMap().getHeight(); j++) {
//				if (hqcoords.contains(new Coordinate(i, j)))
//					System.out.printf("* ");
//				else {
//					if (MovementCostMapper.isValidTerrainAndCost(HQ.class, g.getMap()
//							.getCellAt(j, i).getType()) != -1)
//						System.out.printf("L ");
//					else
//						System.out.printf("- ");
//				}
//				
//			}
//		}
//	}
//
//}
