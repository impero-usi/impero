package ch.usi.inf.sa4.impero.impero.web.rest.test;

import static com.jayway.restassured.RestAssured.expect;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;


/**
 * 
 * @author Kasim Bordogna
 *
 */

public class RestSampleIT{
	String basePath;
	@Before
	public void setUp(){
		 		basePath = "http://localhost:8888/webresources";
		 	}
	
	@Test
	public void testRestSample() throws Exception {
		expect().statusCode(200).contentType(ContentType.JSON).when().get(basePath + "/restsample");
	}
	
	@Test
	public void restRestSerializationDeserialization(){
		JSONObject player = new JSONObject();
		try {
			player.put("playerID", "1");
	        player.put("actionPoint", "100");
	        player.put("name", "Jack");
	        player.put("resources", "100");
	        player.put("teamID", "100");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		given().contentType("Application/JSON").body(player.toString()).
        expect().statusCode(201).when().post(basePath + "/restsample");
	}

	@After
	public void shutDown(){
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
