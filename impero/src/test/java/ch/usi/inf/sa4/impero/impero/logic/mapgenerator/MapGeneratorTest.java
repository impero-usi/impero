package ch.usi.inf.sa4.impero.impero.logic.mapgenerator;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;

public class MapGeneratorTest {
	private HexagonalMap map;
	
	@Before
	public void setUp() throws Exception {
		map = new MapGenerator().generateMap(100, 100, 50, 60, 10, 3, false, false);
	}

	@After
	public void tearDown() throws Exception {
		map = null;
	}

	@Test
	public void test() {
		List<List<Hexagon>> hexMap = map.getMap();
		for (List<Hexagon> list : hexMap) {
			String color = "";
			for (Hexagon hexagon : list) {
				TerrainType t = hexagon.getType();
				if(t == TerrainType.DEEP_WATER) {
					color += "D"; 
				} else if(t == TerrainType.SHALLOW_WATER) {
					color += "W";
				} else if (t == TerrainType.SAND) {
					color += "S";
				} else if (t == TerrainType.FIELD) {
					color += "G";
				} else if (t == TerrainType.FOREST) {
					color += "F";
				} else if (t == TerrainType.MOUNTAIN) {
					color += "M";
				} else {
					color += "E";
				}
			}
			System.out.println(color);
		}
	}
}
