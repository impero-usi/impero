package ch.usi.inf.sa4.impero.impero.db.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.usi.inf.sa4.impero.impero.db.MongoPlayer;



public class MongoPlayerTest {

	@Test
	public void testIsPlayer() throws Exception {
		boolean ret = MongoPlayer.isPlayer("5345b930036405f8f43ade4a");
		assertEquals(ret,true);
	}

}
