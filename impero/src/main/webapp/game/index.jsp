<%@page import="ch.usi.inf.sa4.impero.impero.GameServer"%>
<%@page import="ch.usi.inf.sa4.impero.impero.logic.game.Game"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		response.sendRedirect("/index.jsp");
	} else {
		if (id.matches("[a-f0-9]*")) {
			Game game = GameServer.getGame(id);
			if (game.getGameOver()) {
				response.sendRedirect("/dashboard/statistics/game.jsp?id="
						+ id);
			}
		} else {
			response.sendRedirect("/index.jsp");
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<title>Impero</title>
<%@include file="../views/includes.jsp"%>

<!-- game links -->
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="game/css/style.css">
<script src="js/libs/createjs/createjs.js" type="text/javascript"></script>
<script src="game/js/utilities/constants.js" type="text/javascript"></script>
<script src="game/js/utilities/hexagonUtility.js" type="text/javascript"></script>
<script src="game/js/hexagon.js" type="text/javascript"></script>
<script src="game/js/map.js" type="text/javascript"></script>
<script src="game/js/entity.js" type="text/javascript"></script>
<script src="game/js/fsm/transition.js" type="text/javascript"></script>
<script src="game/js/fsm/state.js" type="text/javascript"></script>
<script src="game/js/fsm/fsm.js" type="text/javascript"></script>
<script src="game/js/game.js" type="text/javascript"></script>
<script src="game/js/panels/actionListMenu.js" type="text/javascript"></script>
<script src="game/js/panels/changeDisplayerMenu.js"
	type="text/javascript"></script>
<script src="game/js/panels/armySummaryDiv.js" type="text/javascript"></script>
<script src="game/js/panels/entityMenu.js" type="text/javascript"></script>
<script src="game/js/panels/playerStatusMenu.js" type="text/javascript"></script>
<script src="game/js/panels/playersMenu.js" type="text/javascript"></script>
<script src="game/js/GUIState.js" type="text/javascript"></script>
<script src="game/js/stateHandler.js" type="text/javascript"></script>
<script src="game/js/changeUpdater.js" type="text/javascript"></script>
<script src="game/js/panels/popUp.js" type="text/javascript"></script>

</head>
<body>
	<div class="modal fade" id="gameModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div id="modalBodyId" class="modal-body"></div>
				<div id="modalFooterId" class="modal-footer"></div>
			</div>
		</div>
	</div>

	<%-- <%@include file="/views/header.jsp"%> --%>

	<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%> --%>

	<!-- DIV TO DISPLAY WHEN LOADING GAME -->
	<div id="loadingGame">
		<h1>IMPERO</h1>
		<h1 id="loadingGameText">LOADING GAME...</h1>
	</div>

	<!-- TOP PLAYER INFO -->
	<div id="top" class="navbar navBackgroud gameGUI"></div>

	<!-- CENTER -->
	<div id="game" class="game2">
		<canvas id="playground"></canvas>
	</div>

	<!-- GAME -->
	<div id="mainGameGUI" class="row gameGUI">

		<!-- LEFT -->
		<div id="left" class="col-md-2 gamePanelContainer">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title">My Army</h3>
				</div>

				<div class="panel-body">
					<div id="idArmySummaryDiv" class="army"></div>
				</div>
			</div>

			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title">My Turn</h3>
				</div>

				<div id="idActionListDiv" class="panel-body list-group">No
					actions</div>
			</div>

		</div>

		<!-- RIGHT -->
		<div id="right" class="col-md-offset-8 col-md-2 gamePanelContainer">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 id="entityMenuTitle" class="panel-title"></h3>
				</div>

				<div class="panel-body">
					<div id="entityMenu" class="list-group"></div>
				</div>
			</div>

			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 id="playersMenuTitle" class="panel-title"></h3>
				</div>

				<div class="panel-body">
					<div id="playersMenu" class="list-group"></div>
				</div>
			</div>

		</div>

	</div>

	<%@include file="/views/footer.jsp"%>

</body>
</html>