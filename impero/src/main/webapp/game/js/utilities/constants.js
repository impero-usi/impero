/**
 * Global variables
 * 
 * @author jesper and matteo.muscella@usi.ch
 * @return Object
 *         - _:   the constants object
 *         - get: return the constants object
 *         - set: set a ['one']['two'] constant to value
 *         - getJson: stringify the _constants object
 */
var Constants = (function(){
	var _HEXAGON_SIZE = 30;
	var _constants =
	{
	    URLS: {

	        BASE: "http: //localhost:8888/impero/webresources/",

	        GAME: "game",
	        ENTITY: "entities",
	        ACTION: "action"
	    },
	    GAME: {

	    	game_id: ""
	    },
	    MAP: {

	    	MAP_WIDTH: 100,
			MAP_HEIGHT: 100,
			MAP_START_X: 0,
			MAP_START_Y: 0,
	    	POLY_SIDES: 6,
	    	POLY_POINT_SIZE: 0,
	    	HEXAGON_SIZE: _HEXAGON_SIZE,
	    	//ENTITY_SIZE: _HEXAGON_SIZE - (1 / 4 * _HEXAGON_SIZE),
	    	ENTITY_SIZE: _HEXAGON_SIZE,
			//MAP_HEX_WIDTH: (3 / 4 * 2 * _HEXAGON_SIZE),
			MAP_HEX_WIDTH: (2 * _HEXAGON_SIZE),
			MAP_HEX_HEIGHT: (Math.sqrt(3) * _HEXAGON_SIZE),
			DRAW_START_ANGLE: 60
	    },
	    UNIT_COLORS: {

	        MY_UNIT_STROKE: "rgba(0,0,0,1)",
	        MY_UNIT_FILL: "rgba(0,255,0,1)",

	        ENEMY_UNIT_STROKE: "rgba(0,0,0,1)",
	        ENEMY_UNIT_FILL: "rgba(255,0,0,1)"
	    },
	    TERRAIN_COLORS: {

	        STROKE: "rgba(170,170,170,1)",
	        
	        0:  "rgba(0,0,128,1)",
	        1:  "rgba(70,130,180,1)",
	        2:  "rgba(245,222,179,1)",
	        3:  "rgba(154,205,50,1)",
	        4:  "rgba(34,149,34,1)",
	        5:  "rgba(112,128,144,1)",
	        6:  "rgba(255,215,0,1)",
	        7:  "rgba(40,40,40,1)",
	        8:  "rgba(0,0,0,1)"
	    },
	    TERRAIN_TYPES: {

	    	0: "deep water",
	        1: "shallow water",
	        2: "sand",
	        3: "field",
	        4: "forest",
	        5: "mountain",
	        6: "energy source",
	        7: "road",
	        8: "undiscovered"
	    }
	};
	return {
		
		_: _constants,
		
		get: function (category, constant) {
			return _constants[category][constant];
		},
		
		set: function (category, constant, value) {
			_constants[category][constant] = value;
		},
		
		getJson: function () {
			return JSON.stringify(_constants);
		}
	};
}());
