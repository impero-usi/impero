/**
 * Constant variables used for drawing Hexagons and Entities
 * 
 * @author Lokesh
 * 
 */

var hexagonUtility = {};

hexagonUtility.getY = function (row, col) {
	var offset = (col % 2 == 0) ? 0 : 1 / 2 * Constants.get("MAP", "MAP_HEX_HEIGHT");
    return Constants.get("MAP", "MAP_START_Y") + row * Constants.get("MAP", "MAP_HEX_HEIGHT") + offset;
};


hexagonUtility.getX = function (col) {
    return Constants.get("MAP", "MAP_START_X") + col * 3 / 4 * Constants.get("MAP", "MAP_HEX_WIDTH");
};

hexagonUtility.drawMovementLines = function (listOfPath, color, thickness){
//	console.log(listOfPath);
//    console.log("@@@I should be drawing something@@@");
  
	
	createjs.Graphics.prototype.dashedLineTo = function(x1, y1, x2, y2, dashLen) {
	    this.moveTo(x1, y1);
	    
	    var dX = x2 - x1;
	    var dY = y2 - y1;
	    var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
	    var dashX = dX / dashes;
	    var dashY = dY / dashes;
	    
	    var q = 0;
	    while (q++ < dashes) {
	        x1 += dashX;
	        y1 += dashY;
	        this[q % 2 == 0 ? 'moveTo' : 'lineTo'](x1, y1);
	    }
	    this[q % 2 == 0 ? 'moveTo' : 'lineTo'](x2, y2); 
	};
	
	
	
	
    if (listOfPath) {
		var referenceListOfDraw = [];
			var n = 0;
			while(listOfPath[n+1] != null){
				console.log("ishouldbecalled");
				var graphic = new createjs.Shape();
				graphic.graphics.moveTo(hexagonUtility.getX(listOfPath[n].col) , hexagonUtility.getY(listOfPath[n].row, listOfPath[n].col));
//				graphic.graphics.setStrokeStyle(thickness, 1).beginStroke(color).beginFill(color).lineTo(hexagonUtility.getX(listOfPath[n+1].col) , hexagonUtility.getY(listOfPath[n+1].row, listOfPath[n+1].col));
				
				graphic.graphics.setStrokeStyle(thickness, 1).beginStroke(color).beginFill(color).dashedLineTo(hexagonUtility.getX(listOfPath[n].col) , hexagonUtility.getY(listOfPath[n].row, listOfPath[n].col), hexagonUtility.getX(listOfPath[n+1].col), hexagonUtility.getY(listOfPath[n+1].row, listOfPath[n+1].col), 6);
				
				
				
				console.log(listOfPath[n]);
				console.log(listOfPath[n+1]);
				console.log(n);
//				stage.addChild(graphic);
				UIContainer.addChild(graphic);
				n++;
				referenceListOfDraw.push(graphic);
			}
			stage.update();
			
			return referenceListOfDraw;
	    }
};
