/**
 * This function keeps track of the state of the Game.Depending on the user action the state changes to a new state.
 * 
 * The GUI can be in the following states:
 * 
 * 1) waiting - the game is waiting for user input / action
 * 2) firstClickUnit - the user has clicked a Unit
 # 3) firstClickTerrain - the user has clicked a Terrain cell
 * 3) clickTarget - the user clicked a target that was displayed due to the first click
 * 4) end - the user clicked end turn
 * 
 * possible user actions:
 * 1) click entity
 * 2) create unit
 * 3) move unit
 * 4) attack
 * 5) conquer
 * 6) validTarget
 * 7) invalidTarget
 * 8) escape
 * 9) skip turn
 * 10) end turn
 * 11) (debug) end all turns
 * 
 * @author Simone, Jesper
 */

var GUIState = {};

GUIState.GUIStateSwitch = function (userAction, entity) {

	// Deselect last entity
	if (lastEntity != null && lastEntity != entity) {
		lastEntity.dehighlight();
	}

	// console.log("Entity");
	// console.dir(entity);
	
	switch (currentGUIState) {
	
	/* ---- WAITING ---- */
	case "waiting":

		switch (userAction) {
		case "clickEntity":
			if (getEntityAt(entity.row, entity.col) == null) {
				currentGUIState = "firstClickTerrain";
				stateHandler.firstClickTerrain(entity);
				lastEntity = entity;
			} else {
				currentGUIState = 'firstClickUnit';
				lastEntity = entity;
				currentOwnUnitSelected = entity;
				stateHandler.firstClickUnit(entity);
			}
			
			break;
		case "skipTurn":
		case "endTurn":
			currentGUIState = "end";
			// call end turn function
			stateHandler.endTurn();
			break;
		case "endAllTurns":
			currentGUIState = "end";
			stateHandler.endAllTurns();
			break;
		default:
			break;
		}
		
		break;
	
	/* ---- FIRST CLICK ON UNIT---- */
	case "firstClickUnit":
		
		switch (userAction) {
		case "escape":
			currentGUIState = "waiting";
			stateHandler.waiting(entity);
			break;
			
		case "skipTurn":
		case "endTurn":
			currentGUIState = "end";
			// call end turn function
			stateHandler.endTurn();
			break;

		case "create":
			// stay in first click and create new unit
			stateHandler.create(entity);
			break;
		
		case "clickEntity":
			// if entity is an own unit stay in first click
			var ownUnitClick = false;
			for (var i = map.entity.length - 1; i >= 0; i--) {
				if (map.entity[i].row == entity.row && map.entity[i].col == entity.col && playerID == entity.owner) {
					ownUnitClick = true;
				}
			}

			// console.log("Click entity");

			if (ownUnitClick) {
				currentOwnUnitSelected = entity;
				if (!currentOwnUnitSelected.actionDone) {
					stateHandler.firstClickUnit(entity);
				} else {
					// console.log("Unit can not act");
					currentOwnUnitSelected = null;
					currentGUIState = "waiting";
					openPopUp("Unit Action", "Unit already did an action in this turn.", 1, null);
				}
			} else {
				if (currentOwnUnitSelected.actions.canMove || currentOwnUnitSelected.actions.canAttack) {
					// console.log("Target click");
					currentGUIState = "clickTarget";
					stateHandler.validTarget(entity);
					lastEntity = entity;
				} else {
					currentGUIState = "firstClickTerrain";
					stateHandler.firstClickTerrain(entity);
				}
			}
			break;

		case "endAllTurns":
			currentGUIState = "end";
			stateHandler.endAllTurns();
			break;
		default:
			break;
		}
		
		break;

	/* ---- FIRST CLICK ON TERRAIN ---- */
	case "firstClickTerrain":
		
		switch (userAction) {
		case "escape":
			currentGUIState = "waiting";
			stateHandler.waiting(entity);
			break;
			
		case "skipTurn":
		case "endTurn":
			currentGUIState = "end";
			// call end turn function
			stateHandler.endTurn();
			break;

		case "create":
			// stay in first click and create new unit
			stateHandler.create(entity);
			break;
		
		case "clickEntity":
			if (getEntityAt(entity.row, entity.col) == null) {
				currentGUIState = "firstClickTerrain";
				stateHandler.firstClickTerrain(entity);
			} else {
				currentGUIState = 'firstClickUnit';
				if (entity.owner == playerID) {
					currentOwnUnitSelected = entity;
				}
				lastEntity = entity;
				stateHandler.firstClickUnit(entity);
			}
			break;

		case "endAllTurns":
			currentGUIState = "end";
			stateHandler.endAllTurns();
			break;
		default:
			break;
		}
		
		break;
	
	/* ---- CLICK TARGET ---- */
	case "clickTarget":
		
		switch (userAction) {
		case "escape":
			currentGUIState = "waiting";
			stateHandler.waiting(entity);
			break;
			
		case "valid":
			currentGUIState = "waiting";
			currentOwnUnitSelected.dehighlight();
			currentOwnUnitSelected.actionDone = true;
			currentOwnUnitSelected = null;
			break;
			// add action
		case "invalid":
			currentGUIState = "firstClickUnit";
			// current entity remains selected
			break;
		
		case "skipTurn":
		case "endTurn":
			currentGUIState = "end";
			// end the turn
			stateHandler.endTurn();
			break; 
		
		case "clickEntity":
			// check if target is valid
			stateHandler.validTarget(entity);
			break;
			
		case "endAllTurns":
			currentGUIState = "end";
			stateHandler.endAllTurns();
			break;
			
		default:
			break;
		}
		
		break;
	
	/* ---- END ---- */
	case "end":
		
		switch (userAction) {
		case "turnExecuted":
			currentGUIState = "waiting";
			stateHandler.waiting(entity);
			stateHandler.turnExecuted();
			break;

		default:
			break;
		}
		
	default:
		break;
	}
	
	lastAction = userAction;
	//This does not work here
	//lastEntity = entity;

	 console.log("Last entity is...");
	 console.dir(lastEntity);
	
	// console.log("Last own unit selected is...");
	// console.dir(currentOwnUnitSelected);
	
	
	 console.log("state changed to " + currentGUIState);
	 console.log("last user action " + lastAction);
	
};