/*
 * Handler for displaying changes to the player.
 * 
 * @ Orestis & Lokesh
 */

var changeUpdater = {};

// Necessary for keeping function and arguments for dispatching later.
changeUpdater.changeCallbacks = [];
changeUpdater.callbackArguments = [];
changeUpdater.currentIndex = 0;
changeUpdater.currentDisplayIndex = 1;

changeUpdater.baseInterval = 1000; // In milliseconds.

// Used for calling functions with parameters.
function dispatch(fn, args) {
	return fn.apply(null, args);
}

// Used to execute the next function in the callback chain.
function callNext(extraTime) {
	setTimeout(
			function() {
				if (changeUpdater.currentIndex === changeUpdater.changeCallbacks.length - 1)
					return;

				changeUpdater.currentIndex++;
				dispatch(
						changeUpdater.changeCallbacks[changeUpdater.currentIndex - 1],
						[ changeUpdater.callbackArguments[changeUpdater.currentIndex - 1] ]);
			}, changeUpdater.baseInterval + extraTime);
}

/**
 * Calls all appropriate change methods.
 */
changeUpdater.update = function(allchange) {
	console.log("In changeUpdater!");
	console.dir(allchange);

	// Change state.
	GUIState.GUIStateSwitch("turnExecuted", null);
	
	// Initialize action list.
	changeDisplayerMenu.loadChangeDisplayerMenu();
	changeUpdater.currentDisplayIndex = 1;

	// Take each change and display it (pass it to the appropriate function).
	var sequence = allchange.sequence;

	// Push every function that handles a change into a callback queue.
	$.each(sequence, function(index, data) {
		// Get specific change type.
		type = data.type;
		typeIndex = data.index;

		switch (type) {
		case "CreateEntityChange": {
			changeUpdater.changeCallbacks
					.push(changeUpdater.createEntityChange);
			changeUpdater.callbackArguments
					.push(allchange.createEntityChanges[typeIndex]);
			break;
		}
		case "EconomyChange": {
			changeUpdater.changeCallbacks.push(changeUpdater.economyChange);
			changeUpdater.callbackArguments
					.push(allchange.economyChanges[typeIndex]);
			break;
		}
		case "MoveChange": {
			changeUpdater.changeCallbacks.push(changeUpdater.moveChange);
			changeUpdater.callbackArguments
					.push(allchange.moveChanges[typeIndex]);
			break;
		}
		case "RemoveEntityChange": {
			changeUpdater.changeCallbacks
					.push(changeUpdater.removeEntityChange);
			changeUpdater.callbackArguments
					.push(allchange.removeEntityChanges[typeIndex]);
			break;
		}
		case "UpdateEntityChange": {
			changeUpdater.changeCallbacks
					.push(changeUpdater.updateEntityChange);
			changeUpdater.callbackArguments
					.push(allchange.updateEntityChanges[typeIndex]);
			break;
		}
		case "AttackChange": {
			changeUpdater.changeCallbacks.push(changeUpdater.attackChange);
			changeUpdater.callbackArguments
					.push(allchange.attackChanges[typeIndex]);
			break;
		}
		case "BattleChange": {
			changeUpdater.changeCallbacks.push(changeUpdater.battleChange);
			changeUpdater.callbackArguments
					.push(allchange.battleChanges[typeIndex]);
			break;
		}
		case "MapChange": {
			changeUpdater.changeCallbacks.push(changeUpdater.mapChanges);
			changeUpdater.callbackArguments
					.push(allchange.mapChange[typeIndex]);
			break;
		}
		case "VisibleEntityChange": {
			console.dir(allchange.visibleEntityChanges[typeIndex]);
			changeUpdater.changeCallbacks.push(changeUpdater.visibleEntityChanges);
			changeUpdater.callbackArguments
					.push(allchange.visibleEntityChanges[typeIndex]);
			break;
		}
		case "DefeatedPlayerChange": {
			changeUpdater.changeCallbacks
					.push(changeUpdater.defeatedPlayerChange);
			changeUpdater.callbackArguments
					.push(allchange.defeatedPlayerChange[typeIndex]);
			break;
		}
		case "GameOverChange": {
			if (allchange.gameOverChange) {
				changeUpdater.changeCallbacks.push(changeUpdater
						.gameOverChange);
			}
			break;
		}
		}
	});

	// Call the first change and the chain will start.
	callNext(0);

	// change to default state

	// GUIState.GUIStateSwitch("turnExecuted", null);

};

/**
 * Author : Orestis Melkonian 
 * Creates the new entities and updates army summary list. 
 * Expects an createEntityChangePOJO.
 */
changeUpdater.createEntityChange = function(change) {
//	console.log("In createEntityChange!");
//	console.dir(change);

	var playerName, description;
		
	var col = change.col;
	var row = change.row;
	
	playerName = getPlayerName(change.owner);

	description = " Created : " + change.type
			+ "<br> &nbsp;&nbsp;&nbsp;&nbsp; Owner : " + playerName;

	changeDisplayerMenu.displayAction(description,
			changeUpdater.currentDisplayIndex, col, row);
	changeUpdater.currentDisplayIndex++;


	var newEntity = new Entity(hexagonUtility.getX(col), hexagonUtility
			.getY(row, col), Constants.get("MAP", "ENTITY_SIZE"), row, col,
			//TODO add orientation
			change.owner, change.type, change.health, change.maxHealth, 0);

	map.entity.push(newEntity);

	ArmyPanel.refresh();
	map.centerMapOnHex(col, row);

	callNext(0);
};

/**
 * Update action and/or energy points of the current player. Expects an
 * economyChangePOJO.
 * 
 * @author Lokesh
 */
changeUpdater.economyChange = function(change) {
	// //console.log("In economyCHANGES!");
	if (change.playerID === playerID) {
		for (var i = 0; i < players.length; i++) {
			if (change.playerID === players[i].id) {
				players[i].energy = change.newEnergy;
				setEnergyPoints(change.newEnergy);
				players[i].actionPoints = change.actionPoints;
				setActionPoints(change.actionPoints);
				setEnergyPoints(change.newEnergy);
			}
		}

	}

	callNext(0);
};

/**
 * Move unit from start cell to end cell. Expects a moveChangePOJO.
 * 
 * @author Lokesh
 */
changeUpdater.moveChange = function(change) {
	console.dir(change);
	// for (var i = 0; i < map.entity.length; i++) {
	// 	if (map.entity[i].col === change.startCell.col
	// 			&& map.entity[i].row === change.startCell.row
	// 			&& map.entity[i].typeName === change.entityType) {
	// 		var typeOfUnit = map.entity[i].typeName;
	// 		var healthOfUnit = map.entity[i].health;
	// 		var maxHealthOfUnit = map.entity[i].maxHealth;
	// 		var sizeOfUnit = map.entity[i].size;
	// 		var ownerOfUnit = map.entity[i].owner;

	// 		playgroundContainerUnit.removeChild(map.entity[i].shape);
	// 		map.entity.splice(i, 1);

	// 		var newEntity = new Entity(
	// 				hexagonUtility.getX(change.endCell.col),
	// 				hexagonUtility.getY(change.endCell.row, change.endCell.col),
	// 				sizeOfUnit, change.endCell.row, change.endCell.col,
	// 				ownerOfUnit, typeOfUnit, healthOfUnit, maxHealthOfUnit);

	// 		r = change.movementOrientation * 60;
	// 		if (change.entityType !== "Soldier") {
	// 			if (r == 60) {
	// 				newEntity.shape.rotation = r;
	// 				newEntity.shape.x = hexagonUtility.getX(change.endCell.col) + 10;
	// 				newEntity.shape.y = hexagonUtility.getY(change.endCell.row,
	// 						change.endCell.col) - 32;
	// 			} else if (r == 120) {
	// 				newEntity.shape.rotation = r;
	// 				newEntity.shape.x = hexagonUtility.getX(change.endCell.col) + 32;
	// 				newEntity.shape.y = hexagonUtility.getY(change.endCell.row,
	// 						change.endCell.col) - 10;
	// 			} else if (r == 180) {
	// 				newEntity.shape.rotation = r;
	// 				newEntity.shape.x = hexagonUtility.getX(change.endCell.col) + 24;
	// 				newEntity.shape.y = hexagonUtility.getY(change.endCell.row,
	// 						change.endCell.col) + 22;
	// 			} else if (r == 240) {
	// 				newEntity.shape.rotation = r;
	// 				newEntity.shape.x = hexagonUtility.getX(change.endCell.col) - 8;
	// 				newEntity.shape.y = hexagonUtility.getY(change.endCell.row,
	// 						change.endCell.col) + 32;
	// 			} else if (r == 300) {
	// 				newEntity.shape.rotation = r;
	// 				newEntity.shape.x = hexagonUtility.getX(change.endCell.col) - 32;
	// 				newEntity.shape.y = hexagonUtility.getY(change.endCell.row,
	// 						change.endCell.col) + 10;
	// 			}
	// 		}

	// 		map.entity.push(newEntity);
	// 		ArmyPanel.refresh();
	// 		map.centerMapOnHex(change.endCell.col, change.endCell.row);
	// 	}
	// }

	var entity = getEntityAt(change.startCell.row, change.startCell.col);

	playgroundContainerUnit.removeChild(entity.shape);

	entity.orientation = change.movementOrientation;

	entity.row = change.endCell.row;
	entity.col = change.endCell.col;

	entity.x = map.hexagons[entity.row * Constants.get("MAP","MAP_WIDTH") + entity.col].centerX;
	entity.y = map.hexagons[entity.row * Constants.get("MAP","MAP_WIDTH") + entity.col].centerY;

	entity.shape = new createjs.Shape();

	entity.createGraphic();

	ArmyPanel.refresh();
	
	// does not work well
//	map.centerMapOnHex(change.endCell.col, change.endCell.row);

	stage.update();

	callNext(-changeUpdater.baseInterval/2);
};

/**
 * Author : Orestis Melkonian Remove entity from game and update army summary
 * list. Expects an removeEntityChangePOJO.
 */
changeUpdater.removeEntityChange = function(change) {
	console.log("In removeEntityChange!");
	console.dir(change);
	console.dir(map.entity);
	var playerName, description;
	var entity = getEntityOfOwnerAt(change.cell.row, change.cell.col,
			change.owner);
	
	if (entity != null) {
		if (entity.owner.playerID == change.owner.playerID) {
			playerName = getPlayerName(change.owner);
			

			description = " Removed : &nbsp;&nbsp;&nbsp;&nbsp;"
					+ entity.typeName
					+ "<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Owner : "
					+ playerName;

			changeDisplayerMenu.displayAction(description,
					changeUpdater.currentDisplayIndex, change.cell.col, change.cell.row);
			changeUpdater.currentDisplayIndex++;
			
			map.centerMapOnHex(change.cell.col, change.cell.row);

			playgroundContainerUnit.removeChild(entity.shape);
			for (var i = 0; i < map.entity.length; i++) {
				if (map.entity[i].row == change.cell.row
						&& map.entity[i].col == change.cell.col
						&& map.entity[i].owner == entity.owner) {
					map.entity.splice(i, 1);
					break;
				}
			}

			stage.update();
		}
	}

	callNext(0);
};

/**
 * Update entity's info. Expects an updateEntityChangePOJO.
 * 
 * @author Lokesh && Orestis
 */
changeUpdater.updateEntityChange = function(change) {

	var entity = getEntityOfOwnerAt(change.cell.row, change.cell.row,
			change.owner);


	if (entity != null) {
		if (entity.typeName == change.type) {
			var playerName = getPlayerName(change.owner);

			var damageInflicted = entity.health - change.health;
			var description = damageInflicted + " Damage to  "
					+ entity.typeName
					+ "<br> &nbsp;&nbsp;&nbsp;&nbsp; Owner : " + playerName;
		
		
			changeDisplayerMenu.displayAction(description,
					changeUpdater.currentDisplayIndex, change.cell.col, change.cell.row);
			changeUpdater.currentDisplayIndex++;
			
			map.centerMapOnHex(change.cell.col, change.cell.row);

			// getEntityAt(change.cell.row, change.cell.row).owner =
			// change.owner;
			getEntityAt(change.cell.row, change.cell.row).health = change.health;
			// refreshArmySummaryList(map.entity);
		}
	}
	callNext(0);
};

/**
 * 
 */
changeUpdater.attackChange = function(change) {
//	console.log("In attackChange!");
//	console.dir(change);
	
	// Find typeNames,playerNames
	var entity1 = getEntityAt(change.attacker.row, change.attacker.col);
	var playerName1 = getPlayerName(entity1.owner);
	var typeName1 = entity1.typeName;

	var entity2 = getEntityAt(change.defender.row, change.defender.col);
	var playerName2 = getPlayerName(entity2.owner);
	var typeName2 = entity2.typeName;

	var description = " Attack : <br>" + "&nbsp;&nbsp;&nbsp;&nbsp; from "
			+ typeName1
			+ "<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  ("
			+ playerName1 + ")<br>" + "&nbsp;&nbsp;&nbsp;&nbsp; to "
			+ typeName2
			+ "<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  ("
			+ playerName2 + ")";
	
	var focusCol, focusRow;
	if (entity1.owner == playerID) {
		focusCol = change.attacker.col;
		focusRow = change.attacker.row;
	}
	else {
		focusCol = change.defender.col;
		focusRow = change.defender.row;
	}
	
	changeDisplayerMenu.displayAction(description,
			changeUpdater.currentDisplayIndex, focusCol, focusRow);
	changeUpdater.currentDisplayIndex++;
	
	map.centerMapOnHex(focusCol, focusRow);

	callNext(0);
};

/**
 * Displays when a battle is occured. Expects an BattleChangePOJO.
 * 
 * @author Lokesh
 */
changeUpdater.battleChange = function(change) {
//	console.log("In battleCHANGES!");
//	console.dir(change);
	var description = " Battle Initiated!";
	changeDisplayerMenu.displayAction(description,
			changeUpdater.currentDisplayIndex, change.position.col, change.position.row);
	changeUpdater.currentDisplayIndex++;
	
	map.centerMapOnHex(change.position.col, change.position.row);

	callNext(0);
};

/**
 * Redraw the whole map. Expects an MapChangesPOJO.
 * 
 * @author Lokesh
 */
changeUpdater.mapChanges = function(changes) {
	console.log("In mapCHANGES!");
	console.log(changes.change);
	for (var j = 0; j < changes.change.length; j++) {
		var hexagon = map.hexagons[changes.change[j].row * Constants.get("MAP", "MAP_WIDTH") + changes.change[j].col];

		hexagon.type = changes.change[j].type;

		hexagon.discovered = changes.change[j].discovered;

		hexagon.shape = new createjs.Shape();

		hexagon.createGraphic();

		
	}

	// update stage only once
	stage.update();

	callNext(-changeUpdater.baseInterval);
};

/*
 * show pop-up when player has been defeated
 * 
 * @author Jesper
 */
changeUpdater.defeatedPlayerChange = function(change) {
	var description;
	if (change.id === playerID) {
		description = " You have been defeated! ";
	}
	else {
		var playerName = getPlayerName(change.id);
		description =  playerName + " was defeated.";
	}
	changeDisplayerMenu.displayAction(description, changeUpdater.currentDisplayIndex, null, null);
	changeUpdater.currentDisplayIndex++;
	
	callNext(0);
};

/*
 * show pop-up when game is over
 * 
 * @author Jesper
 */
changeUpdater.gameOverChange = function() {
	
	setTimeout(openPopUp("GAME OVER", "The game is over.", 2), 3 * changeUpdater.baseInterval);
	
};



/**
 * Updates enemys HQ or units accordingly. Expects an visibleEntityChangesPOJO.
 * 
 * @author Lokesh
 */
changeUpdater.visibleEntityChanges = function(changes) {
	console.log("In visibleEntityChanges!");
	console.log(changes);
	for(var i = 0; i < changes.newVisibleEntities.length; i++){
		console.log(changes.newVisibleEntities);
		var col = changes.newVisibleEntities[i].col;
		var row = changes.newVisibleEntities[i].row;
		var newEntity = new Entity(hexagonUtility.getX(col), hexagonUtility
				.getY(row, col), Constants.get("MAP", "ENTITY_SIZE"), row, col,
				changes.newVisibleEntities[i].owner, changes.newVisibleEntities[i].type, changes.newVisibleEntities[i].health, changes.newVisibleEntities[i].maxHealth);

		map.entity.push(newEntity);
		stage.update();
	}
	
	for(var i = 0; i < changes.removedVisibleEntities.length; i++){
		console.log(changes.removedVisibleEntities);
		var col = changes.removedVisibleEntities[i].col;
		var row = changes.removedVisibleEntities[i].row;
		var owner = changes.removedVisibleEntities[i].owner;
		var entity = getEntityOfOwnerAt(row, col, owner);
		
		playgroundContainerUnit.removeChild(entity.shape);
		for (var i = 0; i < map.entity.length; i++) {
			if (map.entity[i].row == row
					&& map.entity[i].col == col
					&& map.entity[i].owner == owner) {
				map.entity.splice(i, 1);
				stage.update();
				//break;
			}
		}
	}
	callNext(0);
};