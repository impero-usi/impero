//this file contains all the entity related functions

function Entity(x, y, size, row, col, owner, type, health, maxHealth, orientation) {
    var graphic = new createjs.Shape();
    this.shape = graphic; // store that shape for later reference
    this.x = x;
    this.y = y;
    this.size = size;
    this.row = row;
    this.col = col;
    //this.cell = map.hexagons[col + row * Constants.get("MAP","MAP_WIDTH")];
    this.owner = owner;
    this.typeName = type;
    this.health = health;
    this.maxHealth = maxHealth;
    //Add field to store if the unit already did an action so you can not interact with it anymore
    this.actionDone = false;
    //Store current unit orientation
    this.orientation = orientation;

    // id for divs click listeners
    this.tid = type.toLowerCase() +'@'+ owner +'@'+ x +'*'+ y;

    this.matrix = new createjs.Matrix2D();
    this.matrix.scale(0.26, 0.26);
    this.matrix.translate(this.x - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, this.y - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2);
    
    
    this.highlight = function(){
    	//console.log("highlithing...");
    	this.shape.filters = [ new createjs.ColorFilter(1,1,1,1,-50,-50,-50,0) ]; 
    	this.shape.cache(this.x - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, this.y - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2, Constants.get("MAP", "MAP_HEX_WIDTH"), Constants.get("MAP", "MAP_HEX_HEIGHT"));   
    	map.hexagons[col + row * Constants.get("MAP","MAP_WIDTH")].highlight();   
    };
    
    this.dehighlight = function(){
    	//console.log("dehighlithing...");

        map.hexagons[col + row * Constants.get("MAP","MAP_WIDTH")].dehighlight();

        playgroundContainerUnit.removeChild(this.shape);

        this.shape = new createjs.Shape();

    	this.createGraphic();  
    
    };

    this.createGraphic = function() {
        // create specific properties depending on type
        switch (this.typeName) {
            case "HQ":
                HQ(this);
                break;
            case "Bomber":
                AirUnit(this);
                break;
            case "Tank":
                GroundUnit(this);
                break;
            case "Battleship":
                NavalUnit(this);
                break;
            case "EEF":
                EEF(this);
                break; 
            case "RadarTower":
                RadarTower(this);
                break;
            case "Soldier":
                Soldier(this);
                break;
            case "Harbour":
                Harbour(this);
                break;
            default:
                HQ(this);
                break;
        }

         // On click callback function
        this.shape.on('click', function(ent) {
            return function() {
                GUIState.GUIStateSwitch('clickEntity', ent);
            };
        }(this));

        playgroundContainerUnit.addChild(this.shape);

        stage.update();
    };

    this.createGraphic();

    
        
}

// ----------- HQ ------------------- //

function HQ(parent) {

    if (parent.owner == playerID) {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['HQF'].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                      Constants.get("MAP", "POLY_POINT_SIZE"), 
                      Constants.get("MAP", "DRAW_START_ANGLE"));
    } else {
        // Create graphic for enemy unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['HQE'].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    }
    //graphic.name = '';
    	
}

//------------Radar Tower---------------//
function RadarTower(parent) {

    if (parent.owner == playerID) {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['radarTowerF'].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    } else {
        // Create graphic for enemy unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['radarTowerE'].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));  
    }
    //graphic.name = '';
    	
}


//------------Soldier---------------//
function Soldier(parent){
    
    parent.matrix = new createjs.Matrix2D();
    parent.matrix.scale(0.26, 0.26);
    parent.matrix.translate(parent.x - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, parent.y - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2);

    if (parent.owner == playerID) {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['soldierF' + getPostfixForOrientation(parent)].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    } else {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['soldierE' + getPostfixForOrientation(parent)].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    }
    //graphic.name = '';
}




/*
 * @author Lokesh
 */
function AirUnit(parent) {	

    parent.matrix = new createjs.Matrix2D();
    parent.matrix.scale(0.26, 0.26);
    parent.matrix.translate(parent.x - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, parent.y - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2);

    if (parent.owner == playerID) {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['bomberF' + getPostfixForOrientation(parent)].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    } else {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['bomberE' + getPostfixForOrientation(parent)].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    }
    //graphic.name = '';
    
}

/*
 * @author Lokesh
 */
function GroundUnit(parent) {

    parent.matrix = new createjs.Matrix2D();
    parent.matrix.scale(0.26, 0.26);
    parent.matrix.translate(parent.x - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, parent.y - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2);

    if (parent.owner == playerID) {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['tankF' + getPostfixForOrientation(parent)].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    } else {   
        // Create graphic for enemy unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['tankE' + getPostfixForOrientation(parent)].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
        
    }
    //graphic.name = '';
    
}


/*
 * @author Lokesh
 */
function NavalUnit(parent) {  

	parent.matrix = new createjs.Matrix2D();
    parent.matrix.scale(0.26, 0.26);
    parent.matrix.translate(parent.x - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, parent.y - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2);

    if (parent.owner == playerID) {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['battleshipF' + getPostfixForOrientation(parent)].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    } else {
        // Create graphic for enemy unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['battleshipE' + getPostfixForOrientation(parent)].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    }
    //graphic.name = '';
    
}



/*
 * @author Lokesh & Orestis
 */
function EEF(parent) {
    
    if (parent.owner == playerID) {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['EEFFriend'].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    } else {
        // Create graphic for enemy unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['EEFEnemy'].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    }
    //graphic.name = '';
    
}


function Harbour(parent) {
	
    if (parent.owner == playerID) {
        // Create graphic for own unit
        parent.shape.graphics
            .beginBitmapFill(bitmaps['harborF'].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));
    } else {
        // Create graphic for enemy unit
        gparent.shape.graphics
            .beginBitmapFill(bitmaps['harborE'].image, "no-repeat", parent.matrix)
            .drawPolyStar(parent.x, parent.y, parent.size, Constants.get("MAP", "POLY_SIDES"), 
                          Constants.get("MAP", "POLY_POINT_SIZE"), 
                          Constants.get("MAP", "DRAW_START_ANGLE"));

    }
    //graphic.name = '';
	
}

function getPostfixForOrientation(entity) {
    switch(entity.orientation) {
        case 0:
            return '_N';
            break;
        case 1:
            return '_NE';
            break;
        case 2:
            return '_SE';
            break;
        case 3:
            return '_S';
            break;
        case 4:
            return '_SO';
            break;
        case 5:
            return '_NO';
            break;
        default:
            return '';
    }
}
