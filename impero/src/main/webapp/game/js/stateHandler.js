/*
 * handles when GUI states changes
 * 
 * @ jesper 
 */

var stateHandler = {};
stateHandler.intervals = [];

stateHandler.waiting = function(entity) {
	// clear the entityMenu
	entityMenu.clearEntityMenu();
	stateHandler.clearTarget();
};

/*
 * Highlight all ranges
 */
stateHandler.highlightRanges = function(entity) {

	this.clearTarget();

	$.ajax({
				url : "/webresources/action/pathRequest/" + gameID,
				data : '{"row":"' + entity.row + '", "col":"' + entity.col
						+ '", "type":"' + entity.type + '" ,"playerID":"'
						+ playerID + '"}',
				type : "POST",
				contentType : "application/json ; charset=UTF-8",
				dataType : "json",
				success : function(data) {

					// console.log("Json received for " + entity.typeName);

					//console.dir(data);

					// Highlighting all the possible path for the unit to move
					for (var i = 0; i < data.path.length; i++) {

						var hexX = map.hexagons[data.path[i].col
								+ Constants.get("MAP", "MAP_WIDTH")
								* data.path[i].row].centerX;
						var hexY = map.hexagons[data.path[i].col
								+ Constants.get("MAP", "MAP_WIDTH")
								* data.path[i].row].centerY;

						var hexagon = map.hexagons[data.path[i].col
						 				+ Constants.get("MAP", "MAP_WIDTH")
						 				* data.path[i].row];


						hexagon.shape.filters = [ new createjs.ColorFilter(1, 1, 1,
								1, 50, 50, 50, 0) ];
						hexagon.shape.cache(hexX
								- Constants.get("MAP", "MAP_HEX_WIDTH") / 2,
								hexY - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2,
								Constants.get("MAP",
										"MAP_HEX_WIDTH"), Constants.get("MAP",
										"MAP_HEX_HEIGHT"));

						//add to list of target
						target.push(map.hexagons[data.path[i].col
								+ Constants.get("MAP", "MAP_WIDTH")
								* data.path[i].row]);
					}

					// Highlight the attack range
					for (var i = 0; i < data.attack.length; i++) {

						var hexX = map.hexagons[data.attack[i].col
								+ Constants.get("MAP", "MAP_WIDTH")
								* data.attack[i].row].centerX;
						var hexY = map.hexagons[data.attack[i].col
								+ Constants.get("MAP", "MAP_WIDTH")
								* data.attack[i].row].centerY;

						var hexagon = map.hexagons[data.attack[i].col
										+ Constants.get("MAP", "MAP_WIDTH")
										* data.attack[i].row];

						hexagon.shape.filters = [ new createjs.ColorFilter(1, 0.5,
								0.5, 1, 50, 0, 0, 0) ];
						hexagon.shape.cache(hexX - Constants.get("MAP", "MAP_HEX_WIDTH") / 2,
								hexY - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2, 
								Constants.get("MAP", "MAP_HEX_WIDTH"),
								Constants.get("MAP", "MAP_HEX_HEIGHT"));
						
						// put it at beginning to avoid hiding entity on top
						//playgroundContainerMap.setChildIndex(hexagon, 1);

						// add to list of target
						target.push(map.hexagons[data.attack[i].col
								+ Constants.get("MAP", "MAP_WIDTH")
								* data.attack[i].row]);
					}

					stage.update();

					console.dir(data.path);
					console.dir(data.cost);

				}

			});

};

stateHandler.clearTarget = function() {
	// remove changed color (by Lokesh)
	for ( var t in target) {

		// var hexagon = playgroundContainerMap.getObjectUnderPoint(
		// 		target[t].centerX, target[t].centerY);
		// if (hexagon.filters != undefined) {

		// 	hexagon.filters = [];

		// 	hexagon.cache(target[t].centerX - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, 
		// 		target[t].centerY - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2, 
		// 		Constants.get("MAP", "MAP_HEX_WIDTH"), Constants.get("MAP", "MAP_HEX_HEIGHT"));
		// }
		target[t].shape = new createjs.Shape();

		target[t].createGraphic();
	}

	target = [];

	stage.update();
};

/*
 * handle first click of user on a unit
 */
stateHandler.firstClickUnit = function(entity) {

	// check if entity is a hexagon that contains a building or unit, and call
	// the click
	if (entity instanceof Hexagon
			&& (ent = getEntityAt(entity.row, entity.col)) !== null) {
		// console.log("clicking entity on hex");
		ent.shape._listeners.click[0]();
		return;
	}

	// Deselect last entity
	if (lastEntity != null && lastEntity != entity) {
		lastEntity.dehighlight();
	}

	lastEntity = entity;

	// Select new entity
	entity.highlight();

	if (target.length != 0) {
		this.clearTarget();
	}

	// Build of the possible build actions
	$.ajax({
		url : "/webresources/game/entities/" + gameID,
		data : '{"row":"' + entity.row + '", "col":"' + entity.col
				+ '" ,"playerID":"' + entity.owner + '"}',
		type : "POST",
		// async set to false otherwise options are not display unless you click
		// on another cell and back
		async : false,
		contentType : "application/json ; charset=UTF-8",
		dataType : "json",
		success : function(data) {
			// console.dir(data);
			entity.actions = data;
			// know which entity to create
			entity.actions.entityIndex = null;
		}
	});

	// Highlight ranges if unit is own unit
	if (entity.owner === playerID
			&& (entity.actions.canMove || entity.actions.canAttack)) {
		this.highlightRanges(entity);
	}

	stage.update();

	entityMenu.displayEntityMenu(entity, entity.actions);
};

/*
 * handle first click of user on terrain
 */
stateHandler.firstClickTerrain = function(entity) {

	// check if entity is a hexagon that contains a building or unit, and call
	// the click

	if (entity instanceof Hexagon
			&& (ent = getEntityAt(entity.row, entity.col)) !== null) {
		// console.log("clicking entity on hex");
		ent.shape._listeners.click[0]();
		return;
	}

	// Deselect last entity
	if (lastEntity != null && lastEntity != entity) {
		lastEntity.dehighlight();
	}

	lastEntity = entity;

	// Select new entity
	entity.highlight();

	// var thisRef = this;

	if (target.length != 0) {
		this.clearTarget();
	}

	// Build of the possible build actions
	$.ajax({
		url : "/webresources/game/entities/" + gameID,
		data : '{"row":"' + entity.row + '", "col":"' + entity.col
				+ '" ,"playerID":"' + entity.owner + '"}',
		type : "POST",
		// async set to false otherwise options are not display unless you click
		// on another cell and back
		async : false,
		contentType : "application/json ; charset=UTF-8",
		dataType : "json",
		success : function(data) {
			// console.dir(data);
			entity.actions = data;
			// know which entity to create
			entity.actions.entityIndex = null;
		}
	});

	stage.update();

	entityMenu.displayEntityMenu(entity, entity.actions);
};

stateHandler.moveAttack = function(entity) {
	// body...
};

/*
 * handle create action
 */
stateHandler.create = function(entity) {

	var type = entity.actions.entities[entity.actions.entityIndex].name;

	if (type === "EEF" || type === "Road" || type === "RadarTower" || type === "Harbour") {
		$.ajax({
			url : "/webresources/action/createBuilding/" + gameID,
			data : '{"row":"' + entity.row + '", "col":"' + entity.col
					+ '" , "playerID":"' + playerID + '","type":"' + type
					+ '"}',
			type : "POST",
			entity : entity,
			contentType : "application/json ; charset=UTF-8",
			dataType : "json",
			success : function(data) {
//				console.log("##############THIS IS THE DATA FOR THE BUILDING");
//				console.log(data);
				if (data.valid) {
//					refreshActionList("Create " + type, null, entity);
					actionListMenu.addAction("Create " + type, null, entity);
					setActionPoints(getActionPoints() - data.cost);
					setEnergyPoints(getEnergyPoints() - data.energyCost);
					
				} else {
					openPopUp("Invalid Action", "The action is not valid.", 1, null);
				}
//				 console.log("Json received for action scheduling");
//				 console.dir(data);
			}
		});
	} else {
		$.ajax({
			url : "/webresources/action/createUnit/" + gameID,
			data : '{"row":"' + entity.row + '", "col":"' + entity.col
					+ '" , "type":"' + type + '","playerID":"' + playerID
					+ '"}',
			type : "POST",
			entity : entity,
			contentType : "application/json ; charset=UTF-8",
			dataType : "json",
			success : function(data) {
//				console.log("##############THIS IS THE DATA FOR THE ELSEEEE of EEF");
//				console.log(data);
				if (data.valid) {
//					refreshActionList("Create new " + type, entity.owner);
					actionListMenu.addAction("Create " + type, null, entity);
					setActionPoints(getActionPoints() - data.cost);
					setEnergyPoints(getEnergyPoints() - data.energyCost);
				} else {
					openPopUp("Invalid Action", "The action is not valid.", 1, null);
				}
				 console.log("Json received for action scheduling");
				 console.dir(data);
			}
		});
	}
};

/*
 * handle move action
 * 
 * @author Lokesh
 */
stateHandler.move = function(entity) {
	console.log("move");
};

/*
 * handle attack action
 */
stateHandler.attack = function(entity) {
	console.log("attack");
};

/*
 * handle conquer action
 */
stateHandler.conquer = function(entity) {
	console.log("conquer");
};

/*
 * handle valid target
 */
stateHandler.validTarget = function(entity) {
	// console.log("validTarget");
	// check with server if valid
	var entity1 = entity;
	$.ajax({
		url : "/webresources/action/moveattack/" + gameID,
		data : '{"rowFrom":"' + currentOwnUnitSelected.row + '", "colFrom":"'
				+ currentOwnUnitSelected.col + '", "rowTo":"' + entity.row
				+ '", "colTo":"' + entity.col + '","playerID":"' + playerID
				+ '"}',
		type : "POST",
		contentType : "application/json ; charset=UTF-8",
		dataType : "json",
		success : function(data) {

			 console.log("move attack data");
			 console.dir(data);

			// update state (by jesper)
			var action = data.valid ? "valid" : "invalid";
			if (action == "valid") {
				
				var entity = getEntityAt(data.path[0].row, data.path[0].col);
				
				// check if attack or move
				if (data.attack) {
					var refMoveLines = hexagonUtility.drawMovementLines(
							data.path, "red", 3);
//					refreshActionList("Attack", refMoveLines, playerID);
					actionListMenu.addAction("Attack", refMoveLines, entity);
					
					setActionPoints(getActionPoints() - data.cost);
					
				} else {
					
					var refMoveLines = hexagonUtility.drawMovementLines(
							data.path, "black", 3);
//					refreshActionList("Move Unit", refMoveLines, playerID);
					actionListMenu.addAction("Move " + entity.typeName, refMoveLines, entity);
					
				}
//				console.log("##############THIS IS THE DATA FOR THE MOVEEEE");
//				console.log(data);
//				thisRef.clearTarget();

				stateHandler.clearTarget();
				
				// TODO set that entity has performed its action
//				entity1.actionDone = true;
			} else {
//				openPopUp("Invalid Action", "The action is not valid.", 1, null);
			}
			
			GUIState.GUIStateSwitch(action, entity1);
		}
	});

};

/*
 * handle end turn
 */
stateHandler.endTurn = function() {
	// console.log("end turn");
	// end turn
	$.ajax({
		url : "/webresources/action/turnend/" + gameID,
		data : '{"playerID":"' + playerID + '"}',
		type : "POST",
		contentType : "application/json ; charset=UTF-8",
		dataType : "json",
		success : function(data) {
//			 console.log("Json received for end turn");
//			 console.dir(data);
			// if turn was scheduled successfully
			if(data) {

				stateHandler.intervals.push(setInterval(turnExecuted, 5000));
				turnExecuted();
				// refresh players status
				updatePlayerStatuses();
				
			}

		
		}
	});

};

// depricated - use super admin account will automatically end all turns
stateHandler.endAllTurns = function() {
	// console.log("end turn for all");
	// end turn
	$
	.each(
		players,
		function(player) {
			$
			.ajax({
				url : "/webresources/action/turnend/"
				+ gameID,
				data : '{"playerID":"' + players[player].id
				+ '"}',
				type : "POST",
				contentType : "application/json ; charset=UTF-8",
				dataType : "json",
				success : function(data) {
					// console.log("Json received for end turn");
					// console.dir(data);

				// check if turn has executed every 30s
				// console.log("turn executed");
				stateHandler.intervals
				.push(setInterval(turnExecuted,
					1000));
			
				

			}
		});

	});
};

/*
 * handle turn executed
 */
stateHandler.turnExecuted = function(entity) {
	
	actionListMenu.clearAllLines();
	
	actionListMenu.init();
};

/*
 * check if turn was executed
 */
function turnExecuted() {
	$(function() {

		$.ajax({
			url : "/webresources/changes/" + gameID,
			type : "GET",
			contentType : "application/json ; charset=UTF-8",
			dataType : "json",
			success : function(data) {
//				 console.log("Json received for changes");
//				 console.dir(data);
				if (data.sequence) {
					
					// enable end turn button
					$('#endTurnButton').bind('click');
					$('#endTurnButton > .a-btn').removeClass('active');
					$('#endTurnButton .a-fixed-btn-text').text('Skip Turn');
					
					$.each(stateHandler.intervals, function(index, interval) {
						console.log("interval " + interval);
						clearInterval(interval);
					});
					
					stateHandler.intervals.length = 0;
					openPopUp("Turn was executed", 
							"All players have sent their turns. " +
							"Press 'OK' to display the moves and start a new turn.", 
							3, data);
//					window.alert("TURN WAS EXECUTED\n");
					// change state when turn has executed
//					GUIState.GUIStateSwitch("turnExecuted", null);
//					
//					changeUpdater.update(data);
				}
			}
		});

	});
};
