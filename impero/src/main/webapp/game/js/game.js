/**
 * initializing the game, loading other scripts, declaring global variables
 * 
 * @author
 * 
 */

// global variables
var stage, playgroundContainerMap, playgroundContainerUnit, UIContainer, canvas, playerID, gameName, debug = true;

var gameURL = 'webresources/game/';

var map = Map();
var players = [];
var target = []; // keep track of target displayed
var currentActions = [];

//array for storing the bitmaps
var imagesPath = {};
var bitmaps = {};



var state = 'viewing';
// the initial state of the GUI - see GUIState.js for details
var currentGUIState = "waiting";
var lastAction; // keep track of last GUI action from user (used for target)
var currentOwnUnitSelected = null; //keep track of the unit you are interacting with
var lastEntity; // keep track of last entity clicked (used for target)

/**
 * 
 */
function init() {
	
	// hide game for user
	$("#loadingGame").css({"margin-top" : ($(document).height()/2 - $('#loadingGame').height()/2)+ "px"});
	$(".gameGUI").hide();

	gameID = document.location.toString().match(/\?id=([a-f0-9]+)/);
	if (gameID != null && typeof gameID[1] != 'undefined') {
		gameID = gameID[1];
	} else {
		gameID = '123123';
	}

    //Debug console log
	// console.log(document.location.toString());
	// console.log('Loading game : ' + gameID + "...");

	playerID = document.cookie.split("=")[1];

	// console.dir(playerID);

	window.addEventListener('resize', resizeCanvas, false);
	
	// catch escape key to reset state to waiting (by Jesper)
	$(document).keyup(function(e) {
	  if (e.keyCode == 27) { 
		GUIState.GUIStateSwitch("escape", null);  
	  }   
	});

	stage = new createjs.Stage('playground');

	playgroundContainerMap = new createjs.Container();
    playgroundContainerUnit = new createjs.Container();
	UIContainer = new createjs.Container();

	fsm = new Fsm();

	// get the game object
	$.getJSON('/webresources/game/' + gameID, null, function(data, textStatus,
			jqXHR) {
		
        //Debug console log
		console.log("received game");
		console.dir(data);
		
		// display game for user and remove loading text
		$("#loadingGame").remove();
		$(".gameGUI").show();
		
		stage.clear();
		// set the players
		players = data.players;
		// set name of the game
		gameName = data.name;
		
		//set the actions for this turn
//		console.log(data.currentActions);
		if(data.currentActions != null){
			currentActions = data.currentActions;
		}
		
		// calculate the map offset on canvas (by jesper & musci)
		Constants.set("MAP", "MAP_WIDTH", data.width);
		Constants.set("MAP", "MAP_HEIGHT", data.height);
		stage.canvas.width = $('#game').width();
		stage.canvas.height = window.innerHeight - $('#top').outerHeight() - 15; // TODO should not be hardcoded
//		stage.canvas.width = window.innerWidth;
//		stage.canvas.height = window.innerHeight;
		calculateMapOffset();

		map = new Map();
		// console.log(data);

		map.initMap(data.map);
		map.initEntities(data.entities);

		map.height = data.height;
		map.width = data.width;
		
        stage.addChild(playgroundContainerMap); //Index of map is 0
		stage.addChild(UIContainer); //Index of UIcontainer is 1
        stage.addChild(playgroundContainerUnit); //Index of units is 2


        console.log("stage log");
        console.dir(stage);

		map.ZoomAndPan();
		// console.log('Game loaded.');
		
		// add listeners to window
		$( window ).resize(resizeCanvas);
		
		//LOAD AND REFRESH MENUS - call after lists have been initialized
		refreshPlayerStatus(playerID);
		playersMenu.loadMenu(); // display player statuses
		ArmyPanel.refresh();
		entityMenu.loadEntityMenu();
		actionListMenu.init();
		afterLoadRefreshActionListAndMap();
		
		stage.update();
		
		// update player statuses, automatically
		setInterval(updatePlayerStatuses, 1000*30);
		
		// focus stage on HQ
		map.centerMapOnHex(map.entity[0].col, map.entity[0].row);

	});

}

function afterLoadRefreshActionListAndMap(){
//	console.log("WHY IM NOT WORKING??");
	for(var i=0;i<currentActions.length ; i++){
		var refMoveLines = null;
		if(currentActions[i].path != null){
			refMoveLines = hexagonUtility.drawMovementLines(currentActions[i].path, "black", 3);
		}else{
			refMoveLines = "";
		}
		console.log("refreshing actions");
		console.dir(currentActions);

		if(currentActions[i].name == "MoveAction"){
//			console.log("IAM FABULOUS AND I SHOULD BE WORKING.");
//			refreshActionList("Move Unit", refMoveLines, playerID);
			var entity = getEntityAt(currentActions[i].path[0].row, currentActions[i].path[0].col);
			actionListMenu.addAction("Move " + entity.typeName, refMoveLines, entity);
			
		}else if(currentActions[i].name == "SpawnUnitAction"){
			
//			refreshActionList("Create Unit " + currentActions[i].type, [], playerID);
			var entity = getEntityAt(currentActions[i].row, currentActions[i].col);
			actionListMenu.addAction("Create " + currentActions[i].type, refMoveLines, entity);
			
		}else if(currentActions[i].name == "SpawnBuildingAction"){
			
//			refreshActionList("Create Building " + currentActions[i].type, [], playerID);
			var entity = getEntityAt(currentActions[i].row, currentActions[i].col);
			actionListMenu.addAction("Create " + currentActions[i].type, refMoveLines, entity);
		}
		
	}
	
}

function resizeCanvas(event) {
	canvas = $('#game');
	stage.canvas.width = canvas.width();
	stage.canvas.height = canvas.height();
	stage.update();
}

function loadBitmaps() {
    imagesPath['deep water'] = '../../img/deepWaterTile.jpg';
    imagesPath['forest'] = '../../img/forestTile.jpg';
    imagesPath['field'] = '../../img/GrassTile.jpg';
    imagesPath['sand'] = '../../img/sandTile2.jpg';
    imagesPath['shallow water'] = '../../img/shallowWaterTile.jpg';
    imagesPath['mountain'] = '../../img/mountainTile.jpg';
    imagesPath['energy source'] = '../../img/energyTile.jpg';
    imagesPath['road'] = '../../img/groundTile.jpg';
    imagesPath['radarTowerF'] = '../../img/radarTileFriend.jpg';
    imagesPath['radarTowerE'] = '../../img/radarTileEnemy.jpg';
    imagesPath['EEFEnemy'] = '../../img/EEFTileEnemy.jpg';
    imagesPath['EEFFriend'] = '../../img/EEFTileFriend.jpg';
    imagesPath['HQF'] = '../../img/HQTileFriend.jpg';
    imagesPath['HQE'] = '../../img/HQTileEnemy.jpg';
    imagesPath['harborF'] = '../../img/harborTileFriend.jpg';
    imagesPath['harborE'] = '../../img/harborTileEnemy.jpg';
    //Store path to bitmaps of units that can move
    //Friend bomber
    imagesPath['bomberF_N'] = '../../img/Airplanes/AirplaneNFriendAlpha.png';
    imagesPath['bomberF_NE'] = '../../img/Airplanes/AirplaneNEFriendAlpha.png';
    imagesPath['bomberF_NO'] = '../../img/Airplanes/AirplaneNOFriendAlpha.png';
    imagesPath['bomberF_S'] = '../../img/Airplanes/AirplaneSFriendAlpha.png';
    imagesPath['bomberF_SE'] = '../../img/Airplanes/AirplaneSEFriendAlpha.png';
    imagesPath['bomberF_SO'] = '../../img/Airplanes/AirplaneSOFriendAlpha.png';
    //Enemy bomber
    imagesPath['bomberE_N'] = '../../img/Airplanes/AirplaneNEnemyAlpha.png';
    imagesPath['bomberE_NE'] = '../../img/Airplanes/AirplaneNEEnemyAlpha.png';
    imagesPath['bomberE_NO'] = '../../img/Airplanes/AirplaneNOEnemyAlpha.png';
    imagesPath['bomberE_S'] = '../../img/Airplanes/AirplaneSEnemyAlpha.png';
    imagesPath['bomberE_SE'] = '../../img/Airplanes/AirplaneSEEnemyAlpha.png';
    imagesPath['bomberE_SO'] = '../../img/Airplanes/AirplaneSOEnemyAlpha.png';
    //Friend tank
    imagesPath['tankF_N'] = '../../img/Tanks/tankNFriend.png';
    imagesPath['tankF_NE'] = '../../img/Tanks/tankNEFriend.png';
    imagesPath['tankF_NO'] = '../../img/Tanks/tankNOFriend.png';
    imagesPath['tankF_S'] = '../../img/Tanks/tankSFriend.png';
    imagesPath['tankF_SE'] = '../../img/Tanks/tankSEFriend.png';
    imagesPath['tankF_SO'] = '../../img/Tanks/tankSOFriend.png';
    //Enemy tank
    imagesPath['tankE_N'] = '../../img/Tanks/tankNEnemy.png';
    imagesPath['tankE_NE'] = '../../img/Tanks/tankNEEnemy.png';
    imagesPath['tankE_NO'] = '../../img/Tanks/tankNOEnemy.png';
    imagesPath['tankE_S'] = '../../img/Tanks/tankSEnemy.png';
    imagesPath['tankE_SE'] = '../../img/Tanks/tankSEEnemy.png';
    imagesPath['tankE_SO'] = '../../img/Tanks/tankSOEnemy.png';
    //Friend soldier
    imagesPath['soldierF_N'] = '../../img/soldier/soldierNOKFriendAlpha.png';
    imagesPath['soldierF_NE'] = '../../img/soldier/soldierNEOKFriendAlpha.png';
    imagesPath['soldierF_NO'] = '../../img/soldier/soldierNOOKFriendAlpha.png';
    imagesPath['soldierF_S'] = '../../img/soldier/soldierSOKFriend.png';
    imagesPath['soldierF_SE'] = '../../img/soldier/soldierSEOKFriend.png';
    imagesPath['soldierF_SO'] = '../../img/soldier/SoldierSOOKFriendAlpha.png';
	//Enemy soldier
    imagesPath['soldierE_N'] = '../../img/soldier/soldierNOKEnemyAlpha.png';
    imagesPath['soldierE_NE'] = '../../img/soldier/soldierNEOKEnemyAlpha.png';
    imagesPath['soldierE_NO'] = '../../img/soldier/soldierNOOKEnemyAlpha.png';
    imagesPath['soldierE_S'] = '../../img/soldier/soldierSOKEnemyAlpha.png';
    imagesPath['soldierE_SE'] = '../../img/soldier/soldierSEOKEnemyAlpha.png';
    imagesPath['soldierE_SO'] = '../../img/soldier/soldierSOOKEnemyAlpha.png';
    //Friend battleship
    imagesPath['battleshipF_N'] = '../../img/BattleShips/battleshipNFriendAlpha.png';
    imagesPath['battleshipF_NE'] = '../../img/BattleShips/battleshipNEFriendAlpha.png';
    imagesPath['battleshipF_NO'] = '../../img/BattleShips/battleshipNOFriendAlpha.png';
    imagesPath['battleshipF_S'] = '../../img/BattleShips/battleshipSFriendAlpha.png';
    imagesPath['battleshipF_SE'] = '../../img/BattleShips/battleshipSEFriendAlpha.png';
    imagesPath['battleshipF_SO'] = '../../img/BattleShips/battleshipSOFriendAlpha.png';
    //Enemy battleship
    imagesPath['battleshipE_N'] = '../../img/BattleShips/battleshipNEnemyAlpha.png';
    imagesPath['battleshipE_NE'] = '../../img/BattleShips/battleshipNEEnemyAlpha.png';
    imagesPath['battleshipE_NO'] = '../../img/BattleShips/battleshipNOEnemyAlpha.png';
    imagesPath['battleshipE_S'] = '../../img/BattleShips/battleshipSEnemyAlpha.png';
    imagesPath['battleshipE_SE'] = '../../img/BattleShips/battleshipSEEnemyAlpha.png';
    imagesPath['battleshipE_SO'] = '../../img/BattleShips/battleshipSOEnemyAlpha.png';


    for (var value in imagesPath) {
        bitmaps[value] = new createjs.Bitmap(imagesPath[value]);
    };

    console.dir(bitmaps);

}

/*
 * calculate the map offset to center the map
 * 
 * @author
 */
function calculateMapOffset() {
	Constants._["MAP"]["MAP_START_X"] = stage.canvas.width
			/ 2
			- (((Constants.get("MAP", "MAP_WIDTH") - 1) * 3 / 4) * Constants
					.get("MAP", "MAP_HEX_WIDTH")) / 2;
	Constants._["MAP"]["MAP_START_Y"] = stage.canvas.height / 2
			- Constants.get("MAP", "MAP_HEIGHT")
			* Constants.get("MAP", "MAP_HEX_HEIGHT") / 2;
}

function Player(name) {
	this.name = name;
}

/*
 * get a players name from the player ID
 * 
 * @author Jesper
 */
function getPlayerName(playerID) {
	for ( var ind in players) {
		
		if (players[ind].id === playerID) {
			return players[ind].name;
		}
	}
	return null;
}

/*
 * get entity at coordinate, otherwise return null
 */
function getEntityAt(row, col){
	
	for ( var i in map.entity) {
		entity = map.entity[i];
		if(entity.row === row && entity.col === col) {
			return entity;
		}
	}
	return null;
}

/*
 * get entity of owner at coordinate, otherwise return null
 */
function getEntityOfOwnerAt(row, col, playerID){
	
	for ( var i in map.entity) {
		entity = map.entity[i];
		if(entity.row === row && entity.col === col && entity.owner == playerID) {
			return entity;
		}
	}
	return null;
}

/*
 * update players status
 * 
 * @jesper
 */
function updatePlayerStatuses() {
	
	$.ajax({
		url: "/webresources/players/" + gameID
	}).done(function(data){
//		console.log("getting players statuses");
//		console.dir(data);
		if(data && data.length >0) {
			players = data;
			playersMenu.loadMenu(); // display player statuses
			refreshPlayerStatus(playerID);
		}
		
	});
	
}

// $(preload);
// var timer = setInterval(function() {
//     if (imgLoaded) {
//         clearInterval(timer);
//         console.log('deleted interval');
//         init();
//     }
// }, 200);

$(loadBitmaps);
$(init);

