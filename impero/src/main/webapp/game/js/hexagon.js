function Hexagon(x, y, size, row, col, type, discovered) {
	
	var hexagon_graphic = new createjs.Shape();
	this.shape = hexagon_graphic;
	this.centerX = x;
	this.centerY = y;
	this.size = size;
	this.row = row;
	this.col = col;
	this.type = type;

	this.discovered = discovered;

	this.matrix = new createjs.Matrix2D();
	this.matrix.scale(0.26, 0.26);
	this.matrix.translate(this.centerX - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, this.centerY - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2);
	
	this.highlight = function(){
    	// console.log("highlithing hex...");
    	this.shape.filters = [
    	      			       new createjs.ColorFilter(1,1,1,1,-50,-50,-50,0)
    	      	   			 ];   
    	this.shape.cache(this.centerX - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, this.centerY - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2, Constants.get("MAP", "MAP_HEX_WIDTH"), Constants.get("MAP", "MAP_HEX_HEIGHT"));   
    };
	    
    this.dehighlight = function() {
    	//console.log("dehighlithing hex...");

    	playgroundContainerMap.removeChild(this.shape);

    	this.shape = new createjs.Shape();

    	this.createGraphic();

    };

    this.createGraphic = function() {
    	this.shape.on('click', function(hex) {
			return function() {
				GUIState.GUIStateSwitch('clickEntity', hex);
			};		
		}(this));
		//
		// get the type and the color from the Constants Object
		//
		this.typeName = Constants.get("TERRAIN_TYPES", this.type || 0);
	
		if (this.type == 8) {
			this.shape.graphics
				.beginFill(Constants.get("TERRAIN_COLORS", this.type || 0))
				.drawPolyStar(this.centerX, this.centerY, this.size, Constants.get("MAP", "POLY_SIDES"), 
	                          Constants.get("MAP", "POLY_POINT_SIZE"), 
	                          Constants.get("MAP", "DRAW_START_ANGLE"));
		} else {
			this.shape.graphics
					.beginBitmapFill(bitmaps[Constants.get("TERRAIN_TYPES", this.type || 0)].image, "no-repeat", this.matrix)
					.drawPolyStar(this.centerX, this.centerY, this.size, Constants.get("MAP", "POLY_SIDES"), 
		                          Constants.get("MAP", "POLY_POINT_SIZE"), 
		                          Constants.get("MAP", "DRAW_START_ANGLE"));
		}
		

		if(discovered) {
			this.shape.filters = [
	   	      			       new createjs.ColorFilter(1,1,1,1,-80,-80,-80,0)
	   	      	   			 ];   
	   	 	//this.shape.cache(this.centerX - Constants.get("MAP", "MAP_HEX_WIDTH") / 2, this.centerY - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2, Constants.get("MAP", "MAP_HEX_WIDTH"), Constants.get("MAP", "MAP_HEX_HEIGHT"));
		}

		this.shape.cache(this.centerX - Constants.get("MAP", "MAP_HEX_WIDTH") / 2,
								this.centerY - Constants.get("MAP", "MAP_HEX_HEIGHT") / 2, 
								Constants.get("MAP", "MAP_HEX_WIDTH"),
								Constants.get("MAP", "MAP_HEX_HEIGHT"));
		
		playgroundContainerMap.addChild(this.shape);

		stage.update();
	};

	this.createGraphic();

}
