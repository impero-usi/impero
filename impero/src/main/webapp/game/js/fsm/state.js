function State(state_name) {
	this.state_name;
	if (state_name != null) {
		this.state_name = state_name;
	}
}

State.prototype.execute = function(transition) {};

//All possible states

//Waiting state
Waiting.prototype = new State();
Waiting.prototype.constructor = Waiting;

function Waiting() {
	this.state_name = 'Waiting';
}

Waiting.prototype.execute = function(transition) {
	switch(transition.transition) {
		case 'First click':
			return new First_click();
			break;
	}
};

//First click
First_click.prototype = new State();
First_click.prototype.constructor = First_click;

function First_click() {
	this.state_name = 'First_click';
}

First_click.prototype.execute = function(transition) {
	switch(transition.transition) {
		case 'Second click':
		break;
	}
};