//this file contains the map object implementation

function Map() {
	this.height;
	this.width;
	this.click = [];
	this.hexagons = [];
	this.entity = [];
	
}

Map.prototype.initMap = function(json) {
	for (var cell = 0; cell < json.length; cell++) {
		//console.log(json[cell]);
		var row = json[cell].row;
		var col = json[cell].col;
		var type = json[cell].type;
		this.hexagons.push(new Hexagon(hexagonUtility.getX(col), hexagonUtility
				.getY(row, col), Constants.get("MAP", "HEXAGON_SIZE"), row,
				col, type, json[cell].discovered));
	}
	// get upper left corner of the map

	// console.dir(this.hexagons);
};

Map.prototype.initEntities = function(json) {
	for (var cell = 0; cell < json.length; cell++) {
		console.log(json);
		var row = json[cell].row;
		var col = json[cell].col;
		var owner = json[cell].owner;
		var type = json[cell].type;
		console.log(type);
		var health = json[cell].health;
		var maxHealth = json[cell].maxHealth
		this.entity.push(new Entity(hexagonUtility.getX(col), hexagonUtility
				.getY(row, col), Constants.get("MAP", "ENTITY_SIZE"), row, col,
				owner, type, health, maxHealth, json[cell].orientation));
	}
};

Map.prototype.ZoomAndPan = function() {
	var zoom;
	var zoomOffset = {
			x : 0,
			y : 0
	};
	
//	 $('#playground').bind('mousewheel', function(event) {
////		console.log("mouse wheel");
////		console.dir(event);
//		var e = event.originalEvent;
//		
//		if(zoomOffset.x != 0) {
//			console.log("zoom offset in zoom x=" + zoomOffset.x + " y=" + zoomOffset.y);
////			console.dir(zoomOffset);
//		}
//		
//		if(Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))>0)
//			zoom=1.1;
//		else
//			zoom=1/1.1;
//		
////		console.log("regX=" + stage.regX + " stage.x=" + stage.x + "mouseX=" + stage.mouseX);
//		stage.regX = stage.mouseX + zoomOffset.x;
//		stage.regY = stage.mouseY + zoomOffset.y;
//		stage.x = stage.mouseX + zoomOffset.x;
//		stage.y = stage.mouseY + zoomOffset.y;	
//		stage.scaleX = stage.scaleY *= zoom;
//		
//		zoomOffset.x = 0;
//		zoomOffset.y = 0;
//		
////		console.log("regX=" + stage.regX + " stage.x=" + stage.x + "mouseX=" + stage.mouseX);
//		
//		if (stage.scaleX < 0.5) {
//			stage.scaleX = stage.scaleY = 0.50000000;
//		} else if (stage.scaleX > 5.0) {
//			stage.scaleX = stage.scaleY = 5.000000000;
//		}
//		stage.update();
//	});

	map_global = this;
	stage.addEventListener("stagemousedown", function(e) {
		
		var panOffset = {
			x : stage.x - e.stageX,
			y : stage.y - e.stageY
		};
		
		stage.addEventListener("stagemousemove", function(ev) {
			
			stage.x = ev.stageX + panOffset.x;
			stage.y = ev.stageY + panOffset.y;
			
			// if (stage.x > 0) {
			// stage.x = 0;
			// }
			// if (stage.y > 0) {
			// stage.y = 0;
			// }

			// console.log(stage.x + " " + stage.y);
			// console.log(stage.regX + " " + stage.regY);
			stage.update();
//			delete offsetBanana;

		});
		
	});
	
	stage.addEventListener("stagemouseup", function(event) {
//		console.log("regX=" + stage.regX + " stage.x=" + stage.x + "mouseX=" + stage.mouseX);
		// add offset for zoom
		console.dir(event);
		zoomOffset.x = stage.x + event.stageX;
		zoomOffset.y = stage.y + event.stageY;
		console.log("zoom offset in mouseup");
		console.dir(zoomOffset);
		stage.removeAllEventListeners("stagemousemove");
	});

};

Map.prototype.centerMapOnHex = function(col, row) {
	
	console.log("stage scaleX=" + stage.scaleX);
	// multiply by -1 for correct position
	var x = -1 * hexagonUtility.getX(col) * stage.scaleX;
	var y = -1 * hexagonUtility.getY(row, col) * stage.scaleY;

	stage.x = x + stage.canvas.width/2 * stage.scaleX;
	stage.y = y + stage.canvas.height/2 * stage.scaleY;
	
	stage.update();

};
