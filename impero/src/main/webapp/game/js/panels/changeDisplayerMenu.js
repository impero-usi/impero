/**
 * functions for handling the menu displaying the changes.
 * 
 * @author Jesper && Orestis
 */

var changeDisplayerMenu = {};

changeDisplayerMenu.domObj = {};

/*
 * loads the basic structure of the entity menu
 */
changeDisplayerMenu.loadChangeDisplayerMenu = function() {
	
	//1. Clear the actionList.
	$('#idActionListDiv').empty();
	//2. Change title of action list from "My Turn" to "Last Turn"
	$("#actionListTitle").text("Last Turn");
	
	$('#idActionListDiv').append('<ul id="turnList"></ul>');
	
	changeDisplayerMenu.domObj = $("#turnList");
	
};

/*
 * fills the status and actions according to the entity
 */
changeDisplayerMenu.displayAction = function(description, index, col, row) {
//	console.log("Appending! - index : " + index);
	var insideText = index + ") " + description;
	changeDisplayerMenu.domObj.append('<li id="action' + index + '" class="actionLi">' + insideText + '</li>');

	// add mouse listener for click (focus camera on associated entity)
	$('#action' + index).click(function(eventObject){
		if (col != null && row != null) {
			map.centerMapOnHex(col, row);
			if (getEntityAt(row, col) != null)
				getEntityAt(row, col).shape._listeners.click[0]();
		}
	
	});
};