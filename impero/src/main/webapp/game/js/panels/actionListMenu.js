/*
 * action list menu
 * 
 * @author Orestis, Jesper
 */

var actionListMenu = {};

/*
 * list to keep actions
 */
actionListMenu.actionList = [];

/*
 * object to store action details
 */
actionListMenu.action = function(index, actionText, pathListRef, entity) {
		this.index = index;
		this.pathListRef = pathListRef;
		this.actionText = actionText;
		this.entity = entity;
};

/*
 * initialize the action list (create divs)
 */
actionListMenu.init = function() {
	
	$('#idActionListDiv').empty();
	
	$('#idActionListDiv').append('<ul id="actionList"></ul>');
	
	$("#actionList" ).sortable({
		  update: actionListMenu.moveAction
	});
    $("#actionList" ).disableSelection();
    
    actionListMenu.actionList.length = 0;
	
};


/*
 * add new action to the list
 */
actionListMenu.addAction = function(msgText, pathListRef, entity) {

	// if displaying turn list, empty first
	if(($('#idActionListDiv').children()[0]).id === "turnList")
		actionListMenu.init();
	
	var index = actionListMenu.actionList.length;
	var action = new actionListMenu.action(index, msgText, pathListRef, entity);
	
	actionListMenu.actionList.push(action);
	// added to make sure skip turn is not displayed
	refreshPlayerStatus(playerID);
	
	var actionLi = document.createElement('li');
	var actionObj = $(actionLi);
	actionObj.append('<i class="fa fa-arrows-v fa-fw">&nbsp;');
	actionObj.addClass('actionLi');
	actionObj.attr('id', 'actionId' + index);
	actionObj.append(msgText);

	// add button for deleting
	actionObj.append('<button id ="actionBtn'+  index +'" type="button" class="btn btn-danger btn-xs pull-right">X</button>');
	
	$('#idActionListDiv > ul').append(actionLi);
	
	$('#actionBtn' + index).click(actionListMenu.deleteAction);
	
	$('#actionId' + index).click(function(event) {

		if(event.srcElement.localName !== "button") {
			var index = $(event.currentTarget).index();
			var entity = actionListMenu.actionList[index].entity;
			map.centerMapOnHex(entity.col, entity.row);
		}
		
	});
	
};


/*
 * mouse handler for delete action from list
 */
actionListMenu.deleteAction = function(event) {
	
	var index = event.currentTarget.id.slice(9);
	
	console.log("delete action with index " + index);
//	console.dir(event.currentTarget);
	
	$.ajax({
		url : "/webresources/action/delete/" + gameID,
		data : '{"playerID":"' + playerID + '", "actionIndex":"' + index + '"}',
		type : "POST",
		index : index,
		contentType : "application/json ; charset=UTF-8",
		dataType : "json",
		success : function(data) {
			
			if(data) {
				var index = this.index;
				setEnergyPoints(data.newEnergyLevel);
				setActionPoints(data.newActionPoints);
				
				$('#actionId' + index).remove();
				
				// delete lines
				var list = actionListMenu.actionList[index].pathListRef;
				for ( var i in list) {
					UIContainer.removeChild(list[i]);
				}
				
				if(actionListMenu.actionList[index].entity)
					actionListMenu.actionList[index].entity.actionDone = false;
				
				actionListMenu.actionList.splice(index, 1);
				
				actionListMenu.reorderActions();
				
				refreshPlayerStatus(playerID);
				
				stage.update();
				 
			 } else {
				 // probably not needed
			 }

		}
	});

};

/*
 * mouse handler when moving position of an action in the list
 */
actionListMenu.moveAction = function(event, ui) {
	
//	console.log("swapping");

	var from = event.srcElement.id.slice(8);
	var to = $(event.srcElement).index();
	
//	console.log("from " + from + " to " + to);
	
	$.ajax({
		url : "/webresources/action/swap/" + gameID,
		data : '{"from":"' + from + '", "to":"'
				+ to + '", "playerID":"' + playerID + '"}',
		type : "POST",
		from: from,
		contentType : "application/json ; charset=UTF-8",
		dataType : "json",
		success : function(data) {

			if (data) {

				// update all index
				actionListMenu.reorderActions();
				
			} else {
				// swap back - probably never reached
				
				if(from === 0) {
					$("#actionId" + from).insertBefore("#actionId" + from);
				} else {
					$("#actionId" + from).insertAfter("#actionId" + (from-1));
				}
				
			}
		}
	});
	
};

/*
 * adjust index and ids, after change in the list
 */
actionListMenu.reorderActions = function() {
	
	// update all actions index 
	$('#actionList').children().each(function(index, value){
		console.dir(this);
		var childObj = $(this);
		childObj.attr("id", "actionId" + index);
		// also for the button
		$(childObj.children()[1]).attr("id", "actionBtn" + index);
	});
	
	// can be removed after refactoring
	for (var int = 0; int < actionListMenu.actionList.length; int++) {
		actionListMenu.actionList[int].index = int;
	}
};

/*
 * clear all move and attack lines
 */
actionListMenu.clearAllLines = function() {
	
	for (var int = 0; int < actionListMenu.actionList.length; int++) {
		
		var list = actionListMenu.actionList[int].pathListRef;
		
		for ( var i in list) {
			UIContainer.removeChild(list[i]);
		}
	}
	
};
