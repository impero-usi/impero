/**
 * @author Orestis Melkonian and Matteo Muscella
 * Javascript for Player Status menu (top of the screen).
 * It displays the player's name, action points and energy points (ended turn flag).
 */

var playerName, energyPoints, actionPoints, status;

function refreshPlayerStatus(playerString){

	for(var i = 0; i< players.length ; i++){
		if(players[i].id == playerID){
			playerName = players[i].name;
			energyPoints = players[i].energy;
			actionPoints = players[i].actionPoints;
			status = players[i].status;
			break;
		}
	}
	
	console.log("players");
	console.dir(players);
	
	loadOnPagePlayerStatus();
	
	// disable end turn if player is waiting for turn execution
	if(status === "FINISHED") {
		$('#endTurnButton').unbind('click');
		$('#endTurnButton > .a-btn').addClass('active');
		$('#endTurnButton .a-fixed-btn-text').text('waiting');
	}
}

function loadOnPagePlayerStatus(){
		
	var playerStatus = $ ("#top");
	playerStatus.empty();
	playerStatus.append('<div class="statusProperty"><a class="" href="/index.jsp">IMPERO</a></div>');
	playerStatus.append('<div id="gameName" class="statusProperty" style="font-size:0.9ex">' + gameName + '</div>');
	playerStatus.append('<div id="playerName" class="statusProperty" style="font-size:1ex">' + playerName + ' (' + status.toLowerCase() + ')' + '</div>');
    playerStatus.append('<div id="energyPoints" class="statusProperty" style="color:orange;">'+ energyPoints + '</div>');
    playerStatus.append('<div id="actionPoints" class="statusProperty" style="color:green;">'+ actionPoints + '</div>');    
        
    // button FrontEnd Style
    var _button_div =  document.createElement('div');
    $(_button_div).addClass('button_margin');
    $(_button_div).attr('id', 'endTurnButton');

    var _a_button = document.createElement('a');
    $(_a_button).addClass('a-btn');

    var _btn_span = document.createElement('span');
    $(_btn_span).addClass('a-fixed-btn-text');
    
    var buttonText = "Skip Turn";
    if (actionListMenu.actionList.length > 0)
     	buttonText = "End Turn";
    
    $(_btn_span).text(buttonText);

	var _btn_span = document.createElement('span');
	$(_btn_span).addClass('a-fixed-btn-text');
	
	var buttonText = "Skip Turn";
	if (actionListMenu.actionList.length > 0)
		buttonText = "End Turn";
	
	$(_btn_span).text(buttonText);

    $(_a_button).append(_btn_span);
    $(_button_div).append(_a_button);
    playerStatus.append(_button_div);
    
//    endTurnButton.innerHTML = buttonText;
    
    // add other players status (by Vitor)
    // needs support from REST
//    for(var i =0; i < players.length; i++){
////    	console.log(players);
//    	if(players[i].id != playerID){
//    		playerStatus.append('<div id="playerStatus" class="statusProperty">'+ players[i].name +" = "+ players[i].status + '</div>');
//    	}
//    }
    
    playerStatus.append('<div class="statusProperty"><p></p></div>'); // needed to keep buttons in place
    
    // add mouse listener for click
	$("#endTurnButton").click(function(){
		if(buttonText == "Skip Turn"){
			openPopUp("Pay Attention !", "If you skip you will not perform any actions this turn. Are you sure about skip the turn ?", 0, null);
		}else{
//			console.log("clicked on end turn --- player name : " + getCurrentPlayerName() + " buttonText : " + buttonText);
			GUIState.GUIStateSwitch("endTurn", null);
		}
		
	});
}

/*Getter for player name. */
function getCurrentPlayerName(){
	return playerName;
}
/*Getter for energy points.. */
function getEnergyPoints(){
	return energyPoints;
}
/*Getter for playerName. */
function getActionPoints(){
	return actionPoints;
}
/*Setters for attributes. */
function setPlayerName(playerString){
	playerName = playerString;
	loadOnPagePlayerStatus();
}
function setEnergyPoints(energy){
	console.log("setting energy to " + energy);
	energyPoints = energy;
	loadOnPagePlayerStatus();
}
function setActionPoints(action){
	console.log("setting action points to " + action);
	actionPoints = action;
	loadOnPagePlayerStatus();
}