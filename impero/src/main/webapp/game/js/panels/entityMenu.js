/**
 * functions for handling the menu displayed when selecting an entity
 * 
 * @author Jesper
 */

var entityMenu = {};


/*
 * loads the basic structure of the entity menu
 */
entityMenu.loadEntityMenu = function () {
	
	var domObj = $("#entityMenu");
	
	// create div for stats
	domObj.append('<div id="entityStats" class="row"></div>');
	
	// create div for actions
	domObj.append('<div id="entityActions" class="row"></div>');
	
	this.clearEntityMenu();
	
};

/*
 * fills the status and actions according to the entity
 */
entityMenu.displayEntityMenu = function (entity, actions) {
	// clear entity menu
	this.clearEntityMenu();
	
	// set status
	$("#entityMenuTitle").text(entity.typeName);
	var entityStats = $("#entityStats");
	
	if(entity.owner) {
		entityStats.append('<div id="entityInfo"><span>Own: </span>' + getPlayerName(entity.owner) + '</div>');
	}
	if (entity.health) {
		var unit_health = document.createElement('span');
		$(unit_health).append(entity.health +'/'+ entity.maxHealth).addClass('label label-info entity_health pull-right');
		$('#entityInfo').append(unit_health);
//		$('#entityInfo').append('<span>health</span>' + entity.health +'/'+ entity.maxHealth + '</p>');
	}
	
	
	// add actions if defined
	if (actions && (entity.owner === playerID || !entity.owner)) {
		
		var entityActions = $('#entityActions');
		
		for ( var id in actions.entities) {
			
			entityActions.append(
					'<div id="ea' + id +'" class="entityAction list-group-item row">' +
					'<div class="row entityActionRow">Create '+ actions.entities[id].name + '</div>'+
					'<div class="row entityActionRow">' + 
					'<span class="label label-warning col-md-6" style="padding:10px;">EP: ' + actions.entities[id].energyCost +'</span>' +
					'<span class="label label-success col-md-6" style="padding:10px;">AP: ' + actions.entities[id].actionPointCost +'</span>' +
					'</div>'
					);
			
			// add mouse listener for click
			$('#ea' + id).click(function(eventObject){
				console.log("clicked on id " + eventObject.currentTarget.id.slice(2));
				entity.actions.entityIndex = eventObject.currentTarget.id.slice(2);
				GUIState.GUIStateSwitch("create", entity);
			});
		}
		
	}
};

/*
 * remove content from entity menu
 */
entityMenu.clearEntityMenu = function () {
	// remove stats
	var entityStats = $("#entityStats");
	entityStats.empty();
	$("#entityMenuTitle").text("nothing selected");
	
	// remove actions
	entityStats.next().empty();
};