/**
 * functions for handling the menu displayed when selecting an entity
 * 
 * @author Jesper
 */

var playersMenu = {};


/*
 * loads the basic structure of the entity menu
 */
playersMenu.loadMenu = function () {
	
	$("#playersMenuTitle").text("Other Players");
	var domObj = $("#playersMenu");
	domObj.empty();
	
//	console.log("load players menu");
//	console.dir(players);
	
	for ( var i in players) {
		// fill players data
		if(players[i].id !== playerID) {
			div = $('<div class="row playerStatus"></div>');
			div.append('<i class="fa fa-user"></i>');
			div.append(" "+ players[i].name);
			div.append(" (" + players[i].status.toLowerCase() +")");
			span = $('<span class="badge pull-right">');
			switch (players[i].status) {
			case "PLAYING" :
				span.append($('<i class="fa fa-clock-o">'));
				break;
			case "FINISHED" :
				span.append($('<i class="fa fa-check">'));
				break;
			case "WON" :
				span.append($('<i class="fa fa-star">'));
				break;
			case "DEFEATED" :
				span.append($('<i class="fa fa-times">'));
				break;
			}
			
			div.append(span);
			domObj.append(div);
		}		
	}
	
	
	
};