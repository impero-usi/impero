/**
 * Author: Vitor Carmo Vannuchi
 * This Javascript file is responsible for the armySummaryDiv (the army summary menu).
 * He display all the units in the map but should display only the units of the owner (player logged in).
 * Its clickable, when you click he shows an alert with the PLAYERID "logged and acting (you)"
 */

var armySummaryList = [];
var entitiesInActions;
/**
 * Author: Vitor We iterate through the entities parameter and display on the
 * browser the units that belong to your own player.
 * 
 * @param entities
 *            is an list of JSON objects that have the type, row, col and all
 *            info of the units in the map.
 */
//function refreshArmySummaryList(entities) {
//	armySummaryList = [];
//	entitiesInActions = entities;
//	var counter = 1;
//	for (var i = 0; i < entities.length; i++) {
//		if (entities[i].owner === playerID) {
//			var unitsOfArmy = {
//				"Type" : 'message',
//				"Owner" : 'owner',
//				"Index" : 'index',
//				"Row" : 'row',
//				"Col" : 'col',
//				"innerText" : 'innertext',
//				"cssId" : 'cssId'
//			};
//			unitsOfArmy.Type = entities[i].type;
//			unitsOfArmy.Owner = playerID;
//			unitsOfArmy.Index = i;
//			unitsOfArmy.Row = entities[i].row;
//			unitsOfArmy.Col = entities[i].col;
//			unitsOfArmy.innerText = counter + "- Type: " + entities[i].type
//					+ " - Position: " + entities[i].row + "," + entities[i].col;
//			unitsOfArmy.cssId = "unit" + counter;
//			armySummaryList.push("Type: " + entities[i].type + " - Position: "
//					+ entities[i].row + "," + entities[i].col);
//			armySummaryList.push(unitsOfArmy);
////			console.log(armySummaryList);
//			counter++;
//		}
//
//	}
//	// loadOnPageArmySummary();
//}
//
///**
// * Author: Vitor This is the load function, every time that is called, its
// * redraw the menu on the html page.
// */
//function loadOnPageArmySummary() {
//	ArmySummary = document.getElementById("idArmySummaryDiv");
//	ArmySummary.innerHTML = '<ul>';
//
//	for (var i = 0; i < armySummaryList.length; i++) {
//		ArmySummary.innerHTML += '<div id="' + armySummaryList[i].cssId
//				+ '" style="height:30px" class="armyUnitClass">'
//				+ armySummaryList[i].innerText + '</div>';
//	}
//	ArmySummary.innerHTML += '</ul>';
//
//	for (var i = 0; i < armySummaryList.length; i++) {
//		document.getElementById(armySummaryList[i].cssId).addEventListener('click', function(event) {
//							for (var x = 0; x < map.entity.length; x++) {
//								if (map.entity[x].owner == playerID) {
//									if ((x + 1 + "- Type: "
//											+ map.entity[x].typeName
//											+ " - Position: "
//											+ map.entity[x].row + "," + map.entity[x].col) == event.target.innerText) {
//										// console.log(map.entity[x].row,
//										// map.entity[x].col);
//										// console.dir(map.entity[x].shape._listeners);
////										console.log("clicked on me");
//										map.entity[x].shape._listeners.click[0]();
//									}
//								}
//							}
//
//						}, false);
//	}
//
//}
//
function ListItem (string, _id, classes, otherElements)
{
	var _class = classes || '';
	
	var div = $(document.createElement('div'));
	
	div.append($(document.createElement('span'))
			         .append(string||'')
			         .attr('id', _id))
			 .append(otherElements||'');
	
	return $($(document.createElement('li'))
	         .addClass(_class))
			 .append(div);
}

/**
 * Army Panel
 * 
 * @param String
 *            elementId Id of the html div
 * @author matteo.muscella@usi.ch
 */
var ArmyPanel = (function(elementId) {

	var that = this;
	var el = elementId;

	var entities = null;
	var _targets = new Array();
	var army = new Array();

	/**
	 * Reloads the 
	 */
	var reload = function() {

		for ( var i in entities)
		{
			(function saveEntity(i){

				var _e = entities[i];

				if (_e.owner === playerID)
				{
					if (army[_e.typeName] == null)
						army[_e.typeName] = [];

					army[_e.typeName].push(_e);
					_targets[_e.tid] = _e;
				}
			})(i);
		}
	};

	var redraw = function() {
		
		$(el).empty(); // empty the div before redrawing
		var _list = document.createElement('ul');
		$(el).append(_list);
		
		for (var _t in army)
		{
			(function(units){

				var nOfUnits = document.createElement('span');
				$(nOfUnits).append(units.length).addClass('label label-info entity_health');
				
				var current_type = ListItem(_t, units[0].tid, 'aUnit', nOfUnits);
				$(_list).append(current_type);

					var _children = document.createElement('ul');
					$(current_type).append(_children);
					$(current_type).removeClass('aUnit');
					$(current_type).removeAttr('id');
					
					for (u in units)
					{
						(function(unit){

							var unit_health = document.createElement('span');
							
							$(_children).append(ListItem(_t,
							                    unit.tid,
							                    'aUnit',
							                    unit_health));
							$(unit_health)
								.append(unit.health +'/'+ unit.maxHealth)
								.addClass('label label-info entity_health');

						})(units[u]);
					}

			})(army[_t]);
		}
	};

	var clicks = function() {
		$(el + ' .aUnit').on('click', function(e) {
			_targets[e.target.id].shape._listeners.click[0]();
			map.centerMapOnHex(_targets[e.target.id].col, _targets[e.target.id].row);
		});
	};

	var folds = function() {
		$(el + ' li:has(ul)').addClass('parent_li').find(' > div');
		
		$(el + ' li.parent_li > div').each(
				function() {
					var children = $(this).parent('li.parent_li').find(
							' > ul > li');
					if (children.is(":visible")) {
						children.hide('fast');
						$(this).find(' > i').addClass('glyphicon').addClass(
										'glyphicon-plus').removeClass(
										'glyphicon glyphicon-minus');
					}
				});
		$(el + ' li.parent_li > div').on(
				'click',
				function(e) {
					var children = $(this).parent('li.parent_li').find(
							' > ul > li');
					if (children.is(":visible")) {
						children.hide('fast');
						$(this).find(' > i').addClass(
										'glyphicon glyphicon-plus')
								.removeClass('glyphicon glyphicon-minus');
					} else {
						children.show('fast');
						$(this).find(
								' > i').addClass('glyphicon glyphicon-minus')
								.removeClass('glyphicon glyphicon-plus');
					}
					e.stopPropagation();
				});
	};

	return {
		init : function() {
			entities = map.entity;
		},
		_clear : function() {
			entities = [];
			_targets = new Array();
			army = new Array();
		},
		refresh : function() {
			this._clear();
			this.init();
			reload();
			redraw();
			clicks();
			folds();
			return true;
		},
		log : function() {
			console.log("LOGGING ARMY: ", army);
			console.log("LOGGING TARGETS: ", _targets);
			return true;
		}
	};
}("#idArmySummaryDiv"));
