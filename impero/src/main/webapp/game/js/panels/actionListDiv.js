/**
 * Author: Vitor Carmo Vannuchi This Javascript file is responsible for all the
 * events about the actionListDiv (menu) in game. By the moment the only
 * functions that he have is get all the units on the map and then display it.
 * they are clickable and if you click on it he will show an alert with the id
 * logged
 */

var actionList = [];
var indexList = [];
var counter = 0;
var deletionFix = 0;

var listIdAndOwners = [];
var idCounter = 0;
var List;
var PlaceHolder;

/**
 * This Variables are created to the swap of elements, and to post elements on
 * the table.
 */
var indexElementDragBegin = 0;
var indexElementDragEnd = 0;
var listActionsDragBegin = [];
var listActionsDragEnd = [];

var deleteCopyOfActionList = [];

function clearActionList() {

	// console.log("Im inside ClearActionList");
	document.getElementById("idActionListDiv").innerHTML = '';
	actionList.length = 0;
	indexList.length = 0;
	counter = 0;
	listIdAndOwners.length = 0;
	idCounter = 0;

	// allow entities to make new actions (by Jesper)
	for ( var e in map.entity) {
		e.actionDone = false;
	}

	loadOnPageActionListDiv();

}




/**
 * Author: Vitor This method takes all the elements of the parameter and then
 * put all the elements in this ActionList
 */
function refreshActionList(act, listOfPath, owner) {
	var elementsOfActions = {
		"Msg" : 'message',
		"Owner" : 'owner',
		"Index" : 'index',
		"RefListOfPath" : 'list'
	};
	var idAndOwnerElement = {
		"Id" : 'id',
		"Owner" : 'owner',
		"RefListOfPath" : 'list'
	};
	idAndOwnerElement.Id = "e" + (idCounter + 1);
	idAndOwnerElement.Owner = owner;
	idAndOwnerElement.RefListOfPath = listOfPath;
	idCounter++;
	listIdAndOwners.push(idAndOwnerElement);

	elementsOfActions.Msg = act + counter;
	elementsOfActions.Owner = owner;
	elementsOfActions.Index = idCounter - 1;
	elementsOfActions.RefListOfPath = listOfPath;
	actionList.push(elementsOfActions);
	counter++;
	deleteCopyOfActionList = actionList;
	loadOnPageActionListDiv();
	// console.log(actionList);
	// console.log(listIdAndOwners);
}

/**
 * Author: Vitor This method receive the ID (CSS ID) of the action in the list,
 * and then return the (PLAYER ID) Owner of that (CSS ID) id.
 * 
 * @param id
 * @returns
 */
function getOwnerById(id) {
	for (var i = 0; i < listIdAndOwners.length; i++) {
		if (id == listIdAndOwners[i].Id) {
			return listIdAndOwners[i].Owner;
		}
	}
}

/**
 * Author: Google Copied by Vitor From here on, is the drag, drop, swap draw
 * function all about
 */
function Position(x, y) {
	this.X = x;
	this.Y = y;

	this.Add = function(val) {
		var newPos = new Position(this.X, this.Y);
		if (val != null) {
			if (!isNaN(val.X))
				newPos.X += val.X;
			if (!isNaN(val.Y))
				newPos.Y += val.Y;
		}
		return newPos;
	};

	this.Subtract = function(val) {
		var newPos = new Position(this.X, this.Y);
		if (val != null) {
			if (!isNaN(val.X))
				newPos.X -= val.X;
			if (!isNaN(val.Y))
				newPos.Y -= val.Y;
		}
		return newPos;
	};

	this.Min = function(val) {
		var newPos = new Position(this.X, this.Y);
		if (val == null)
			return newPos;

		if (!isNaN(val.X) && this.X > val.X)
			newPos.X = val.X;
		if (!isNaN(val.Y) && this.Y > val.Y)
			newPos.Y = val.Y;

		return newPos;
	};

	this.Max = function(val) {
		var newPos = new Position(this.X, this.Y);
		if (val == null)
			return newPos;

		if (!isNaN(val.X) && this.X < val.X)
			newPos.X = val.X;
		if (!isNaN(val.Y) && this.Y < val.Y)
			newPos.Y = val.Y;

		return newPos;
	};

	this.Bound = function(lower, upper) {
		var newPos = this.Max(lower);
		return newPos.Min(upper);
	};

	this.Check = function() {
		var newPos = new Position(this.X, this.Y);
		if (isNaN(newPos.X))
			newPos.X = 0;
		if (isNaN(newPos.Y))
			newPos.Y = 0;
		return newPos;
	};

	this.Apply = function(element) {
		if (typeof (element) == "string")
			element = document.getElementById(element);
		if (element == null)
			return;
		if (!isNaN(this.X))
			element.style.left = this.X + 'px';
		if (!isNaN(this.Y))
			element.style.top = this.Y + 'px';
	};
}

function hookEvent(element, eventName, callback) {
	if (typeof (element) == "string")
		element = document.getElementById(element);
	if (element == null)
		return;
	if (element.addEventListener) {
		element.addEventListener(eventName, callback, false);
	} else if (element.attachEvent)
		element.attachEvent("on" + eventName, callback);
}

function unhookEvent(element, eventName, callback) {
	if (typeof (element) == "string")
		element = document.getElementById(element);
	if (element == null)
		return;
	if (element.removeEventListener)
		element.removeEventListener(eventName, callback, false);
	else if (element.detachEvent)
		element.detachEvent("on" + eventName, callback);
}

function cancelEvent(e) {
	e = e ? e : window.event;
	if (e.stopPropagation)
		e.stopPropagation();
	if (e.preventDefault)
		e.preventDefault();
	e.cancelBubble = true;
	e.cancel = true;
	e.returnValue = false;
	return false;
}

function getEventTarget(e) {
	e = e ? e : window.event;
	return e.target ? e.target : e.srcElement;
}

function absoluteCursorPostion(eventObj) {
	eventObj = eventObj ? eventObj : window.event;

	if (isNaN(window.scrollX))
		return new Position(eventObj.clientX
				+ document.documentElement.scrollLeft
				+ document.body.scrollLeft, eventObj.clientY
				+ document.documentElement.scrollTop + document.body.scrollTop);
	else
		return new Position(eventObj.clientX + window.scrollX, eventObj.clientY
				+ window.scrollY);
}

function dragObject(element, attachElement, lowerBound, upperBound,
		startCallback, moveCallback, endCallback, attachLater) {
	if (typeof (element) == "string")
		element = document.getElementById(element);
	if (element == null)
		return;

	if (lowerBound != null && upperBound != null) {
		var temp = lowerBound.Min(upperBound);
		upperBound = lowerBound.Max(upperBound);
		lowerBound = temp;
	}

	var cursorStartPos = null;
	var elementStartPos = null;
	var dragging = false;
	var listening = false;
	var disposed = false;

	function dragStart(eventObj) {
		if (dragging || !listening || disposed)
			return;
		dragging = true;

		if (startCallback != null)
			startCallback(eventObj, element);

		cursorStartPos = absoluteCursorPostion(eventObj);

		elementStartPos = new Position(parseInt(element.style.left),
				parseInt(element.style.top));

		elementStartPos = elementStartPos.Check();

		hookEvent(document, "mousemove", dragGo);
		hookEvent(document, "mouseup", dragStopHook);

		return cancelEvent(eventObj);
	}

	function dragGo(eventObj) {
		if (!dragging || disposed)
			return;

		var newPos = absoluteCursorPostion(eventObj);
		newPos = newPos.Add(elementStartPos).Subtract(cursorStartPos);
		newPos = newPos.Bound(lowerBound, upperBound);
		newPos.Apply(element);
		if (moveCallback != null)
			moveCallback(newPos, element, eventObj);
		return cancelEvent(eventObj);
	}

	function dragStopHook(eventObj) {
		dragStop();
		return cancelEvent(eventObj);

	}

	function dragStop() {
		if (!dragging || disposed)
			return;
		unhookEvent(document, "mousemove", dragGo);
		unhookEvent(document, "mouseup", dragStopHook);
		cursorStartPos = null;
		elementStartPos = null;
		if (endCallback != null)
			endCallback(element);
		dragging = false;
	}

	this.Dispose = function() {
		if (disposed)
			return;
		this.StopListening(true);
		element = null;
		attachElement = null;
		lowerBound = null;
		upperBound = null;
		startCallback = null;
		moveCallback = null;
		endCallback = null;
		disposed = true;
	};

	this.StartListening = function() {
		if (listening || disposed)
			return;
		listening = true;
		hookEvent(attachElement, "mousedown", dragStart);
	};

	this.StopListening = function(stopCurrentDragging) {
		if (!listening || disposed)
			return;
		unhookEvent(attachElement, "mousedown", dragStart);
		listening = false;

		if (stopCurrentDragging && dragging)
			dragStop();
	};

	this.IsDragging = function() {
		return dragging;
	};
	this.IsListening = function() {
		return listening;
	};
	this.IsDisposed = function() {
		return disposed;
	};

	if (typeof (attachElement) == "string")
		attachElement = document.getElementById(attachElement);
	if (attachElement == null)
		attachElement = element;

	if (!attachLater)
		this.StartListening();
}
/**
 * Here ends the methods of draw dragging.
 */

/**
 * Author: Vitor modified a lot the code from Google This is the method that is
 * called everytime the drag begins
 */
function itemDragBegin(eventObj, element) {
//	console.log("@@@@item drag begin instanciated@@@");
	listActionsDragBegin.length = 0;
	indexList.length = 0;
	deletionFix = 0;
	var all = null;
	all = document.getElementsByClassName("actionListClass");
	for (var i = 0; i < all.length; i++) {
		var elementsOfActions = {
			"Msg" : 'message',
			"Owner" : 'owner',
			"Index" : 'index',
			"Id" : 'id',
			"RefListOfPath" : 'list'
		};
		elementsOfActions.Id = listIdAndOwners[i].Id;
		elementsOfActions.Msg = all[i].innerText;
		elementsOfActions.Owner = listIdAndOwners[i].Owner;
		elementsOfActions.Index = i;
		elementsOfActions.RefListOfPath = listIdAndOwners[i].RefListOfPath;
		listActionsDragBegin.push(all[i]);
		indexList.push(elementsOfActions);
	}
//	console.log(element + " Drag Begin");
	// <!-- flag = true; -->
	// <!-- console.log("dragbegin", flag); -->
	element.style.top = element.offsetTop + 'px';
	element.style.left = element.offsetLeft + 'px';

	element.className = "drag";
	PlaceHolder.style.height = element.style.height;
	List.insertBefore(PlaceHolder, element);
	PlaceHolder.SourceI = element;

	// console.log(listActionsDragBegin);
	for (var i = 0; i < listActionsDragBegin.length; i++) {
		if (element.id == listActionsDragBegin[i].id) {
			indexElementDragBegin = i;
		}
	}

}

/**
 * Author: Vitor modified a lot the code from Google This is the method that is
 * called everytime the is happening
 */
function itemMoved(newPos, element, eventObj) {
	eventObj = eventObj ? eventObj : window.event;
	var yPos = newPos.Y
			+ (eventObj.layerY ? eventObj.layerY : eventObj.offsetY);

	var temp;
	var bestItem = "end";
	for (var i = 0; i < List.childNodes.length; i++) {
		if (List.childNodes[i].className == "actionListClass") {
			temp = parseInt(List.childNodes[i].style.height);
			if (temp / 2 >= yPos) {
				bestItem = List.childNodes[i];
				break;
			}
			yPos -= temp;
		}
	}

	if (bestItem == PlaceHolder || bestItem == PlaceHolder.SourceI)
		return;

	PlaceHolder.SourceI = bestItem;
	if (bestItem != "end")
		List.insertBefore(PlaceHolder, List.childNodes[i]);
	else
		List.appendChild(PlaceHolder);
}

/**
 * Author: Vitor modified a lot the code from Google This is the method that is
 * called everytime the drag ends
 * 
 * @param element
 */
function itemDragEnd(element) {

	// Post for swap actions
	// console.log(element + " Drag End");
	if (PlaceHolder.SourceI != null) {
		PlaceHolder.SourceI = null;
		List.replaceChild(element, PlaceHolder);
	}

	element.className = 'actionListClass';
	element.style.top = '0px';
	element.style.left = '0px';

	var all = null;
	all = document.getElementsByClassName("actionListClass");
	listActionsDragEnd.length = 0;
	actionList = [];
	for (var i = 0; i < all.length; i++) {
		var elementsOfActions = {
			"Msg" : 'message',
			"Owner" : 'owner',
			"Index" : 'index',
			"Id" : 'id',
			"RefListOfPath" : 'list'
		};
		elementsOfActions.Msg = all[i].innerText;
		elementsOfActions.Owner = listIdAndOwners[i].Owner;
		elementsOfActions.Index = i;
		elementsOfActions.Id = listIdAndOwners[i].Id;
		elementsOfActions.RefListOfPath = listIdAndOwners[i].RefListOfPath;
		actionList.push(elementsOfActions);
		listActionsDragEnd.push(all[i]);

	}
	deleteCopyOfActionList = actionList;
	// console.log("element, listActionsDragEnd");
	// console.log(element, listActionsDragEnd);
	for (var i = 0; i < listActionsDragEnd.length; i++) {
		if (element.id == listActionsDragEnd[i].id) {
			indexElementDragEnd = i;
		}

	}
//	console.log("from: " + indexElementDragBegin + " to: + indexElementDragEnd);"
	if (indexElementDragBegin != indexElementDragEnd) {
		$.ajax({
					url : "/webresources/action/swap/" + gameID,
					data : '{"from":"' + indexElementDragBegin + '", "to":"'
							+ indexElementDragEnd + '", "playerID":"'
							+ playerID + '"}',
					type : "POST",
					contentType : "application/json ; charset=UTF-8",
					dataType : "json",
					success : function(data) {
						// console.log("Json received for end turn");
						// console.dir(data);
						if (data) {
							// console.log("SWAPPED AEAEAEAEAE");
							// console.log(data);
						} else {
							actionList = [];
							for (var i = 0; i < listActionsDragBegin.length; i++) {
								var elementsOfActions = {
									"Msg" : 'message',
									"Owner" : 'owner',
									"Index" : 'index',
									"Id" : 'id',
									"RefListOfPath" : 'list'
								};
								elementsOfActions.Msg = listActionsDragBegin[i].innerText;
								elementsOfActions.Owner = listIdAndOwners[i].Owner;
								elementsOfActions.Index = i;
								elementsOfActions.Id = listActionsDragBegin[i].id;
								elementsOfActions.RefListOfPath = listIdAndOwners[i].RefListOfPath;
								actionList.push(elementsOfActions);
							}
							deleteCopyOfActionList = actionList;
							loadOnPageActionListDiv();
							alert("Swap of Action not Allowed !");
						}
					}
				});

		// console.log("@@@DRAG END@@@");
		// console.log(all);
		// console.log(actionList);
		// console.log(listActionsDragBegin);
		// console.log(listActionsDragEnd);
		// console.log("@@@@ACTIONLIST@@@@")
		// console.log(actionList);
	}

}

/**
 * Author Vitor Function isEmpty so Orestis can change the state of the button
 * (EndTurn/SkipTurn)
 */
function isActionListEmpty() {
	if (actionList.length == 0) {
		return true;
	}
	return false;
}

/**
 * Author: Vitor This method is called to reload the GUI of the menus..
 */
function loadOnPageActionListDiv() {
	$("#actionListTitle").text("My Turn");
	
	List = document.getElementById("idActionListDiv");
	List.innerHTML = '';

	PlaceHolder = document.createElement("DIV");
	PlaceHolder.className = "actionListClass";
	PlaceHolder.style.backgroundColor = "rgb(225,225,225)";
	PlaceHolder.SourceI = null;

	for (var i = 0; i < actionList.length; i++) {
		List.innerHTML += '<div id="e'
				+ (i + 1)
				+ '" style="height:30px" class="actionListClass">'
				+ ' <button id ="ebutton'+ i +'" type="button" class="btn btn-danger btn-xs close_box" value ="">X</button>--'
				+ actionList[i].Msg
				+ '</div>';	
				
	}

	for (var i = 0; i < actionList.length; i++) {
		new dragObject("e" + (i + 1), null, null, null, itemDragBegin,
				itemMoved, itemDragEnd, false);
	}
	
	$(document).on('click','.close_box', deleteClick);
	
}


/*
 * function to handle delete click
 */
var deleteClick = function() {
	flagClick = true;
//	console.log("CLICKED CLOSE");
	// $(".close_box").on('click', function(){
//	console.log("clicked close");
	if (deletionFix < 1) {
		// console.log($(this).parent().attr('id'));
		var all = null;
		all = document.getElementsByClassName("actionListClass");
		var deletedElement = $(this).parent().text();
//		console.log("THIS IS THE DELETEDELEMENT: " + deletedElement);
		actionList = [];
		for (var i = 0; i < all.length; i++) {
			var elementsOfActions = {
				"Msg" : 'message',
				"Owner" : 'owner',
				"Index" : 'index',
				"Id" : 'id',
				"RefListOfPath" : 'list'

			};
			elementsOfActions.Msg = all[i].innerText;
			elementsOfActions.Owner = getOwnerById($(this).parent().attr('id'));
			elementsOfActions.Index = i;
			elementsOfActions.Id = listIdAndOwners[i].Id;
			elementsOfActions.RefListOfPath = listIdAndOwners[i].RefListOfPath;
			actionList.push(elementsOfActions);

//			console.log(actionList);
		}

		deleteCopyOfActionList = actionList;
		
//		console.log("###############THIS IS THE ACTION LIST =");
//		console.log(all);
//		console.log(deleteCopyOfActionList);
//		console.log(actionList);
//		console.log(actionList[0].Msg);
//		console.log(deletedElement);
//		console.log(" "+actionList[0].Msg == deletedElement);
		$(this).parent().remove();
		deletionFix = 1;
		for (var i = 0; i < actionList.length; i++) {
			if (" "+actionList[i].Msg == deletedElement) {
//				 console.log("@@@@@@@@@@@@@@POST DELETE");
//				 console.log(indexList[i]);
//				 console.log(deleteCopyOfActionList[i]);
//				 console.log(deletedElement);
				$.ajax({
							url : "/webresources/action/delete/" + gameID,
							data : '{"playerID":"' + playerID
									+ '", "actionIndex":"' + i + '"}',
							type : "POST",
							contentType : "application/json ; charset=UTF-8",
							dataType : "json",
							success : function(data) {
//								console.log("@@@@@@@@@@@@@@POST DELETE");
//								console.log(data);
								setEnergyPoints(data.newEnergyLevel);
								setActionPoints(data.newActionPoints);
//								actionList.pop(i);
								indexList.pop(i);
//								console.log(deleteCopyOfActionList);
//								console.log(actionList);
								for (var j = 0; j < deleteCopyOfActionList[i].RefListOfPath.length; j++) {
//									stage.removeChild(deleteCopyOfActionList[i].RefListOfPath[j]);
									UIContainer.removeChild(deleteCopyOfActionList[i].RefListOfPath[j]);
//									console.log("removing" + j);
								}
								for(var k = 0; k < map.entity.length ; k++){
									console.log(k);
									console.log(map.entity[k].col, map.entity[k].row)
									console.log(deleteCopyOfActionList[0].col, deleteCopyOfActionList[0].row)
									if(map.entity[k].col == deleteCopyOfActionList[0].col && map.entity[k].row == deleteCopyOfActionList[0].row){
										map.entity[k].actionDone = false;
//										console.log("THIS ENTITY IS ACTIONDONE FALSE. !!!");
									}
								}
								stage.update();
								listIdAndOwners.pop(i);
//								deleteCopyOfActionList.pop(i);
//								console.log(deleteCopyOfActionList);
//								console.log("IM HAPPENING BBBBBBBBBBBBBBBBBBB")
								// console.log("Json received for end turn");
								// console.dir(data);
								// console.log("DELETEDDDAEAEAEEA");
								// console.log(data);
								
								
							}
						});
			}
		}
		actionList = [];
		for (var i = 0; i < all.length; i++) {
			var elementsOfActions = {
				"Msg" : 'message',
				"Owner" : 'owner',
				"Index" : 'index',
				"Id" : 'id',
				"RefListOfPath" : 'list'
			};
			elementsOfActions.Msg = all[i].innerText;
			elementsOfActions.Owner = getOwnerById($(this).parent().attr('id'));
			elementsOfActions.Index = i;
			elementsOfActions.Id = elementsOfActions[i].Id;
			elementsOfActions.RefListOfPath = listIdAndOwners[i].RefListOfPath;
			actionList.push(elementsOfActions);
		}
//		console.log("we are after the crazy ajax call");
//		console.log(actionList);
//		deleteCopyOfActionList = actionList;
	}

};