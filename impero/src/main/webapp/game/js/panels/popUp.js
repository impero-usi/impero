/**
 * 
 */

function openPopUp(title, text, type, changeData){
	console.log("In popUP!");
	
	$('#gameModal').modal('show');
	
	$('#myModalLabel').empty();
	$('#myModalLabel').append(title);
	
	$('#modalBodyId').empty();
	$('#modalBodyId').append(text);
	
	$('#modalFooterId').empty();
	if(type === 0) {
		$('#modalFooterId').append('<button type="button" onClick="confirmSkipAction(true)" class="btn btn-default" data-dismiss="modal">Yes</button><button type="button" onClick="confirmSkipAction(false)" class="btn btn-default" data-dismiss="modal">No</button>');
	} else if(type === 1) {
		$('#modalFooterId').append('<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>');
	} else if (type === 2) {
		// the game over
		$('#modalFooterId').append('<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>');
		
		$('#modalFooterId > button').click(function(event) {
			document.location.replace("/dashboard/statistics/game.jsp?id=" + gameID);
		});
	
	} else if (type === 3) {
		$('#modalFooterId').append('<button type="button" id="proceedToChange" class="btn btn-default" data-dismiss="modal">OK</button>');
		document.getElementById('proceedToChange').addEventListener("click", function() {
		    changeUpdater.update(changeData);
		}, false);	
	}
	
	// set button as active
	$('#modalFooterId > .btn').focus();
}

function confirmSkipAction(flag){
	if(flag){
//		console.log("clicked on end turn --- player name : " + getCurrentPlayerName() + " buttonText : " + buttonText);
		GUIState.GUIStateSwitch("endTurn", null);
//		console.log("Im confirming the skip action \o/");
	}else{
//		console.log("We didnt skip the turn <3");
	}
}