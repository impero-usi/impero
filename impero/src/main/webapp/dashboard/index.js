/**
 * When page loads gets all of your frinds
 */
$(function() {
	friendsAjax = $.ajax({
		type : 'GET',
		url : '/webresources/friend',
		dataType : 'json',
		success : function(playerList) {
			console.log(playerList);
			for (var i = 0; i < playerList.length; i++) {
				/*$('.friend-list').append(
						'<li  class="list-group-item" id='+playerList[i].playerID+'>' + playerList[i].name
								+ '<span class="badge friend"><i class="fa fa-minus" style="color: red;"></i></span></li>');*/
				li = $("<li class='list-group-item' id=" +playerList[i].playerID+">");
				href = $("<a href='/profile/index.jsp?name=" + playerList[i].name + "'>").text(playerList[i].name);
				del = '<span class="badge friend"><i class="fa fa-minus" style="color: red;"></i></span>';
				$('.friend-list').append(li.append(href).append(del));
			}
		}
	});
	friendsAjax.then(handleBadgeClick);
});

/**
 * Adds handler for badge click and removes the element
 */
function handleBadgeClick() {
	$('span.friend').on('click',function() {
		var element = this;
		$.ajax({
			url : '/webresources/friend/' + $(element).parent().attr("id"),
			type : 'DELETE'
		}).success(function () {
			$(element).parent().remove();
		});
	});
}

/**
 * Set the handler for enter key to add friend
 */
$(document).keypress(function(e) {
	if (e.which == 13) {
		$("#addFriend").click();
	}
});

// Typeahead part
var names = new Bloodhound({
	name : 'names',
	datumTokenizer : function(d) {
		return Bloodhound.tokenizers.whitespace(d.name);
	},
	limit : 4,
	queryTokenizer : Bloodhound.tokenizers.whitespace,
	/* remote : {
		url : '/webresources/players?prefix=%QUERY'
	}, */
	prefetch : '/webresources/players'
});

names.initialize();

$(function() {
	$('[data-target="autocomplete"]').typeahead(null, {
		name : "names",
		displayKey : 'name',
		source : names.ttAdapter()
	});
});

$("#addFriend").click(function() {
	if ($('input#addFriendInput').val().length != 0) {
		var playerID = "";
		var playerName = "";
		var ajaxID = $.ajax({
			async : false,
			type : 'GET',
			url : '/webresources/players?prefix=' + $('#addFriendInput').val(),
			dataType : 'json'
		});
		$.when(ajaxID).then(function(data) {
			console.dir(data);
			if(data.length == 0) {
				return;
			} else {
				playerName = data[0].name;
				playerID = data[0].id;
				ajaxPost = $.ajax({
					type : 'POST',
					url : '/webresources/friend/'+data[0].id,
					dataType : 'application/json',
				});
			}
		}).then(function() {
			/*$('.friend-list').append(
					'<li  class="list-group-item" id=' + playerID +'>' + playerName
							+ '<span class="badge friend"><i class="fa fa-minus" style="color: red;"></i></span></li>'); */
			li = $("<li class='list-group-item' id=" +playerID+">");
			href = $("<a href='/profile/index.jsp?name=" + playerName + "'>").text(playerName);
			del = '<span class="badge friend"><i class="fa fa-minus" style="color: red;"></i></span>';
			$('.friend-list').append(li.append(href).append(del));
			handleBadgeClick();
		});
	}
});

//Click handler for delete game
$('#gameList span.badge .fa-times-circle-o').click(function(event) {
	var gameId = $(this).parent().parent().find('a').attr('href').match(/\?id=([a-f0-9]+)$/)[1];
	var li = $(this).parent().parent();
	$.ajax({
		url : '/webresources/game/' + gameId,
		type : "DELETE"
	}).success(function() {
		li.remove();
	}); 
});

$('#gameList span.gameIcon').click(function(event) {
	var gameId = $(this).parent().find('a').attr('href').match(/\?id=([a-f0-9]+)$/)[1];
	var gameName = $(this).parent().text().trimLeft();
	$.ajax({
		url : '/webresources/players/' + gameId,
		type : "GET"
	}).success(function(response) {
		$('#gameDetails').removeClass('hidden');
		$('#gameDetails .panel-title').text(gameName);
		$('#gameDetails ul li').remove();
		$.each(response,function(index,data) {
			var where = $('#gameDetails .panel-body ul');
			var li = $('<li class="list-group-item background-trasparent">');
			li.append(data.name);
			span = $('<span class="badge">');
			switch (data.status) {
			case "PLAYING" :
				span.append($('<i class="fa fa-clock-o">'));
				break;
			case "FINISHED" :
				span.append($('<i class="fa fa-check">'));
				break;
			case "WON" :
				span.append($('<i class="fa fa-star">'));
				break;
			case "DEFEATED" :
				span.append($('<i class="fa fa-times">'));
				break;
			}
			li.append(span);
			where.append(li);
		});
	});
});