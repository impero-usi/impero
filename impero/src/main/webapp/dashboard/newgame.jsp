<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>New game</title>
<%@include file="../views/includes.jsp"%>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/libs/typeahead/typeahead.bundle.js"
	type="text/javascript"></script>
</head>
<body style="font-family: 'Distress';">
	<%@include file="../views/header.jsp"%>

	<!-- Modal -->
	<div class="modal fade" id="errorModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Please invite more</h4>
				</div>
				<div class="modal-body">You cannot play with yourself.
				<h3>Please invite more players </h3>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Go back</button>
				</div>
			</div>
		</div>
	</div>

	<div class="jumbotron navBackgroud navb login">
		<div class="container userpass">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" id="selectionTab">
				<li class="active"><a href="#simpleCreation" data-toggle="tab">Simple
						Creation</a></li>
				<li><a href="#advancedCreation" data-toggle="tab">Advanced</a></li>
				<!-- <li><a href="#easterEgg" data-toggle="tab">Click Me</a></li> -->
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="simpleCreation">
					<%@include file='views/CreateGameSimple.jsp'%>
				</div>
				<div class="tab-pane" id="advancedCreation">
					<%@include file='views/CreateGameAdvanced.jsp'%>
				</div>
<!-- 				<div class="tab-pane" id="easterEgg">
					<iframe width="640" height="480"
						src="//www.youtube.com/embed/T8IZ4hcMVqA?autoplay=1"
						frameborder="0" allowfullscreen></iframe>
				</div> -->
			</div>

		</div>
	</div>


	<script>
		$('#selectionTab a').click(function(e) {
			e.preventDefault();
			$(this).tab('show');
		});

		$('[data-slider]').each(function(index) {
			$(this).bind("slider:changed slider:ready", function(event, data) {
				$(this).attr('value', data.value);
				$('.' + $(this).attr('id') + '-output').text(data.value);
			});
		});
	</script>
	<script src="dashboard/newgame.js"></script>
	<%@include file="/views/footer.jsp"%>
</body>
</html>