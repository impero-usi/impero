<%@page import="ch.usi.inf.sa4.impero.impero.logic.player.PlayerState"%>
<%@page import="ch.usi.inf.sa4.impero.impero.logic.player.Player"%>
<%@page import="ch.usi.inf.sa4.impero.impero.db.MongoGame"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="ch.usi.inf.sa4.impero.impero.db.MongoGames ,ch.usi.inf.sa4.impero.impero.db.MongoGames , java.util.Map"%>
<%
	Cookie[] cookies = request.getCookies();
	String id = "";
	for (int i = 0; i < cookies.length; i++) {
		if (cookies[i].getName().equals("id")) {
			id = cookies[i].getValue();
		}
	}
	if (id == "") {
		System.out.println("Cookie expired");
		Cookie cookie = new Cookie("id", session.getAttribute("id")
				.toString());
	}
%>
<!DOCTYPE html>
<html>
<head>
<title>Dashboard</title>
<%@include file="/views/includes.jsp"%>
<style>
.badge{
	background-color: transparent;
	line-height: 0;
	font-size:24px;
	padding: 0px 0px;
	cursor: pointer;
}
</style>
</head>
<body style="font-family: 'Distress';">
	<%@include file="/views/header.jsp"%>

	<div class="jumbotron navBackgroud navb login">
		<div class="container" style="padding-left:110px;float:left;margin-bottom:10px;">
			<a href="dashboard/newgame.jsp" class="a-btn"> <span
				class="a-fixed-btn-text">New Game</span> <span
				class="a-btn-slide-text">Create</span> <span
				class="a-btn-icon-right"><span class="a-fixed-btn-text"><i class="fa fa-plus"></i></span></span>
			</a>
		</div>
		<div style="margin-left:55px; margin-top:30px;" class="row">
			<ul class="list-group col-md-6" id="gameList">
			<%
				for (Map.Entry<String, String> game : MongoGames.getGameListByPlayerID(id).entrySet()) {
			%>
			<li class="list-group-item background-trasparent">
			<% 
				if(MongoGame.isOwner(game.getKey(), request.getSession().getAttribute("id").toString())) {
			%>
			<span class="badge" style="background-color: transparent;"><i class='fa fa-times-circle-o' style='color: red;'></i></span>
			<%
				} 
			%>
			<%
				for(Player p : MongoGame.getPlayersInGame(game.getKey())) {
					if(p.getPlayerID().equals(request.getSession().getAttribute("id").toString())) {
						String faIcon = "";
						switch(p.getPlayerState()) {
						case DEFEATED : 
							faIcon = "fa-times";
							break;
						case FINISHED :
							faIcon = "fa-check";
							break;
						case PLAYING :
							faIcon = "fa-clock-o";
							break;
						case WON : 
							faIcon = "fa-star";
						}
						out.println("<span class='gameIcon'><i class='fa " + faIcon +"'></i></span>");
					}
				}
				
			 %>
			<a href="game/index.jsp?id=<%=game.getKey()%>"><%=game.getValue()%></a></li>
			<%
				}
			%>
			</ul>
			<div class='col-md-6 hidden' id="gameDetails">
				<div class="panel panel-default" style="background-color: transparent;">
					<div class="panel-heading">
						<h4 class="panel-title">
							
						</h4>
					</div>
					<div class="panel-body" style="color:white;">
						<ul class="list-group">
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div style="padding-top: 30px; padding-left:75px;">
			<h3 style="color:white; padding-left:40px;">Friends</h3>
			<ul style = "padding-left:40px; width:250px; color:#428bca; font-size:large; cursor:default;" class="list-group friend-list">
			</ul>
			<div class="input-group" style="padding-left:40px;">
				<input type="text" class="form-control" data-target="autocomplete" id="addFriendInput"> <span style = "float: left;"
					class="input-group-btn">
					<button class="btn btn-default" type="button" id="addFriend">Add</button>
				</span>
			</div>
		</div>
	</div>


	<script src="dashboard/index.js"></script>

	<%@include file="/views/footer.jsp"%>
</body>
</html>