/**
 * randomWord, needs to be implemented
 */
var randomWord;
/**
 * the global variable of the return form as an array
 */
var returnJSON;

// Change the submit handler of the form
$('#creategameform').submit(function(event) {
	event.preventDefault();
	if ($('#invited-list-advanced li').size() == 1) {
		$('#errorModal').modal('show');
		return;
	}
	defer = createArray($('#creategameform'));
	defer.then(function() {
		var answer = $.ajax({
			async : false,
			type : 'POST',
			data : JSON.stringify(returnJSON),
			url : "/webresources/startgame",
			contentType : "application/json",
			dataType : 'json'
		});
		if (answer.statusText == "OK") {
			document.location.replace("/game/index.jsp?id="
					+ answer.responseText);
		}
	});
});

// Creates the player list and the invited list
//TODO add this badge to invited list <span class="badge">delete</span>
$(function() {
	$.ajax({
		type : 'GET',
		url : '/webresources/friend',
		dataType : 'json',
		success : function(playerList) {
			console.log(playerList);
			for (var i = 0; i < playerList.length; i++) {
				$('.friend-list').append(
						'<li  class="list-group-item">' + playerList[i].name
								+ '</li>');
			}
			$('.friend-list').sortable({
				connectWith : '.invited-list'
			}).disableSelection();
			$('.invited-list').sortable({
				connectWith : '.friend-list',
				cancel : ".li-disabled"
			}).disableSelection();
		}
	});
});

/**
 * 
 * @param form
 *            takes the form to make as array and stores it in a global variable
 *            returnJSON
 * @returns handler for all the ajax calls, the caller function must wait for
 *          everything to finish
 */
function createArray(form) {
	var ret = {};
	ret["players"] = new Array();
	ret["owner"] = getCookie("id");
	var ajaxResponses = new Array();
	var arrayForm = form.serializeArray();
	var nameAjax = RandomWord();
	ajaxResponses.push(nameAjax);
	// Send an ajax request for every player in the list
	$('#invited-list-advanced > li').each(function(index, element) {
		ajaxResponses.push($.ajax({
			type : 'GET',
			dataType : 'json',
			url : '/webresources/players?prefix=' + $(element).text()
		}));
	});
	// Wait for the requests to finish
	var defer = $.when.apply($, ajaxResponses);
	defer.done(function() {
		$.each(arguments, function(index, response) {
			console.log(response);
			if(typeof response[0].Word != 'undefined') {
				ret['name'] = "Custom game of " + response[0].Word.trimRight();
			} else {
				ret['players'].push(response[0][0].id);
			}
		});
		for (var i = 0; i < arrayForm.length; i++) {
			if(arrayForm[i].name == 'mapsize') {
				ret['width'] = ret['height'] = arrayForm[i].value;
			} else {
				ret[arrayForm[i].name] = arrayForm[i].value;
			}
		}
		if($('#rivers').hasClass("active")) {
			ret['rivers'] = true;
		} else {
			ret['rivers'] = false;
		}
		if($('#lakes').hasClass("active")) {
			ret['lakes'] = true;
		} else {
			ret['lakes'] = false;
		}
	});
	returnJSON = ret;
	return defer;
}

// Get cookie value from its name
function getCookie(c_name) {
	var i, x, y, ARRcookies = document.cookie.split(";");

	for (i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x == c_name) {
			return unescape(y);
		}
	}
}

/**
 * Function to jsonp the random word
 * 
 * @returns the ajax object of the call
 */
function RandomWord() {
	var requestStr = "http://randomword.setgetgo.com/get.php";
	return $.ajax({
		type : "GET",
		async : false,
		url : requestStr,
		dataType : "jsonp",
		success : function(data) {
			ret = data;
		}
	});
}

// Submit the game handler
$("#submit").click(function() {
	$('#creategameform').submit();
});

$("#submitSimple").click(function() {
	$("#creategameformSimple").submit();
});

// Typeahead part
var names = new Bloodhound({
	name : 'names',
	datumTokenizer : function(d) {
		return Bloodhound.tokenizers.whitespace(d.name);
	},
	limit : 7,
	queryTokenizer : Bloodhound.tokenizers.whitespace,
	dupDetector : function(remote,local) {
		if(remote==local) {
			return true;
		} else {
			return false;
		}
	},
	remote : {
		url : '/webresources/players?prefix=%QUERY'
	},
	prefetch : '/webresources/friend'
});

$(function() {
	$('.form-control[id="player"]').keyup(function() {
		names.get($(this).val(),function(suggestions) {
			$('.friend-list').empty();
			$.each(suggestions,function(index,suggestion) {
				if(suggestion.id != getCookie('id')) {
					$('.friend-list').append('<li  class="list-group-item">' + suggestion.name + '</li>');
				}
			});
			$('friend-list').sortable('refresh');
		});
	});
});

names.initialize();


$("#creategameformSimple").submit(function(event) {
	event.preventDefault();
	var ajaxResponses = new Array();
	var ret = {};
	ret["players"] = new Array();
	ret["owner"] = getCookie("id");
	ret['width'] = types.sizes[$('#simpleSize label.active input').val()].width;
	ret['height'] = types.sizes[$('#simpleSize label.active input').val()].height;
	option = $('#simpleType label.active input').val();
	var nameAjax = RandomWord();
	ajaxResponses.push(nameAjax);
	if ($('#invited-list-simple li').size() == 1) {
		$('#errorModal').modal('show');
		return;
	}
	$('#invited-list-simple > li').each(function(index, element) {
		ajaxResponses.push($.ajax({
			type : 'GET',
			dataType : 'json',
			url : '/webresources/players?prefix=' + $(element).text()
		}));
	});
	var defer = $.when.apply($, ajaxResponses);
	defer.done(function() {
		$.each(arguments, function(index, response) {
			if(typeof response[0].Word != 'undefined') {
				ret['name'] = types.types[option].name +" of " + response[0].Word.trimRight();
			} else {
				ret['players'].push(response[0][0].id);
			}
		});
		$.each(types.types[option].parameters,function(index,data) {
			ret[index] = data;
		});
	});
	defer.then(function() {
		var answer = $.ajax({
			async : false,
			type : 'POST',
			data : JSON.stringify(ret),
			url : "/webresources/startgame",
			contentType : "application/json",
			dataType : 'json'
		});
		if (answer.statusText == "OK") {
			document.location.replace("/game/index.jsp?id="
					+ answer.responseText);
		}
	});
	
});
