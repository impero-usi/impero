var dataPlaceHolder = {
	"type" : "pie",
	"pathToImages" : "js/libs/amcharts/images/",
	"angle" : 12,
	"balloonText" : "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
	"depth3D" : 15,
	"innerRadius" : "40%",
	"titleField" : "name",
	"valueField" : "energy",
	"theme" : "black",
	"handDrawn" : true,
	"allLabels" : [],
	"balloon" : {},
	"legend" : {
		"align" : "center",
		"markerType" : "circle"
	},
	"titles" : [],
	"dataProvider" : [ {
		"energy" : "Energy 1",
		"column-1" : 8
	}, {
		"energy" : "Energy 2",
		"column-1" : 6
	}, {
		"energy" : "Energy 3",
		"column-1" : 2
	} ]
};

var dataSerialPlaceHolder = {
	"type" : "serial",
	"pathToImages" : "js/libs/amcharts/images/",
	"categoryField" : "name",
	"angle" : 30,
	"depth3D" : 50,
	"colors" : [ "#009d00", "#d80000", "#eea638", "#a7a737", "#86a965",
			"#8aabb0", "#69c8ff", "#cfd27e", "#9d9888", "#916b8a", "#724887",
			"#7256bc" ],
	"startDuration" : 1,
	"theme" : "black",
	"categoryAxis" : {
		"gridPosition" : "start"
	},
	"trendLines" : [],
	"graphs" : [ {
		"balloonText" : "[[category]] created [[value]] units",
		"fillAlphas" : 1,
		"id" : "AmGraph-1",
		"title" : "Created",
		"type" : "column",
		"valueField" : "created"
	},{
		"balloonText" : "[[category]] lost [[value]] units",
		"fillAlphas" : 1,
		"id" : "AmGraph-3",
		"title" : "Lost",
		"type" : "column",
		"valueField" : "lost"
	}, {
		"balloonText" : "[[category]] destroyed [[value]] units",
		"fillAlphas" : 1,
		"id" : "AmGraph-2",
		"title" : "Destroyed",
		"type" : "column",
		"valueField" : "destroyed"
	} ],
	"guides" : [],
	"valueAxes" : [ {
		"id" : "ValueAxis-1",
		//"stackType" : "100%",
		"title" : "Units"
	} ],
	"allLabels" : [],
	"balloon" : {},
	"legend" : {
		"useGraphSettings" : true
	},
	"titles" : [ {
		"id" : "Title-1",
		"size" : 15,
		"text" : ""
	} ],
	"dataProvider" : [ {
		"name" : "category 1",
		"created" : 8,
		"destroyed" : 5
	}, {
		"name" : "category 2",
		"column-1" : 6,
		"column-2" : 7
	}, {
		"name" : "category 3",
		"column-1" : 2,
		"column-2" : 3
	} ]
};
$.ajax({
	url : '/webresources/players/'
			+ window.location.href.match(/\?id=([a-f0-9]*)/)[1],
	type : 'GET',
	contentType : 'application/json',
	success : function(response) {
		energyData = $.extend(true, {}, dataPlaceHolder);
		energyData['titleField'] = "name";
		energyData['valueField'] = "energy";
		energyData['dataProvider'].length = 0;
		$.each(response, function(index, data) {
			energyData['dataProvider'].push({
				name : data.name,
				energy : data.energyGathered
			});
		});
		AmCharts.makeChart("chartdivenergy", energyData);

		actionData = $.extend(true, {}, dataPlaceHolder);
		actionData['titleField'] = "name";
		actionData['valueField'] = "actionPoints";
		actionData['dataProvider'].length = 0;
		$.each(response, function(index, data) {
			actionData['dataProvider'].push({
				name : data.name,
				actionPoints : data.actionPoints
			});
		});
		AmCharts.makeChart("chartdivactionpoints", actionData);

		dataSerialPlaceHolder['dataProvider'].length = 0;
		$.each(response, function(index, data) {
			dataSerialPlaceHolder['dataProvider'].push({
				name : data.name,
				created : data.unitsCreated,
				destroyed : data.unitsDestroyed,
				lost : data.unitsLost
			});
		});
		AmCharts.makeChart("chartdivunits", dataSerialPlaceHolder);
	}
});
