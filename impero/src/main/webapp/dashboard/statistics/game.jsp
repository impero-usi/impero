<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Game statistics</title>
<%@include file="/views/includes.jsp"%>
<script src="js/libs/amcharts/amcharts.js" type="text/javascript"></script>
<script src="js/libs/amcharts/pie.js" type="text/javascript"></script>
<script src="js/libs/amcharts/serial.js" type="text/javascript"></script>
<script src="js/libs/amcharts/themes/black.js" type="text/javascript"></script>
<script src="dashboard/statistics/gameStatistics.js"
	type="text/javascript"></script>
<style type="text/css">
h3 {
	text-align: center;
	color: white;
}
</style>
</head>
<body style="font-family: 'Distress';">
	<%@include file="/views/header.jsp"%>
	<div id="container" class="jumbotron navBackgroud navb login">
		<div class="row" style="background-color: rgba(0, 0, 0, 0.6)">
			<div class="col-md-6">
				<h3>Energy Points</h3>
				<div id="chartdivenergy"
					style="width: 100%; height: 400px;"></div>
			</div>

			<div class="col-md-6">
				<h3>Action Points</h3>
				<div id="chartdivactionpoints"
					style="width: 100%; height: 400px;"></div>
			</div>
		</div>
		
		<div class="row" style="background-color: rgba(0, 0, 0, 0.6)">
		<h3>Unit Statistics</h3>
			<div id="chartdivunits"
					style="width: 100%; height: 400px;"></div>
		</div>
	</div>
	<%@include file="/views/footer.jsp"%>
</body>
</html>