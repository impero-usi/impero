<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<form class="form-horizontal" id="creategameform">
	<div class="form-group"></div>
	<fieldset class="fieldsetpadding">
		<legend class="userpass" style="padding-top: 20px;">Map Type</legend>

		<div class='form-group' style="padding-top: 40px;">
			<label class="userpass col-sm-2 control-label" for="map_size"
				style="margin-top: -10px;">Map Size</label>
			<div class="col-sm-2">
				<input type="text" name='mapsize' data-slider="true" value="20"
					data-slider-range="8,50" data-slider-step="2" id='mapsize'
					data-slider-highlight="true" data-slider-theme="volume"
					class="form-control">
			</div>
			<label style="margin-left: 190px;" for='water userpass control-label'
				class='mapsize-output'>20</label> &times; <label
				for='water userpass control-label' class='mapsize-output'>20</label>
		</div>

		<div class='form-group' style="padding-top: 40px;">
			<label class="userpass col-sm-2 control-label" for="land"
				style="margin-top: -10px;">Land percentage</label>
			<div class="col-sm-2">
				<input type="text" name='land' data-slider="true" value="60"
					data-slider-range="10,100" data-slider-step="1" id='land'
					data-slider-highlight="true" data-slider-theme="volume"
					class="form-control">
			</div>
			<label style="margin-left: 190px;" for='land userpass control-label'
				class='land-output'>20</label>%
		</div>



		<div class='form-group' style="padding-top: 40px;">
			<label class="userpass col-sm-2 control-label" for="mountain"
				style="margin-top: -10px;">Mountain percentage </label>
			<div class="col-sm-2">
				<input type="number" name="mountain" data-slider="true" value="10"
					data-slider-range="1,50" data-slider-step="1" id="mountain"
					data-slider-highlight="true" data-slider-theme="volume"
					class="form-control">
			</div>
			<label style="margin-left: 190px;"
				for='mountain userpass control-label' class='mountain-output'>10</label>%
		</div>



		<div class='form-group' style="padding-top: 40px;">
			<label class="userpass col-sm-2 control-label" for="mountain"
				style="margin-top: -10px;">Resource percentage </label>
			<div class="col-sm-2">
				<input type="number" name="resource" data-slider="true" value="10"
					data-slider-range="1,20" data-slider-step="1" id="resource"
					data-slider-highlight="true" data-slider-theme="volume"
					class="form-control">
			</div>
			<label style="margin-left: 190px;"
				for='resource userpass control-label' class='resource-output'>10</label>%
		</div>
		

		<div class='form-group' style="padding-top: 40px;">
		<label class="userpass col-sm-2 control-label"
				style="margin-top: -10px;">Additional </label>
			<div class="btn-group" style="margin-top:-10px; margin-left:20px;" data-toggle="buttons">
				<label class="btn btn-primary" id="lakes"> <input type="checkbox">
					Lakes
				</label> <label id="rivers" class="btn btn-primary" style="margin-left: 10px;"> <input type="checkbox">
					Rivers
				</label>
			</div>
		</div>
		
			<div class='form-group' style="padding-top: 40px;">
			<label class="userpass col-sm-2 control-label" for="water"
				style="margin-top: -10px;">Island water percentage</label>
			<div class="col-sm-2">
				<input type="text" name='water' data-slider="true" value="20"
					data-slider-range="1,30" data-slider-step="1" id='water'
					data-slider-highlight="true" data-slider-theme="volume"
					class="form-control">
			</div>
			<label style="margin-left: 190px;" for='water userpass control-label'
				class='water-output'>20</label>%
		</div>
		
	</fieldset>
	<fieldset class="fieldsetpadding">
		<legend class=" userpass">Game Type</legend>
		<div class="form-group" style="margin-bottom: 40px;">

			<div class='form-group' style="padding-top: 40px;">
				<label class="userpass col-sm-2 control-label" for="inputaction"
					style="margin-top: -10px;">Action per turn </label>
				<div class="col-sm-2">
					<input type="text" name="actionpoints" data-slider="true"
						value="50" data-slider-range="10,100" data-slider-step="10"
						id="inputaction" data-slider-highlight="true"
						data-slider-theme="volume" class="form-control">
				</div>
				<label style="margin-left: 190px;"
					for='inputaction userpass control-label' class='inputaction-output'>50</label>
			</div>

			<div class='form-group' style="padding-top: 40px;">
				<label class="userpass col-sm-2 control-label" for="inputenergy"
					style="margin-top: -10px;">Starting Energy </label>
				<div class="col-sm-2">
					<input type="number" name="energy" data-slider="true" value="100"
						data-slider-range="50,300" data-slider-step="10"
						id="inputheight" data-slider-highlight="true"
						data-slider-theme="volume" class="form-control">
				</div>
				<label style="margin-left: 190px;"
					for='inputheight userpass control-label' class='inputheight-output'>100</label>
			</div>
		</div>

		<div class='friends form-group col-sm-6'>
			<div class='input-group col-sm-6'
				style="margin-left: 46px; margin-top: -17px;">
				<p style="color: rgba(255, 255, 255, 0.89); font-weight: 700;">Search
					Players</p>
				<input type='text' class='form-control' id='player'
					autocomplete='off' style="height: 40px;">
			</div>

			<ul class='friend-list list-group palyer-list'>
			</ul>

		</div>


		<div class='friends form-group col-sm-6'
			style="padding-top: -20px; border-color: rgba(30, 197, 30, 0.89);">
			<p
				style="color: rgba(255, 255, 255, 0.89); font-weight: 700; margin-top: -15px; padding-left: 100px;">Invited
				Player list</p>
			<ul id="invited-list-advanced" class='invited-list list-group'
				style="cursor: move; margin-top: -30px !important; min-height: 100px;">
				<li class="list-group-item li-disabled"
					style="cursor: default; border-color: rgba(30, 197, 30, 0.89);"><%=session.getAttribute("username")%></li>
			</ul>
		</div>

	</fieldset>
	<fieldset id="players"></fieldset>
	<div class="col-sm-offset-2 col-sm-10"
		style="margin-left: 470px; margin-top: -100px;">
		<a href="javascript:void(0);" class="a-btn" id="submit"> <span
			class="a-fixed-btn-text"> New Game</span> <span
			class="a-btn-slide-text">Start</span> <span class="a-btn-icon-right"><span></span></span>
		</a>
	</div>
</form>