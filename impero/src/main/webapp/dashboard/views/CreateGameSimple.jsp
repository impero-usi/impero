<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<form class="form-horizontal" id="creategameformSimple">
	<div class="form-group"></div>
	<fieldset class="fieldsetpadding">
		<legend class="userpass" style="padding-top: 20px;">Game Type</legend>
		<!-- <label for="gameOptions" class="userpass col-sm-2 control-label" style="padding-top: 20px;">Preset</label> -->
		<!-- <select class="form-control col-sm-5" id="gameOptions" style="margin-top: 7px; height:3em;">
		</select> -->

		<div class="btn-group" data-toggle="buttons"
			style="margin-left: 50px;" id="simpleType">


			<label class="btn btn-primary preset"> <input type="radio"
				value="0"> <i class="map-img fa fa-life-ring fa-4x "></i>
			</label> <label class="btn btn-primary preset"> <input type="radio"
				value="1"> <i class="map-img fa fa-ra fa-4x"></i>
			</label> <label class="btn btn-primary preset active"> <input
				type="radio" value="2"> <i
				class="map-img fa fa-empire fa-4x"></i>

			</label>
		</div>




		<legend class="userpass" style="padding-top: 20px;">Map Size</legend>
		<div class="btn-group" data-toggle="buttons"
			style="margin-left: 50px;" id="simpleSize">


			<label class="btn btn-primary preset"> <input type="radio"
				value="0"><img src='img/islandsize/small.jpg' alt="small">
			</label> <label class="btn btn-primary preset active"> <input
				type="radio" value="1"> <img src='img/islandsize/medium.jpg'
				alt="medium">
			</label> <label class="btn btn-primary preset"> <input type="radio"
				value="2"><img src='img/islandsize/large.jpg' alt="large">
			</label>

		</div>

	</fieldset>
	<fieldset class="fieldsetpadding">
		<legend class="userpass">Players</legend>
		<div class='friends form-group col-sm-6'>
			<div class='input-group col-sm-6'
				style="margin-left: 46px; margin-top: -17px;">
				<p
					style="color: rgba(255, 255, 255, 0.89); font-weight: 700; padding-top: 20px;"">Search
					Players</p>
				<input type='text' class='form-control' id='player'
					autocomplete='off' style="height: 40px;">
			</div>

			<ul class='friend-list list-group palyer-list'
				style="cursor: move; min-height: 100px;">
			</ul>
		</div>
		<div class='friends form-group col-sm-6'
			style="padding-top: -20px; border-color: rgba(30, 197, 30, 0.89);">

			<p
				style="color: rgba(255, 255, 255, 0.89); font-weight: 700; margin-top: -15px; padding-left: 100px; padding-top: 20px;">
				Invited Player list</p>
			<ul id="invited-list-simple" class='invited-list list-group'
				style="cursor: move; margin-top: -30px !important; min-height: 100px;">
				<li class="list-group-item li-disabled"
					style="cursor: default; border-color: rgba(30, 197, 30, 0.89);"><%=session.getAttribute("username")%></li>
			</ul>
		</div>
	</fieldset>
	<fieldset id="players"></fieldset>
	<div class="col-sm-offset-2 col-sm-10"
		style="margin-left: 470px; margin-top: -100px;">
		<a href="javascript:void(0);" class="a-btn" id="submitSimple"> <span
			class="a-fixed-btn-text"> New Game</span> <span
			class="a-btn-slide-text">Start</span> <span class="a-btn-icon-right"><span></span></span>
		</a>
	</div>
</form>

<script type="text/javascript">
	var types;
	$.getJSON("dashboard/js/gameTypes.json", function(data) {
		types = data;
		$.each(data.types, function(index, params) {
			$('#gameOptions').append(new Option(params.name, index));
		});
	});
</script>

