<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.net.URLEncoder" %>
<%
	String publicAppId = "101328723315523";
	String scope = "basic_info,email";
	String contextPath = request.getContextPath();
	String port = Integer.toString(request.getServerPort());
	String redirectURL = URLEncoder.encode("http://localhost:"+port + contextPath + "/LoginServlet", "UTF-8");
    String fbURL; //= "http://www.facebook.com/dialog/oauth?client_id=myfacebookappid&redirect_uri=" + URLEncoder.encode("http://myappengineappid.appspot.com/signin_fb.do") + "&scope=email";
	fbURL = "http://www.facebook.com/dialog/oauth?client_id=" + publicAppId + "&redirect_uri=" + redirectURL + "&scope=" + scope;
%>
<!DOCTYPE html>
<html>
<head>
<title>Log in</title>
<%@include file="views/includes.jsp"%>
</head>

<body style="font-family: 'Distress';">
	<%@include file="views/header.jsp"%>

	<div class="jumbotron navBackgroud navb login">
		<div class="container">
			<h2 class="userpass">Log in</h2>
			<hr>
			<form class="form-horizontal" action="LoginServlet" method="post" id="login">
				<div class="form-group">
					<label for="inputmail" class="col-sm-2 control-label userpass">Username</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputmail"
							name="username" placeholder="name" autofocus>
					</div>
				</div>
				<div class="form-group">
					<label for="inputpass" class="col-sm-2 control-label userpass">Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="inputpass"
							placeholder="*****" name="password">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a class="a-btn" id="submit" href="javascript:void(0);"> 
						<span class="a-fixed-btn-text">Log in</span>
						<span class="a-btn-slide-text">Enter</span>
						 <span class="a-btn-icon-right"><span></span></span>
						</a>
					</div>
				</div>
			</form>
			<hr>
			<!--<h2 class="userpass">Log in with </h2>
			<hr>
			<div class="btn-group center">
			<div style="float: left;text-align: center; padding-right:15px; ">
			<a class="a-btn" href="<%= fbURL %>"> 
						<span class="a-fixed-btn-text"><img src="img/FB.jpg" alt="FB" height="30" width="30"></span>
						<span class="a-btn-slide-text">Log in</span>
						 <span class="a-btn-icon-right"><span></span></span>
						</a>
						</div>
						
					<div style="float: left;">
			<a class="a-btn" href="javascript:void(0);"> 
						<span class="a-fixed-btn-text"><img src="img/Twitter_bird_logo.jpg" alt="FB" height="30" width="30"></span>
						<span class="a-btn-slide-text">Log in</span>
						 <span class="a-btn-icon-right"><span></span></span>
						</a>
						</div>	
						
						
								<div style="float: left;padding-left:15px;">
			<a class="a-btn" href="javascript:void(0);"> 
						<span class="a-fixed-btn-text"><img src="img/g+.jpg" alt="FB" height="30" width="30"></span>
						<span class="a-btn-slide-text">Log in</span>
						 <span class="a-btn-icon-right"><span></span></span>
						</a>
						</div>	
				
						
			
		

		</div> -->
	</div>


	<%@include file="views/footer.jsp"%>
	<script>
		$("#submit").click(function () {
			$('#login').submit();
		});
		


		$("input").keypress(function(event) {
		    if (event.which == 13) {
		        event.preventDefault();
		        $("#login").submit();
		    }
		});


	</script>
</body>
</html>