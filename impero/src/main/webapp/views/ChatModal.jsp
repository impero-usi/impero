<%@page import="ch.usi.inf.sa4.impero.impero.db.MongoPlayer"%>
<%@page import="ch.usi.inf.sa4.impero.impero.db.MessagePOJO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="ch.usi.inf.sa4.impero.impero.db.MongoChat"%>

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">&times;</button>
		<ul class="modal-title nav nav-tabs" id="messageTab">
			<li class="active"><a href="#inbox" data-toggle="tab">Inbox</a></li>
			<li><a href="#outbox" data-toggle="tab">Outbox</a></li>
			<li><a href="#send" data-toggle="tab">Send</a></li>
		</ul>
	</div>
	<div class="modal-body" style="font-family: 'Helvetica';">
		<div class="tab-content">
			<div class="tab-pane active" id="inbox">
				<ul class='media-list'>
					<%
						for (MessagePOJO m : MongoChat.getPlayerMessages(request
								.getSession().getAttribute("id").toString(), "1", true)) {
					%>
					<li class="media"><a class="pull-left" href="#"> <img
							class="media-object"
							src="http://www.gravatar.com/avatar/<%=MongoPlayer.getMailHashById(m.getFrom())%>?s=40&d=retro"
							alt="profile">
					</a>
						<div class="media-body">
							<h4 class="media-heading">
								<i class="fa fa-envelope-o fa-fw"></i>
								<%=MongoPlayer.getPlayerByID(m.getFrom())%></h4>
							<%=m.getMessage()%>
						</div></li>
					<%
						}
					%>
				</ul>
			</div>
			<div class="tab-pane" id="outbox">
				<ul class='media-list' style="font-family: 'Helvetica';">
					<%
						for (MessagePOJO m : MongoChat.getPlayerMessages(request
								.getSession().getAttribute("id").toString(), "1", false)) {
					%>
					<li class="media"><a class="pull-left" href="#"> <img
							class="media-object"
							src="http://www.gravatar.com/avatar/<%=MongoPlayer.getMailHashById(m.getTo())%>?s=40&d=retro"
							alt="profile">
					</a>
						<div class="media-body">
							<h4 class="media-heading">
								<i class="fa fa-envelope-o fa-fw"></i>
								<%=MongoPlayer.getPlayerByID(m.getTo())%></h4>
							<%=m.getMessage()%>
						</div></li>
					<%
						}
					%>
				</ul>
			</div>
			<div class="tab-pane" id="send">
				<form class="form-horizontal">
					<div class="form-group">
						<label for="player" class="col-sm-2 control-label">User</label>
						<div class="col-sm-10">
							<input type="text" class="form-control pull-right" id="player"
								placeholder="name" data-target="autocomplete">
						</div>
					</div>
					<div class="form-group">
						<div>
							<textarea class="form-control" rows="3"
								style="resize: none; width: 100%"></textarea>
						</div>
					</div>
					<div class="form-group">
						<button class='btn btn-default' id='sendMessage'>
							Send <i class="fa fa-caret-square-o-right "></i>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</div>