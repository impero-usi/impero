<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>

<%@include file="ChatModal.html" %>

<nav role="navigation" class="navBackgroud navb">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<a class="navbar-brand font" href="index.jsp">IMPERO</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<%
				if (session.getAttribute("username") != null) {
			%>
			<ul class="nav navbar-nav font">
				<li><a data-toggle="modal" data-target="#chatModal" style="background-color:rgba(0, 0, 0, 0.51);" class='btn chat' href="views/ChatModal.jsp">Chat</a></li>
			</ul>
			<%
				}
			%>
			<div class="nav navbar-nav navbar-right">
				<%
					if (session.getAttribute("username") == null) {
				%>
				<div class="btn-group" style="font-family: 'Distress';">
					<div class="button_right">
						<a class="a-btn" href="signup.jsp"> <span
							class="a-fixed-btn-text">Register now</span> <span
							class="a-btn-slide-text">Sign up</span> <span
							class="a-btn-icon-right"><span></span></span>
						</a>
					</div>
					<div class="button_margin">
						<a class="a-btn" href="login.jsp"> <span
							class="a-fixed-btn-text">Enter</span> <span
							class="a-btn-slide-text">Sign in</span> <span
							class="a-btn-icon-right"><span></span></span>
						</a>
					</div>
				</div>
				<%
					} else {
				%>
				<div>
					<div style="float: left; text-align: center; padding-top: 10px;">
					</div>
					<div style="float: left;">
						<a href="/LogoutServlet" class="a-btn"> <span
							class="a-fixed-btn-text"><%=session.getAttribute("username")%></span>
							<span class="a-btn-slide-text logout">Logout</span> <span
							class="a-btn-icon-right"><span></span></span>
						</a>
					</div>
					<%
						}
					%>
				</div>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</div>
</nav>