<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!-- 
    

 ________  _________ ___________ _____ 
|_   _|  \/  || ___ \  ___| ___ \  _  |
  | | | .  . || |_/ / |__ | |_/ / | | |
  | | | |\/| ||  __/|  __||    /| | | |
 _| |_| |  | || |   | |___| |\ \\ \_/ /
 \___/\_|  |_/\_|   \____/\_| \_|\___/ 
                                       
                                       

    
     -->
<base href="${pageContext.request.contextPath}/" />
<meta charset="UTF-8">
<link href="/img/favicon.ico" rel="icon" type="image/x-icon" />
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/welcome.css">
<link rel="stylesheet" href="css/button.css">
<link
	href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Flamenco:300,400'
	rel='stylesheet' type='text/css'>
	
	

<!-- <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->


<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<script src="dashboard/simple-slider.js"></script>
<script src="js/libs/typeahead/typeahead.bundle.js"></script>

<link href="css/simple-slider.css" rel="stylesheet"
	type="text/css" />
<link href="css/simple-slider-volume.css" rel="stylesheet"
	type="text/css" />