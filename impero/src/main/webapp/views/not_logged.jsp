<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div class="impero-text navBackgroud navb login">
	<h1 style="font-size: 6em;">IMPERO</h1>
	<p class="imperoFont">Prepare to experience the battle of your life. You can
	<ul class="imperoFont" style="text-align:left;">
		<li>participate in an asynchronous, yet exciting game</li>
		<li>raise and maneuver a large-scale military exercise of troops, warships, and other forces</li>
		<li>exploit resources to gain power</li>
		<li>and much much more...</li>
	</ul>
</div>



<!-- <div class="impero-text navBackgroud navb login" >
	<h1 class="imperoFont btn" style="font-size: 6em;"><a href="/impero/signup.jsp">Sign up now</a></h1>
</div> -->
