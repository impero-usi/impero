<%@page import="java.util.Collection"%>
<%@page import="java.util.Map"%>
<%@page import="ch.usi.inf.sa4.impero.impero.db.MongoGames"%>
<%@page import="ch.usi.inf.sa4.impero.impero.db.FriendEntryPOJO"%>
<%@page import="ch.usi.inf.sa4.impero.impero.db.PlayerEntryPOJO"%>
<%@page import="ch.usi.inf.sa4.impero.impero.db.MongoFriend"%>
<%@page import="ch.usi.inf.sa4.impero.impero.logic.player.Player"%>
<%@page import="ch.usi.inf.sa4.impero.impero.db.MongoPlayer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.net.URLEncoder"%>
<%
	String title;
	String playerID = MongoPlayer.getPlayerByName(request
			.getParameter("name"));
	if (playerID != null) {
		title = request.getParameter("name");
	} else {
		title = "Not found";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title><%=title%></title>
<%@include file="/views/includes.jsp"%>
<script src="js/libs/amcharts/amcharts.js" type="text/javascript"></script>
<script src="js/libs/amcharts/pie.js" type="text/javascript"></script>
<script src="js/libs/amcharts/serial.js" type="text/javascript"></script>
<script src="js/libs/amcharts/themes/black.js" type="text/javascript"></script>
<style>
.profile {
	border-radius: 20px;
}
</style>
</head>

<body style="font-family: 'Distress';">
	<%@include file="/views/header.jsp"%>
	<div id="container" class="jumbotron navBackgroud navb login">
		<div class="row">
			<div class="col-md-offset-1 col-md-4">
				<img class="profile"
					src="http://www.gravatar.com/avatar/<%=MongoPlayer.getMailHashById(playerID)%>?s=250&d=retro">
				<div id="friends">
					<h3>Statistics</h3>
						<div id="chartdiv" style="width: 100%; height: 300px; background-color: transparent;" ></div>
					<h3>Friends</h3>
					<ul class="list-group">
						<%
							for (FriendEntryPOJO p : MongoFriend.getFriends(playerID)) {
								out.print("<li class='list-group-item'>");
								out.print("<a href='/profile/index.jsp?name=" + p.getName()
										+ "'>" + p.getName() + "</a>");
								out.print("</li>");
							}
						%>
					</ul>
				</div>
			</div>
			<div class="col-md-7">
				<h1 style="color: white;" player-id="<%=playerID %>">
					<%=title%></h1>

				<div id="games">
				<h3>Games <small style="float:right">Click to view statistics</small></h3>
					<ul class="list-group">
						<%
							for (Map.Entry<String, String> game : MongoGames
									.getGameListByPlayerID(playerID).entrySet()) {
								out.print("<li class='list-group-item'>");
								out.print("<a href='/dashboard/statistics/game.jsp?id="
										+ game.getKey() + "'>");
								out.print(game.getValue());
								out.print("</a>");
								out.println("</li>");
							}
						%>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<%@include file="/views/footer.jsp"%>
	<script type="text/javascript" src="profile/profile.js"></script>
</body>
</html>