/**
 * @author Stefanos Gatsios
 */

var playerStatisticsData = {
	"type" : "pie",
	"pathToImages" : "js/libs/amcharts/images/",
	//"balloonText" : "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
	"innerRadius" : "40%",
	"minRadius": 95,
	"colors" : [ "#006D13", "#8F0A00","#848D00" ],
	"startDuration" : 1,
	"titleField" : "status",
	"valueField" : "ammount",
	"theme" : "black",
	"allLabels" : [],
	"balloon" : {},
	"legend" : {
		"align" : "center",
		"markerType" : "circle"
	},
	"titles" : [],
	"dataProvider" : [ {
		"status" : "won"
	}, {
		"status" : "defeated"
	} ]
};

$(function() {
	var playerID = $('[player-id]').attr('player-id');
	$.ajax({
		url : "/webresources/players/stats/" + playerID,
	}).success(function (ev) {
		console.dir(ev);
		
		playerStatisticsData['dataProvider'][0].ammount = ev.wonGames;
		playerStatisticsData['dataProvider'][1].ammount = ev.lostGames;
		AmCharts.makeChart("chartdiv",playerStatisticsData);
	});

});
