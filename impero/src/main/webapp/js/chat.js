/**
 * @author Stefanos Gatsios Chat implementation
 */

var Chat = {};
var connection;
var messageCount;

//Typeahead part
Chat.names = new Bloodhound({
	name : 'names',
	datumTokenizer : function(d) {
		return Bloodhound.tokenizers.whitespace(d.name);
	},
	limit : 4,
	queryTokenizer : Bloodhound.tokenizers.whitespace,
	/* remote : {
		url : '/webresources/players?prefix=%QUERY'
	}, */
	prefetch : '/webresources/players'
});

$('#chatModal').on('loaded.bs.modal', function(e) {
	$('button#sendMessage').click(function(e) {
		e.preventDefault();
		var form = $('#chatModal form');
		var a = $.ajax({
			type : 'GET',
			url : '/webresources/player?name=' + form.find('#player').val()
		});
		a.success(function() {
			if (form.find('#player').val() == "") {
				return;
			} else {
				ret = {};
				ret['to'] = a.responseText;
				if(a.responseText == "") {
					form.get(0).reset();
					return;
				}
				ret['message'] = form.find('textarea').val();
				$.ajax({
					type : "POST",
					url : '/webresources/message/',
					data : JSON.stringify(ret),
					contentType : 'application/json'
				}).then(function (e){
					console.log(JSON.stringify({session : ret['to'], message : ret['message'] }));
					connection.send(JSON.stringify({session : ret['to'], message : ret['message'] }));
				});
			}
		});
		a.then(function() {
			form.get(0).reset();
		});
	});
	// /////////////////
	$('#messageTab a').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
	});
	// /////////////////
	$(function() {
		Chat.names.initialize();
		$('#chatModal [data-target="autocomplete"]').typeahead(null, {
			name : "names",
			displayKey : 'name',
			source : Chat.names.ttAdapter()
		});
	});

});




$(document).ready(function() {
	var where = document.location.host;
	connection = new WebSocket('ws:/' + where +'/chat');
	window.onbeforeunload = function() {
	    connection.onclose = function () {}; // disable onclose handler first
	    connection.close();
	};
	connection.onmessage = function(e) {
		var jsonData = JSON.parse(e.data);
		//console.log("Data received : " + e.data);
		//console.dir(jsonData);
		if(typeof jsonData.message == 'undefined') {
			messageCount = e.data;
		} else {
			messageCount++;
			$('.btn.chat').popover({title : jsonData.from , content : jsonData.message, animation : { hide :100 , show :100}}).popover('show');
			window.setTimeout(function() {$('.btn.chat').popover('destroy');}, 10000);
		}
		$('.btn.chat').children().remove();
		$('.btn.chat').append(" ").append($(" <i class='fa fa-envelope'>").append(" " + messageCount));
	};
	
	connection.onopen = function(e) {

	};
});

/*
 * function fetchInbox() { var ajaxList = Array(); var ret = {}; $.ajax({ url :
 * '/webresources/message/inbox', contentType : "application/json"
 * }).success(function (data) { $.each(data,function(index,data) {
 * ajaxList.push($.ajax({ url : "/webresources/player?id=" + data.from }));
 * ajaxList.push($.ajax({ url : "/webresources/player?id=" + data.to })); });
 * }); var defer = $.when.apply($,ajaxList); defer.then(function() {
 * $.each(arguments,function(index,response) { console.dir(response); }); }); };
 */