<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Sign up</title>
<%@include file="views/includes.jsp"%>
</head>
<body style="font-family: 'Distress';">
	<%@include file="views/header.jsp"%>
	<%
		String error = request.getParameter("error");
		if (error == null) {
			error = "";
		}
	%>
	<div class="jumbotron navBackgroud navb login">
		<div class="container">
			<h2 class="userpass">Sign up</h2>
			<hr>
			<form class="form-horizontal" action="SignUpServlet" method="post"
				id="signupform">
				<div
					class="form-group <%=(error.contains("username") ? "has-error" : "")%>">
					<label for="inputname" class="col-sm-2 control-label  userpass">Username</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputname"
							name="username" placeholder="name" autofocus>
					</div>
				</div>
				<div
					class="form-group <%=(error.contains("email") ? "has-error" : "")%>">
					<label for="inputmail" class="col-sm-2 control-label  userpass">Email</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="inputmail"
							name="email" placeholder="email">
					</div>
				</div>
				<div
					class="form-group <%=(error.contains("password") ? "has-error" : "")%>">
					<label for="inputpass" class="col-sm-2 control-label  userpass">Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="inputpass"
							placeholder="*****" name="password">
					</div>
				</div>
				<div class="form-group">
					<label for="inputpass2" class="col-sm-2 control-label  userpass">Password
						confirmation</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="inputpass2"
							placeholder="*****" name="password2">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<!-- <input type="submit" class="btn btn-default" name="signup"
							value="Join us"> -->
						<a class="a-btn" id="submit" href="javascript:void(0);"> <span class="a-fixed-btn-text">Join
								us</span> <span class="a-btn-slide-text">Submit</span> <span
							class="a-btn-icon-right"><span></span></span>
						</a>
					</div>
				</div>
			</form>
		</div>
	</div>


	<%@include file="views/footer.jsp"%>
	<!-- After jquery include rules for validation -->
	<script
		src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
	<script>
		$(function() {
			$('#signupform').validate({
				debug : false,
				highlight : function(element,errorClass) {
					var label = $(element);
					label.parent().parent().removeClass('has-success').addClass("has-error");
				},
				unhighlight : function(element,errorClass) {
					var label = $(element);
					label.parent().parent().removeClass('has-error').addClass('has-success');
				},
				rules : {
					username : {
						required : true,
						minlength : 6
					},
					email : {
						email : true,
						required : true
					},
					password : {
						required : true,
						minlength : 8
					},
					password2 : {
						required : true,
						equalTo : '#inputpass'
					}
				},
				messages : {
					username : {
						requried : "Name is required",
						minlength : "Please enter more than {0} digits"
					},
					email : {
						required : "Enter your email",
						email : "Please enter a valid email"
					},
					password : {
						requried : "Please enter your password",
						minlength : "Please enter more than 8 characters"
					},
					password2 : {
						required : "Please retype your password",
						equalTo : "Your two passwords must match"
					}
				},
				submitHandler : function(form) {
					form.submit();
				}
			});
		});

		$("#submit").click(function() {
			$('#signupform').submit();
		});
	</script>
</body>
</html>