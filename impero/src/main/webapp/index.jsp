<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>Welcome Page</title>
	<%@include file="views/includes.jsp"%>
</head>
<body  style="font-family:'Distress';">
	<%@include file="views/header.jsp"%>

	<%
		if (session.getAttribute("username") == null) {
	%>
	<%@include file="views/not_logged.jsp"%>
	<%
		} else {
			response.sendRedirect("dashboard/");
		}
	%>

	<%-- <%@include file="views/footer.jsp"%> --%>
</body>
</html>