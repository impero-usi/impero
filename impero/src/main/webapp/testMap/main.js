var stage,
    playgroundContainer,
    UIContainer,
    canvas;

var MAP_WIDTH = 15,
    MAP_HEIGHT = 7,
    MAP_START_X = 60,
    MAP_START_Y = 60,
    HEXAGON_SIZE = 20,
    MAP_HEX_WIDTH = 3 / 4 * 2 * HEXAGON_SIZE,
    MAP_HEX_HEIGHT = Math.sqrt(3) / 2 * 2 * HEXAGON_SIZE,
    MAP_IDX = 1,
    ENTITY_IDX = 2,
    CANVAS_WIDTH = 1620,
    CANVAS_HEIGTH = 800;

var gameURL = 'localhost:8888/webresources/game/'

var map = {};
var currentPlayer = {
    name: 'Matteo',
    id: '74737',
    color: 'purple'
};
var state = 'viewing';


function init() {
    console.log('Loading...');
    
    canvas = document.getElementById('playground');
    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGTH;
    
    stage = new createjs.Stage('playground');
    
    playgroundContainer = new createjs.Container();
    UIContainer = new createjs.Container();
    
    $.getJSON('http://localhost:8888/webresources/game/123123', null, function(data, textStatus, jqXHR) {
        console.log(data.map);
        map = data.map;
        displayMap(map);
        Entity(60, 60, 15, 0, 0);
    
        stage.addChild(playgroundContainer);
        stage.addChild(UIContainer);

        ZoomAndPan();

        stage.update();
    });

    
}

function Map(width, height) {
    for (var row = 0; row < height; row++) {
        for (var col = 0; col < width; col++) {
            var offset = (col % 2 == 0) ? 0 : 1 / 2 * MAP_HEX_HEIGHT;
            var x = MAP_START_X + col * MAP_HEX_WIDTH;
            var y = MAP_START_Y + row * MAP_HEX_HEIGHT + offset;
            new Hexagon(x, y, HEXAGON_SIZE, row, col, type);
        }
    }
}

function displayMap(json) {
    for (var cell = 0; cell < json.length; cell++) {
        //console.log(json[cell]);
        var row = json[cell].x;
        var col = json[cell].y;
        var type  = json[cell].type;
        var offset = (col % 2 == 0) ? 0 : 1 / 2 * MAP_HEX_HEIGHT;
        var x = MAP_START_X + col * MAP_HEX_WIDTH;
        var y = MAP_START_Y + row * MAP_HEX_HEIGHT + offset;
        new Hexagon(x, y, HEXAGON_SIZE, row, col, type);
    }
}

function Hexagon(x, y, size, row, col, type) {
    var hexagon = new createjs.Shape();
    
    hexagon.row = row;
    hexagon.col = col;
    hexagon.type = type;
    hexagon.on('mousedown', function() {
        console.log(this.id, this.row, this.col, this.type);
    });

    switch (type) {
        case 0:
            // deep water
            hexagon.graphics.beginStroke("#aaa").beginFill("rgba(0,0,128,1)");
            break;
        case 1:
            // shallow water
            hexagon.graphics.beginStroke("#aaa").beginFill("rgba(70,130,180,1)");
            break;
        case 2:
            // sand
            hexagon.graphics.beginStroke("#aaa").beginFill("rgba(245,222,179,1)");
            break;
        case 3:
            // field
            hexagon.graphics.beginStroke("#aaa").beginFill("rgba(154,205,50,1)");
            break;
        case 4:
            // forest
            hexagon.graphics.beginStroke("#aaa").beginFill("rgba(34,149,34,1)");
            break;
        case 5:
            // mountain
            hexagon.graphics.beginStroke("#aaa").beginFill("rgba(112,128,144,1)");
            break;
        case 6:
            // energy source
            hexagon.graphics.beginStroke("#aaa").beginFill("rgba(255,215,0,1)");
            break;
        default:
            // water
            hexagon.graphics.beginStroke("#aaa").beginFill("#fff");
            break;
    }

    hexagon.graphics.drawPolyStar(x, y, size, 6, 0, 60);

    playgroundContainer.addChild(hexagon);
}

function Entity(x, y, size, row, col) {
    var entity = new createjs.Shape();
    entity.graphics.beginStroke("#FFE11A").beginFill("#FFE11A").drawPolyStar(x, y, size, 3, 0, 30);
    entity.row = row;
    entity.col = col;
    entity.on('click', function() {
        console.log(this.id, row, col);
        var entity_state;
        if (state == 'viewing') {
            state = 'interacting';
            entity.filters = [
                new createjs.ColorFilter(0, 0, 0, 1, 0, 0, 255, 0)
            ];
            entity.cache(0, 0, 100, 100);
            var neighbor = playgroundContainer.getChildAt(this.row + 1);
            neighbor.filters = [
                new createjs.ColorFilter(0, 0, 0, .2, 0, 0, 255, 0)
            ];
            neighbor.cache(0, 50, 110, 100);
        } else {
            state = 'viewing';
            entity.filters = [];
            entity.cache(0, 0, 100, 100);
            var neighbor = playgroundContainer.getChildAt(this.row + 1);
            neighbor.filters = [];
            neighbor.cache(0, 50, 110, 100);
        }
        stage.update();
    });
    playgroundContainer.addChild(entity);
}

function ZoomAndPan() {
    var zoom;
    canvas.addEventListener("mousewheel", function(e) {
        if (Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))) > 0) {
            zoom = 1.1;
        } else {
            zoom = 1 / 1.1;
        }
        var new_point = new createjs.Point(stage.regX + parseInt((stage.mouseX - stage.regX) / 2), stage.regY + parseInt((stage.mouseY - stage.regY) / 2));
        stage.x = stage.regX = new_point.x;
        stage.y = stage.regY = new_point.y;
        stage.scaleX = stage.scaleY *= zoom;
        if (stage.scaleX < 0.5) {
            stage.scaleX = stage.scaleY = 0.500000000;
        } else if (stage.scaleX > 5.0) {
            stage.scaleX = stage.scaleY = 5.000000000;
        }
        stage.update();
    }, false);
    stage.addEventListener("stagemousedown", function(e) {
        var offset = {
            x: playgroundContainer.x - e.stageX,
            y: playgroundContainer.y - e.stageY
        };
        stage.addEventListener("stagemousemove", function(ev) {
            console.log(ev.stageX, ev.stageY);
            playgroundContainer.x = ev.stageX + offset.x;
            playgroundContainer.y = ev.stageY + offset.y;
            stage.update();
        });
        stage.addEventListener("stagemouseup", function() {
            stage.removeAllEventListeners("stagemousemove");
        });
    });
}

function activateListenerOnCells() {}
$(init);