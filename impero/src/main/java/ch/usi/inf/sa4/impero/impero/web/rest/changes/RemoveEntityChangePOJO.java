package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

public class RemoveEntityChangePOJO {
	private CoordinatePOJO cell;
	private String owner;
	
	public CoordinatePOJO getCell() {
		return cell;
	}
	public void setCell(CoordinatePOJO cell) {
		this.cell = cell;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	

}
