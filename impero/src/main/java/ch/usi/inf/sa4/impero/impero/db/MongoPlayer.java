package ch.usi.inf.sa4.impero.impero.db;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * 
 * @author Kasim Bordogna
 * 
 *         Class to make a player persistent and to load a game from the
 *         databases
 */

public class MongoPlayer {
	private static DBCollection players;

	static {
		MongoPlayer.players = MongoConnection.getDB().getCollection("user");
	}

	public static List<PlayerEntryPOJO> getPlayers(String prefix) {
		List<PlayerEntryPOJO> ret = new ArrayList<PlayerEntryPOJO>();
		BasicDBObject q = new BasicDBObject();
		q.put("username", java.util.regex.Pattern.compile(prefix));
		DBCursor pList = MongoPlayer.players.find(q);
		while (pList.hasNext()) {
			DBObject current = pList.next();
			PlayerEntryPOJO entry = new PlayerEntryPOJO();
			entry.setId(current.get("_id").toString());
			entry.setName(current.get("username").toString());
			ret.add(entry);
		}
		return ret;
	}

	public static PlayerEntryPOJO getPlayer(String playerID) {
		PlayerEntryPOJO entry = new PlayerEntryPOJO();
		BasicDBObject query = new BasicDBObject("_id",
				ObjectId.massageToObjectId(playerID));
		BasicDBObject keys = new BasicDBObject("username", 1);
		DBCursor cur = MongoPlayer.players.find(query, keys);

		if (cur.hasNext()) {
			DBObject curPlayer = cur.next();
			entry.setId(curPlayer.get("_id").toString());
			entry.setName(curPlayer.get("username").toString());
		}
		return entry;
	}

	public static boolean isPlayer(String playerID) {
		BasicDBObject query = new BasicDBObject("_id",
				ObjectId.massageToObjectId(playerID));
		BasicDBObject keys = new BasicDBObject("username", 1);
		DBCursor cur = MongoPlayer.players.find(query, keys);
		if (cur.hasNext()) {
			return true;
		} else {
			return false;
		}
	}

	public static String getPlayerByName(String name) {
		BasicDBObject query = new BasicDBObject("username", name);
		BasicDBObject keys = new BasicDBObject("_id", 1);
		DBCursor cur = MongoPlayer.players.find(query, keys);
		if (cur.hasNext()) {
			return ((DBObject) cur.next()).get("_id").toString();
		} else {
			return null;
		}
	}

	public static String getPlayerByID(String id) {
		BasicDBObject query = new BasicDBObject("_id",
				ObjectId.massageToObjectId(id));
		BasicDBObject keys = new BasicDBObject("username", 1);
		DBCursor cur = MongoPlayer.players.find(query, keys);
		if (cur.hasNext()) {
			return ((DBObject) cur.next()).get("username").toString();
		} else {
			return null;
		}
	}

	public static String getMailHashById(String id) {
		BasicDBObject query = new BasicDBObject("_id",
				ObjectId.massageToObjectId(id));
		BasicDBObject keys = new BasicDBObject("email", 1);
		DBCursor cur = MongoPlayer.players.find(query, keys);
		if (cur.hasNext()) {
			return md5Hex(((DBObject) cur.next()).get("email").toString());
		} else {
			return null;
		}
	}

	private static String hex(byte[] array) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < array.length; ++i) {
			sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(
					1, 3));
		}
		return sb.toString();
	}

	private static String md5Hex(String message) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			return hex(md.digest(message.getBytes("CP1252")));
		} catch (NoSuchAlgorithmException e) {
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

	
	public static GameStatsPOJO getStats(String playerID){
		GameStatsPOJO entry = new GameStatsPOJO();
		
		BasicDBObject query = new BasicDBObject("_id", ObjectId.massageToObjectId(playerID));
		
		DBCursor cur = MongoPlayer.players.find(query);

		if (cur.hasNext()) {
			DBObject curPlayer = cur.next();
			entry.setWonGames(Integer.parseInt( curPlayer.get("wonGames") != null ? curPlayer.get("wonGames").toString() : "0" ));
			entry.setLostGames(Integer.parseInt( curPlayer.get("lostGames") != null ? curPlayer.get("lostGames").toString() : "0" ));
			entry.setTotalUnitsCreated(Integer.parseInt( curPlayer.get("totalUnitsCreated") !=null? curPlayer.get("totalUnitsCreated").toString():"0" ));
			entry.setTotalUnitDestroyed(Integer.parseInt( curPlayer.get("totalUnitsDestroyed") !=null? curPlayer.get("totalUnitsDestroyed").toString():"0" ));
			entry.setTotalUnitLost(Integer.parseInt( curPlayer.get("totalUnitsLost") !=null? curPlayer.get("totalUnitsLost").toString():"0" ));
			entry.setTotalEnergyGathered(Integer.parseInt( curPlayer.get("totalEnergyGathered") !=null? curPlayer.get("totalEnergyGathered").toString():"0" ));	
		}else{
			System.err.println("Player not found.");
		}
	return entry;
	}
	
	public static void updateStats(GameStatsPOJO stats, String playerID){
		
		int wonGames, lostGames, totalUnitsCreated, totalUnitsDestroyed, totalUnitsLost,totalEnergyGathered;
		
		BasicDBObject query = new BasicDBObject("_id",
				ObjectId.massageToObjectId(playerID));
		
		DBCursor cur = MongoPlayer.players.find(query);

		if (cur.hasNext()) {
			DBObject curPlayer = cur.next();
			wonGames = Integer.parseInt( curPlayer.get("wonGames") != null ? curPlayer.get("wonGames").toString() : "0" );
			lostGames = Integer.parseInt( curPlayer.get("lostGames") != null ? curPlayer.get("lostGames").toString() : "0" );
			totalUnitsCreated = Integer.parseInt( curPlayer.get("totalUnitsCreated") !=null? curPlayer.get("totalUnitsCreated").toString():"0" );
			totalUnitsDestroyed = Integer.parseInt( curPlayer.get("totalUnitsDestroyed") !=null? curPlayer.get("totalUnitsDestroyed").toString():"0" );
			totalUnitsLost = Integer.parseInt( curPlayer.get("totalUnitsLost") !=null? curPlayer.get("totalUnitsLost").toString():"0" );
			totalEnergyGathered = Integer.parseInt( curPlayer.get("totalEnergyGathered") !=null? curPlayer.get("totalEnergyGathered").toString():"0" );
			
			// Update stats bases on POJO
			if(stats.isWon()){
				wonGames++;
			}else{
				lostGames++;
			}
			
			totalUnitsCreated += stats.getTotalUnitsCreated();
			totalUnitsDestroyed += stats.getTotalUnitsDestroyed();
			totalUnitsLost += stats.getTotalUnitsDestroyed();
			totalEnergyGathered += stats.getTotalEnergyGathered();
			
			//Updates values
			curPlayer.put("wonGames", wonGames);
			curPlayer.put("lostGames", lostGames);
			curPlayer.put("totalUnitsCreated", totalUnitsCreated);
			curPlayer.put("totalUnitsLost", totalUnitsLost);
			curPlayer.put("totalUnitsDestroyed", totalUnitsDestroyed);
			curPlayer.put("totalEnergyGathered", totalEnergyGathered);
			
			//Updates mongo
			 MongoPlayer.players.update(query, curPlayer);
		}else{
			System.err.println("User not found");
		}
	}
}
