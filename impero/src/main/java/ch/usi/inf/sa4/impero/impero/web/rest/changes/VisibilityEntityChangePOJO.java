package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.web.rest.EntityPOJO;

public class VisibilityEntityChangePOJO {

	private  List<EntityPOJO> newVisibleEntities;
	private  List<EntityPOJO> removedVisibleEntities;
	public List<EntityPOJO> getNewVisibleEntities() {
		return newVisibleEntities;
	}
	public void setNewVisibleEntities(List<EntityPOJO> newVisibleEntities) {
		this.newVisibleEntities = newVisibleEntities;
	}
	public List<EntityPOJO> getRemovedVisibleEntities() {
		return removedVisibleEntities;
	}
	public void setRemovedVisibleEntities(List<EntityPOJO> removedVisibleEntities) {
		this.removedVisibleEntities = removedVisibleEntities;
	}
	
}
