package ch.usi.inf.sa4.impero.impero.logic.map.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractAirUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.map.Coordinate;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.mappers.MovementCostMapper;


/**
 * This class provide useful operations on an hexagonal board. The provided methods works with different inputs and provide different outputs. For example some
 * method can be called by passing the actual Hexagon objects, while other can be called by passing the coordinates (mostly used in MapGenerator class since
 * the Hexagon objects are not instanciated yet). The operations provided by this class are: get the neighbors at any level (direct neighbors or neighbors of the
 * surrounding neighbors and so on), get the possible cells where a unit can move and computing the shortest path between two cells.
 * 
 * @author Simone Raimondi & Luca Dotti
 *
 */
public class HexagonalStrategy implements BoardStrategy {
	//The current costs to the cells. Every time that the getAllCosts method is called, this field is updated. This field has been created because
	//clients need first all the possible cells that a unit can reach given an amount of action points and then the shortest path between the starting
	//cell and one of the computed cells. In this way Disjkra's algorithm only run once for each movement.
	private Map<Hexagon, Hexagon> currentPaths;
	
	@Override
	public Map<Integer, Hexagon> getNeighbours(final HexagonalMap boardReference, final int row, final int col) throws IndexOutOfBoundsException {
		if (row < 0 || row >= boardReference.getHeight() || col < 0 || col >= boardReference.getWidth()) {
			throw new IndexOutOfBoundsException();
		}
        else {
            if (col % 2 == 0) {
                return computeNeighbourEvenCol(boardReference, row, col);
            } else {
                return computeNeighbourOddCol(boardReference, row, col);
            }
        }
	}
	
	@Override
	public Map<Integer, Coordinate> getNeighboursCoordinates(final HexagonalMap boardReference, final int row, final int col) throws IndexOutOfBoundsException {
		if (row < 0 || row >= boardReference.getHeight() || col < 0 || col >= boardReference.getWidth()) {
			throw new IndexOutOfBoundsException();
		}
		else {
			if (col % 2 == 0) {
				return computeNeighbourEvenColCoordinates(boardReference.getWidth(), boardReference.getHeight(), row, col);
			} else {
				return computeNeighbourOddColCoordinates(boardReference.getWidth(), boardReference.getHeight(), row, col);
			}
		}
	}
	
	@Override
	public Map<Integer, Hexagon> getNeighbour(HexagonalMap boardReference, Hexagon cell) {
		final int mapWidth = boardReference.getWidth();
		final int mapHeight = boardReference.getHeight();
		
		for (int row = 0; row < mapHeight; row++) {
			for (int col = 0; col < mapWidth; col++) {
				if (boardReference.getCellAt(row, col).equals(cell)) {
					return getNeighbours(boardReference, row, col);
				}
			}
		}
		return null;
	}
	
	/**
     * Compute the neighbours for odd column.
     * 
     * @param boardReference the board to use to compute the neighbours
     * @param row the row of the cell you want to get the neighbours
     * @param col the column of the cell you want to get the neighbours
     * @return an hashmap with all the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
     */
    private Map<Integer, Hexagon> computeNeighbourOddCol(final HexagonalMap boardReference, final int row, final int col) {
    	
        Map<Integer, Hexagon> neighbours = new HashMap<>();
        final int colLength = boardReference.getHeight();
        final int rowLength = boardReference.getWidth();
        //Neighbour 0
        if (row - 1 >= 0) {
        	neighbours.put(0, boardReference.getCellAt(row - 1, col));
        }
        //Neighbour 1
        if (col < rowLength - 1) {
        	neighbours.put(1, boardReference.getCellAt(row, col + 1));
        }
        //Neighbour 2
        if (row < colLength - 1 && col < rowLength - 1) {
        	neighbours.put(2, boardReference.getCellAt(row + 1, col + 1));
        }
        //Neighbour 3
        if (row < colLength - 1) {
        	neighbours.put(3, boardReference.getCellAt(row + 1, col));
        }
        //Neighbour 4
        if (row < colLength - 1 && col - 1 >= 0) {
        	neighbours.put(4, boardReference.getCellAt(row + 1, col - 1));
        }
        //Neighbour 5
        if (col - 1 >= 0) {
        	neighbours.put(5, boardReference.getCellAt(row, col - 1));
        }

        return neighbours;
    }
    
    /**
     * Compute the neighbours for even column
     * @param boardReference the board to use to compute the neighbours
     * @param row the row of the cell you want to get the neighbours
     * @param col the column of the cell you want to get the neighbours
     * @return an hashmap with all the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
     */
    private Map<Integer, Hexagon> computeNeighbourEvenCol(final HexagonalMap boardReference, final int row, final int col) {
        Map<Integer, Hexagon> neighbours = new HashMap<>();
        final int colLength = boardReference.getHeight();
        final int rowLength = boardReference.getWidth();

        //Neighbour 0
        if (row - 1 >= 0) {
        	neighbours.put(0, boardReference.getCellAt(row - 1, col));
        }
        //Neighbour 1
        if (row - 1 >= 0 && col < rowLength - 1) {
        	neighbours.put(1, boardReference.getCellAt(row - 1, col + 1));
        }
        //Neighbour 2
        if (col < rowLength - 1) {
        	neighbours.put(2, boardReference.getCellAt(row, col + 1));
        }
        //Neighbour 3
        if (row < colLength - 1) {
        	neighbours.put(3, boardReference.getCellAt(row + 1, col));
        }
        //Neighbour 4
        if (col - 1 >= 0) {
        	neighbours.put(4, boardReference.getCellAt(row, col - 1));
        }
        //Neighbour 5
        if (row - 1 >= 0 && col - 1 >= 0) {
        	neighbours.put(5, boardReference.getCellAt(row - 1, col - 1));
        }

        return neighbours;
    }
    
    /**
     * Compute the neighbours coordinates for odd column.
     * 
     * @param width board's width
     * @param height board's height
     * @param row the row of the cell you want to get the neighbours
     * @param col the column of the cell you want to get the neighbours
     * @return an hashmap with all the coordinates of the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
     */
    static public Map<Integer, Coordinate> computeNeighbourOddColCoordinates(final int width, final int height, final int row, final int col) {
    	Map<Integer, Coordinate> neighbours = new HashMap<>();
    	final int colLength = height;
        final int rowLength = width;
        //Neighbour 0
        if (row - 1 >= 0) {
        	neighbours.put(0, new Coordinate(row - 1, col));
        }
        //Neighbour 1
        if (col < rowLength - 1) {
        	neighbours.put(1, new Coordinate(row, col + 1));
        }
        //Neighbour 2
        if (row < colLength - 1 && col < rowLength - 1) {
        	neighbours.put(2, new Coordinate(row + 1, col + 1));
        }
        //Neighbour 3
        if (row < colLength - 1) {
        	neighbours.put(3, new Coordinate(row + 1, col));
        }
        //Neighbour 4
        if (row < colLength - 1 && col - 1 >= 0) {
        	neighbours.put(4, new Coordinate(row + 1, col - 1));
        }
        //Neighbour 5
        if (col - 1 >= 0) {
        	neighbours.put(5, new Coordinate(row, col - 1));
        }

        return neighbours;
    }
    
    /**
     * Compute the neighbours coordinates for even column.
     * 
     * @param width board's width
     * @param height board's height
     * @param row the row of the cell you want to get the neighbours
     * @param col the column of the cell you want to get the neighbours
     * @return an hashmap with all the coordinates of the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
     */
    static public Map<Integer, Coordinate> computeNeighbourEvenColCoordinates(final int width, final int height, final int row, final int col) {
    	HashMap<Integer, Coordinate> neighbours = new HashMap<>();
    	final int colLength = height;
        final int rowLength = width;

        //Neighbour 0
        if (row - 1 >= 0) {
        	neighbours.put(0, new Coordinate(row - 1, col));
        }
        //Neighbour 1
        if (row - 1 >= 0 && col < colLength - 1) {
        	neighbours.put(1, new Coordinate(row - 1, col + 1));
        }
        //Neighbour 2
        if (col < rowLength - 1) {
        	neighbours.put(2, new Coordinate(row, col + 1));
        }
        //Neighbour 3
        if (row < colLength - 1) {
        	neighbours.put(3, new Coordinate(row + 1, col));
        }
        //Neighbour 4
        if (col - 1 >= 0) {
        	neighbours.put(4, new Coordinate(row, col - 1));
        }
        //Neighbour 5
        if (row - 1 >= 0 && col - 1 >= 0) {
        	neighbours.put(5, new Coordinate(row - 1, col - 1));
        }

        return neighbours;
    }

	@Override
	public List<Hexagon> getNeighboursInRange(final HexagonalMap boardReference, final int row, final int col, final int range) throws IndexOutOfBoundsException {
		if (row < 0 || row >= boardReference.getHeight() || col < 0 || col >= boardReference.getWidth()) {
			throw new IndexOutOfBoundsException();
		}
		else{
			//Convert from odd-q to cubic
			List<Integer> originCubic = convertOddqToCubic(row, col);
			//Create list of neighbours
			List<Hexagon> result = new ArrayList<Hexagon>();
			//Go through all the map to check if a cell is in the range
			for (List<Hexagon> rowList : boardReference.getMap()) {
				for (Hexagon hexagon : rowList) {
					//convert the current coordinate to cubic
					List<Integer> hexagonRow = convertOddqToCubic(hexagon.getRow(), hexagon.getCol());
					//compute the distances between the compontents of the given cell and the neighbor ones
					int deltaX = distance(originCubic, hexagonRow, 0);
					int deltaY = distance(originCubic, hexagonRow, 1);
					int deltaZ = distance(originCubic, hexagonRow, 2);
					//test if the current cubic coordinate is valid
					if(deltaX >= - range && deltaX <= range && 
							deltaY >= -range && deltaY <= range && deltaZ >= -range && deltaZ <= range) {
						if(deltaX + deltaY + deltaZ == 0) {
							result.add(hexagon);
							
						}
					}
				}
			} 
			//remove the starting cell
			result.remove(boardReference.getCellAt(row, col));
			return result;
		}
	}
	
	/**
	 * Compute the distance between two cubic components.
	 * 
	 * @param start
	 * @param end
	 * @param index
	 * @return
	 */
	private int distance(final List<Integer> start, final List<Integer> end, final int index) {
		return end.get(index) - start.get(index);
	}
	
	/**
	 * Convert odd-q coordinate to cubic.
	 * 
	 * @param row row of the cell
	 * @param col col of the cell
	 * @return arraylist containing the corrdinates in x y z order
	 */
	private List<Integer> convertOddqToCubic(final int row, final int col) {
		List<Integer> coordinates = new ArrayList<Integer>();
		//convert the row and column to cubic components x y and z
		int x = col;
		int z = row - (col - (col & 1)) / 2;
		int y = - (x + z);
		
		coordinates.add(x);
		coordinates.add(y);
		coordinates.add(z);
		
		return coordinates;
	}

	@Override
	public Map<Hexagon, Integer> getAllCosts(final HexagonalMap boardReference, final int startRow, final int startCol, final int actionPoints, final int range, final AbstractUnit unit, final List<AbstractUnit> units, final List<AbstractBuilding> buildings, final List<AbstractProductionBuilding> pBuildings, boolean flying) throws IndexOutOfBoundsException {
		if (startRow < 0 || startRow >= boardReference.getHeight() || startCol < 0 || startCol >= boardReference.getWidth()) throw new IndexOutOfBoundsException();
		else {
			//get the starting hexagon from the map
			Hexagon start = boardReference.getCellAt(startRow, startCol);

			//this list contains all possible cell that can be reached assuming that the given unit can move on every cell with cost 1. 
			//In this way we ensure that all the possible cells are chosen
			List<Hexagon> possibleCells = this.getNeighboursInRange(boardReference, startRow, startCol, range);
			possibleCells.add(start);
			//this hash map holds all the cells and their costs
			Map<Hexagon, Integer> dist = new HashMap<Hexagon, Integer>();
			//this hash map holds the precedent cell that must be reached in order to reach the key cell
			Map<Hexagon, Hexagon> precedent = new HashMap<Hexagon, Hexagon>();
			
			//initialization
			for (Hexagon hexagon : possibleCells) {
				if(hexagon.equals(start)) {
					//give to the starting Hexagon cost 0
					dist.put(hexagon, 0);
				} else {
					//give to all other Hexagon the max value of and integer to simulate infinite
					dist.put(hexagon, Integer.MAX_VALUE);
				}
			}
			//add the termination entry to the precedent map
			precedent.put(start, start);
			//the queue used in Disktra's algorithm
			List<Hexagon> q = new ArrayList<Hexagon>(possibleCells);
			while(q.size() > 0) {
				Hexagon u = findSmallest(dist, q);

				if(u == null) {
					break;
				}
				if(dist.get(u) == Integer.MAX_VALUE) {
					break;
				}
				q.remove(u);
				
				List<Hexagon> neighbors = new ArrayList<Hexagon>(this.getNeighbour(boardReference, u).values());
				
				for (Hexagon hexagon : neighbors) {
					if(possibleCells.contains(hexagon)) {

						//test for the actionpoints
						int cost = MovementCostMapper.isValidTerrainAndCost(unit.getClass(), hexagon.getType());
						//System.out.println(MovementCostMapper.isValidTerrainAndCost(unit.getClass(), hexagon.getType())*unit.getActionPointCostPerCell());
						//System.out.println(boardReference.getCellAt(hexagon.getRow(), hexagon.getCol()).getType());
						if(cost >= 0) {
							int distBet = dist.get(u) + cost;
							
							if(distBet < dist.get(hexagon) && distBet <= actionPoints && flying && !occupied(hexagon, units, buildings, pBuildings, AbstractAirUnit.class)) {
								dist.put(hexagon, distBet);
								//System.out.println(dist);
								precedent.put(hexagon, u);
							}
							
							if(distBet < dist.get(hexagon) && distBet <= actionPoints && !occupied(hexagon, units, buildings, pBuildings, AbstractEntity.class)) {
								dist.put(hexagon, distBet);
								//System.out.println(dist);
								precedent.put(hexagon, u);
							}
						}
					}
				}
			}
			dist.remove(start);
			//System.out.println("before"+dist);
			
			Iterator<Hexagon> it = dist.keySet().iterator();
			while (it.hasNext()) {
				Hexagon hexagon = (Hexagon) it.next();
				if(dist.get(hexagon) >= Integer.MAX_VALUE) {
					it.remove();
				}
			}
			
			this.currentPaths = precedent;
			return dist;
		}
	}	
	
	/**
	 * 
	 * @param cell
	 * @param units
	 * @param buildings
	 * @param pBuildings
	 * @return
	 */
	private boolean occupied(Hexagon cell, final List<AbstractUnit> units, final List<AbstractBuilding> buildings, final List<AbstractProductionBuilding> pBuildings, final Class<? extends AbstractEntity> c) {
		if (cell != null && units != null && buildings != null && pBuildings != null) {
			for (AbstractUnit unit : units) {
				if (unit.getHexagon().equals(cell) && c.isAssignableFrom(unit.getClass())) {
					return true;
				}
			}
			for (AbstractBuilding building : buildings) {
				if (building.getHexagon().equals(cell) && c.isAssignableFrom(building.getClass())) {
					return true;
				}
			}
			for (AbstractProductionBuilding productionBuilding : pBuildings) {
				if (productionBuilding.getHexagon().equals(cell) && c.isAssignableFrom(productionBuilding.getClass())) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public List<Hexagon> getShortestPath(Hexagon start, Hexagon end) {
		//create a stack where push the cells of the path
		Stack<Hexagon> stack = new Stack<Hexagon>();
		Hexagon w = end;
		//go in reverse way from the end hexagon to the start hexagon
		while(this.currentPaths.get(w) != start) {
			stack.push(w);
			w = this.currentPaths.get(w);
		}
		stack.push(w);
		List<Hexagon> results = new ArrayList<Hexagon>(stack);
		//reverse the path
		Collections.reverse(results);
		return results;
	}
	
	/**
	 * Find the hexagon with the smallest cost.
	 * 
	 * @param dist
	 * @param q
	 * @return
	 */
	private Hexagon findSmallest(Map<Hexagon, Integer> dist, List<Hexagon> q) {
		int min = Integer.MAX_VALUE;
		Hexagon minIndex = null;
		Iterator<Entry<Hexagon, Integer>> it = dist.entrySet().iterator();
		//loop to find the smallest Hexagon with the smallest cost
		while(it.hasNext()) {
			Map.Entry<Hexagon, Integer> pair = (Map.Entry<Hexagon, Integer>) it.next();
			if(q.contains(pair.getKey())) {
				if(pair.getValue() < min) {
					min = pair.getValue();
					minIndex = pair.getKey();
					
				}
			}
		}
		return minIndex;
	}
	
	
}
