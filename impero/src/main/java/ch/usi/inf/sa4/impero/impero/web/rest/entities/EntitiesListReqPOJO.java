/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.entities;

/**
 * @author jesper
 *
 */
public class EntitiesListReqPOJO {
	
	private int row;
	private int col;
	private String playerID;
	
	
	public EntitiesListReqPOJO() {}
	
	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}
	/**
	 * @param row the row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}
	/**
	 * @return the col
	 */
	public int getCol() {
		return col;
	}
	/**
	 * @param col the col to set
	 */
	public void setCol(int col) {
		this.col = col;
	}

	/**
	 * @return the playerID
	 */
	public String getPlayerID() {
		return playerID;
	}

	/**
	 * @param playerID the playerID to set
	 */
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

}
