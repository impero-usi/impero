package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class Road extends AbstractBuilding {

	private Road(Player owner, Hexagon hexagon, String name) {
		super(owner, hexagon, name, 1, 0, 0, 0);
	}

}
