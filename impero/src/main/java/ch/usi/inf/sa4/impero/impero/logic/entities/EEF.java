package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.BuildingAttributeMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class EEF extends AbstractBuilding {

	public EEF(final Player owner, final Hexagon hexagon) {
		super(owner, hexagon,
				EEF.class.getSimpleName(), BuildingAttributeMapper.getHealth(EEF.class),
				BuildingAttributeMapper.getVisibleRange(EEF.class), BuildingAttributeMapper.getUnitCapacity(EEF.class), BuildingAttributeMapper.getHealth(EEF.class));
	}

}
