package ch.usi.inf.sa4.impero.impero.web.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value="/LogoutServlet", name="Logout")
public class LogoutServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		if (session != null) {
			session.removeAttribute("username");
			session.invalidate();
			for (Cookie c : request.getCookies()) {
				c.setMaxAge(0);
				response.addCookie(c);
			}
		}
		response.sendRedirect(request.getContextPath() + "/index.jsp");
	}

}
