package ch.usi.inf.sa4.impero.impero.web.rest.session;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

/***
 * 
 * @author Kasim Bordogna
 * 
 *
 */

@Path("/session")
public class SessionRest {
	@GET
	@Produces({ "application/json" })
	public String getSessionInfo(@Context HttpServletRequest request){
		return request.getSession().getAttribute("id").toString();
	}
}
