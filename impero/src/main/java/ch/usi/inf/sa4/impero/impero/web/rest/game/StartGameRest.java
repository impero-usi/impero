package ch.usi.inf.sa4.impero.impero.web.rest.game;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;

/**
 * 
 * @author Kasim Bordogna
 *
 * Service that creates a game and stores it to the System.games list
 */

@Path("/startgame")
public class StartGameRest {

	@POST 
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String StartGame(StartGameReqPOJO request,@Context HttpServletRequest rq){
		//Create a new game in Mongo and get the game ID
		
		
		String playerID = rq.getSession(false).getAttribute("id").toString();
	
		
		System.out.println(" received create game request " + request);
		String gameID = MongoGame.createGame(request.getName(), playerID);
		
		
		//public Game(final Integer mapWidth, final Integer mapHeight,
		//		final Integer actionPointsPerTurn, final Integer startEnergy,
		//		final List<String> players, final String gameID, final Integer waterPercent, final int landPercent,
		//		final int mountainPercent, final int energyPercent, final boolean lakes, final boolean rivers) {

		System.out.println("Water level: "+ request.getWater());
		System.out.println("Land level: "+ request.getLand());
		System.out.println("Mountain level: "+ request.getMountain());
		System.out.println("Starting energy: "+ request.getEnergy());
		System.out.println("Resources: "+ request.getResource());
		System.out.println("Lakes? "+ request.getLakes());
		System.out.println("Rivers? "+ request.getRivers());
		System.out.println("Players sent: "+ request.getPlayers().size());
		
		//Create the actual game and add it to the system
		Game g = new Game(
				request.getWidth(),
				request.getHeight(),
				request.getActionpoints(),
				request.getEnergy(),
				request.getPlayers(),
				gameID,
				request.getWater(),
				request.getLand(),
				request.getMountain(),
				request.getResource(),
				request.getLakes(),
				request.getRivers()
				);

		System.out.println("Players receives: "+ g.getPlayerList().size());
		
		//Store game details in mongo
		MongoGame.addGameDetails(g);
		
		//Store the game into GameSystem games
		GameServer.putGame(gameID, g);
		return gameID;
	}
	
}
