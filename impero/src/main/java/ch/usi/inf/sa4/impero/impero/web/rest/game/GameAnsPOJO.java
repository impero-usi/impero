package ch.usi.inf.sa4.impero.impero.web.rest.game;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.db.PlayerEntryPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinateEntryPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.EntityActionPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.EntityPOJO;

public class GameAnsPOJO {
	private String gameID;
	private int width;
	private int height;
	private String name;
	private List<CoordinateEntryPOJO> map;
	private List<EntityPOJO> entities;
	private List<EntityActionPOJO> entitiesActions;
	private List<PlayerEntryPOJO> players;
	private List<CoordinatePOJO> discoveredHexagons;
	private List<ActionPOJO> currentActions;
	public GameAnsPOJO(){
		
	}
	
	/** Session code
	 * @return the discoveredHexagons
	 */
	public List<CoordinatePOJO> getDiscoveredHexagons() {
		return discoveredHexagons;
	}

	/** Session code
	 * @param discoveredHexagons the discoveredHexagons to set
	 */
	public void setDiscoveredHexagons(List<CoordinatePOJO> discoveredHexagons) {
		this.discoveredHexagons = discoveredHexagons;
	}
	
	public List<CoordinateEntryPOJO> getMap() {
		return map;
	}
	public void setMap(List<CoordinateEntryPOJO> map) {
		this.map = map;
	}
	public List<EntityPOJO> getEntities() {
		return entities;
	}
	public void setEntities(List<EntityPOJO> entities) {
		this.entities = entities;
	}
	public List<EntityActionPOJO> getEntitiesActions() {
		return entitiesActions;
	}
	public void setEntitiesActions(List<EntityActionPOJO> entitiesActions) {
		this.entitiesActions = entitiesActions;
	}

	public void setGameID(String gameID) {
		this.gameID = gameID;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public List<PlayerEntryPOJO> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerEntryPOJO> players) {
		this.players = players;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ActionPOJO> getCurrentActions() {
		return currentActions;
	}

	public void setCurrentActions(List<ActionPOJO> currentActions) {
		this.currentActions = currentActions;
	}
	
}
