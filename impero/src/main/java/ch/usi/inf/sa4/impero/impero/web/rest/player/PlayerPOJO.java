package ch.usi.inf.sa4.impero.impero.web.rest.player;


/**
 * 
 * @author Kasim Bordogna
 *
 *	Plain Old Java Object representing a player
 *  to be exchanged via REST Services
 */

public class PlayerPOJO {
	private String playerID;
	private Integer actionPoint;
	private Integer teamID;
	private String name;
	private Integer resources;
	private String status;
	
	public String getPlayerID() {
		return playerID;
	}
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	public Integer getActionPoint() {
		return actionPoint;
	}
	public void setActionPoint(Integer actionPoint) {
		this.actionPoint = actionPoint;
	}
	public Integer getTeamID() {
		return teamID;
	}
	public void setTeamID(Integer teamID) {
		this.teamID = teamID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getResources() {
		return resources;
	}
	public void setResources(Integer resources) {
		this.resources = resources;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}


	
}
