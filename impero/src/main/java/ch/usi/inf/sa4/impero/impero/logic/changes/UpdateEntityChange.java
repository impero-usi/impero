package ch.usi.inf.sa4.impero.impero.logic.changes;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * This class represent a change to a unit. For now the only change is the health
 * @author Luca
 *
 */
public class UpdateEntityChange implements AbstractChange {
	//the owner of the entity to update
	private Player owner;
	//the cell position of the entity to update
	private Hexagon cell;
	//the type of the entity
	private Class<? extends AbstractEntity> type;
	//the health to change
	private int health;
	
	/**
	 * Constructor. It take the owner, the spawnCell, the type and the health
	 * @param owner
	 * @param spawnCell
	 * @param type
	 * @param health
	 */
	public UpdateEntityChange(Player owner, Hexagon cell, Class<? extends AbstractEntity> type, int health) {
		this.owner = owner;
		this.cell = cell;
		this.type = type;
		this.health = health;
	}
	
	/**
	 * getter for the field health
	 * @return
	 */
	public int getHealth() {
		return health;
	}
	
	/**
	 * Getter for the field owner
	 * @return
	 */
	public Player getOwner() {
		return owner;
	}
	
	/**
	 * Getter for the field cell
	 * @return
	 */
	public Hexagon getCell() {
		return cell;
	}
	
	/**
	 * Getter for the field type
	 * @return
	 */
	public Class<? extends AbstractEntity> getType() {
		return type;
	}
}
