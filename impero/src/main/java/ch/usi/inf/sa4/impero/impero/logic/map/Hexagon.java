package ch.usi.inf.sa4.impero.impero.logic.map;

/**
 * This class represent an hexagon in the map.
 * 
 * @author Luca Dotti, Simone Raimondi
 * Remember if we keep this class we get free beers at the end of this semester!
 */
public class Hexagon {
	//
	private TerrainType type;
	//
	private final int row;
	//
	private final int col;
	//
	private final TerrainType defaultType;
	//
	private boolean discovered;
	
	/**
	 * 
	 * @param type
	 */
	public Hexagon(TerrainType type, final int row, final int col) {
		this.type = type;
		this.row = row;
		this.col = col;
		defaultType = type;
	}
	
	public Hexagon(Hexagon hex, boolean boo) {
		this.type = hex.type;
		this.row = hex.row;
		this.col = hex.col;
		defaultType = hex.type;
		this.discovered = boo;
	}
	
	/**
	 * @return the discovered
	 */
	public boolean isDiscovered() {
		return discovered;
	}

	/**
	 * @param discovered the discovered to set
	 */
	public void setDiscovered(boolean discovered) {
		this.discovered = discovered;
	}

	/**
	 * 
	 * @return
	 */
	public TerrainType getType() {
		return this.type;
	}
	
	/**
	 * 
	 * @return
	 */
	public void setType(final TerrainType type) {
		this.type=type;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getRow() {
		return this.row;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getCol() {
		return this.col;
	}
	
	/**
	 * 
	 * @return
	 */
	public TerrainType getDefaultType() {
		return defaultType;
	}
	
	@Override
	public String toString() {
		return this.row + " " + this.col;
	}
	
	@Override
	public int hashCode() {
		//TODO
		return 1;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Hexagon) {
			return this.row == ((Hexagon)obj).row && this.col == ((Hexagon)obj).col;
		}
		return false;
	}
}
