package ch.usi.inf.sa4.impero.impero.web.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.web.rest.player.PlayerPOJO;

/**
 * 
 * @author Kasim Bordogna
 * 
 * Simple REST services that return a player object serialized in JSON
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Path("/restsample")
public class RestSample {
	private Player player;
	
	public RestSample(){
		this.player = new Player("Jack",1,100,100);
	}
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON)
 	public Player getPlayer() {
		return this.player;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setPlayer(PlayerPOJO player){
		this.player = new Player(player.getName(),player.getTeamID(),player.getActionPoint(),player.getResources());
		return Response.status(201).entity("OK.").build();	
	}
}
