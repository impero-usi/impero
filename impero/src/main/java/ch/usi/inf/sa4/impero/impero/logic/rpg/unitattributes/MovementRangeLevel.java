package ch.usi.inf.sa4.impero.impero.logic.rpg.unitattributes;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The available levels of the movementRange attribute of units.
 * @author Orestis Melkonian
 */
public enum MovementRangeLevel {
	INSANE_SHORT(1),
	CRAZY_SHORT(2),
	VERY_SHORT(3),
	LITTLE_SHORT(4),
	MEDIUM(5),
	LITTLE_LONG(6),
	VERY_LONG(7),
	CRAZY_LONG(9),
	INSANE_LONG(11);
	
	/**
	 * Map from integer values to level enum expressions.
	 */
     private static final Map<Integer, MovementRangeLevel> LOOKUP 
          = new HashMap<Integer, MovementRangeLevel>();

     static {
          for (MovementRangeLevel mrl 
        		  : EnumSet.allOf(MovementRangeLevel.class)) {
        	  LOOKUP.put(mrl.getMovementRange(), mrl);
          }
     }
     /**
      * The movementRange of the current unit.
      */
     private int movementRange;
     /**
      * Constructor.
      * @param mr The integer associated with the current attribute level.
      */
     private MovementRangeLevel(final int mr) {
          this.movementRange = mr;
     }
     /**
      * Getter for movementRange.
      * @return The current level's movement range.
      */
     public int getMovementRange() { 
    	 return this.movementRange; 
     }
}
