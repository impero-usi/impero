package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.db.PlayerEntryPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.EntityPOJO;

public class ChangeRestAnsPOJO {
	
	private List<SequencePOJO> sequence;
	private List<AttackChangePOJO> attackChanges;
	private List<BattleChangePOJO> battleChanges;
	private List<EconomyChangePOJO> economyChanges;
	private List<EntityPOJO> createEntityChanges;
	private List<MoveChangePOJO> moveChanges;
	private List<RemoveEntityChangePOJO> removeEntityChanges;
	private List<UpdateEntityChangePOJO> updateEntityChanges;
	private List<MapChangesPOJO> mapChange;
	private List<PlayerEntryPOJO> defeatedPlayerChange;
	private List<VisibilityEntityChangePOJO> visibleEntityChanges;
	private boolean gameOverChange;
		
	public ChangeRestAnsPOJO(){}
	
	/**
	 * @return the sequence
	 */
	public List<SequencePOJO> getSequence() {
		return sequence;
	}
	/**
	 * @param sequence the sequence to set
	 */
	public void setSequence(List<SequencePOJO> sequence) {
		this.sequence = sequence;
	}
	
	/**
	 * @return the attackChanges
	 */
	public List<AttackChangePOJO> getAttackChanges() {
		return attackChanges;
	}
	/**
	 * @param attackChanges the attackChanges to set
	 */
	public void setAttackChanges(List<AttackChangePOJO> attackChanges) {
		this.attackChanges = attackChanges;
	}
	/**
	 * @return the battleChanges
	 */
	public List<BattleChangePOJO> getBattleChanges() {
		return battleChanges;
	}
	/**
	 * @param battleChanges the battleChanges to set
	 */
	public void setBattleChanges(List<BattleChangePOJO> battleChanges) {
		this.battleChanges = battleChanges;
	}
	public List<RemoveEntityChangePOJO> getRemoveEntityChanges() {
		return removeEntityChanges;
	}
	public void setRemoveEntityChanges(List<RemoveEntityChangePOJO> removeEntityChanges) {
		this.removeEntityChanges = removeEntityChanges;
	}
	public List<UpdateEntityChangePOJO> getUpdateEntityChanges() {
		return updateEntityChanges;
	}
	public void setUpdateEntityChanges(List<UpdateEntityChangePOJO> updateEntityChanges) {
		this.updateEntityChanges = updateEntityChanges;
	}
	public List<EconomyChangePOJO> getEconomyChanges() {
		return economyChanges;
	}
	public void setEconomyChanges(List<EconomyChangePOJO> economyChanges) {
		this.economyChanges = economyChanges;
	}
	public List<EntityPOJO> getCreateEntityChanges() {
		return createEntityChanges;
	}
	public void setCreateEntityChanges(List<EntityPOJO> createEntityChanges) {
		this.createEntityChanges = createEntityChanges;
	}
	public List<MoveChangePOJO> getMoveChanges() {
		return moveChanges;
	}
	public void setMoveChanges(List<MoveChangePOJO> moveChanges) {
		this.moveChanges = moveChanges;
	}

	/**
	 * @return the mapChange
	 */
	public List<MapChangesPOJO> getMapChange() {
		return mapChange;
	}

	/**
	 * @param mapChange the mapChange to set
	 */
	public void setMapChange(List<MapChangesPOJO> mapChange) {
		this.mapChange = mapChange;
	}

	/**
	 * @return the defeatedPlayerChange
	 */
	public List<PlayerEntryPOJO> getDefeatedPlayerChange() {
		return defeatedPlayerChange;
	}

	/**
	 * @param defeatedPlayerChange the defeatedPlayerChange to set
	 */
	public void setDefeatedPlayerChange(List<PlayerEntryPOJO> defeatedPlayerChange) {
		this.defeatedPlayerChange = defeatedPlayerChange;
	}

	/**
	 * @return the gameOverChange
	 */
	public boolean isGameOverChange() {
		return gameOverChange;
	}

	/**
	 * @param gameOverChange the gameOverChange to set
	 */
	public void setGameOverChange(boolean gameOverChange) {
		this.gameOverChange = gameOverChange;
	}

	public List<VisibilityEntityChangePOJO> getVisibleEntityChanges() {
		return visibleEntityChanges;
	}

	public void setVisibleEntityChanges(List<VisibilityEntityChangePOJO> visibleEntityChanges) {
		this.visibleEntityChanges = visibleEntityChanges;
	}
	
	
	
}
