package ch.usi.inf.sa4.impero.impero.logic.changes;

import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class DefeatedPlayerChange implements AbstractChange {

	private final Player player;
	
	public DefeatedPlayerChange(final Player player){
		this.player = player;
	}

	public Player getPlayer() {
		return this.player;
	}
}
