package ch.usi.inf.sa4.impero.impero.logic.mappers;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.EEF;
import ch.usi.inf.sa4.impero.impero.logic.entities.EmptyEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.entities.Harbour;
import ch.usi.inf.sa4.impero.impero.logic.entities.Battleship;
import ch.usi.inf.sa4.impero.impero.logic.entities.RadarTower;
import ch.usi.inf.sa4.impero.impero.logic.entities.Road;
import ch.usi.inf.sa4.impero.impero.logic.entities.Soldier;
import ch.usi.inf.sa4.impero.impero.logic.entities.UndefinedEntity;

public class StringToClass {
	private StringToClass(){
		
	}
	
	static final Map<String, Class<? extends AbstractEntity>> mapper;
	static final Map<String, Class<? extends AbstractUnit>> mapperUnits;

	static{
		mapper = new HashMap<String, Class<? extends AbstractEntity>>();
		mapperUnits = new HashMap<String, Class<? extends AbstractUnit>>();
		
		mapper.put(HQ.class.getSimpleName(), HQ.class);
		mapper.put(EEF.class.getSimpleName(), EEF.class);
		mapper.put(Harbour.class.getSimpleName(), Harbour.class);
		mapper.put(RadarTower.class.getSimpleName(), RadarTower.class);
		mapper.put(Road.class.getSimpleName(), Road.class);
		mapper.put(Tank.class.getSimpleName(), Tank.class);
		mapper.put(Soldier.class.getSimpleName(), Soldier.class);
		mapper.put(Battleship.class.getSimpleName(), Battleship.class);
		mapper.put(Bomber.class.getSimpleName(), Bomber.class);
		mapper.put(EmptyEntity.class.getSimpleName(), EmptyEntity.class);
		mapper.put(UndefinedEntity.class.getSimpleName(), UndefinedEntity.class);
		
		//Fill the mapper with all the AbstractUnit subclasses
		Reflections reflections = new Reflections("ch.usi.inf.sa4.impero.impero.logic.entities");		
		Set<Class<? extends AbstractUnit>> subTypesUnits = 
	               reflections.getSubTypesOf(AbstractUnit.class);		
		for(Class<? extends AbstractUnit> cc : subTypesUnits){
			if(!(Modifier.isAbstract(cc.getModifiers()))){
				mapperUnits.put(cc.getSimpleName(), cc.asSubclass(AbstractUnit.class));
			}
		}
	}

	/**
	 * 
	 * @param clas the String representing the class you want back
	 * @return the class which from the method getSimpleName() is equal to the
	 *         class String taken as parameter.
	 */
	public static  Class<? extends AbstractEntity> getClassFromString(final String clas) {
		return mapper.get(clas);
	}

	/**
	 * 
	 * @param clas the String representing the class you want back
	 * @return the class which from the method getSimpleName() is equal to the
	 *         className String taken as parameter.
	 */

	public static  Class<? extends AbstractUnit> getUnitClassFromString(final String className){
		return mapperUnits.get(className);
	}
}
