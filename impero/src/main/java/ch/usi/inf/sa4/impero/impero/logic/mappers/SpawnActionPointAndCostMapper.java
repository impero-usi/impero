package ch.usi.inf.sa4.impero.impero.logic.mappers;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.EEF;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.entities.Harbour;
import ch.usi.inf.sa4.impero.impero.logic.entities.Battleship;
import ch.usi.inf.sa4.impero.impero.logic.entities.RadarTower;
import ch.usi.inf.sa4.impero.impero.logic.entities.Road;
import ch.usi.inf.sa4.impero.impero.logic.entities.Soldier;

public class SpawnActionPointAndCostMapper {

	private static Map<Class<? extends AbstractEntity>, Integer> costMap = new HashMap<>();
	private static Map<Class<? extends AbstractEntity>, Integer> actionPointMap = new HashMap<>();

	static {
		costMap.put(Tank.class, 100);
		costMap.put(Bomber.class, 250);
		costMap.put(Battleship.class, 400);
		costMap.put(EEF.class, 100);
		costMap.put(Road.class, 25);
		costMap.put(RadarTower.class, 200);
		costMap.put(Harbour.class, 200);
		costMap.put(Soldier.class, 50);

		actionPointMap.put(Tank.class, 15);
		actionPointMap.put(Bomber.class, 25);
		actionPointMap.put(Battleship.class, 50);
		actionPointMap.put(EEF.class, 50);
		actionPointMap.put(Road.class, 5);
		actionPointMap.put(RadarTower.class, 25);
		actionPointMap.put(Harbour.class, 35);
		actionPointMap.put(Soldier.class, 5);
	}

	public static Integer getSpawnEnergyCost(
			final Class<? extends AbstractEntity> entity) {
		return costMap.get(entity);
	}

	public static Integer getSpawnActionCost(
			final Class<? extends AbstractEntity> entity) {
		return actionPointMap.get(entity);
	}

}
