/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.turnEnd;

/**
 * @author jesper
 *
 */
public class TurnEndPOJO {
	private String playerID;

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
}
