package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.BuildingAttributeMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class HQ extends AbstractProductionBuilding {

	/**
	 * Constructor for HQ.
	 * 
	 * @param owner
	 *            The owner of the HQ.
	 * @param hexagon
	 *            The hexagon the HQ is placed.
	 */
	public HQ(Player owner, Hexagon hexagon) {
		super(owner, hexagon, HQ.class.getSimpleName(), BuildingAttributeMapper
				.getHealth(HQ.class), BuildingAttributeMapper
				.getVisibleRange(HQ.class), BuildingAttributeMapper
				.getUnitCapacity(HQ.class),
				BuildingAttributeMapper
				.getHealth(HQ.class));

	}
}
