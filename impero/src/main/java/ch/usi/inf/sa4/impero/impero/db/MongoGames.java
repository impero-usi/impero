package ch.usi.inf.sa4.impero.impero.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class MongoGames {
	private static DBCollection games;
	static{
		MongoGames.games = MongoConnection.getDB().getCollection("game");
	}
	
	/**
	 * Return the entire list of gameID's
	 * @return list of gameID's
	 */
	public static List<String> getGameList(){
	    List<String> ret = new ArrayList<String>();
			BasicDBObject query = new BasicDBObject();
		    BasicDBObject keys = new BasicDBObject("_id", 1);
		    DBCursor cur = MongoGames.games.find(query,keys);
		    while(cur.hasNext()){
				DBObject curGame = cur.next(); 
				ret.add(curGame.get("_id").toString());	
				System.out.println(curGame.get("_id").toString());
		    } 
		    return ret;	
	}
	
	/**
	 * Return a list of gameID's in which the specified player is involved
	 * @param playerID
	 * @return list of gameID's
	 */
	public static Map<String,String> getGameListByPlayerID(String playerID){
	    Map<String,String> ret = new HashMap<String,String>();
		BasicDBObject query = new BasicDBObject("players.playerID",playerID);
	    BasicDBObject keys = new BasicDBObject("_id", 1).append("name",2);
	    DBCursor cur = MongoGames.games.find(query,keys);
	    while(cur.hasNext()){
			DBObject curGame = cur.next(); 
			ret.put(curGame.get("_id").toString(), curGame.get("name").toString());
	    } 
	    return ret;
	}
}
