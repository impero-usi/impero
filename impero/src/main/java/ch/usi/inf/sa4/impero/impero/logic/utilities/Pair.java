package ch.usi.inf.sa4.impero.impero.logic.utilities;

public class Pair<A, B> {
	
	private A firstObject;
	private B secondObject;
	
	public Pair(A firstObject, B secondObject) {
		this.firstObject = firstObject;
		this.secondObject = secondObject;
	}
	
	public A getFirstElement() {
		return firstObject;
	}
	
	public B getSecondElement() {
		return secondObject;
	}
	
	public void setFirstObject(A firstObject) {
		this.firstObject = firstObject;
	}
	
	public void setSecondObject(B secondObject) {
		this.secondObject = secondObject;
	}
}
