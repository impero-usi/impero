/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.deleteAction;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.logic.action.ActionVerifier;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;

/**
 * @author jesper
 *
 */
@Path("/action/delete/{gameID}")
public class DeleteRest {
	
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public DeleteAnsPOJO deleteAction(@PathParam("gameID")  String gameID, DeleteReqPOJO request,@Context HttpServletRequest rq){
		
		
		String playerID = rq.getSession(false).getAttribute("id").toString();
	
		System.out.println("received DELETE action request for game " + gameID);
		Game g = GameServer.getGame(gameID);
		ActionVerifier verifier = g.getVerifier();
		
		List<Integer> newBalance = verifier.deleteAction(request.getActionIndex(), playerID);
		
		DeleteAnsPOJO answer = null;
		
		if (newBalance != null) {
			answer = new DeleteAnsPOJO();
			
			System.out.println("receieved " + newBalance);
			
			answer.setNewActionPoints(newBalance.get(0));
			answer.setNewEnergyLevel(newBalance.get(1));
		}
		MongoGame.persistPlayers(g);

		return answer;
	}

}
