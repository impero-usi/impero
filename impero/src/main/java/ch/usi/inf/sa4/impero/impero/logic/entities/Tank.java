package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.UnitAttributeMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class Tank extends AbstractGroundUnit {

	/**
	 * This is the constructor for the GroundUnit01 class.
	 * 
	 * @param owner
	 *            the player who creates the entity.
	 * @param hexagon
	 *            the cell where the entity is created.
	 */
	public Tank(Player owner, Hexagon hexagon) {
		super(owner, hexagon, Tank.class.getSimpleName(),
				UnitAttributeMapper.getHealth(Tank.class),
				UnitAttributeMapper.getAttackCost(Tank.class),
				UnitAttributeMapper.getAttackRange(Tank.class),
				UnitAttributeMapper.getMovementRange(Tank.class),
				UnitAttributeMapper.getOffensiveDamage(Tank.class),
				UnitAttributeMapper.getDefensiveDamage(Tank.class),
				UnitAttributeMapper.getVisibleRange(Tank.class),
				UnitAttributeMapper.getAttackClass(Tank.class),
				UnitAttributeMapper.getHealth(Tank.class));
	}
}
