 /**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.deleteAction;

/**
 * @author jesper
 *
 */
public class DeleteReqPOJO {
	
	private String playerID;
	private int actionIndex;
	
	public DeleteReqPOJO() {}
	
	/**
	 * @return the playerID
	 */
	public String getPlayerID() {
		return playerID;
	}
	/**
	 * @param playerID the playerID to set
	 */
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	/**
	 * @return the actionIndex
	 */
	public int getActionIndex() {
		return actionIndex;
	}
	/**
	 * @param actionIndex the actionIndex to set
	 */
	public void setActionIndex(int actionIndex) {
		this.actionIndex = actionIndex;
	}
	
	

}
