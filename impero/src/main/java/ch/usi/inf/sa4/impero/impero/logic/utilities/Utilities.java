package ch.usi.inf.sa4.impero.impero.logic.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.changes.AbstractChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.AttackChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.BattleChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.CreateEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.DefeatedPlayerChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.EconomyChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.GameOverChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.MapChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.MoveChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.RemoveEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.UpdateEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.VisibleEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.EmptyEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.UndefinedEntity;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.mappers.StringToClass;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.logic.player.PlayerState;

/**
 * 
 * @author ChristianVuerich This class contains different methods used in
 *         different classes
 */
public class Utilities {
	/**
	 * a reference to the map object
	 */
	private final HexagonalMap map;
	/**
	 * a reference to the list of units in the game
	 */
	private final List<AbstractUnit> unitList;
	/**
	 * a reference to the list of buildings(not production buildings) in the
	 * game
	 */
	private final List<AbstractBuilding> buildingList;
	/**
	 * a reference to the list of production buildings in the game
	 */
	private final List<AbstractProductionBuilding> productionBuildingList;
	/**
	 * a reference to the list of players in the game
	 */
	private final List<Player> playerList;

	/**
	 * 
	 * @param map
	 *            reference to the map of the game
	 * @param unitList
	 *            reference to the list of units in the game
	 * @param buildingList
	 *            reference to the list of buildings(not production buildings)
	 *            in the game
	 * @param productionBuildingList
	 *            to the list of production buildings in the game
	 * @param playerList
	 *            to the list of players in the game
	 */
	public Utilities(final HexagonalMap map, final List<AbstractUnit> unitList,
			final List<AbstractBuilding> buildingList,
			final List<AbstractProductionBuilding> productionBuildingList,
			final List<Player> playerList) {
		this.map = map;
		this.unitList = unitList;
		this.buildingList = buildingList;
		this.productionBuildingList = productionBuildingList;
		this.playerList = playerList;
	}

	/**
	 * 
	 * @param row
	 *            coordinate on the map
	 * @param col
	 *            coordinate on the map
	 * @return the class of the Entity at the given coordinates,
	 *         EmptyEntity.class if there is no entity
	 */
	public Class<? extends AbstractEntity> getClassFromRowCol(
			final Integer row, final Integer col, Player p) {
		Hexagon cell = map.getCellAt(row, col);
		if (cell != null && p!=null) {
			if(p.getVisibleHexagons().contains(cell)){
				AbstractEntity entity = getEntityAt(row, col);
				if (entity != null) {
					return StringToClass.getClassFromString(entity.getName());
				}
				return EmptyEntity.class;
			}
			else{
				return UndefinedEntity.class;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param row
	 *            coordinate on the map
	 * @param col
	 *            coordinate on the map
	 * @return the Entity at the given coordinates, null if there is anything
	 *         there
	 */
	public AbstractEntity getEntityAt(Integer row, Integer col) {
		final Hexagon cell = map.getCellAt(row, col);
		if (cell != null) {
			for (AbstractUnit unit : unitList) {
				if (unit.getHexagon().equals(cell)) {
					return unit;
				}
			}
			for (AbstractBuilding building : buildingList) {
				if (building.getHexagon().equals(cell)) {
					return building;
				}
			}
			for (AbstractProductionBuilding productionBuilding : productionBuildingList) {
				if (productionBuilding.getHexagon().equals(cell)) {
					return productionBuilding;
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @param cell
	 *            reference of the hexagon you want to check
	 * @param player
	 *            reference to the player for which you want to check if the
	 *            cell "contains" an enemy entity
	 * @return boolean: true if the cell contains an entity enemy of the player,
	 *         false otherwise
	 */
	public boolean isEnemyEntity(final Hexagon cell, final Player player) {
		for (AbstractUnit unit : unitList) {
			if (unit.getHexagon().equals(cell)
					&& (!unit.getOwner().equals(player) && !unit.getOwner()
							.getTeamID().equals(player.getTeamID()))) {
				return true;
			}
		}
		for (AbstractBuilding building : buildingList) {
			if (building.getHexagon().equals(cell)
					&& (!building.getOwner().equals(player) && !building
							.getOwner().getTeamID().equals(player.getTeamID()))) {
				return true;
			}
		}
		for (AbstractProductionBuilding productionBuilding : productionBuildingList) {
			if (productionBuilding.getHexagon().equals(cell)
					&& (!productionBuilding.getOwner().equals(player) && !productionBuilding
							.getOwner().getTeamID().equals(player.getTeamID()))) {
				return true;
			}
		}
		return false;

	}

	/**
	 * 
	 * @param playerID
	 *            the string which identify a player
	 * @return the player which is identified by the playerID
	 */
	public Player getPlayer(final String playerID) {
		for (Player p : playerList) {
			if (p.getPlayerID().equals(playerID) && p.getPlayerState() == PlayerState.PLAYING) {
				return p;
			}
		}
		return null;
	}
	
	public Player getPlayerFromID(final String playerID){
		for (Player p : playerList) {
			if (p.getPlayerID().equals(playerID)) {
				return p;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param cell
	 *            reference of the cell you want to get the neighbors
	 * @return An hashmap of the hexagons next to the given hexagon, the integer
	 *         represent the side. 
	 */
	public Map<Integer, Hexagon> getNeigbors(final Hexagon cell) {
		return map.getNeighbour(cell);
	}

	/**
	 * 
	 * @param row
	 *            coordinate of the hexagon you want
	 * @param col
	 *            coordinate of the hexagon you want
	 * @return return the hexagon which is at coordinates(row, col) or null.
	 */
	public Hexagon getCellAt(final Integer row, final Integer col) {
		return map.getCellAt(row, col);
	}

	/**
	 * 
	 * @param row
	 *            coordinate of the hexagon you want to know the neigbors
	 * @param col
	 *            coordinate of the hexagon you want to know the neigbors
	 * @param range
	 *            where to find the neighbors
	 * @return return a list of hegagons which are in range from the given cell
	 *         at row, col.
	 */
	public List<Hexagon> getNeighboursInRange(final Integer row,
			final Integer col, final Integer range) {
		return map.getNeighboursInRange(row, col, range);
	}

	/**
	 * 
	 * @param row
	 *            coordinate of the hexagon you want
	 * @param col
	 *            coordinate of the hexagon you want
	 * @param player
	 *            reference to the player for which it's checked if the cell is
	 *            occupied
	 * @param actionList
	 *            reference to the map player:list of actions. In which i check
	 *            if the player is planning to move or spawn something at row,
	 *            col.
	 * @return true of the cell at row, col if is not occupied by an entity or a
	 *         future entity you will spawn there or move.False otherwise
	 */

	public boolean cellOccupied(final Integer row, final Integer col,
			final Player player,
			final Map<Player, List<AbstractAction>> actionList) {
		final Hexagon cell = map.getCellAt(row, col);
		if (cell != null) {
			for (AbstractUnit unit : unitList) {
				if (unit.getHexagon().equals(cell)) {
					return true;
				}
			}
			for (AbstractBuilding building : buildingList) {
				if (building.getHexagon().equals(cell)) {
					return true;
				}
			}
			for (AbstractProductionBuilding productionBuilding : productionBuildingList) {
				if (productionBuilding.getHexagon().equals(cell)) {
					return true;
				}
			}
			if (player != null) {
				if (actionList != null) {
					final List<AbstractAction> actions = actionList.get(player);
					if (actions != null) {
						for (AbstractAction action : actions) {
							if (action.getTarget().equals(cell)) {
								return true;
							}
						}
					}
				}
			}
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param subclass
	 *            Class which its consider the subclass
	 * @param superclass
	 *            Class which its consider the superClass
	 * @return true if the subclass extends the supreClass, false otherwise.
	 */
	public static boolean isSubClassOf(final Class<?> subclass,
			final Class<?> superclass) {
		if (subclass != null && superclass != null) {
			return superclass.isAssignableFrom(subclass);
		}
		return false;
	}

	/**
	 * 
	 * @param startRow
	 *            coordinate of the hexagon you want
	 * @param startCol
	 *            coordinate of the hexagon you want
	 * @param actionPoints
	 *            the amount of actionPoints the units can use to move
	 * @param unit
	 *            reference to an Abstract unit you want to move
	 * @return A map of Hexagon Integer which represent the cost to get to the
	 *         particular hexagon from the cell at startRow startCol using max
	 *         actionPoints.
	 */
	public Map<Hexagon, Integer> getAllCosts(final int startRow,
			final int startCol, final int actionPoints, final int range, final AbstractUnit unit, boolean flying) {
		return map.getAllCosts(startRow, startCol, actionPoints, range, unit,
				unitList, buildingList, productionBuildingList, flying);
	}

	/**
	 * 
	 * @param start
	 *            coordinate of the hexagon you want
	 * @param end
	 *            coordinate of the hexagon you want
	 * @return a list of hexagons which represent the shortest path from start
	 *         to end from the last request of getAllCosts.
	 */
	public List<Hexagon> getShortestPath(Hexagon start, Hexagon end) {
		return map.getShortestPath(start, end);
	}

	/**
	 * Updates the visible area of a {@link Player}
	 * 
	 * @param p
	 *            the player
	 */
	public void updateVisibleMap(Player p) {
		Set<Hexagon> hexList = new HashSet<>();
		for (AbstractUnit u : this.unitList) {
			if (u.getOwner().equals(p)) {
				hexList.addAll(map.getNeighboursInRange(
						u.getHexagon().getRow(), u.getHexagon().getCol(),
						u.getVisibleRange()));
				hexList.add(u.getHexagon());
				/*System.err.println("AUnit visible range : " + u.getVisibleRange());
				System.err.println("AUnit neighbours : " + map.getNeighboursInRange(
						u.getHexagon().getRow(), u.getHexagon().getCol(),
						u.getVisibleRange()).size()); */
			}
		}
		for (AbstractBuilding u : this.buildingList) {
			if (u.getOwner().equals(p)) {
				hexList.addAll(map.getNeighboursInRange(
						u.getHexagon().getRow(), u.getHexagon().getCol(),
						u.getVisibleRange()));
				hexList.add(u.getHexagon());
				/*System.err.println("ABuilding visible range : " + u.getVisibleRange());
				System.err.println("ABuilding neighbours : " + map.getNeighboursInRange(
						u.getHexagon().getRow(), u.getHexagon().getCol(),
						u.getVisibleRange()).size()); */
			}
		}
		for (AbstractProductionBuilding u : this.productionBuildingList) {
			if (u.getOwner().equals(p)) {
				hexList.addAll(map.getNeighboursInRange(
						u.getHexagon().getRow(), u.getHexagon().getCol(),
						u.getVisibleRange()));
				hexList.add(u.getHexagon());
				/*System.err.println("AProBuilding visible range : " + u.getVisibleRange());
				System.err.println("AProbuilding neighbours : " + map.getNeighboursInRange(
						u.getHexagon().getRow(), u.getHexagon().getCol(),
						u.getVisibleRange()).size()); */
			}
		}
		// Set the visible area to the player
		p.getDiscoveredHexagons().addAll(p.getVisibleHexagons());
		p.getDiscoveredHexagons().removeAll(hexList);
	    //System.out.println("Discovered cells:"+ p.getDiscoveredHexagons().size());
		p.setVisibleHexagons(hexList);
	}
	
	public List<AbstractChange> filterChanges(Player player, List<AbstractChange> changes){
		List<AbstractChange> filteredChanges = new ArrayList<>();
		for(AbstractChange change : changes){
			if( change instanceof AttackChange){
				if(player.getVisibleHexagons().contains(((AttackChange) change).getAttacker()) && player.getVisibleHexagons().contains(((AttackChange) change).getDefender())){
					filteredChanges.add(change);
				}
			}else if(change instanceof BattleChange){
				if(player.getVisibleHexagons().contains(((BattleChange) change).getPosition())){
					filteredChanges.add(change);
				}
			}else if(change instanceof CreateEntityChange){
				if(player.getVisibleHexagons().contains(((CreateEntityChange) change).getEntity().getHexagon())){
					filteredChanges.add(change);
				}
			}else if(change instanceof EconomyChange){
				if(player.equals(((EconomyChange) change).getPlayer())){
					filteredChanges.add(change);
				}
			}else if(change instanceof MoveChange){
				if(player.getVisibleHexagons().contains(((MoveChange) change).getStartCell()) && player.getVisibleHexagons().contains(((MoveChange) change).getEndCell())){
					filteredChanges.add(change);
				}
			}else if(change instanceof RemoveEntityChange){
				if(player.getVisibleHexagons().contains(((RemoveEntityChange) change).getCell())){
					filteredChanges.add(change);
				}
			}else if(change instanceof UpdateEntityChange){
				if(player.getVisibleHexagons().contains(((UpdateEntityChange) change).getCell())){
					filteredChanges.add(change);
				}
			}else if(change instanceof DefeatedPlayerChange){
				filteredChanges.add(change);
			}else if(change instanceof GameOverChange){
				filteredChanges.add(change);
			}else if(change instanceof MapChange){
				if(player.equals(((MapChange) change).getPlayer())){
					filteredChanges.add(change);
				}
			}else if(change instanceof VisibleEntityChange){
				if(player.equals(((VisibleEntityChange) change).getPlayer())){
					filteredChanges.add(change);
				}
			}
		}
		return filteredChanges;
	}

	public boolean canAttack(AbstractUnit abstractUnit, AbstractEntity enemy) {
		List<Class<? extends AbstractEntity>> list = abstractUnit.getAttackClass();
		for(Class<? extends AbstractEntity> parentClass : list){
			if(isSubClassOf(StringToClass.getClassFromString(enemy.getName()), parentClass)){
				return true;
			}
		}
		return false;
	}
}
