/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.action.ActionVerifier;
import ch.usi.inf.sa4.impero.impero.logic.action.AttackAction;
import ch.usi.inf.sa4.impero.impero.logic.action.MoveAction;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

/**
 * @author jesper, kasim
 * 
 */
@Path("/action/moveattack/{gameID}")
public class MoveActionRest {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MoveActionAnsPOJO move(@PathParam("gameID") String gameID,
			MoveActionReqPOJO request,@Context HttpServletRequest rq) {

		System.out.println("received a MOVE action request for game " + gameID);
		
		String playerID = rq.getSession(false).getAttribute("id").toString();
	
		Game g = GameServer.getGame(gameID);

		ActionVerifier v = g.getVerifier();
		AbstractAction reta;
		MoveActionAnsPOJO response = new MoveActionAnsPOJO();
		List<CoordinatePOJO> path;
		CoordinatePOJO entry;

		//TODO Ensure security here
		if (v.isAttack(request.getRowTo(), request.getColTo())) {
			reta = v.verifyAttackAction(playerID,
					request.getRowTo(), request.getColTo(),
					request.getRowFrom(), request.getColFrom());
			if (reta != null) {
				response.setAttack(true);
				response.setValid(true);
				response.setCost(reta.getActionPointCost());
				
				path = new ArrayList<CoordinatePOJO>();
				
				Hexagon hex = ((AttackAction) reta).getActor().getHexagon();
				entry = new CoordinatePOJO();
				entry.setCol(hex.getCol());
				entry.setRow(hex.getRow());
				path.add(entry);
				
				hex = ((AttackAction) reta).getTarget();
				entry = new CoordinatePOJO();
				entry.setCol(hex.getCol());
				entry.setRow(hex.getRow());
				path.add(entry);
				response.setPath(path);

			} else {
				response.setValid(false);
				response.setAttack(false);
				response.setPath(new ArrayList<CoordinatePOJO>());
				response.setCost(-1);
			}
		} else {
			reta = v.verifyMoveAction(request.getRowFrom(),
					request.getColFrom(), request.getRowTo(),
					request.getColTo(), playerID);
			if (reta != null) {
				response.setAttack(false);
				response.setValid(true);
				response.setCost(reta.getActionPointCost());

				path = new ArrayList<CoordinatePOJO>();
				for (Hexagon hex : ((MoveAction) reta).getPath()) {
					entry = new CoordinatePOJO();
					entry.setCol(hex.getCol());
					entry.setRow(hex.getRow());
					path.add(entry);
				}
				response.setPath(path);
			} else {
				response.setValid(false);
				response.setAttack(false);
				response.setPath(new ArrayList<CoordinatePOJO>());
				response.setCost(-1);
			}
		}

		MongoGame.persistPlayers(g);
		return response;
	}

}
