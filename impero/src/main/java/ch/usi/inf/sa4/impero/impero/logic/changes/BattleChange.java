package ch.usi.inf.sa4.impero.impero.logic.changes;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;

/**
 * This class represent a change to the involved units in a cell-to-cell battle
 * @author Luca, Matteo Morisoli
 *
 */
public class BattleChange implements AbstractChange{
	//the attacker unit
	private final Hexagon position;
	
	/**
	 * Constructor.takes the position of the battle
	 * @param position
	 */
	public BattleChange(Hexagon position) {
		this.position = position;
	}
	
	/**
	 * Getter for the field attacker
	 * @return
	 */
	public Hexagon getPosition() {
		return position;
	}

}
