package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.BuildingAttributeMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class RadarTower extends AbstractBuilding {
	public RadarTower(final Player owner, final Hexagon hexagon) {
		super(owner, hexagon, RadarTower.class.getSimpleName(),
				BuildingAttributeMapper.getHealth(RadarTower.class),
				BuildingAttributeMapper.getVisibleRange(RadarTower.class),
				BuildingAttributeMapper.getUnitCapacity(RadarTower.class),
				BuildingAttributeMapper.getHealth(RadarTower.class));
	}

}
