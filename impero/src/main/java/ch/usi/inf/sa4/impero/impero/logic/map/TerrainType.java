package ch.usi.inf.sa4.impero.impero.logic.map;

/**
 * Thi enum represent the different type of terrains.
 * 
 * @author Luca Dotti, Simone Raimondi
 *
 */
public enum TerrainType {
	DEEP_WATER,
	SHALLOW_WATER,
	SAND,
	FIELD,
	FOREST,
	MOUNTAIN,
	ENERGY_RESOURCE,
	ROAD,
	UNDISCOVERED;
}
