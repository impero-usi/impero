package ch.usi.inf.sa4.impero.impero.db;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinateEntryPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class MongoVisibleHexagon {
	private static DBCollection visibleHexagon;
	
	static{
		MongoVisibleHexagon.visibleHexagon = MongoConnection.getDB().getCollection("visibleHexagon");
	}

	
	public static boolean persistVisibleHexagon(String gameID, String playerID, List<CoordinatePOJO> list){
		//Lookup for document into mongodb
		BasicDBObject query = new BasicDBObject("gameID",gameID).append("playerID", playerID);
	    BasicDBObject keys = new BasicDBObject("_id", 1);
	
	    DBCursor cur = MongoVisibleHexagon.visibleHexagon.find(query,keys);
    	List<DBObject> hlist = new ArrayList<DBObject>();
		DBObject newEntry = new BasicDBObject();
		newEntry.put("gameID", gameID);
		newEntry.put("playerID", playerID);
		DBObject hcurrent;
		for(CoordinatePOJO c : list){
			hcurrent = new BasicDBObject();
			hcurrent.put("col", c.getCol());
			hcurrent.put("row", c.getRow());
			hlist.add(hcurrent);
		}
		newEntry.put("visibleHexagons", hlist);
		
		if(cur.hasNext()){
			MongoVisibleHexagon.visibleHexagon.update(query,newEntry);
		}else{
			MongoVisibleHexagon.visibleHexagon.save(newEntry);
		}
		
		return true;
	}
	
	public static List<CoordinatePOJO> retrieveVisibleHexagon(String gameID, String playerID){
		List<CoordinatePOJO> ret = new ArrayList<CoordinatePOJO>();
		BasicDBObject query = new BasicDBObject("gameID",gameID).append("playerID", playerID);
	    BasicDBObject keys = new BasicDBObject("visibleHexagons", 1);
	    DBCursor cur = MongoVisibleHexagon.visibleHexagon.find(query,keys);
		
	    if(cur.hasNext()){
			DBObject curMap = cur.next(); 
			BasicDBList e = (BasicDBList) curMap.get("visibleHexagons");
			CoordinatePOJO entry;
			if(e != null){
				for(Object d : e){
					DBObject current = (DBObject)d;
					entry = new CoordinatePOJO();
					entry.setCol(Integer.parseInt(current.get("col").toString()));
					entry.setRow(Integer.parseInt(current.get("row").toString()));
					//Build the list
					ret.add(entry);
				}
			}
	    }
		return ret;
	}
	
	public static boolean removeVisibleHexagon(String gameID){
		BasicDBObject query = new BasicDBObject("gameID",gameID);
		MongoVisibleHexagon.visibleHexagon.remove(query);
		return true;
	}
}
