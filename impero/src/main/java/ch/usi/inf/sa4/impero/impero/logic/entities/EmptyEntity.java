package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;


public class EmptyEntity extends AbstractEntity {

	private EmptyEntity(final Hexagon cell) {
		super(null, null, cell, EmptyEntity.class.toString(),0,0); //TODO this zero as well
	}

}
