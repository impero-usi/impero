package ch.usi.inf.sa4.impero.impero.logic.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.map.strategy.BoardStrategy;
import ch.usi.inf.sa4.impero.impero.logic.map.strategy.HexagonalStrategy;

/**
 * This class represent the actual map.
 * 
 * @author Luca Dotti, Simone Raimondi
 *
 */
public class HexagonalMap {
	//width of the map
	private final int width;
	//height of the map
	private final int height;
	//the map
	private List<List<Hexagon>> map;
	//Neighbour strategy
	private final BoardStrategy mapStrategy;
	
	/**
	 * Constructor of the map class. It take a list of lists of terrain type and then instanciate the map with the Hexagon objects as cells.
	 * 
	 * @param width
	 * @param height
	 */
	public HexagonalMap(final List<List<TerrainType>> map) {
		this.width = map.get(0).size();
		this.height = map.size();
		//initialization
		this.map = new ArrayList<List<Hexagon>>(this.height);
		int rowIndex = 0;
		int colIndex = 0;
		for (List<TerrainType> list : map) {
			List<Hexagon> row = new ArrayList<Hexagon>(this.width);
			for (TerrainType type : list) {
				row.add(new Hexagon(type, rowIndex, colIndex));
				colIndex++;
			}
			this.map.add(row);
			colIndex = 0;
			rowIndex++;
		}
		mapStrategy = new HexagonalStrategy();
	}
	
	/**
	 * Getter for the field map.
	 * 
	 * @return map
	 */
	public List<List<Hexagon>> getMap() {
		return this.map;
	}
	
	/**
	 * Returns the map as a one direction (<3) map
	 * @return the map
	 */
	public List<Hexagon> get1DMap() {
		List<Hexagon> ret = new ArrayList<>();
		for(List<Hexagon> hexList : this.map) {
			ret.addAll(hexList);
		}
		return ret;
	}
	
	/**
	 * Getter for the field width.
	 * 
	 * @return width
	 */
	public int getWidth() {
		return this.width;
	}
	
	/**
	 * Getter for the field height.
	 * 
	 * @return height
	 */
	public int getHeight() {
		return this.height;
	}
	
	/**
	 * Get the neighbour of a cell given by row and col.
	 * 
	 * @param row row of the cell
	 * @param col colum of the cell
	 * @return an hashmap that contains all the neighbours of the cell
	 */
	public Map<Integer, Hexagon> getNeigbour(final int row, final int col) {
		return this.mapStrategy.getNeighbours(this, row, col);
	}

	/**
	 * Get the neighbour of a cell given by row and col.
	 * 
	 * @param row row of the cell
	 * @param col colum of the cell
	 * @return an hashmap that contains all the neighbours coordinates of the cell
	 */
	public Map<Integer, Coordinate> getNeighbourCoordinate(final int row, final int col) {
		return this.mapStrategy.getNeighboursCoordinates(this, row, col);
	}
	
	/**
	 * Get the neighbour of a cell given.
	 * 
	 * @param hex Given cell
	 * @return an hashmap that contains all the neighbours of the cell
	 */
	public Map<Integer, Hexagon> getNeighbour(final Hexagon hex) {
		return this.mapStrategy.getNeighbour(this, hex);
	}
	
	/**
	 * Return the hexagon at the given row and column.
	 * 
	 * @param row number of row
	 * @param column number of column
	 * 
	 * @return the hexagon at the given coordinates
	 */
	public Hexagon getCellAt(final int row, final int column) {
		return this.map.get(row).get(column);
	}
	
	/**
	 * Get the neighbours of a cell in the given range.
	 * 
	 * @param row row of the cell
	 * @param col column of the cell
	 * @param range range of the neighbour
	 * @return a list with all the neighbours
	 */
	public List<Hexagon> getNeighboursInRange(final int row, final int col, final int range) {
		return this.mapStrategy.getNeighboursInRange(this, row, col, range);
	}
	
	public Map<Hexagon, Integer> getAllCosts(final int startRow, final int startCol, final int actionPoints, final int range, final AbstractUnit unit, final List<AbstractUnit> units, final List<AbstractBuilding> buildings, final List<AbstractProductionBuilding> pBuildings, boolean flying){
		return this.mapStrategy.getAllCosts(this, startRow, startCol, actionPoints, range, unit, units, buildings, pBuildings, flying);
	}
	
	public List<Hexagon> getShortestPath(Hexagon start, Hexagon end){
		return this.mapStrategy.getShortestPath(start, end);
	}
}
