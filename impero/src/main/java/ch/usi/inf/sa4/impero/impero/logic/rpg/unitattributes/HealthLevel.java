package ch.usi.inf.sa4.impero.impero.logic.rpg.unitattributes;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The available levels of the health attribute of units.
 * @author Orestis Melkonian
 */
public enum HealthLevel {
	INSANE_WEAK(50),
	CRAZY_WEAK(100),
	VERY_WEAK(150),
	LITTLE_WEAK(200),
	MEDIUM(300),
	LITTLE_STRONG(400),
	VERY_STRONG(600),
	CRAZY_STRONG(800),
	INSANE_STRONG(1000);
	
	/**
	 * Map from integer values to level enum expressions.
	 */
     private static final Map<Integer, HealthLevel> LOOKUP 
          = new HashMap<Integer, HealthLevel>();

     static {
          for (HealthLevel hl : EnumSet.allOf(HealthLevel.class)) {
        	  LOOKUP.put(hl.getHealth(), hl);
          }
     }
     /**
      * The health of the current unit.
      */
     private int health;
     /**
      * Constructor.
      * @param h The integer associated with the current attribute level.
      */
     private HealthLevel(final int h) {
          this.health = h;
     }
     /**
      * Getter for health.
      * @return The current level's health.
      */
     public int getHealth() { 
    	 return this.health; 
     }
}
