/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.createUnit;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.action.AbstractSpawnAction;
import ch.usi.inf.sa4.impero.impero.logic.action.ActionVerifier;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;

/**
 * @author jesper
 *
 */
@Path("/action/createUnit/{gameID}")
public class CreateUnitRest {
	
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreateUnitAnsPOJO createUnit(@PathParam("gameID") String gameID, CreateUnitReqPOJO request,@Context HttpServletRequest rq) {
		
		System.out.println("received CREATE UNIT request for game " + gameID);
		String playerID = rq.getSession(false).getAttribute("id").toString();
	
		Game g = GameServer.getGame(gameID);
		
		ActionVerifier verifier = g.getVerifier();
		
		AbstractSpawnAction action = verifier.verifySpawnUnitAction(
				playerID, request.getRow(), request.getCol(), request.getType());
				
		CreateUnitAnsPOJO answer = new CreateUnitAnsPOJO();
		
		if(action == null) {
			answer.setCost(-1);
			answer.setEnergyCost(-1);
			answer.setValid(false);
			return answer;
		}
		
		answer.setCost(action.getActionPointCost());
		answer.setEnergyCost(action.getCost());
		answer.setValid(true);
		
		MongoGame.persistPlayers(g);

		return answer;
	}
	
}
