package ch.usi.inf.sa4.impero.impero.web.rest;

import java.util.List;

public class EntityPOJO {
	private Integer row;
	private Integer col;
	private String type;
	private String owner;
	private Integer health;
	private List<String> attackClass;
	private Integer orientation;
	private Integer maxHealth;
	

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getCol() {
		return col;
	}

	public void setCol(Integer col) {
		this.col = col;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Integer getHealth() {
		return health;
	}

	public void setHealth(Integer health) {
		this.health = health;
	}

	/**
	 * @return the attackClass
	 */
	public List<String> getAttackClass() {
		return attackClass;
	}

	/**
	 * @param attackClass the attackClass to set
	 */
	public void setAttackClass(List<String> attackClass) {
		this.attackClass = attackClass;
	}

	public Integer getOrientation() {
		return orientation;
	}

	public void setOrientation(Integer orientation) {
		this.orientation = orientation;
	}

	public Integer getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(Integer maxHealth) {
		this.maxHealth = maxHealth;
	}
}
