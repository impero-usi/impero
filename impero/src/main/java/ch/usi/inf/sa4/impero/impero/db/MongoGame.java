package ch.usi.inf.sa4.impero.impero.db;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;

import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.mappers.StringToClass;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.logic.player.PlayerState;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinateEntryPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * 
 * @author Kasim Bordogna
 * 
 * Class to make persistent/load  a game to/from MongoDB
 *
 */

public class MongoGame {
	private static DBCollection games;
	static{
		MongoGame.games = MongoConnection.getDB().getCollection("game");
	}
	
	/**
	 * Add details to game object in Mongo (basic informations, map, units, buildings, productionbuildings)
	 * @param g game to be stored
	 * @return successful or not
	 */
	public static boolean addGameDetails(Game g ){
		List<BasicDBObject> plist = new ArrayList<BasicDBObject>();
		BasicDBObject playerobj;
		
		List<Player> playerList = g.getPlayerList();
		for(Player p: playerList){
			playerobj = new BasicDBObject();
			playerobj.put("playerID", p.getPlayerID());
			playerobj.put("teamID", p.getTeamID());
			playerobj.put("actionPoints", p.getActionPoint());
			playerobj.put("energy", p.getEnergy());
			plist.add(playerobj);	
		}

		BasicDBObject q = new BasicDBObject("_id",ObjectId.massageToObjectId(g.getGameID()));
		
		DBCollection col = MongoConnection.getDB().getCollection("game");
		DBCursor cur = col.find(q);
		
		if(cur.hasNext()){
			DBObject curGame = cur.next(); 
			curGame.put("playerCount",plist.size());
			curGame.put("players",plist);
			curGame.put("width", g.getMap().getWidth());
			curGame.put("height", g.getMap().getHeight());
			curGame.put("actionPointsPerTurn", g.getActionPointsPerTurn());
			curGame.put("map", MongoGame.getMap(g));
			curGame.put("units", MongoGame.getUnits(g));
			curGame.put("buildings", MongoGame.getBuildings(g));
			curGame.put("productionBuildings", MongoGame.getProductionBuildings(g));
			col.save(curGame);
			return true;
		}else{
			System.err.println("Game not found.");
			return false;
		}
	}
	
	
	public static boolean persistGameDetails(Game g){
		BasicDBObject q = new BasicDBObject("_id",ObjectId.massageToObjectId(g.getGameID()));
		DBCollection col = MongoConnection.getDB().getCollection("game");
		DBCursor cur = col.find(q);
		
		if(cur.hasNext()){
			DBObject curGame = cur.next();
			curGame.put("GameOver", g.checkGameOver() ? "1":"0");
			col.save(curGame);
			return true;
		}else{
			System.err.println("Game not found.");
			return false;
		}
	}
	
	
	/**
	 * Inserts a game into MongoDB
	 * @param name name of the game 
	 * @param ownerId playerID of the owner of the game
	 * @return gameID of the newly created game
	 */
	public static String createGame(String name, String ownerId){
		//Store basic informations
		BasicDBObject doc = new BasicDBObject("name", name).
                					   append("owner", ownerId);
		
		//Save and return ID
		MongoGame.games.insert(doc);
		return ((ObjectId)doc.get( "_id" )).toString();
	}
	
	/**
	 * Returns a string containing the name of the game
	 */
	public static String getGameName(String gameID){
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID));
	    BasicDBObject keys = new BasicDBObject("name", 1);
	    DBCursor cur = MongoGame.games.find(query,keys);
	    if(cur.hasNext()){
			DBObject curQuery = cur.next();
			return curQuery.get("name").toString();
	    }else{
			return "unknow";
	    }
	}
	
	/**
	 * Returns a DBOject containing all the buildings
	 * @param g game to be considered
	 * @return Mongo object containing buildings
	 */
	private static List<DBObject> getBuildings(Game g){
		List<DBObject> ret = new ArrayList<DBObject>();
		DBObject entry;
		DBObject position;
		for(AbstractBuilding u : g.getBuildingList()){
			entry = new BasicDBObject();
			position = new BasicDBObject();
			entry.put("name", u.getName());
			entry.put("owner", u.getOwner().getPlayerID());
			entry.put("health", u.getHealth());
			position.put("row", u.getHexagon().getRow());
			position.put("col", u.getHexagon().getCol());
			position.put("type", u.getHexagon().getType().ordinal());
			entry.put("hexagon", position);
			ret.add(entry);		
		}
		return ret;
	}

	/**
	 * Return a DBObject containing the map of the game
	 * @param g game to be considered
	 * @return Mongo object containing the map
	 */
	private static List<BasicDBObject> getMap(Game g){
		List<BasicDBObject> plist = new ArrayList<BasicDBObject>();
		DBObject entry;
		
		//Add list of cells
		Integer count = 0;
		for(List<Hexagon> l : g.getMap().getMap()){
			for(Hexagon h : l){
				entry = new BasicDBObject();
				entry.put("row", h.getRow());
				entry.put("col", h.getCol());
				entry.put("type", h.getType().ordinal());
				plist.add((BasicDBObject) entry);
				count++;
			}
		}
	
		return plist;
	}

	/**
	 * Returns a DBObject containing all the productionbuildings
	 * @param g game to be considered
	 * @return Mongo object containing productionbuildings
	 */
	private static List<DBObject> getProductionBuildings(Game g){
		List<DBObject> ret = new ArrayList<DBObject>();
		DBObject entry;
		DBObject position;
		for(AbstractProductionBuilding u : g.getProductionBuildingList()){
			entry = new BasicDBObject();
			position = new BasicDBObject();
			entry.put("name", u.getName());
			entry.put("owner", u.getOwner().getPlayerID());
			entry.put("health", u.getHealth());
			position.put("row", u.getHexagon().getRow());
			position.put("col", u.getHexagon().getCol());
			position.put("type", u.getHexagon().getType().ordinal());
			entry.put("hexagon", position);
			ret.add(entry);		
		}
		return ret;
	}
	
	/**
	 * Returns a DBObject containing all the units
	 * @param g game to be considered
	 * @return Mongo object containing units
	 */
	private static List<DBObject> getUnits(Game g){
		List<DBObject> ret = new ArrayList<DBObject>();
		DBObject entry;
		DBObject position;
		for(AbstractUnit u : g.getUnitList()){
			entry = new BasicDBObject();
			position = new BasicDBObject();
			entry.put("name", u.getName());
			entry.put("owner", u.getOwner().getPlayerID());
			entry.put("health", u.getHealth());
			entry.put("defensiveDamage", u.getDefensiveDamage());
			entry.put("offensiveDamage",u.getOffensiveDamage());
			entry.put("movementRange", u.getMovementRange());
			entry.put("attackRange",u.getAttackRange());
			entry.put("attackActionPointCost", u.getAttackActionPointCost());
			position.put("row", u.getHexagon().getRow());
			position.put("col", u.getHexagon().getCol());
			position.put("type", u.getHexagon().getType().ordinal());
			entry.put("hexagon", position);
			ret.add(entry);
		}
		return ret;
	}
	
	
	/**
	 * Inserts/updates the buildings of the game into MongoDB
	 * @param g game whose buildings are saved
	 * @return successful or not
	 */
	private static boolean persistBuildings(Game g){
		BasicDBObject q = new BasicDBObject("_id",ObjectId.massageToObjectId(g.getGameID()));
			
			DBCollection col = MongoConnection.getDB().getCollection("game");
			DBCursor cur = col.find(q);
			
			if(cur.hasNext()){
				DBObject curGame = cur.next(); 
				curGame.put("buildings", MongoGame.getBuildings(g));
				col.save(curGame);
				return true;
			}else{
				System.err.println("Game not found.");
				return false;
			}
	}
	
	
	/**
	 * Persist game to MongoDB
	 * @param g game
	 * @return true | false
	 */
	public static boolean persistGame(Game g){
		boolean ret = true;
		ret &= MongoGame.persistPlayers(g);
		ret &= MongoGame.persistBuildings(g);
		ret &= MongoGame.persistMap(g);
		ret &= MongoGame.persistProductionBuildings(g);
		ret &= MongoGame.persistUnits(g);
		ret &= MongoGame.persistGameDetails(g);
		System.out.println("Game "+ g.getGameID() + " persisted to MongoDB with status: "+ ret );
		return ret;
	}
	
	
	
	/**
	 * Inserts/updates the map of the game into MongoDB
	 * @param g game whose map is saved
	 * @return successful or not
	 */
	private static boolean persistMap(Game g){
		BasicDBObject q = new BasicDBObject("_id",ObjectId.massageToObjectId(g.getGameID()));
		
		DBCollection col = MongoConnection.getDB().getCollection("game");
		DBCursor cur = col.find(q);
		
		if(cur.hasNext()){
			DBObject curGame = cur.next(); 
			curGame.put("map", MongoGame.getMap(g));
			col.save(curGame);
			return true;
		}else{
			System.err.println("Game not found.");
			return false;
		}
	}
	
	/**
	 * Inserts/updates the players of the game into MongoDB
	 * @param g game whose players are saved
	 */
	public static boolean persistPlayers(Game g){
		BasicDBObject q = new BasicDBObject("_id",ObjectId.massageToObjectId(g.getGameID()));
		
		DBCollection col = MongoConnection.getDB().getCollection("game");
		DBCursor cur = col.find(q);
		
		List<BasicDBObject> plist = new ArrayList<BasicDBObject>();
		BasicDBObject playerobj;
		
		List<Player> playerList = g.getPlayerList();
		List<CoordinatePOJO> listVisibleHex;
		List<CoordinatePOJO> listDiscoveredHex;
		
		
		for(Player p: playerList){
			listVisibleHex = new ArrayList<>();
			listDiscoveredHex = new ArrayList<>();
			playerobj = new BasicDBObject();
			playerobj.put("playerID", p.getPlayerID());
			playerobj.put("teamID", p.getTeamID());
			playerobj.put("actionPoints", p.getActionPoint());
			playerobj.put("energy", p.getEnergy());
			playerobj.put("status", ((Integer)p.getPlayerState().ordinal()).toString());
			playerobj.put("unitsCreated",p.getUnitsCreated().toString());
			playerobj.put("unitsDestroyed", p.getUnitsDestroyed().toString());
			playerobj.put("unitsLost", p.getUnitsLost().toString());
			playerobj.put("energyGathered", p.getEnergyGathered().toString());
			
			//Discovered
			for(Hexagon hex : p.getDiscoveredHexagons()) {
				CoordinatePOJO pojo = new CoordinatePOJO();
				pojo.setCol(hex.getCol());
				pojo.setRow(hex.getRow());
				listDiscoveredHex.add(pojo);
			}
			
			//Visible
			for(Hexagon hex : p.getVisibleHexagons()) {
				CoordinatePOJO pojo = new CoordinatePOJO();
				pojo.setCol(hex.getCol());
				pojo.setRow(hex.getRow());
				listVisibleHex.add(pojo);
			}
			
			MongoVisibleHexagon.persistVisibleHexagon(g.getGameID(), p.getPlayerID(), listVisibleHex);
			MongoDiscoveredHexagon.persistDiscoveredHexagon(g.getGameID(),p.getPlayerID(), listDiscoveredHex);
			plist.add(playerobj);	
		}

		if(cur.hasNext()){
			DBObject curGame = cur.next(); 
			curGame.put("players", plist);
			col.save(curGame);
			return true;
		}else{
			System.err.println("Game not found.");
			return false;
		}
	}
	
	/**
	 * Inserts/updates the production buildings of the game into MongoDB
	 * @param g game whose production buildings are saved
	 * @return successful or not
	 */
	private static boolean persistProductionBuildings(Game g){
		BasicDBObject q = new BasicDBObject("_id",ObjectId.massageToObjectId(g.getGameID()));
		
		DBCollection col = MongoConnection.getDB().getCollection("game");
		DBCursor cur = col.find(q);
		
		if(cur.hasNext()){
			DBObject curGame = cur.next(); 
			curGame.put("productionBuildings", MongoGame.getProductionBuildings(g));
			col.save(curGame);
			return true;
		}else{
			System.err.println("Game not found.");
			return false;
		}
	}
	
	
	/**
	 * Inserts/updates the units of the game into MongoDB
	 * @param g game whose units are saved
	 * @return successful or not
	 */
	private static boolean persistUnits(Game g){
		BasicDBObject q = new BasicDBObject("_id",ObjectId.massageToObjectId(g.getGameID()));
			
			DBCollection col = MongoConnection.getDB().getCollection("game");
			DBCursor cur = col.find(q);
			
			if(cur.hasNext()){
				DBObject curGame = cur.next(); 
				curGame.put("units", MongoGame.getUnits(g));
				col.save(curGame);
				return true;
			}else{
				System.err.println("Game not found.");
				return false;
			}		
	}
	
	/**
	 * Retrieves building from MongoDB
	 * @param gameID
	 * @param map 
	 * @param players 
	 * @return List of abstract buildings
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	private static List<AbstractBuilding> retrieveBuildings(String gameID, List<Player> players, HexagonalMap map) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		List<AbstractBuilding> ret = new ArrayList<AbstractBuilding>();
		DBObject entry;
		
		//Query to get list of entities from MongoDB
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID));
	    BasicDBObject keys = new BasicDBObject("buildings", 1);
	    DBCursor cur = MongoGame.games.find(query,keys);
	    if(cur.hasNext()){
			BasicDBObject curGame = (BasicDBObject) cur.next();
			if(curGame.get("buildings").toString() != "{ }"){
			BasicDBList e = (BasicDBList) curGame.get("buildings");
			for(Object d : e){
				entry = (DBObject) d;
				//Create entity
				Class<? extends AbstractEntity> tbc = StringToClass.getClassFromString(entry.get("name").toString());
				Constructor<? extends AbstractEntity> c = null;
				c = tbc.getConstructor(Player.class,Hexagon.class);
				if(AbstractProductionBuilding.class.isAssignableFrom(tbc)){
					AbstractProductionBuilding etbc = null;
					String owner = entry.get("owner").toString();
						Player out = null;
						 for(Player p: players){
							 if(p.getPlayerID().equals(owner) ){
								 out = p;
							 }
						 }
						 DBObject hex = (DBObject) entry.get("hexagon");
						 Hexagon htbc = map.getCellAt(Integer.parseInt(hex.get("row").toString()), Integer.parseInt(hex.get("col").toString()));
						 etbc = (AbstractProductionBuilding) c.newInstance(new Object[]{out,htbc});
						 etbc.setHealth(Integer.parseInt(entry.get("health").toString()));
					ret.add(etbc);					
				}else{
					AbstractBuilding etbc = null;
					String owner = entry.get("owner").toString();
						Player out = null;
						 for(Player p: players){
							 if(p.getPlayerID().equals(owner) ){
								 out = p;
							 }
						 }
						 DBObject hex = (DBObject) entry.get("hexagon");
						 Hexagon htbc = map.getCellAt(Integer.parseInt(hex.get("row").toString()), Integer.parseInt(hex.get("col").toString()));
						 etbc = (AbstractBuilding) c.newInstance(new Object[]{out,htbc});
						 etbc.setHealth(Integer.parseInt(entry.get("health").toString()));
					ret.add(etbc);
				}
			}
			}
	    }
		return ret;
	}
	
	/**
	 * Retrieves the game object from MongoDB
	 * @param gameId
	 * @return Game
	 */
	public static Game retrieveGame(String gameID) {
		try{
		GamePropertiesPOJO g = new GamePropertiesPOJO();
		//Gest basic attributes
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID));
	    BasicDBObject keys = new BasicDBObject("actionPointsPerTurn", 1).append("gameOver", 2);
	    DBCursor cur = MongoGame.games.find(query,keys);
	    if(cur.hasNext()){
			DBObject curQuery = cur.next(); 
			g.setActionPointsPerTurn(Integer.parseInt(curQuery.get("actionPointsPerTurn").toString()));
			g.setGameID(gameID);
			
			//Set collections
			HexagonalMap map = MongoGame.retrieveMap(gameID);
			List<Player> players = MongoGame.retrievePlayers(gameID);
			
			//Create an empty hashmap of actionslists
			Map<Player,List<AbstractAction>> actionlist = new HashMap<Player,List<AbstractAction>>();
			for(Player p :players){
				actionlist.put(p, new ArrayList<AbstractAction>());
			}
			
			g.setMap(map);
			g.setPlayerList(players);
			g.setBuildingList(MongoGame.retrieveBuildings(gameID,players,map));
			g.setUnitList(MongoGame.retrieveUnits(gameID,players,map));
			g.setProductionList(MongoGame.retrieveProductionBuildings(gameID,players,map));
			g.setActionList(actionlist);
			
			
			Game gg = new Game(g);
			//Gameover flag
			Boolean gameOver = Integer.parseInt(curQuery.get("gameOver") == null ? "0": curQuery.get("gameOver").toString())== 1 ? true : false;
			gg.setGameOver(gameOver);
			
			return gg;
	    }else{
	    	System.err.println("RetrieveGame: Game not found. GameID="+gameID);
	    	return null;
	    }
		}
		catch (Exception e){
			//System.err.println("RetrieveGame: Error retrieving game, GameID="+gameID +"\n"+ e.getStackTrace());
	    	e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * Retrieves the map from the MongoDB
	 * @param gameID
	 * @return HexagonalMap
	 */
	
	private static HexagonalMap retrieveMap(String gameID){
		int  type;
		DBObject entry;
		
		//Hexagon hex;
		List<List<TerrainType>> ret = new ArrayList<List<TerrainType>>();
		
		//Get map DBObject from Mongo
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID));
	    BasicDBObject keys = new BasicDBObject("map", 1).append("width", 2).append("height", 3);
	    DBCursor cur = MongoGame.games.find(query,keys);
		int ccount = 0;
	
	    if(cur.hasNext()){
			DBObject curMap = cur.next(); 
			BasicDBList e = (BasicDBList) curMap.get("map");
			Integer width = (Integer) curMap.get("width");			
			//Create the actual map
			
			List<TerrainType> rr = new ArrayList<TerrainType>();
			
			for(Object d : e){
				entry = (DBObject)d;
				type = Integer.parseInt(entry.get("type").toString());
				rr.add(TerrainType.values()[type]);
				ccount++;
				if(ccount == width){
					ret.add(rr);
					rr = new ArrayList<TerrainType>();
					ccount = 0;
				}
			}
			
	    }else{
	    	System.err.println("RetrieveMap: Game not found. GameID="+gameID);
	    }
		return new HexagonalMap(ret);
	}
	
	private static List<Player> retrievePlayers(String gameID) {
		List<Player> ret = new ArrayList<Player>();
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID));
	    BasicDBObject keys = new BasicDBObject("players", 1);
		DBCursor cur = MongoGame.games.find(query,keys);
	    if(cur.hasNext()){
			DBObject curGame = cur.next();
			BasicDBList e = (BasicDBList) curGame.get("players");
			Player p = null;
			Set<Hexagon> discoveredKasimsHexagons;
			Set<Hexagon> visibleKasimsHexagons;
			for(Object d : e){
				DBObject entry = (DBObject)d;
				List<CoordinatePOJO> discoveredHexagons = MongoDiscoveredHexagon.retrieveVisibleHexagon(gameID, entry.get("playerID").toString());
				List<CoordinatePOJO> visibleHexagons = MongoVisibleHexagon.retrieveVisibleHexagon(gameID, entry.get("playerID").toString());
				discoveredKasimsHexagons = new HashSet<>();
				visibleKasimsHexagons = new HashSet<>();
				if(discoveredHexagons.size()>0){
					for(CoordinatePOJO hexObj : discoveredHexagons) {
						Hexagon hex = new Hexagon(TerrainType.DEEP_WATER,hexObj.getRow(),hexObj.getCol());
						discoveredKasimsHexagons.add(hex);
					}
				}
				if(visibleHexagons.size()>0){
					for(CoordinatePOJO hexObj : visibleHexagons) {
						Hexagon hex = new Hexagon(TerrainType.DEEP_WATER,hexObj.getRow(),hexObj.getCol());
						visibleKasimsHexagons.add(hex);
					}
				}
				p = new Player(entry.get("playerID").toString(), Integer.parseInt(entry.get("teamID").toString()), Integer.parseInt(entry.get("actionPoints").toString()), Integer.parseInt(entry.get("energy").toString()));
				
				p.setPlayerState(PlayerState.values()[Integer.parseInt(entry.get("status")==null?"0":entry.get("status").toString())]);
				
				p.setDiscoveredHexagons(discoveredKasimsHexagons);
				
				p.setVisibleHexagons(visibleKasimsHexagons);
				//Stats
				
				
				p.setUnitsCreated(Integer.parseInt(entry.get("unitsCreated")==null?"0":entry.get("unitsCreated").toString()));
				p.setUnitsDestroyed(Integer.parseInt(entry.get("unitsDestroyed")==null?"0":entry.get("unitsDestroyed").toString()));
				p.setUnitsLost(Integer.parseInt(entry.get("unitsLost")==null?"0":entry.get("unitsLost").toString()));
				p.setEnergyGathered(Integer.parseInt(entry.get("energyGathered")==null?"0":entry.get("energyGathered").toString()));
				ret.add(p);
			}
	    }
		return ret;
	}

	/**
	 * Retrieves production building from MongoDB
	 * @param players 
	 * @param map 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	private static List<AbstractProductionBuilding> retrieveProductionBuildings(String gameID, List<Player> players, HexagonalMap map) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		List<AbstractProductionBuilding> ret = new ArrayList<AbstractProductionBuilding>();
		DBObject entry;
		
		//Query to get list of entities from MongoDB
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID));
	    BasicDBObject keys = new BasicDBObject("productionBuildings", 1);
	    DBCursor cur = MongoGame.games.find(query,keys);
	    if(cur.hasNext()){
			DBObject curGame = cur.next();
			if(curGame.get("productionBuildings").toString() != "{ }"){
			BasicDBList e = (BasicDBList) curGame.get("productionBuildings");
			for(Object d : e){
				entry = (DBObject) d;
				//Create entity
				Class<? extends AbstractEntity> tbc = StringToClass.getClassFromString(entry.get("name").toString());
				Constructor<? extends AbstractEntity> c = null;
				c = tbc.getConstructor(Player.class,Hexagon.class);
				AbstractProductionBuilding etbc = null;
				String owner = entry.get("owner").toString();
					Player out = null;
					 for(Player p: players){
						 if(p.getPlayerID().equals(owner) ){
							 out = p;
						 }
					 }
					 DBObject hex = (DBObject) entry.get("hexagon");
					 Hexagon htbc = map.getCellAt(Integer.parseInt(hex.get("row").toString()), Integer.parseInt(hex.get("col").toString()));
					 etbc = (AbstractProductionBuilding) c.newInstance(new Object[]{out,htbc});
					 
					 etbc.setHealth(Integer.parseInt(entry.get("health").toString()));
				
				ret.add(etbc);
			}
			}
	    }
		return ret;
	}

	/**
	 * Retrieve units from MongoDB
	 * @param map 
	 * @param players 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	private static List<AbstractUnit> retrieveUnits(String gameID, List<Player> players, HexagonalMap map) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		List<AbstractUnit> ret = new ArrayList<AbstractUnit>();
		DBObject entry;
		
		//Query to get list of entities from MongoDB
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID));
	    BasicDBObject keys = new BasicDBObject("units", 1);
	    DBCursor cur = MongoGame.games.find(query,keys);
	    if(cur.hasNext()){
			DBObject curGame = cur.next();
			BasicDBList e = (BasicDBList) curGame.get("units");
			System.out.println("RetrieveUnits:"+e);
			AbstractUnit ee = null;
			for(Object d : e){
				entry = (DBObject) d;
				System.out.println(d);
				String name = (String)entry.get("name");
				String owner = (String)entry.get("owner");
				Integer health = (Integer)entry.get("health");
				
				// Maybe later
				//Integer defensiveDamage = (Integer) entry.get("defensiveDamage");
				//Integer offensiveDamage = (Integer) entry.get("offensiveDamage");
				//Integer movementRange = (Integer) entry.get("movementRange");
				//Integer attackRange = (Integer) entry.get("attackRange");
				//Integer actionPointCostPerCell = (Integer) entry.get("actionPointCostPerCell");
				//Integer attackActionPointCost = (Integer) entry.get("attackActionPointCost");
				
				DBObject position = (DBObject) entry.get("hexagon");	
				Integer row = (Integer) position.get("row");
				Integer col = (Integer) position.get("col");
				
				Hexagon hex = map.getCellAt(row, col);
				
				Player out = null;
				 for(Player p: players){
					 if(p.getPlayerID().equals(owner) ){
						 out = p;
					 }
				 }		
				 
				Class<? extends AbstractUnit> c = StringToClass.getUnitClassFromString(name);
				Constructor<? extends AbstractUnit> cc = c.getConstructor(Player.class,Hexagon.class); 
				
				ee = cc.newInstance(new Object[]{out,hex});
				ee.setHealth(health);
				System.out.println("RetrieveUnits:" + ee);
				ret.add(ee);
			}
	    }else{
	    	System.err.println("Game not found.");
	    }
		return ret;
	}
	
	/**
	 * Method that removes the game from MongoDB and it's details
	 * @param gameID
	 * @return
	 */
	public static boolean removeGame(String gameID){
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID));
	    MongoGame.games.remove(query);
		MongoDiscoveredHexagon.removeVisibleHexagons(gameID);
		MongoVisibleHexagon.removeVisibleHexagon(gameID);
		return true;
	}
	
	/** 
	 * Method that returns if a players is the owner of the game or not
	 * 
	 */
	public static boolean isOwner(String gameID, String playerID){
		BasicDBObject query = new BasicDBObject("_id",ObjectId.massageToObjectId(gameID)).append("owner", playerID);
	    BasicDBObject keys = new BasicDBObject("owner", 1);
	    DBCursor cur = MongoGame.games.find(query,keys);
	    if(cur.hasNext()){
	    	return true;
	    }else{
		    return false;	    	
	    }
	}
	
	/**
	 * Method that returns the list of player partecipating in a game
	 * @param gameID
	 * @return
	 */
	public static List<Player> getPlayersInGame(String gameID) {
		return MongoGame.retrievePlayers(gameID);
	}
}
