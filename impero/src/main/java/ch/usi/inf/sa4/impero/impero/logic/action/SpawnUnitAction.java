package ch.usi.inf.sa4.impero.impero.logic.action;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class SpawnUnitAction extends AbstractSpawnAction {

	private final AbstractEntity actor;

	/**
	 * 
	 * @param owner	owner of the new unit
	 * @param actionPointCost cost of the new unit
	 * @param target where to deploy the new unit
	 * @param actor ? ? ? ?
	 * @param spawned object to be spawned
	 * @param cost ? ? ? ?
	 */
	public SpawnUnitAction(final Player owner,
			final Integer actionPointCost, final Hexagon target,
			final AbstractEntity actor, final Class<?extends AbstractEntity> spawned,
			final Integer cost) {
		super(owner, actionPointCost, target, cost,spawned);
		this.actor = actor;
	}

	public AbstractEntity getActor() {
		return actor;
	}
}
