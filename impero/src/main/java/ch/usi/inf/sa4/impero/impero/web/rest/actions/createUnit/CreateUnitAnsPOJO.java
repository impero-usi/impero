/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.createUnit;

/**
 * @author jesper
 *
 */
public class CreateUnitAnsPOJO {
	
	private int cost;
	private boolean valid;
	private int energyCost;
	/**
	 * @param cost
	 * @param valid
	 */
	public CreateUnitAnsPOJO(int cost, boolean valid) {
		this.cost = cost;
		this.valid = valid;
	}

	
		
	public CreateUnitAnsPOJO() {}

	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

	/**
	 * @return the valid
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * @param valid the valid to set
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}



	/**
	 * @return the energyCost
	 */
	public int getEnergyCost() {
		return energyCost;
	}



	/**
	 * @param energyCost the energyCost to set
	 */
	public void setEnergyCost(int energyCost) {
		this.energyCost = energyCost;
	}
	
	
	
}
