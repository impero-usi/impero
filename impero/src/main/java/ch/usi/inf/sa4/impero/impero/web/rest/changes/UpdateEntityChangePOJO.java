package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;


public class UpdateEntityChangePOJO {

	private String owner;
	private CoordinatePOJO cell;
	private String type;
	private int health;
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public CoordinatePOJO getCell() {
		return cell;
	}
	public void setCell(CoordinatePOJO cell) {
		this.cell = cell;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}
	
	
}
