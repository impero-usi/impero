package ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

public class PathRequestAnsPOJO {
	
	List<CoordinatePOJO> path = new ArrayList<>();
	List<CoordinatePOJO> attack = new ArrayList<>();
	List<Integer> cost = new ArrayList<>();
	
	public PathRequestAnsPOJO(){}

	/**
	 * @return the path
	 */
	public List<CoordinatePOJO> getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(List<CoordinatePOJO> path) {
		this.path = path;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<CoordinatePOJO> getAttack(){
		return attack;
	}
	
	
	/**
	 * 
	 * @param attack
	 */
	public void setAttack(List<CoordinatePOJO> attack){
		this.attack = attack;
	}
	
	/**
	 * @param row
	 * @param col
	 */
	public void addCoordinate(int row, int col) {
		CoordinatePOJO p = new CoordinatePOJO();
		p.setCol(col);
		p.setRow(row);
		this.path.add(p);
	}

	/**
	 * 
	 */
	public void addAttackCordinate(int row, int col){
		CoordinatePOJO p = new CoordinatePOJO();
		p.setCol(col);
		p.setRow(row);
		this.attack.add(p);	
	}
	
	
	/**
	 * @return the cost
	 */
	public List<Integer> getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(List<Integer> cost) {
		this.cost = cost;
	}
	
	public void addCost(Integer cost) {
		this.cost.add(cost);
	}

}
