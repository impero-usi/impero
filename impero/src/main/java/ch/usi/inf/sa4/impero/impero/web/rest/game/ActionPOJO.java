package ch.usi.inf.sa4.impero.impero.web.rest.game;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

public class ActionPOJO {
	private Integer sequence;
	private String name;
	private Integer row;
	private Integer col;
	private String actionPoints;
	private List<CoordinatePOJO> path;
	private String type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getCol() {
		return col;
	}

	public void setCol(Integer i) {
		this.col = i;
	}

	public String getActionPoints() {
		return actionPoints;
	}

	public void setActionPoints(String actionPoints) {
		this.actionPoints = actionPoints;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	/**
	 * @return the path
	 */
	public List<CoordinatePOJO> getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(List<CoordinatePOJO> path) {
		this.path = path;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
}
