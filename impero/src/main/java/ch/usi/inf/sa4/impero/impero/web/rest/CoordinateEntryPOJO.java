package ch.usi.inf.sa4.impero.impero.web.rest;

import java.io.Serializable;

public class CoordinateEntryPOJO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer row;
	private Integer col;
	private Integer type;
	private Boolean discovered;
	
	public CoordinateEntryPOJO(){
		
	}
	
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public Integer getCol() {
		return col;
	}
	public void setCol(Integer col) {
		this.col = col;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Boolean getDiscovered() {
		return discovered;
	}
	public void setDiscovered(Boolean discovered) {
		this.discovered = discovered;
	}
	
	
}
