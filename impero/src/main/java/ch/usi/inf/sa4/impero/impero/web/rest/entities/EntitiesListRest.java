package ch.usi.inf.sa4.impero.impero.web.rest.entities;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.action.AttackAction;
import ch.usi.inf.sa4.impero.impero.logic.action.ConquerAction;
import ch.usi.inf.sa4.impero.impero.logic.action.MoveAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnBuildingAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnUnitAction;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.AvailableActionsMapper;
import ch.usi.inf.sa4.impero.impero.logic.mappers.SpawnableEntityMapper;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Pair;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Utilities;

/**
 * 
 * @author Kasim Bordogna
 *
 */

@Path("/game/entities/{gameID}")
public class EntitiesListRest {

	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public EntitiesListAnsPOJO getEntitiesList(@PathParam("gameID")  String gameID, EntitiesListReqPOJO request,@Context HttpServletRequest ctx){
		Game g = GameServer.getGame(gameID);	
		Utilities u  = g.getUtility();
		
		Class<? extends AbstractEntity> c = u.getClassFromRowCol(request.getRow(), request.getCol(),g.getUtility().getPlayerFromID(ctx.getSession().getAttribute("id").toString()));

		Hexagon hex = u.getCellAt(request.getRow(), request.getCol());
		
		EntitiesListAnsPOJO response = new EntitiesListAnsPOJO();
		
		
		// Movable?
		List<Class<? extends AbstractAction>> laction = AvailableActionsMapper.getActions(c);
		
		if(laction != null && laction.contains(MoveAction.class)){
			response.setCanMove(true);
		} else {
			response.setCanMove(false);
		}
		
		// Attacker?
		if(laction != null && laction.contains(AttackAction.class)){
			response.setCanAttack(true);
		} else {
			response.setCanAttack(false);
		}
		
		// Conquerer?
		if(laction != null && laction.contains(ConquerAction.class)){
			response.setCanConquer(true);
		} else {
			response.setCanConquer(false);
		}
		
		List<EntitiesDataAndCostPOJO> entities = new ArrayList<EntitiesDataAndCostPOJO>();
		if(laction != null && ( laction.contains(SpawnBuildingAction.class) || laction.contains(SpawnUnitAction.class))) {
			//Spawnable units/buildings
			Map<Class<? extends AbstractEntity>, Pair<Integer, Integer>> su = SpawnableEntityMapper.getSpawnableUnits(c, hex);
			
			// check that it is not null (by Jesper)
			if (su != null) {
				EntitiesDataAndCostPOJO temp;
				for(Class<? extends AbstractEntity> cc : su.keySet()){
					if(!(Modifier.isAbstract(c.getModifiers()))){
						temp = new EntitiesDataAndCostPOJO();
						temp.setActionPointCost(su.get(cc).getFirstElement());
						temp.setEnergyCost(su.get(cc).getSecondElement());
						temp.setName(cc.getSimpleName());
						entities.add(temp);
					}
				}
				
				response.setEntities(entities);
			}
		}
		
		return response;
	}
	
}
