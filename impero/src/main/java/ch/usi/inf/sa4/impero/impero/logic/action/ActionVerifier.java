package ch.usi.inf.sa4.impero.impero.logic.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractAirUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractGroundUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.EmptyEntity;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.mappers.MovementCostMapper;
import ch.usi.inf.sa4.impero.impero.logic.mappers.SpawnActionPointAndCostMapper;
import ch.usi.inf.sa4.impero.impero.logic.mappers.SpawnableEntityMapper;
import ch.usi.inf.sa4.impero.impero.logic.mappers.StringToClass;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Utilities;

/**
 * 
 * @author ChristianVuerich Class used to verify the request of action from the
 *         clients
 */
public class ActionVerifier {

	private final Utilities utility;
	private final Map<Player, List<AbstractAction>> actionList;
	private Map<Hexagon, Integer> mapcost;
	private List<Hexagon> moveHexagons;
	private List<Hexagon> attackHexagons;

	/**
	 * @param utilities
	 *            Utility object used to check some redundant conditions
	 * @param actionList
	 *            Map of player and list of actions, used to add new actions
	 */
	public ActionVerifier(final Utilities utilities,
			final Map<Player, List<AbstractAction>> actionList) {

		this.actionList = actionList;
		this.utility = utilities;
		this.moveHexagons = new ArrayList<>();
		this.attackHexagons = new ArrayList<>();

	}

	/**
	 * @param playerID
	 *            the ID of the player requesting the action
	 * @param row
	 *            the row where the entity that spawn the new unit
	 * @param col
	 *            the row where the entity that spawn the new unit
	 * @param type
	 *            the string representing the new unit you want to spawn
	 * @return a new SpawnUnitAction if all the condition passes, null in all
	 *         other cases
	 */
	public SpawnUnitAction verifySpawnUnitAction(final String playerID,
			final Integer row, final Integer col, final String type) {

		Player player = utility.getPlayer(playerID);
		final Hexagon cell = utility.getCellAt(row, col);

		if (cell != null && player != null) {
			final Class<? extends AbstractEntity> entity = utility
					.getClassFromRowCol(row, col, player);
			final Class<? extends AbstractEntity> unit = StringToClass
					.getClassFromString(type);
			if (Utilities
					.isSubClassOf(entity, AbstractProductionBuilding.class)) {
				if (SpawnableEntityMapper.getSpawnableUnits(entity, cell).keySet()
						.contains(unit)) {
					Map<Integer, Hexagon> neigbors = utility.getNeigbors(cell);
					Hexagon target;
					for (Integer key : neigbors.keySet()) {
						target = neigbors.get(key);
						if (!utility.cellOccupied(target.getRow(),
								target.getCol(), player, actionList)) {
							if (MovementCostMapper.isValidTerrainAndCost(unit,
									target.getType()) != -1) {
								final Integer actionPoints = SpawnActionPointAndCostMapper
										.getSpawnActionCost(unit);
								final Integer energyCost = SpawnActionPointAndCostMapper
										.getSpawnEnergyCost(unit);
								if (actionPoints <= player.getActionPoint()
										&& energyCost <= player.getEnergy()) {
									final AbstractEntity actor = utility
											.getEntityAt(row, col);
									if (actor.getOwner().equals(player)) {
										player.setEnergy(player.getEnergy()
												- energyCost);
										player.setActionPoint(player
												.getActionPoint()
												- actionPoints);
										final SpawnUnitAction action = new SpawnUnitAction(
												player, actionPoints, target,
												actor, unit, energyCost);
										actionList.get(player).add(action);
										return action;
									}
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param startRow
	 *            row of the starting cell
	 * @param startCol
	 *            column of the starting cell
	 * @param endRow
	 *            row of the end cell
	 * @param endCol
	 *            column of the end cell
	 * @param playerID
	 *            String representing the player requesting the action
	 * @return a new MoveAction if all the condition passes, null in all other
	 *         cases
	 */
	public MoveAction verifyMoveAction(final Integer startRow,
			final Integer startCol, final Integer endRow, final Integer endCol,
			final String playerID) {
		Hexagon start = utility.getCellAt(startRow, startCol);
		Hexagon end = utility.getCellAt(endRow, endCol);
		Player player = utility.getPlayer(playerID);
		AbstractEntity actor = utility.getEntityAt(startRow, startCol);
		if (end != null && start != null && player != null
				&& actor instanceof AbstractUnit && !utility.cellOccupied(endRow, endCol, player, actionList)) {
			final Integer actionPoints = mapcost.get(end);
			if(((AbstractUnit)actor).isActing()){
				return null;
			}
			if (actionPoints != null && actionPoints <= player.getActionPoint()) {
				if (actor.getOwner().equals(player)) {
					List<Hexagon> path = utility.getShortestPath(start, end);
					path.add(0, start);
					final MoveAction action = new MoveAction(player,
							actionPoints, end, (AbstractUnit) actor,
							path);
					player.setActionPoint(player.getActionPoint()
							- actionPoints);
					actionList.get(player).add(action);
					((AbstractUnit) actor).setActing(true);
					return action;
				}
			}

		}

		return null;
	}

	/**
	 * @param row
	 *            of the cell where the unit you want to move is
	 * @param col
	 *            of the cell where the unit you want to move is
	 * @param playerID
	 *            String representing the player requesting the action
	 * @return A map of all the valid cells you can move on and the cost to get
	 *         to it
	 */
	public Map<Hexagon, Integer> getMovementCells(final Integer row,
			final Integer col, final String playerID) {
		this.moveHexagons.clear();
		Player player = utility.getPlayer(playerID);
		Hexagon cell = utility.getCellAt(row, col);
		if (player != null && cell != null) {
			Class<? extends AbstractEntity> entity = utility
					.getClassFromRowCol(row, col, player);
			if (Utilities.isSubClassOf(entity, AbstractUnit.class)) {
				AbstractUnit unit = (AbstractUnit) utility
						.getEntityAt(row, col);
				if (unit.isActing()) {
					return new HashMap<Hexagon, Integer>();
				}
				if (unit != null) {
					Map<Hexagon, Integer> map = utility.getAllCosts(row, col,
							player.getActionPoint(), unit.getMovementRange(),
							unit, Utilities.isSubClassOf(StringToClass.getClassFromString(unit.getName()), AbstractAirUnit.class));
					
					List<Hexagon> set1 = utility.getNeighboursInRange(row, col, unit.getMovementRange()-1);
					List<Hexagon> set2 = utility.getNeighboursInRange(row, col, unit.getMovementRange());
					
					set2.removeAll(set1);
					
					for(Hexagon hex : set2){
						if(utility.cellOccupied(row, col, player, actionList)){
							map.remove(hex);
						}
					}
					
					this.mapcost = map;
					this.moveHexagons.addAll(map.keySet());
					return map;
				}
			}
		}
		return null;
	}

	/**
	 * @param row
	 *            of the cell where the unit you want to attack with
	 * @param col
	 *            of the cell where the unit you want to attack with
	 * @param playerID
	 *            String representing the player requesting the action
	 * @return List of cell where there are valid targets
	 */
	public List<Hexagon> getAttackCells(final Integer row, final Integer col,
			final String playerID) {
		this.attackHexagons.clear();
		AbstractEntity entity = utility.getEntityAt(row, col);
		Player player = utility.getPlayer(playerID);
		if (entity != null && player != null && entity instanceof AbstractUnit) {
			if (((AbstractUnit) entity).isActing()) {
				return new ArrayList<>();
			} else {
				List<Hexagon> list = utility.getNeighboursInRange(row, col,
						((AbstractUnit) entity).getAttackRange());
				final List<Hexagon> result = new ArrayList<>();
				for (Hexagon cell : list) {
					AbstractEntity enemy = utility.getEntityAt(cell.getRow(),
							cell.getCol());
					if ((utility.cellOccupied(cell.getRow(), cell.getCol(),
							null, null) && utility.isEnemyEntity(cell, player) && utility
								.canAttack(((AbstractUnit) entity), enemy)) ){
							//|| cell.getType() == TerrainType.ROAD) {
						result.add(cell);
					}
				}
				this.attackHexagons.addAll(result);
				return result;
			}
		}
		return null;
	}

	/**
	 * @param playerID
	 *            String representing the player requesting the action
	 * @param targetRow
	 *            of the cell where the unit you want to attack
	 * @param targetCol
	 *            of the cell where the unit you want to attack
	 * @param row
	 *            of the cell where the unit you want to attack with
	 * @param col
	 *            of the cell where the unit you want to attack with
	 * @return a new AttackAction if all the condition passes, null in all other
	 *         cases
	 */
	public AttackAction verifyAttackAction(final String playerID,
			final Integer targetRow, final Integer targetCol,
			final Integer row, final Integer col) {
		AbstractEntity entity = utility.getEntityAt(row, col);
		AbstractEntity target = utility.getEntityAt(targetRow, targetCol);
		Player player = utility.getPlayer(playerID);
		if (entity != null
				&& entity instanceof AbstractUnit
				&& player != null
				&& (utility.getCellAt(targetRow, targetCol).getType() == TerrainType.ROAD || (target != null && !target
						.getOwner().equals(player)))) {
			final AbstractUnit unit = (AbstractUnit) entity;
			if(unit.isActing()){
				return null;
			}
			if (unit.getAttackActionPointCost() <= player.getActionPoint()) {
				if (unit.getOwner().equals(player)) {
					player.setActionPoint(player.getActionPoint()
							- unit.getAttackActionPointCost());
					final AttackAction action = new AttackAction(player,
							unit.getAttackActionPointCost(),
							utility.getCellAt(targetRow, targetCol), unit);
					actionList.get(player).add(action);
					unit.setActing(true);
					return action;
				}
			}
		}
		return null;
	}

	/**
	 * @param row
	 *            of the cell where the player want to spawn a building
	 * @param col
	 *            of the cell where the player want to spawn a building
	 * @param type
	 *            string representing the new building you want to spawn
	 * @param playerID
	 *            String representing the player requesting the action
	 * @return a new SpawnBuildingAction if all the condition passes, null in
	 *         all other cases
	 */
	public SpawnBuildingAction verifySpawnBuildingAction(final Integer row,
			final Integer col, final String type, final String playerID) {
		Player player = utility.getPlayer(playerID);
		final Hexagon cell = utility.getCellAt(row, col);
		if (cell != null && player != null) {
			final Class<? extends AbstractEntity> entity = utility
					.getClassFromRowCol(row, col, player);
			final Class<? extends AbstractEntity> unit = StringToClass
					.getClassFromString(type);
			if ((Utilities.isSubClassOf(entity, EmptyEntity.class))) {
				if (SpawnableEntityMapper.getSpawnableUnits(entity, cell).keySet()
						.contains(unit)) {
					if (!utility.cellOccupied(row, col, player, actionList)) {
						if (MovementCostMapper.isValidTerrainAndCost(unit,
								cell.getType()) != -1) {
							final Integer actionPoints = SpawnActionPointAndCostMapper
									.getSpawnActionCost(unit);
							final Integer energyCost = SpawnActionPointAndCostMapper
									.getSpawnEnergyCost(unit);
							if (actionPoints <= player.getActionPoint()
									&& energyCost <= player.getEnergy()) {
								player.setEnergy(player.getEnergy()
										- energyCost);
								player.setActionPoint(player.getActionPoint()
										- actionPoints);
								final SpawnBuildingAction action = new SpawnBuildingAction(
										player, actionPoints, cell, energyCost, unit);
								actionList.get(player).add(action);
								return action;
							}
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Pop the action at index actionIndex1 and insert it at actionIndex2
	 * 
	 * @param actionIndex1
	 *            Integer: action you want to move.
	 * @param actionIndex2
	 *            Integer: The index of where you want to move it.
	 * @param playerName
	 *            String: The player's name.
	 * @return true if the action was deleted, false all other cases
	 */
	public boolean popInstert(Integer actionIndex1, Integer actionIndex2,
			String playerID) {
		Player user = utility.getPlayer(playerID);
		if (user != null && actionIndex1 >= 0 && actionIndex2 >= 0) {
			List<AbstractAction> playerActions = this.actionList.get(user);
			if (actionIndex1 != actionIndex2
					&& actionIndex1 < playerActions.size()
					&& actionIndex2 < playerActions.size()) {
				AbstractAction temp = playerActions.remove(actionIndex1
						.intValue());
				playerActions.add(actionIndex2, temp);
				return true;
			}
		}
		return false;
	}

	/**
	 * Deletes an action from a player's list of actions.
	 * 
	 * @param actionIndex
	 *            Integer: The action to be deleted.
	 * @param playerName
	 *            Sting: The player's name.
	 * @return a list containing the new resources of the player: action points,
	 *         energy.
	 */
	public List<Integer> deleteAction(Integer actionIndex, String playerID) {
		Player user = utility.getPlayer(playerID);
		if (user != null && actionIndex >= 0) {
			List<AbstractAction> playerActions = this.actionList.get(user);
			if (actionIndex < playerActions.size()) {
				final AbstractAction action = playerActions.get(actionIndex);
				user.setActionPoint(user.getActionPoint()
						+ action.getActionPointCost());
				if (Utilities.isSubClassOf(action.getClass(),
						AbstractSpawnAction.class)) {
					user.setEnergy(user.getEnergy()
							+ ((AbstractSpawnAction) action).getCost());
				}
				if (playerActions.get(actionIndex) instanceof AttackAction) {
					((AttackAction) playerActions.get(actionIndex)).getActor()
							.setActing(false);
				} else if (playerActions.get(actionIndex) instanceof MoveAction) {
					((MoveAction) playerActions.get(actionIndex)).getActor()
							.setActing(false);
				}
				playerActions.remove((int) actionIndex);
				final List<Integer> result = new ArrayList<>();
				result.add(utility.getPlayer(playerID).getActionPoint());
				result.add(utility.getPlayer(playerID).getEnergy());
				return result;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param row
	 *            of the cell where the unit you want to conquer with
	 * @param col
	 *            of the cell where the unit you want to conquer with
	 * @param playerID
	 *            String representing the player requesting the action
	 * @return List of cell where there are valid targets
	 */
	public List<Hexagon> getConquerCells(final Integer row, final Integer col,
			final String playerID) {
		AbstractEntity entity = utility.getEntityAt(row, col);
		Player player = utility.getPlayer(playerID);
		if (entity != null && player != null && entity instanceof AbstractUnit) {
			List<Hexagon> list = utility.getNeighboursInRange(row, col,
					((AbstractUnit) entity).getAttackRange());
			final List<Hexagon> result = new ArrayList<>();
			for (Hexagon cell : list) {
				AbstractEntity enemy = utility.getEntityAt(cell.getRow(), cell.getCol());
				if (utility.cellOccupied(cell.getRow(), cell.getCol(), null,
						null)
						&& utility.isEnemyEntity(cell, player)
						&& Utilities.isSubClassOf(
								utility.getEntityAt(cell.getRow(),
										cell.getCol()).getClass(),
								AbstractBuilding.class)
								&& Utilities.isSubClassOf(StringToClass.getClassFromString(entity.getName()), AbstractGroundUnit.class)
								&& utility.canAttack(((AbstractUnit)entity), enemy)) {
					result.add(cell);
				}
			}
			return result;
		}
		return null;
	}

	/**
	 * 
	 * @param playerID
	 *            String representing the player requesting the action
	 * @param targetRow
	 *            of the cell where the unit you want to conquer
	 * @param targetCol
	 *            of the cell where the unit you want to conquer
	 * @param row
	 *            of the cell where the unit you want to conquer with
	 * @param col
	 *            of the cell where the unit you want to conquer with
	 * @return a new ConquerAction if all the condition passes, null in all
	 *         other cases
	 */
	public ConquerAction verifyConquerAction(final String playerID,
			final Integer targetRow, final Integer targetCol,
			final Integer row, final Integer col) {
		AbstractEntity entity = utility.getEntityAt(row, col);
		AbstractEntity target = utility.getEntityAt(targetRow, targetCol);
		Player player = utility.getPlayer(playerID);
		if (entity != null && entity instanceof AbstractUnit && target != null
				&& player != null && !target.getOwner().equals(player)) {
			final AbstractUnit unit = (AbstractUnit) entity;
			if (unit.getAttackActionPointCost() <= player.getActionPoint()) {
				if (unit.getOwner().equals(player)) {
					player.setActionPoint(player.getActionPoint()
							- unit.getAttackActionPointCost());
					final ConquerAction action = new ConquerAction(player,
							unit.getAttackActionPointCost(),
							target.getHexagon(), unit);
					actionList.get(player).add(action);
					return action;
				}
			}
		}
		return null;
	}

	public boolean isAttack(final Integer row, final Integer col) {
		return attackHexagons.contains(utility.getCellAt(row, col));
	}

}
