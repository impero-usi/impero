package ch.usi.inf.sa4.impero.impero.logic.changes;

import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class EconomyChange implements AbstractChange {
	//the cell where the removed entity is
	private final Integer newEnergy;
	private final Player player;
	private final Integer actionPoints;
	
	/**
	 * Constructor.
	 * @param cell
	 */
	public EconomyChange(final Player player, final Integer newEnergy, final Integer actionPoints) {
		this.player = player;
		this.newEnergy = newEnergy;
		this.actionPoints = actionPoints;
	}
	
	/**
	 * Getter for the field cell
	 * @return
	 */
	public Player getPlayer() {
		return player;
	}

	public Integer getNewEnergy() {
		return newEnergy;
	}

	public Integer getActionPoints() {
		return actionPoints;
	}

}
