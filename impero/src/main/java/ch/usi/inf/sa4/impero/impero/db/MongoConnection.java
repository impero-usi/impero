package ch.usi.inf.sa4.impero.impero.db;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class MongoConnection {
	
	private static String hostname = "atelier.inf.usi.ch";
	private static int port = 3000;
	private static String dbname = "impero";
	private static String username = "impero";
	private static DB db;
	private static char[] password = "1mp3r0.".toCharArray();
	private static boolean auth = false;

	static{
		MongoClient mongoClient;
		try {
			mongoClient = new MongoClient( MongoConnection.hostname , MongoConnection.port );
			MongoConnection.db = mongoClient.getDB( MongoConnection.dbname);
			MongoConnection.auth = MongoConnection.db.authenticate(MongoConnection.username,MongoConnection.password);
		} catch (UnknownHostException e) {
		}
	}
	
	public static DB getDB(){
		return MongoConnection.db;
	}
	
	public static boolean isAuth(){
		return MongoConnection.auth;
	}
}
