package ch.usi.inf.sa4.impero.impero.logic.entities;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public abstract class AbstractGroundUnit extends AbstractUnit {
	/**
	 * This is the constructor for the AbstractGroundUnit class.
	 * 
	 * @param health
	 *            the initial health of the entity.
	 * @param owner
	 *            the player who creates the entity.
	 * @param hexagon
	 *            the cell where the entity is created.
	 * @param name
	 *            the name of the type of the entity.
	 * @param offensiveDamage
	 *            the base damage that the unit can do when it attacks an enemy
	 *            entity.
	 * @param defensiveDamage
	 *            the base damage that the unit can do when it is attacked by an
	 *            enemy unit.
	 * @param movementRange
	 *            the maximum movement range of the unit.
	 * @param attackRange
	 *            the maximum attack range of the unit.
	 */
	public AbstractGroundUnit(final Player owner, final Hexagon hexagon,
			final String name, final Integer health, final Integer attackCost,
			final Integer attackRange, final Integer movementRange,
			final Integer offensiveDamage, final Integer defensiveDamage,
			final Integer visibleRange,
			final List<Class<? extends AbstractEntity>> attackClass,
			final Integer maxHealth) {
		super(owner, hexagon, name, health, attackCost, attackRange,
				movementRange, offensiveDamage, defensiveDamage, visibleRange, attackClass, maxHealth);
	}
}
