package ch.usi.inf.sa4.impero.impero.logic.action;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
/**
 * This class model the spawn action action of one player.
 * @author ChristianVuerich
 *
 */
public class SpawnBuildingAction extends AbstractSpawnAction {

	
	public SpawnBuildingAction(Player owner,
			Integer actionPointCost, Hexagon target, final Integer cost, final Class<?extends AbstractEntity> spawned) {
		super(owner, actionPointCost, target, cost, spawned);

	}
}
