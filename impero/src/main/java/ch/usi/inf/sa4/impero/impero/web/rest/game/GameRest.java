package ch.usi.inf.sa4.impero.impero.web.rest.game;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.db.MongoFriend;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.db.MongoPlayer;
import ch.usi.inf.sa4.impero.impero.db.PlayerEntryPOJO;
import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.action.AbstractSpawnAction;
import ch.usi.inf.sa4.impero.impero.logic.action.AttackAction;
import ch.usi.inf.sa4.impero.impero.logic.action.ConquerAction;
import ch.usi.inf.sa4.impero.impero.logic.action.MoveAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnBuildingAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnUnitAction;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinateEntryPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.EntityPOJO;

@Path("/game/{gameID}")
public class GameRest {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public GameAnsPOJO getGame(@PathParam("gameID") String gameID,
			@Context HttpServletRequest request) {
		// Get the game from GameServer
		System.out.println("received get game request for game " + gameID);
		Game g;
		// TODO Replace this fake game sooner or later
		if (gameID.equals("123123") && !GameServer.containsGame("123123")) {
			List<String> ll = new ArrayList<String>();
			ll.add("5346a7ed0364b9a1436f6f1f");
			ll.add("5346a81f03649f076148271d");
			ll.add("5346640303643dd80eb5ea8d");
			g = new Game(80, 60, 100, 100, ll, "FAKE", 50, 10, 10, 5, false,
					false);
			GameServer.putGame(gameID, g);
		} else {
			g = GameServer.getGame(gameID);
		}
		// Find the player according to ID

		System.err.println("request : "
				+ request.getSession(false).getAttribute("id").toString());
		Player thisPLayer = g.getUtility().getPlayerFromID(
				request.getSession(false).getAttribute("id").toString());
		if (thisPLayer == null) {
			return null;
		}
		// Start building the answer object

		GameAnsPOJO answer = new GameAnsPOJO();
		
		// Get the game name
		answer.setGameID(gameID);
		// Get the game name
		answer.setName(MongoGame.getGameName(gameID));
		answer.setHeight(g.getMap().getHeight());
		answer.setWidth(g.getMap().getWidth());

		// Add map
		List<CoordinateEntryPOJO> list = new ArrayList<CoordinateEntryPOJO>();
		CoordinateEntryPOJO e;

		
		// Jesper gets the whole map
		if(thisPLayer.getPlayerID().equals("5345b930036405f8f43ade4a")){
			System.out.println("Jesper gets the whole map uncovered");
				for (Hexagon h : g.getMap().get1DMap()) {
					e = new CoordinateEntryPOJO();
					e.setCol(h.getCol());
					e.setRow(h.getRow());
					e.setType(h.getType().ordinal());
					e.setDiscovered(h.isDiscovered());
					list.add(e);
				}
		}else{
			for (Hexagon h : g.getPlayerMap(thisPLayer)) {
				e = new CoordinateEntryPOJO();
				e.setCol(h.getCol());
				e.setRow(h.getRow());
				e.setType(h.getType().ordinal());
				e.setDiscovered(h.isDiscovered());
				list.add(e);
			}
			
		}

		answer.setMap(list);

		// Add entities
		List<EntityPOJO> le = new ArrayList<EntityPOJO>();
		EntityPOJO ee;

		// Simple buildings
		List<AbstractBuilding> b = g.getVisibleBuildingList(thisPLayer);
		for (AbstractBuilding bb : b) {
			ee = new EntityPOJO();
			ee.setType(bb.getName());
			ee.setOwner(bb.getOwner().getPlayerID());
			ee.setRow(bb.getHexagon().getRow());
			ee.setCol(bb.getHexagon().getCol());
			ee.setHealth(bb.getHealth());
			ee.setMaxHealth(bb.getMaxHealth());
			le.add(ee);
		}
		// Production buildings
		List<AbstractProductionBuilding> pb = g
				.getVisibleProductionBuildingList(thisPLayer);
		for (AbstractProductionBuilding pbb : pb) {
			ee = new EntityPOJO();
			ee.setType(pbb.getName());
			ee.setOwner(pbb.getOwner().getPlayerID());
			ee.setRow(pbb.getHexagon().getRow());
			ee.setCol(pbb.getHexagon().getCol());
			ee.setHealth(pbb.getHealth());
			ee.setMaxHealth(pbb.getMaxHealth());
			le.add(ee);
		}
		answer.setEntities(le);
		// Units
		List<AbstractUnit> u = g.getVisibleUnitList(thisPLayer);
		for (AbstractUnit uu : u) {
			ee = new EntityPOJO();
			ee.setType(uu.getName());
			ee.setOwner(uu.getOwner().getPlayerID());
			ee.setRow(uu.getHexagon().getRow());
			ee.setCol(uu.getHexagon().getCol());
			ee.setHealth(uu.getHealth());
			ee.setMaxHealth(uu.getMaxHealth());
			ee.setOrientation(uu.getOrientation());

			// get attack class and put in list
			List<Class<? extends AbstractEntity>> attacks = uu.getAttackClass();
			List<String> attackClasses = new ArrayList<>();
			for (Class<? extends AbstractEntity> entityClass : attacks) {
				attackClasses.add(entityClass.getSimpleName());
			}
			ee.setAttackClass(attackClasses);

			le.add(ee);
			System.out.println("GameRest:" + uu);
		}
		answer.setEntities(le);

		// Players
		List<PlayerEntryPOJO> playersPOJOList = new ArrayList<PlayerEntryPOJO>();
		List<Player> p = g.getPlayerList();
		System.out.println("Players2:" + g.getPlayerList().size());
		PlayerEntryPOJO entry;
		for (Player pp : p) {
			entry = MongoPlayer.getPlayer(pp.getPlayerID());
			entry.setActionPoints(pp.getActionPoint().toString());
			entry.setEnergy(pp.getEnergy().toString());
			entry.setStatus(pp.getPlayerState().name());
			playersPOJOList.add(entry);
		}

		// Adding players previous actions
		List<AbstractAction> actions = g.getActionList().get(thisPLayer);
		System.out.println("List of actions for player"+ thisPLayer.getPlayerID()+" :"+actions.size());
		List<ActionPOJO> actionsP = new ArrayList<ActionPOJO>();
		ActionPOJO aentry;
		Integer c = 0;
		if(actions != null && actions.size()>0){
			for (AbstractAction a : actions) {
				aentry = new ActionPOJO();
				aentry.setName(a.getClass().getSimpleName());
				aentry.setCol(a.getTarget().getCol());
				aentry.setRow(a.getTarget().getRow());
				aentry.setActionPoints(a.getActionPointCost().toString());
				aentry.setSequence(c);
				
				// add type to be spawned
				if(AbstractSpawnAction.class.isAssignableFrom(a.getClass())) {
					aentry.setType(((AbstractSpawnAction)a).getSpawned().getSimpleName());
				}
				
				// add path if moveAction
				if(a.getClass().equals(MoveAction.class)) {
					CoordinatePOJO hex;
					List<CoordinatePOJO> pathPOJO = new ArrayList<>();
					List<Hexagon> path = ((MoveAction)a).getPath();
					
					for(Hexagon h : path) {
						hex = new CoordinatePOJO();
						hex.setCol(h.getCol());
						hex.setRow(h.getRow());
						pathPOJO.add(hex);
					}
					
					aentry.setPath(pathPOJO);
				}
				
				c++;
				actionsP.add(aentry);
			}
			answer.setCurrentActions(actionsP);
		}

		answer.setPlayers(playersPOJOList);
		
		MongoGame.persistGame(g);
		return answer;
	}

	@DELETE
	@Produces({ "application/json" })
	public boolean deleteGame(@Context HttpServletRequest request,
			@PathParam("gameID") String gameID) {
		// Check if player is owner of the game
		if (MongoGame.isOwner(gameID,
				request.getSession(false).getAttribute("id").toString())) {
			return MongoGame.removeGame(gameID);

		} else {
			System.err.println("Only owners can delete a game");
			return false;
		}
	}
}
