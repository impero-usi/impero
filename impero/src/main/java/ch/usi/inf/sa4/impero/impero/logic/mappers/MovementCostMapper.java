package ch.usi.inf.sa4.impero.impero.logic.mappers;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.EEF;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.entities.Harbour;
import ch.usi.inf.sa4.impero.impero.logic.entities.Battleship;
import ch.usi.inf.sa4.impero.impero.logic.entities.RadarTower;
import ch.usi.inf.sa4.impero.impero.logic.entities.Road;
import ch.usi.inf.sa4.impero.impero.logic.entities.Soldier;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;

public class MovementCostMapper {

	private static Map<Class<? extends AbstractEntity>, Map<TerrainType, Integer>> movementMap = new HashMap<>();

	static {
		movementMap.put(Tank.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.FIELD, 2); 
			put(TerrainType.FOREST, 3);
			put(TerrainType.SAND, 2);
			put(TerrainType.ROAD, 1);
			put(TerrainType.ENERGY_RESOURCE, 3);
		}});
		movementMap.put(Bomber.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.FIELD, 1); 
			put(TerrainType.FOREST, 1);
			put(TerrainType.SAND, 1);
			put(TerrainType.MOUNTAIN, 1);
			put(TerrainType.DEEP_WATER, 1);
			put(TerrainType.SHALLOW_WATER, 1);
			put(TerrainType.ROAD, 1);
			put(TerrainType.ENERGY_RESOURCE, 1);
		}});
		movementMap.put(Battleship.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.DEEP_WATER, 1);
			put(TerrainType.SHALLOW_WATER, 2);
		}});
		movementMap.put(EEF.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.ENERGY_RESOURCE, 1);
		}});
		movementMap.put(HQ.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.FIELD, 1); 
			put(TerrainType.FOREST, 1);
			put(TerrainType.SAND, 1);
		}});
		movementMap.put(Road.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.FIELD, 1); 
			put(TerrainType.FOREST, 1);
			put(TerrainType.SAND, 1);
			put(TerrainType.MOUNTAIN, 1);
			put(TerrainType.SHALLOW_WATER, 1);
		}});
		movementMap.put(RadarTower.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.FIELD, 1); 
			put(TerrainType.FOREST, 1);
			put(TerrainType.SAND, 1);
			put(TerrainType.MOUNTAIN, 1);
		}});
		movementMap.put(Harbour.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.SHALLOW_WATER, 1);
		}});
		movementMap.put(Soldier.class, new HashMap<TerrainType, Integer>(){{
			put(TerrainType.FIELD, 2); 
			put(TerrainType.FOREST, 2);
			put(TerrainType.SAND, 2);
			put(TerrainType.ROAD, 1);
			put(TerrainType.ENERGY_RESOURCE, 3);
		}});
		

	}

	public static Integer isValidTerrainAndCost(
			final Class<? extends AbstractEntity> entity, TerrainType terrain) {
		Map<TerrainType, Integer> map = movementMap.get(entity);
		Integer cost = map.get(terrain);
		if(!(cost == null)){
			return cost;
		}
		else{
			return -1;
		}
	}

}
