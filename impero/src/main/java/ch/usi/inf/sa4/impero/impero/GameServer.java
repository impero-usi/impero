package ch.usi.inf.sa4.impero.impero;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;

/**
 * Class to keep track of ongoing games
 * 
 * @author jesper
 *
 */
public class GameServer{
	
	private static Map<String,Game> games = new HashMap<>();
		
	/**
	 * add a new game
	 * 
	 * @param gameId
	 * @param game
	 * @return the game that was put
	 */
	public static Game putGame(String gameId, Game game) {
		return games.put(gameId, game);
	}
	
	/**
	 * remove a game
	 * 
	 * @param gameId
	 * @return the game that was removed
	 */
	public static Game removeGame(String gameId) {
		return games.remove(gameId);
	}
	
	/**
	 * @param gameId
	 * @return
	 */
	public static boolean containsGame(String gameId) {
		return games.containsKey(gameId);
	}
	
	/**
	 * @param gameID
	 * @return
	 */
	public static Game getGame(String gameID) {
		Game g;
		if((g=games.get(gameID)) == null){
			
			//Try to load game from mongo
			g = MongoGame.retrieveGame(gameID);
			
			GameServer.putGame(gameID, g);
			
			System.out.println("Game "+ gameID +" restored from MONGO into MEMORY");
		}else{
			System.out.println("Game " + gameID + " restored from MEMORY");
		}
		return g;
	}

}
