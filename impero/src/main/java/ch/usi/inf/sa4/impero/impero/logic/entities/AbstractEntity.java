package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * This is the top level class of the Entity hierarchy, it defines the basic
 * attributes of every entity(units and buildings).
 * 
 * @author Matteo Morisoli
 */
public abstract class AbstractEntity {

	/**
	 * this is the health field, it defines the quantity of health that the unit
	 * has.
	 */
	private Integer health;
	
	/**
	 * this is the health field, it defines the quantity of health that the unit
	 * has.
	 */
	private final Integer maxHealth;
	
	/**
	 * this field is a reference to the player who owns the entity.
	 */
	protected Player owner;
	/**
	 * this field is a reference to the current hexagon the entity is on.
	 */
	private Hexagon hexagon;
	/**
	 * this field is the name of the type of unit.
	 */
	private final String name;

	/**
	 * Visible range of an Entity
	 */
	private final Integer visibleRange;

	/**
	 * @return the visibleRange
	 */
	public Integer getVisibleRange() {
		return visibleRange;
	}

	/**
	 * Constructor for the AbstractEntity class.
	 * 
	 * @param health
	 *            the initial health of the entity.
	 * @param owner
	 *            the player who creates the entity.
	 * @param hexagon
	 *            the cell where the entity is created.
	 * @param name
	 *            the name of the type of the entity.
	 */
	public AbstractEntity(final Integer health, final Player owner,
			final Hexagon hexagon, final String name, final Integer visibleRange, final Integer maxHealth) {
		this.setHealth(health);
		this.owner = owner;
		this.setHexagon(hexagon);
		this.name = name;
		this.visibleRange = visibleRange;
		this.maxHealth = maxHealth;
	}

	/**
	 * the getter method for the health field.
	 * 
	 * @return the health of the Entity.
	 */
	public Integer getHealth() {
		return this.health;
	}

	/**
	 * the setter method for the health field.
	 * 
	 * @param health
	 *            the new health of the entity.
	 */
	public void setHealth(Integer health) {
		this.health = health;
	}

	/**
	 * the getter method for the player field.
	 * 
	 * @return the Player owning of the Entity.
	 */
	public Player getOwner() {
		return owner;
	}

	/**
	 * the getter method for the hexagon field.
	 * 
	 * @return the hexagon the Entity is currently.
	 */
	public Hexagon getHexagon() {
		return hexagon;
	}

	/**
	 * the setter method for the hexagon field.
	 * 
	 * @param hexagon
	 *            the new hexagon the unit is on.
	 */
	public void setHexagon(Hexagon hexagon) {
		this.hexagon = hexagon;
	}

	/**
	 * the getter method for the name field.
	 * 
	 * @return the name of the unit.
	 */
	public String getName() {
		return name;
	}

	public Integer getMaxHealth() {
		return maxHealth;
	}

}
