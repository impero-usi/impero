package ch.usi.inf.sa4.impero.impero.logic.rpg;

import java.util.HashMap;
import java.util.Map;

/**
 * A class to convert class names we use in the code with real names.
 * 
 * @author Orestis Melkonian
 */
public final class CodeToRealNameConverter {
	/**
	 * Maps code names to real names (initialized in the constructor).
	 */
	private Map<String, String> nameMapper = new HashMap<String, String>();

	/**
	 * Constructor for initializing the map.
	 */
	private CodeToRealNameConverter() {
		this.nameMapper.put("GroundUnit01", "M4_Sherman");
		this.nameMapper.put("GroundUnit02", "Leopard II");
		this.nameMapper.put("GroundUnit03", "Tiger II");
		this.nameMapper.put("GroundUnit04", "MOWAG Piranha");
		this.nameMapper.put("GroundUnit05", "VT Tank");
		this.nameMapper.put("GroundUnit06", "M1 Abrams");
		this.nameMapper.put("GroundUnit07", "Panzer 68");
		this.nameMapper.put("GroundUnit08", "Jeep Willys");
		this.nameMapper.put("GroundUnit09", "Oshkosh M-ATV");
		this.nameMapper.put("GroundUnit10", "R-11 Refueler");
		this.nameMapper.put("NavalUnit01", "USS Port Royal (CG-73)");
		this.nameMapper.put("NavalUnit02", "HMCS Algonquin (DDG 283)");
		this.nameMapper.put("NavalUnit03", "USS Gerald R. Ford (CVN-78)");
		this.nameMapper.put("NavalUnit04", "USS Wasp (LHD-1)");
		this.nameMapper.put("NavalUnit05", "Admiral Vinogradov");
		this.nameMapper.put("NavalUnit06", "Ashigara (DDG-178)");
		this.nameMapper.put("NavalUnit07", "HMS Vanguard");
		this.nameMapper.put("NavalUnit08", "USS Michigan (SSBN-727)");
		this.nameMapper.put("NavalUnit09", "USS Virginia (SSN-774)");
	}

	/**
	 * @param codeName
	 *            The name of the class in the code.
	 * @return The real name of the given class.
	 */
	public static String getRealName(final String codeName) {
		CodeToRealNameConverter c = new CodeToRealNameConverter();
		return c.nameMapper.get(codeName);
	}
}
