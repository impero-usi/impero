/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

/**
 * @author jesper
 *
 */
public class MoveActionAnsPOJO {
	
	private int cost;
	private boolean valid;
	private boolean attack;
	private List<CoordinatePOJO> path;
		
	public MoveActionAnsPOJO() {}

	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

	/**
	 * @return the valid
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * @param valid the valid to set
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	/**
	 * 
	 * @return
	 */	
	public List<CoordinatePOJO> getPath(){
		return this.path;
	}
	
	public void setPath(List<CoordinatePOJO> path) {
		this.path = path;
	}

	public boolean isAttack() {
		return attack;
	}

	public void setAttack(boolean attack) {
		this.attack = attack;
	}

}
