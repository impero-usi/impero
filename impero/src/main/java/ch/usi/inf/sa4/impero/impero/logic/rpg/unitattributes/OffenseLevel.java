package ch.usi.inf.sa4.impero.impero.logic.rpg.unitattributes;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The available levels of the offense attribute of units.
 * @author Orestis Melkonian
 */
public enum OffenseLevel {
	INSANE_WEAK(10),
	CRAZY_WEAK(20),
	VERY_WEAK(30),
	LITTLE_WEAK(40),
	MEDIUM(50),
	LITTLE_STRONG(60),
	VERY_STRONG(70),
	CRAZY_STRONG(80),
	INSANE_STRONG(90);

	/**
	 * Map from integer values to level enum expressions.
	 */
     private static final Map<Integer, OffenseLevel> LOOKUP 
          = new HashMap<Integer, OffenseLevel>();

     static {
          for (OffenseLevel ol : EnumSet.allOf(OffenseLevel.class)) {
        	  LOOKUP.put(ol.getOffencePower(), ol);
          }
               
     }
     /**
      * The offense of the current unit.
      */
     private int offensePower;
     /**
      * Constructor.
      * @param op The integer associated with the current attribute level.
      */
     private OffenseLevel(final int op) {
          this.offensePower = op;
     }
     /**
      * Getter for offense.
      * @return The current level's offense.
      */
     public int getOffencePower() { 
    	 return this.offensePower; 
     }
}
