package ch.usi.inf.sa4.impero.impero.logic.player;
/**
 * Enum for the player's states
 * @author Matteo Morisoli
 *
 */


public enum PlayerState {
	PLAYING,
	FINISHED,
	DEFEATED,
	WON;
}
