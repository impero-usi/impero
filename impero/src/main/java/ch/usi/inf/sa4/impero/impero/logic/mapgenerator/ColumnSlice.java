package ch.usi.inf.sa4.impero.impero.logic.mapgenerator;

/**
 * @author Kasim and Jesper
 * 
 * A slice of a column of a map
 *
 */
public class ColumnSlice {
	private final int type ;
	private final int length;
	
	/**
	 * @param type - type of terrain
	 * @param length - number of cells
	 */
	public ColumnSlice(int type, int length){
		this.type = type;
		this.length = length;
		
	}
	/**
	 * get the type of the slice
	 * 
	 * @return - the type of the slice
	 */
	public int getType() {
		return this.type;
	}
	/**
	 * get the length (number of cells) of the slice
	 * 
	 * @return - the number of cells, the length
	 */
	public int getLength() {
		return this.length;
	}
	
}
