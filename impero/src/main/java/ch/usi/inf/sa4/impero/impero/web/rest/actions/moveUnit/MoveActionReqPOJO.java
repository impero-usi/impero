/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit;

/**
 * @author jesper
 *
 */
public class MoveActionReqPOJO {
	
	private int rowFrom;
	private int colFrom;
	private int rowTo;
	private int colTo;
	private String playerID;
	
	public MoveActionReqPOJO() {}
	
	
	/**
	 * @return the xFrom
	 */
	public int getRowFrom() {
		return rowFrom;
	}
	/**
	 * @param rowFrom the xFrom to set
	 */
	public void setRowFrom(int rowFrom) {
		this.rowFrom = rowFrom;
	}
	/**
	 * @return the yFrom
	 */
	public int getColFrom() {
		return colFrom;
	}
	/**
	 * @param colFrom the yFrom to set
	 */
	public void setColFrom(int colFrom) {
		this.colFrom = colFrom;
	}
	/**
	 * @return the xTo
	 */
	public int getRowTo() {
		return rowTo;
	}
	/**
	 * @param rowTo the xTo to set
	 */
	public void setRowTo(int rowTo) {
		this.rowTo = rowTo;
	}
	/**
	 * @return the yTo
	 */
	public int getColTo() {
		return colTo;
	}
	/**
	 * @param colTo the yTo to set
	 */
	public void setColTo(int colTo) {
		this.colTo = colTo;
	}


	public String getPlayerID() {
		return playerID;
	}


	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	

}
