package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinateEntryPOJO;

public class MapChangesPOJO {
	
	List<CoordinateEntryPOJO> change;
	
	public MapChangesPOJO() {}

	/**
	 * @return the change
	 */
	public List<CoordinateEntryPOJO> getChange() {
		return change;
	}

	/**
	 * @param change the change to set
	 */
	public void setChange(List<CoordinateEntryPOJO> change) {
		this.change = change;
	}

}
