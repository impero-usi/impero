package ch.usi.inf.sa4.impero.impero.db;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class MongoDiscoveredHexagon {
	private static DBCollection discoveredHexagon;
	
	static{
		MongoDiscoveredHexagon.discoveredHexagon = MongoConnection.getDB().getCollection("discoveredHexagon");
	}

	
	public static boolean persistDiscoveredHexagon(String gameID, String playerID, List<CoordinatePOJO> list){
		BasicDBObject query = new BasicDBObject("gameID",gameID).append("playerID", playerID);
	    BasicDBObject keys = new BasicDBObject("discoveredHexagons", 1);
	    DBCursor cur = MongoDiscoveredHexagon.discoveredHexagon.find(query,keys);
	
		List<DBObject> hlist = new ArrayList<DBObject>();
		DBObject newEntry = new BasicDBObject();
		newEntry.put("gameID", gameID);
		newEntry.put("playerID", playerID);
		DBObject hcurrent;
		for(CoordinatePOJO c : list){
			hcurrent = new BasicDBObject();
			hcurrent.put("col", c.getCol());
			hcurrent.put("row", c.getRow());
			hlist.add(hcurrent);
		}
		newEntry.put("discoveredHexagons", hlist);
		if(cur.hasNext()){
			MongoDiscoveredHexagon.discoveredHexagon.update(query,newEntry);
		}else{
			MongoDiscoveredHexagon.discoveredHexagon.save(newEntry);			
		}
		return true;
	}
	
	public static List<CoordinatePOJO> retrieveVisibleHexagon(String gameID, String playerID){
		List<CoordinatePOJO> ret = new ArrayList<CoordinatePOJO>();
		
		BasicDBObject query = new BasicDBObject("gameID",gameID).append("playerID", playerID);
	    BasicDBObject keys = new BasicDBObject("discoveredHexagons", 1);
	    DBCursor cur = MongoDiscoveredHexagon.discoveredHexagon.find(query,keys);
		
	    if(cur.hasNext()){
			DBObject curMap = cur.next(); 
			BasicDBList e = (BasicDBList) curMap.get("discoveredHexagons");
			CoordinatePOJO entry;
			if(e != null){
				for(Object d : e){
					DBObject current = (DBObject)d;
					entry = new CoordinatePOJO();
					entry.setCol(Integer.parseInt(current.get("col").toString()));
					entry.setRow(Integer.parseInt(current.get("row").toString()));
					//Build the list
					ret.add(entry);
				}
			}else{
				System.out.println("DiscoveredHexagon list is empty");
			}
			
	    }
		return ret;
	}
	
	public static boolean removeVisibleHexagons(String gameID){
		BasicDBObject query = new BasicDBObject("gameID",gameID);
		MongoDiscoveredHexagon.discoveredHexagon.remove(query);
		return true;
	}
}
