package ch.usi.inf.sa4.impero.impero.web.server;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.bson.types.ObjectId;
//import org.springframework.social.facebook.api.Facebook;
//import org.springframework.social.facebook.api.FacebookProfile;
//import org.springframework.social.facebook.api.impl.FacebookTemplate;

import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.impl.FacebookTemplate;

import ch.usi.inf.sa4.impero.impero.db.MongoConnection;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Pair;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.QueryBuilder;
import com.mongodb.WriteResult;

/**
 * 
 * @author Kasim Bordogna
 * 
 *         Class that enables method to verify password stored in mongoDB
 */

public class Login {
	private static DBCollection users;

	static {
		Login.users = MongoConnection.getDB().getCollection("user");
	}

	public static String verify(String username, String password) {
		String ret;
		// Verify password
		QueryBuilder qb = new QueryBuilder();
		qb.put("username").is(username);
		qb.put("hashpassword").is(Login.hashPassword(password));
		DBCursor cur = Login.users.find(qb.get());

		if (cur.hasNext()) {
			DBObject obj = cur.next();
			// User is there and password is ok
			ret = obj.get("_id").toString();
		} else {
			// Either user is not there nor the pass is ok
			ret = null;
		}
		return ret;
	}

	public static String insUser(String username, String password, String email) {
		BasicDBObject doc = new BasicDBObject("username", username).append(
				"hashpassword", Login.hashPassword(password)).append("email",
				email);

		// Check if user already exists
		// Verify password
		QueryBuilder qb = new QueryBuilder();
		qb.put("username").is(username);
		DBCursor cur = Login.users.find(qb.get());

		if (cur.hasNext()) {
			// User already exits
			return null;
		} else {
			// New user
			WriteResult wr = Login.users.insert(doc);
			return ((ObjectId) doc.get("_id")).toString();
		}
	}

	private static String hashPassword(String password) {
		String ret = "";

		try {
			// Hash the input password
			byte[] salt = "123qwe.".getBytes();
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536,
					128);
			SecretKeyFactory f;
			f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash;
			hash = f.generateSecret(spec).getEncoded();

			String saltHash = new BigInteger(1, salt).toString(16);
			String passHash = new BigInteger(1, hash).toString(16);

			ret = saltHash + passHash;

		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
		}
		return ret;
	}

	public static Pair<String, String> facebookLogin(String accessToken) {
		Facebook facebook = new FacebookTemplate(accessToken);
		FacebookProfile profile = facebook.userOperations().getUserProfile();
		return new Pair<String, String>(profile.getName(), profile.getEmail());
	}
}
