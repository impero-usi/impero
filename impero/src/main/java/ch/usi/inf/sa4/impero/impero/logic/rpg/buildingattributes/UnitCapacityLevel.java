package ch.usi.inf.sa4.impero.impero.logic.rpg.buildingattributes;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The available levels of the unit capacity attribute of buildings.
 * @author Orestis Melkonian
 */
public enum UnitCapacityLevel {
	INSANE_CRAMPED(0),
	CRAZY_CRAMPED(1),
	VERY_CRAMPED(2),
	LITTLE_CRAMPED(3),
	MEDIUM(4),
	LITTLE_SPACIOUS(5),
	VERY_SPACIOUS(6),
	CRAZY_SPACIOUS(8),
	INSANE_SPACIOUS(10);
	
	/**
	 * Map from integer values to level enum expressions.
	 */
     private static final Map<Integer, UnitCapacityLevel> LOOKUP 
          = new HashMap<Integer, UnitCapacityLevel>();

     static {
          for (UnitCapacityLevel ucl : EnumSet.allOf(UnitCapacityLevel.class)) {
        	  LOOKUP.put(ucl.getUnitCapacity(), ucl);
          }
               
     }
     /**
      * The unit capacity of the current building.
      */
     private int unitCapacity;
     /**
      * Constructor.
      * @param uc The integer associated with the current attribute level.
      */
     private UnitCapacityLevel(final int uc) {
          this.unitCapacity = uc;
     }
     /**
      * @return Getter for unit capacity.
      */
     public int getUnitCapacity() { 
    	 return this.unitCapacity; 
     }
}
