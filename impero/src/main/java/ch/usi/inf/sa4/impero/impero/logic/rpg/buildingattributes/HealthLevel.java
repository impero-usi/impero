package ch.usi.inf.sa4.impero.impero.logic.rpg.buildingattributes;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The available levels of the health attribute of buildings.
 * @author Orestis Melkonian
 */
public enum HealthLevel {
	INSANE_WEAK(100),
	CRAZY_WEAK(200),
	VERY_WEAK(300),
	LITTLE_WEAK(400),
	MEDIUM(500),
	LITTLE_STRONG(600),
	VERY_STRONG(800),
	CRAZY_STRONG(1000),
	INSANE_STRONG(1200);
	
	/**
	 * Map from integer values to level enum expressions.
	 */
     private static final Map<Integer, HealthLevel> LOOKUP 
          = new HashMap<Integer, HealthLevel>();

     static {
          for (HealthLevel hl : EnumSet.allOf(HealthLevel.class)) {
        	  LOOKUP.put(hl.getHealth(), hl);
          }
               
     }
     /**
      * The health of the current building.
      */
     private int health;
     /**
      * Constructor.
      * @param h The integer associated with the current attribute level.
      */
     private HealthLevel(final int h) {
          this.health = h;
     }
     /**
      * @return Getter for health.
      */
     public int getHealth() { 
    	 return this.health; 
     }
}
