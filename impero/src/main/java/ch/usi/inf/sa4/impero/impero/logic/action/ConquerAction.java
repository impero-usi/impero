package ch.usi.inf.sa4.impero.impero.logic.action;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class ConquerAction extends AbstractAction{
	/**
	 * this field represent the unit that attacks;
	 */
	private final AbstractUnit actor;

	public ConquerAction(Player owner, Integer actionPointCost,
			Hexagon target, final AbstractUnit actor) {
		super(owner, actionPointCost, target);
		this.actor = actor;
	}

	/**
	 * Method that return the unit that wants to attack.
	 * 
	 * @return the actor
	 */
	public AbstractUnit getActor() {
		return actor;
	}


}
