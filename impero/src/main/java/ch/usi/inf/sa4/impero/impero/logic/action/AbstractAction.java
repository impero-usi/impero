package ch.usi.inf.sa4.impero.impero.logic.action;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * This class model the general action one player can do.
 * 
 * @author ChristianVuerich
 * 
 */
public abstract class AbstractAction {
	/**
	 * this field represent the player that "owns" the action.
	 */
	private final Player owner;

	/**
	 * this field represent the number of action points that the action
	 * consumes.
	 */
	private final Integer actionPointCost;
	/**
	 * this field represent the target of the action.
	 */
	private final Hexagon target;
	
	

	/**
	 * The constructor of an AbstractAction.
	 * 
	 * @param owner
	 *            is the player that own the entity that do the action.
	 * @param length
	 *            is the duration of the action.
	 * @param actionPointCost
	 *            is the cost of the action.
	 * @param target
	 *            are the coordinates of the target of the action.
	 */
	public AbstractAction(final Player owner, final Integer actionPointCost,
			final Hexagon target) {
		this.owner = owner;
		this.actionPointCost = actionPointCost;
		this.target = target;
	}

	/**
	 * Method that return the player that owns the acting unit.
	 * 
	 * @return the owner
	 */
	public Player getOwner() {
		return owner;
	}

	public Hexagon getTarget(){
		return this.target;
	}

	/**
	 * Method that return the action points cost of the action.
	 * 
	 * @return the actionPointCost
	 */
	public Integer getActionPointCost() {
		return actionPointCost;
	}

}
