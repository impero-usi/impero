package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * The AbstractProductionBuilding class is the superclass of every Building able
 * to produce Units in the game.
 * 
 * @author Matteo Morisoli
 * 
 */
public abstract class AbstractProductionBuilding extends AbstractBuilding {

	/**
	 * 
	 * @param health
	 * @param owner
	 * @param hexagon
	 * @param name
	 */
	public AbstractProductionBuilding(final Player owner,
			final Hexagon hexagon, final String name, final Integer health,
			final Integer visibleRange, final Integer buildingCapacity, final Integer maxHealth) {
		super(owner, hexagon, name, health, visibleRange, buildingCapacity, maxHealth);
	}

}
