package ch.usi.inf.sa4.impero.impero.logic.rpg;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;

import ch.usi.inf.sa4.impero.impero.logic.utilities.Pair;

/**
 * Class for calculating damage in different types of combat.
 * 
 * @author Orestis Melkonian
 */
public final class DamageExecution {
	/**
	 * Empty constructor.
	 */
	private DamageExecution() {
	}
	/**
	 * Battle between two AbstractUnits: slight advantage to the defender. One
	 * of them will die after DamageExecution.
	 * 
	 * @param attacker
	 *            the unit attacking
	 * @param defender
	 *            the unit defending
	 * @return TRUE: Defender died FALSE: Attacker died.
	 */
	public static Pair<Boolean, Integer> battle(final AbstractUnit attacker,
			final AbstractUnit defender) {
		int attackerHealth = attacker.getHealth();
		int defenderHealth = defender.getHealth();
		while ((attackerHealth > 0) && (defenderHealth > 0)) {
			int[] attackerRolls = new int[attacker.getOffensiveDamage()];
			int[] defenderRolls = new int[defender.getDefensiveDamage()];
			int i;
			for (i = 0; i < attacker.getOffensiveDamage(); i++) {
				attackerRolls[i] = DamageExecution.randomIntFromInterval(0, 6);
				if (i < defender.getDefensiveDamage()) {
					defenderRolls[i] = DamageExecution.randomIntFromInterval(0,
							6);
					if (defenderRolls[i] >= attackerRolls[i]) {
						attackerHealth--;
					} else {
						defenderHealth--;
					}
				} else {
					defenderHealth--;
				}
					
				if (defenderHealth <= 0) {
					return (new Pair<Boolean, Integer>(true, 
							attacker.getHealth() - attackerHealth));
				} else if (attackerHealth <= 0) {
					return new Pair<Boolean, Integer>(false,
							defender.getHealth() - defenderHealth);
				}
			}
			if (defenderHealth <= 0) {
				return new Pair<Boolean, Integer>(true,
						attacker.getHealth() - attackerHealth);
			} else if (attackerHealth <= 0) {
				return new Pair<Boolean, Integer>(false,
						defender.getHealth() - defenderHealth);
			}
		}
		
		return new Pair<Boolean, Integer>(true,
				attacker.getHealth() - attackerHealth);
	}

	/**
	 * An one-way attack: attacker does 60%-100% of his offensivePower to the
	 * receiver, reduced according to the receiver's defensivePower.
	 * 
	 * @param attacker	The unit attacking
	 * @param receiver	The entity receiving the damage.
	 * 
	 * @return the damage the receiver takes.
	 */
	public static int attack(final AbstractUnit attacker) {
		double r1 = 
				(double) DamageExecution.randomIntFromInterval(60, 100) / 100;
		int damage = 
				(int) (attacker.getOffensiveDamage() * r1);
		return damage ;
	}
	
	public static Pair<Integer, Integer> movingAttack(final AbstractUnit attacker,
			final AbstractEntity receiver) {
		double r1 = 
				(double) DamageExecution.randomIntFromInterval(60, 100) / 100;
		double r2 = 
				(double) DamageExecution.randomIntFromInterval(60, 100) / 100;
		int attdamage = (int) (attacker.getDefensiveDamage() * r1);
		int defdamage;
		if (receiver instanceof AbstractUnit ) {
			AbstractUnit unitReceiver = (AbstractUnit) receiver;
			defdamage = (int) (unitReceiver.getDefensiveDamage() * r2);
		}else{
			defdamage = 0;
		}
		return new Pair<Integer, Integer>(attdamage, defdamage);
		
	}

	/**
	 * An attempt of a unit to conquer a building.
	 * 
	 * @param conqueror The unit attempting to conquer the building.	
	 * @param conqueree	The building being conquered.
	 * 
	 * @return TRUE: Building got conquered. FALSE: Otherwise. Integer:
	 *         Building's remaining health.
	 */
	public static Pair<Boolean, Integer> conquer(final AbstractUnit conqueror,
			final AbstractBuilding conqueree) {
		int buildingHealthCopy = conqueree.getHealth();
		int unitHealthCopy = conqueror.getHealth();
		int defenceBonus = 0;
		for (AbstractUnit u : conqueree.unitsInside) {
			defenceBonus += u.getDefensiveDamage();
		}
		int[] conquerorRolls = new int[conqueror.getOffensiveDamage()];
		int[] defenderRolls = new int[defenceBonus];
		int i;
		for (i = 0; i < conqueror.getOffensiveDamage(); i++) {
			conquerorRolls[i] = DamageExecution.randomIntFromInterval(0, 6);
			if (i < defenceBonus) {
				defenderRolls[i] = DamageExecution.randomIntFromInterval(0, 6);
				if (defenderRolls[i] >= conquerorRolls[i]) {
					unitHealthCopy--;
				} else {
					buildingHealthCopy--;
				}
			} else {
				buildingHealthCopy--;
			}
				
			if ((unitHealthCopy > 0)
					&& (buildingHealthCopy < ((int) (0.3 * conqueree
							.getHealth())))) {
				return new Pair<Boolean, Integer>(true, buildingHealthCopy);
			}
		}
		return new Pair<Boolean, Integer>(false, buildingHealthCopy);
	}

	/**
	 * @param min	The desired minimum value to give.
	 * @param max 	The desired maximum value to give.
	 * @return an integer between the minimum and the maximum.
	 */
	private static int randomIntFromInterval(final int min, final int max) {
		return (int) (Math.random() * (max - min + 1) + min);
	}
}
