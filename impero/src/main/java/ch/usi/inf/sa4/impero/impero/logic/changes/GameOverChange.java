package ch.usi.inf.sa4.impero.impero.logic.changes;

public class GameOverChange implements AbstractChange {
	
	private final boolean gameOver;
	
	public GameOverChange(final boolean gameOver){
		this.gameOver = gameOver;
	}

	public boolean isGameOver() {
		return gameOver;
	}
}
