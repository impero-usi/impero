package ch.usi.inf.sa4.impero.impero.logic.map.strategy;

import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.map.Coordinate;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;

public interface BoardStrategy {
	
	/**
    * @param boardReference the board to use to compute the neighbours
    * @param row the row of the cell you want to get the neighbours
    * @param col the column of the cell you want to get the neighbours
    * @return an hashmap with all the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
    * If a cell does not have a neighbour then the value is null
    */
	public abstract Map<Integer, Hexagon> getNeighbours(final HexagonalMap boardReference, final int row, final int col) throws IndexOutOfBoundsException;
	
	/**
	 * Method to get the neighbours of a cell in a certain range
	 * @param boardReference the board to use to compute the neighbours
	 * @param row the row of the cell you want to get the neighbours
	 * @param col the column of the cell you want to get the neighbours
	 * @param range range for the neighbour
	 * @return an arraylist with all the neighbours
	 */
	public abstract List<Hexagon> getNeighboursInRange(final HexagonalMap boardReference, final int row, final int col, final int range) throws IndexOutOfBoundsException;
	
	/**
    * @param boardReference the board to use to compute the neighbours
    * @param row the row of the cell you want to get the neighbours
    * @param col the column of the cell you want to get the neighbours
    * @return an hashmap with all the coordinates of the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
    * If a cell does not have a neighbour then the value is null
    */
	public abstract Map<Integer, Coordinate> getNeighboursCoordinates(final HexagonalMap boardReference, final int row, final int col) throws IndexOutOfBoundsException;
	
	/**
	 * 
	 * @param boardReference the board to use to compute the neighbours
	 * @param cell the cell that you want to get the neighbour
	 * @return an hashmap with all the coordinates of the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
	 */
	public abstract Map<Integer, Hexagon> getNeighbour(final HexagonalMap boardReference, final Hexagon cell);
	
	/**
	 * Compute the costs to all possible cells around the cell at startRow and startCol according to the given actionpoints.
	 * @param boardReference the board to use to compute the shortest path
	 * @param row the row of the starting cell
	 * @param col the column of the starting cell
	 * @param actionPoints number of actionPoints that can be consumed
	 * @return list containing the shortest path
	 */
	public abstract Map<Hexagon, Integer> getAllCosts(final HexagonalMap boardReference, final int startRow, final int startCol, final int actionPoints, final int range, final AbstractUnit unit, final List<AbstractUnit> units, final List<AbstractBuilding> buildings, final List<AbstractProductionBuilding> pBuildings, boolean flying) throws IndexOutOfBoundsException;
	
	/**
	 * Compute the shortest path between start and end
	 * @param start the starting cell
	 * @param end the ending cell
	 * @return the list containing the shortest path from start to end
	 */
	public abstract List<Hexagon> getShortestPath(Hexagon start, Hexagon end); 
		
}
