package ch.usi.inf.sa4.impero.impero.web.rest.entities;

import java.util.List;

public class EntitiesListAnsPOJO {
	private boolean canMove;
	private boolean canAttack;
	private boolean canConquer;
	
	private List<EntitiesDataAndCostPOJO> entities;
	
	public EntitiesListAnsPOJO() {}
	
	/**
	 * @return the entities
	 */
	public List<EntitiesDataAndCostPOJO> getEntities() {
		return entities;
	}
	/**
	 * @param entities the entities to set
	 */
	public void setEntities(List<EntitiesDataAndCostPOJO> entities) {
		this.entities = entities;
	}

	public boolean isCanMove() {
		return canMove;
	}

	public void setCanMove(boolean canMove) {
		this.canMove = canMove;
	}

	public boolean isCanAttack() {
		return canAttack;
	}

	public void setCanAttack(boolean canAttack) {
		this.canAttack = canAttack;
	}

	public boolean isCanConquer() {
		return canConquer;
	}

	public void setCanConquer(boolean canConquer) {
		this.canConquer = canConquer;
	}
	
}
