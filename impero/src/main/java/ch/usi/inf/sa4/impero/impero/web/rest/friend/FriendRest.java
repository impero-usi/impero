package ch.usi.inf.sa4.impero.impero.web.rest.friend;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import ch.usi.inf.sa4.impero.impero.db.FriendEntryPOJO;
import ch.usi.inf.sa4.impero.impero.db.MongoFriend;
import ch.usi.inf.sa4.impero.impero.db.PlayerEntryPOJO;

/**
 * 
 * @author Kasim Bordogna
 * 
 */
@Path("/friend")
public class FriendRest {

	@GET
	@Produces({ "application/json" })
	public List<FriendEntryPOJO> getFriends(@Context HttpServletRequest request) {
		System.out.println("RECEIVED A GET FRIENDS REQUEST "
				+ request.getRequestedSessionId());

		return MongoFriend.getFriends(request.getSession(false).getAttribute("id")
				.toString());
	}

	@POST
	@Path("/{playerID}")
	@Produces({ "application/json" })
	public boolean setFriends(@Context HttpServletRequest request,
			@PathParam("playerID") String playerID) {
		System.out.println("MakeFriend:" + request.getRequestedSessionId()
				+ " with " + playerID);
		return MongoFriend.makeFriends(request.getSession().getAttribute("id")
				.toString(), playerID);
	}

	@DELETE
	@Path("/{playerID}")
	@Produces({ "application/json" })
	public boolean deleteFriends(@Context HttpServletRequest request,
			@PathParam("playerID") String playerID) {
		return MongoFriend.unMakeFriend(request.getSession().getAttribute("id")
				.toString(), playerID);
	}

}
