package ch.usi.inf.sa4.impero.impero.logic.entities;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * The AbstractUnit class is the superclass of every Unit in the game and
 * defines some Unit-specific attributes.
 * 
 * @author Matteo Morisoli
 * 
 */
public abstract class AbstractUnit extends AbstractEntity {

	/**
	 * this field is an array containing the minimum and maximum offensive
	 * damage of the unit.
	 */
	private Integer offensiveDamage;
	/**
	 * this field is an array containing the minimum and maximum defensive
	 * damage of the unit.
	 */
	private Integer defensiveDamage;
	/**
	 * this field is the maximum action points the unit can spend moving.
	 */
	private final Integer movementRange;
	/**
	 * this field is the attack range for ranged units.
	 */
	private final Integer attackRange;
	/**
	 * this field is the actionPointCost for an attack.
	 */
	private final Integer attackActionPointCost;
	/**
	 * this field is the isActing for an attack.
	 */
	private boolean isActing;
	/**
	 * this field is the orientation of the unit.
	 */
	private Integer orientation;
	/**
	 * this field is the attackClass of this units AKA who can i attack.
	 */
	private List<Class<? extends AbstractEntity>> attackClass;

	/**
	 * This is the constructor for the AbstractUnit class.
	 * 
	 * @param health
	 *            the initial health of the entity.
	 * @param owner
	 *            the player who creates the entity.
	 * @param hexagon
	 *            the cell where the entity is created.
	 * @param name
	 *            the name of the type of the entity.
	 * @param offensiveDamage
	 *            the base damage that the unit can do when it attacks an enemy
	 *            entity.
	 * @param defensiveDamage
	 *            the base damage that the unit can do when it is attacked by an
	 *            enemy unit.
	 * @param movementRange
	 *            the maximum movement range of the unit.
	 * @param attackRange
	 *            the maximum attack range of the unit.
	 */
	public AbstractUnit(final Player owner, final Hexagon hexagon,
			final String name, final Integer health, final Integer attackCost, final Integer attackRange, final Integer movementRange, final Integer offensiveDamage, final Integer defensiveDamage, final Integer visibleRange, final List<Class<? extends AbstractEntity>> attackClass, final Integer maxHealth) {
		super(health, owner,
				hexagon, name, visibleRange, maxHealth);
		this.attackActionPointCost = attackCost;
		this.attackRange = attackRange;
		this.movementRange = movementRange;
		this.offensiveDamage = offensiveDamage;
		this.defensiveDamage = defensiveDamage;
		this.setActing(false);
		this.setAttackClass(attackClass);
		this.orientation = 0;
	}

	/**
	 * the getter method for the offensiveDamage field.
	 * 
	 * @return the base offensive damage of the Unit.
	 */
	public Integer getOffensiveDamage() {
		return offensiveDamage;
	}

	/**
	 * the setter method for the offensiveDamage field.
	 * 
	 * @param offensiveDamage
	 *            the new base offensive damage of the Unit.
	 */
	public void setOffensiveDamage(Integer offensiveDamage) {
		this.offensiveDamage = offensiveDamage;
	}

	/**
	 * the getter method for the defensiveDamage field.
	 * 
	 * @return the base defensive damage of the Unit.
	 */
	public Integer getDefensiveDamage() {
		return defensiveDamage;
	}

	/**
	 * the setter method for the defensiveDamage field.
	 * 
	 * @param defensiveDamage
	 *            the new base defensive damage of the Unit.
	 */
	public void setDefensiveDamage(Integer defensiveDamage) {
		this.defensiveDamage = defensiveDamage;
	}

	/**
	 * the getter method for the movementRange field.
	 * 
	 * @return the maximum movement range of the Unit.
	 */
	public Integer getMovementRange() {
		return movementRange;
	}

	/**
	 * the getter method for the attackRange field.
	 * 
	 * @return the maximum attack range of the Unit.
	 */
	public Integer getAttackRange() {
		return attackRange;
	}

	/**
	 * the getter method for the attackActionPointCost field.
	 * 
	 * @return the action point cost of the ranged attack of the unit.
	 */
	public Integer getAttackActionPointCost() {
		return attackActionPointCost;
	}

	public boolean isActing() {
		return isActing;
	}

	public void setActing(boolean isActing) {
		this.isActing = isActing;
	}

	public List<Class<? extends AbstractEntity>> getAttackClass() {
		return attackClass;
	}

	public void setAttackClass(List<Class<? extends AbstractEntity>> attackClass) {
		this.attackClass = attackClass;
	}

	public Integer getOrientation() {
		return orientation;
	}

	public void setOrientation(Integer orientation) {
		this.orientation = orientation;
	}
}
