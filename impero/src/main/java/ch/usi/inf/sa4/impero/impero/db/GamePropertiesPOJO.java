package ch.usi.inf.sa4.impero.impero.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class GamePropertiesPOJO {
	private String gameID;
	private Integer actionPointsPerTurn;
	private HexagonalMap map;
	private List<Player> playerList;
	private Map<Player, List<AbstractAction>> actionList;
	private List<AbstractUnit> unitList;
	private List<AbstractBuilding> buildingList;
	private List<AbstractProductionBuilding> productionList;
	
 	public String getGameID() {
		return this.gameID;
	}

	public Integer getActionPointsPerTurn() {
		return this.actionPointsPerTurn;
	}

	public HexagonalMap getMap() {
		return this.map;
	}

	/**
	 * @param gameID the gameID to set
	 */
	public void setGameID(String gameID) {
		this.gameID = gameID;
	}

	/**
	 * @param actionPointsPerTurn the actionPointsPerTurn to set
	 */
	public void setActionPointsPerTurn(Integer actionPointsPerTurn) {
		this.actionPointsPerTurn = actionPointsPerTurn;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(HexagonalMap map) {
		this.map = map;
	}

	/**
	 * @param playerList the playerList to set
	 */
	public void setPlayerList(List<Player> playerList) {
		this.playerList = playerList;
	}

	/**
	 * @param actionlist2 the actionList to set
	 */
	public void setActionList(Map<Player, List<AbstractAction>> actionlist2) {
		this.actionList = actionlist2;
	}

	/**
	 * @param unitList the unitList to set
	 */
	public void setUnitList(List<AbstractUnit> unitList) {
		this.unitList = unitList;
	}

	/**
	 * @param buildingList the buildingList to set
	 */
	public void setBuildingList(List<AbstractBuilding> buildingList) {
		this.buildingList = buildingList;
	}

	/**
	 * @param productionList the productionList to set
	 * @param players 
	 */
	public void setProductionList(List<AbstractProductionBuilding> productionList) {
		this.productionList = productionList;
	}

	public List<Player> getPlayerList() {
		return this.playerList;
	}

	public Map<Player, List<AbstractAction>> getActionList() {
		return this.actionList;
	}

	public List<AbstractUnit> getUnitList() {
		return this.unitList;
	}

	public List<AbstractBuilding> getBuildingList() {
		return this.buildingList;
	}

	public List<AbstractProductionBuilding> getProductionList() {
		return this.productionList;
	}

}
