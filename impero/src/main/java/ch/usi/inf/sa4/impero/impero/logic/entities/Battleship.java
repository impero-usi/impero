package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.UnitAttributeMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class Battleship extends AbstractNavalUnit{
	
	/**
	 * This is the constructor for the NavalUnit01 class.
	 * 
	 * @param owner
	 *            the player who creates the entity.
	 * @param hexagon
	 *            the cell where the entity is created.
	 */
	public Battleship(Player owner, Hexagon hexagon) {
		super(owner, hexagon, Battleship.class.getSimpleName(),
				UnitAttributeMapper.getHealth(Battleship.class),
				UnitAttributeMapper.getAttackCost(Battleship.class),
				UnitAttributeMapper.getAttackRange(Battleship.class),
				UnitAttributeMapper.getMovementRange(Battleship.class),
				UnitAttributeMapper.getOffensiveDamage(Battleship.class),
				UnitAttributeMapper.getDefensiveDamage(Battleship.class),
				UnitAttributeMapper.getVisibleRange(Battleship.class),
				UnitAttributeMapper.getAttackClass(Battleship.class),
				UnitAttributeMapper.getHealth(Battleship.class));
	}
}
