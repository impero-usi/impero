/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.deleteAction;

/**
 * @author jesper
 *
 */
public class DeleteAnsPOJO {
	
	private int newActionPoints;
	private int newEnergyLevel;
	
	public DeleteAnsPOJO() {}

	/**
	 * @return the newActionPoints
	 */
	public int getNewActionPoints() {
		return newActionPoints;
	}

	/**
	 * @param newActionPoints the newActionPoints to set
	 */
	public void setNewActionPoints(int newActionPoints) {
		this.newActionPoints = newActionPoints;
	}

	/**
	 * @return the newEnergyLevel
	 */
	public int getNewEnergyLevel() {
		return newEnergyLevel;
	}

	/**
	 * @param newEnergyLevel the newEnergyLevel to set
	 */
	public void setNewEnergyLevel(int newEnergyLevel) {
		this.newEnergyLevel = newEnergyLevel;
	}

}
