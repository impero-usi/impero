package ch.usi.inf.sa4.impero.impero.web.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.usi.inf.sa4.impero.impero.logic.utilities.Pair;

/**
 * Servlet implementation class LoginServlet
 */

@WebServlet(value="/LoginServlet", name="Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String facebookToken;
		if((facebookToken = request.getParameter("code"))!=null) {
			Pair<String,String> profile = Login.facebookLogin(facebookToken);
			System.out.println(profile.getFirstElement() + " " + profile.getSecondElement());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String id = verify(username, password);
		if (id != null) {
			HttpSession session = request.getSession(true);
			session.setAttribute("username", username);
			session.setAttribute("id", id);
			// set as a cookie the id of the user
			Cookie cookie = new Cookie("id", id);
			cookie.setMaxAge(request.getSession().getMaxInactiveInterval()*10);
			response.addCookie(cookie);
			response.sendRedirect(request.getContextPath() + "/index.jsp");
		} else {
			response.sendRedirect(request.getContextPath() + "/login.jsp");
		}
	}

	public static String verify(String username, String password) {
		return Login.verify(username, password);
	}

}
