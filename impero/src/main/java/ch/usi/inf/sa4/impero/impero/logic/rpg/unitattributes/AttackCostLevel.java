package ch.usi.inf.sa4.impero.impero.logic.rpg.unitattributes;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The available levels of the attackCost attribute of units.
 * @author Orestis Melkonian
 */
public enum AttackCostLevel {
	INSANE_CHEAP(5),
	CRAZY_CHEAP(10),
	VERY_CHEAP(15),
	LITTLE_CHEAP(20),
	MEDIUM(25),
	LITTLE_EXPENSIVE(30),
	VERY_EXPENSIVE(35),
	CRAZY_EXPENSIVE(40),
	INSANE_EXPENSIVE(50);
	
	/**
	 * Map from integer values to level enum expressions.
	 */
     private static final Map<Integer, AttackCostLevel> LOOKUP 
          = new HashMap<Integer, AttackCostLevel>();

     static {
          for (AttackCostLevel acl : EnumSet.allOf(AttackCostLevel.class)) {
        	  LOOKUP.put(acl.getAttackCost(), acl);
          }
     }
     /**
      * The attackCost of the current unit.
      */
     private int attackCost;
     /**
      * Constructor.
      * @param ac The integer associated with the current attribute level.
      */
     private AttackCostLevel(final int ac) {
          this.attackCost = ac;
     }
     /**
      * Getter for attackCost.
      * @return The current level's attack cost .
      */
     public int getAttackCost() { 
    	 return this.attackCost; 
    }
}
