package ch.usi.inf.sa4.impero.impero.web.rest;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class AbstractActionPOJO {
	private Player owner;
	private Integer actionPointCost;
	private CoordinatePOJO target;
	private String actionType;
	// attack & conquer & move
	private AbstractUnit actor;
	// move
	private List<CoordinatePOJO> path;
	// spawn
	private Integer cost;
	private String spawnedType;
	
}
