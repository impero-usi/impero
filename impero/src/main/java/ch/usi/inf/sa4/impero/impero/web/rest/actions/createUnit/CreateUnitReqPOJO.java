/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.createUnit;

/**
 * @author jesper
 *
 */
public class CreateUnitReqPOJO {
	
	private int row;
	private int col;
	private String type;
	private String playerID;
	
	public CreateUnitReqPOJO(){}
	
	/**
	 * @return the x
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @param row the x to set
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * @return the y
	 */
	public int getCol() {
		return col;
	}

	/**
	 * @param col the y to set
	 */
	public void setCol(int col) {
		this.col = col;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the playerID
	 */
	public String getPlayerID() {
		return playerID;
	}

	/**
	 * @param playerID the playerID to set
	 */
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}	
	

}
