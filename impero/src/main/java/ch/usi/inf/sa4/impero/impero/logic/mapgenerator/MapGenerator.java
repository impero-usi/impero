package ch.usi.inf.sa4.impero.impero.logic.mapgenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ch.usi.inf.sa4.impero.impero.logic.map.Coordinate;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.map.strategy.HexagonalStrategy;

/**
 * This class is a map generator tool to generate random island maps. It use
 * simplex noise to generate the base noise and then it will apply a mask to
 * make the map a island. You can decide the percentage of any kind of terrain
 * in the map and decide to have or not rivers and lakes.
 * 
 * @author Luca Dotti, Simone Raimondi, Jesper Findhal, Kasim Bordogna
 * 
 */
public class MapGenerator {
	// noise represented as integer
	private List<List<Integer>> noise;
	// width of the map
	private int width;
	// height of the map
	private int height;

	// under this level the type of the terrain is DEEP_SEA
	private int deepSeaLevel;
	// under this level the type of the terrain is SEA
	private int seaLevel;
	// under this level the type of the terrain is FIELD
	private int fieldLevel;
	// under this level the type of the terrain is FOREST
	private int forestLevel;
	// under this level the type of the terrain is MOUNTAIN
	private int mountainLevel;

	// percentage of sea
	private int deepSeaPercent;
	// percentage of land
	private int landPercent;
	// percentage of water inside the island
	private int waterPercent;
	// percentage of mountains
	private int mountainPercent;
	// percentage of energy cells
	private int energyPercent;
	//Number of cells to convert in rivers
	private int groundConvertedToWater;
	
	/**
	 * Generates a map with terrains.
	 * 
	 * @param width width (cells) of the map
	 * @param height height (cells) of the map
	 * @param landPercent percent of land
	 * @param waterPercent percent of water on land
	 * @param mountainPercent percent of mountain on land
	 * @param energyPercent percent of energy
	 * @param lakes generate lakes
	 * @param rivers generate rivers
	 * @return a generated hexMap
	 */
	public HexagonalMap generateMap(final int width, final int height, final int landPercent, final int waterPercent,
			final int mountainPercent, final int energyPercent, final boolean lakes, final boolean rivers) {

		this.width = width;
		this.height = height;

		// initialize levels
		this.deepSeaLevel = 0;
		this.seaLevel = 0;
		this.fieldLevel = 0;
		this.forestLevel = 0;
		this.mountainLevel = 0;

		this.deepSeaPercent = 60;
		this.landPercent = landPercent;
		this.waterPercent = waterPercent;
		this.mountainPercent = mountainPercent;
		this.energyPercent = energyPercent;
		this.groundConvertedToWater = 0;

		// initialization
		this.noise = new ArrayList<List<Integer>>(height);

		generateNoise(width, height);

		this.calcLevels();
		if(rivers){
			this.generateRivers();
		}
		
		List<List<TerrainType>> map = new ArrayList<List<TerrainType>>(height);
		
		// iterate over rows
		int x = 0;
		int y = 0;
		for (List<Integer> noiseRow : this.noise) {
			List<TerrainType> row = new ArrayList<TerrainType>();
			x = 0;
			for (Integer integer : noiseRow) {
				TerrainType type;

				if (integer > this.seaLevel
						&& (y < 2 || y > (height - 3) || x < 2 || x > (width - 2))) {
					// need to store also in noise so that we can correctly place
					// other cells like ER
					integer = this.deepSeaLevel;
					this.noise.get(y).set(x, integer);
				}

				double terrainHeight = integer;

				// assign DEEP SEA
				type = TerrainType.DEEP_WATER;

				// assign SEA
				if (terrainHeight > this.deepSeaLevel && terrainHeight <= this.seaLevel) {
					type = TerrainType.SHALLOW_WATER;
				}
				// assign SAND
				else if (terrainHeight > this.seaLevel && terrainHeight < this.fieldLevel) {
					type = TerrainType.SAND;
				}
				// assign FIELD
				else if (terrainHeight >= this.fieldLevel
						&& terrainHeight < this.forestLevel) {
					type = TerrainType.FIELD;
				}
				// assign FOREST
				else if (terrainHeight >= this.forestLevel
						&& terrainHeight <this. mountainLevel) {
					type = TerrainType.FOREST;
				}
				// assign MOUNTAIN
				else if (terrainHeight >= this.mountainLevel) {
					type = TerrainType.MOUNTAIN;
				}
				row.add(type);
				x++;
			}
			map.add(row);
			y++;
		}
		//place energy
		placeEnergyResources(map);
		return new HexagonalMap(map);
	}

	/**
	 * It generate the rivers.
	 * 
	 */
	private void generateRivers() {
		int numberOfGround = 0;
		List<Coordinate> possibleMountains = new ArrayList<Coordinate>();
		for (int y = 0; y < this.height; y++) {
			for (int x = 0; x < this.width; x++) {
				int noiseLevel = this.noise.get(y).get(x);
				if (noiseLevel >= this.mountainLevel) {
					possibleMountains.add(new Coordinate(y, x));
				}

				if (noiseLevel > this.seaLevel) {
					numberOfGround++;
				}
			}
		}

		int numberOfPossibleWater = (int) (numberOfGround * this.waterPercent / 100.0);
		this.groundConvertedToWater = numberOfPossibleWater;
		List<Coordinate> visited = new ArrayList<Coordinate>();
		while (numberOfPossibleWater > 0) {
			numberOfPossibleWater -= generateRiver(possibleMountains,numberOfPossibleWater, visited);
		}

	}

	/**
	 * 
	 * @param possibleMountains
	 * @param numberOfPossibleWater
	 * @return
	 */
	private int generateRiver(final List<Coordinate> possibleMountains, final int numberOfPossibleWater, final List<Coordinate> visited) {
		Coordinate max;
		max = possibleMountains.get(new Random().nextInt(possibleMountains.size() - 1));
		int usedCells = 0;

		while (usedCells < numberOfPossibleWater && max != null) {
			int row = max.getX();
			int col = max.getY();
			visited.add(max);
			this.noise.get(row).set(col, this.seaLevel);
			possibleMountains.remove(new Coordinate(row, col));
			Map<Integer, Coordinate> neighbors;
			if (col % 2 == 0)
				neighbors = HexagonalStrategy.computeNeighbourEvenColCoordinates(this.width, this.height, row, col);
			else
				neighbors = HexagonalStrategy.computeNeighbourOddColCoordinates(this.width, this.height, row, col);
			
			//Find neighbor with lower height

			Coordinate lower = neighbors.get(0);
			int waterNeighbors = 0;
			int lowerKey = 0;
			for (Integer key : neighbors.keySet()) {
				int nRow = neighbors.get(key).getX();
				int nCol = neighbors.get(key).getY();
				if ((lower != null && this.noise.get(nRow).get(nCol) < this.noise.get(lower.getX()).get(lower.getY()) && !visited.contains(new Coordinate(nRow, nCol)))) {
					lower = neighbors.get(key);
					lowerKey = key;
				}
				if (this.noise.get(neighbors.get(key).getX()).get(neighbors.get(key).getY()) < this.seaLevel) 
					waterNeighbors++;
				
			}
			
			//River perturbation
			int newLowerKey = lowerKey;
			if (new Random().nextInt(10) > 3) {
				int i = new Random().nextInt(1);
				if (i == 0) {
					if (lowerKey == 0) {
						newLowerKey = 5;
					} else {
						newLowerKey = lowerKey - 1;
					}
				} else {
					if (lowerKey == 5) {
						newLowerKey = 0;
					} else {
						newLowerKey = lowerKey + 1;
					}
				}
				lower = neighbors.get(newLowerKey);
			}
			usedCells++;
			max = lower;
			if (waterNeighbors >= 3) {
				max = null;
			}
		}
		return usedCells;
	}

	/**
	 * Generate random noise for the heights of the map
	 * 
	 */
	private void generateNoise(final int width, final int height) {
		generateOctavedSimplexNoise(8, 0.3f, 0.05f, width, height);
		generateGradient(width, height);
	}

	/**
	 * Generates random noise using the simplex noise algorithm with multiple
	 * octaves (multiple runs for each coordinate in the matrix)
	 * 
	 * @param octaves
	 * @param roughness
	 *            - the roughness for each layer of noise (0-1)
	 * @param scale
	 *            - the scale/frequency of the layers
	 */
	private void generateOctavedSimplexNoise(int octaves, float roughness,
			float scale, final int width, final int height) {

		float layerFrequency = scale;
		float layerWeight = 1;

		for (int octave = 0; octave < octaves; octave++) {
			SimplexNoiseOctave sn = new SimplexNoiseOctave(octave);
			// Calculate single layer/octave of simplex noise, then add it to
			// total noise
			int noiseValue;
			for (int y = 0; y < height; y++) {
				List<Integer> row = new ArrayList<Integer>(width);
				for (int x = 0; x < width; x++) {
					if (octave > 0) {
						noiseValue = this.noise.get(y).get(x);
					} else {
						noiseValue = 0;
					}
					noiseValue += (int) (100 * (1 + (float) sn.noise(x
							* layerFrequency, y * layerFrequency)
							* layerWeight));
					row.add(noiseValue);
				}

				if (octave == 0) {
					this.noise.add(row);
				} else {
					for (int i = 0; i < width; i++) {
						this.noise.get(y).set(i, this.noise.get(y).get(i) + row.get(i));
					}
				}
			}

			// Increase variables with each incrementing octave
			layerFrequency *= 2;
			layerWeight *= roughness;
		}
	}

	/**
	 * Adds a gradient matrix to the noise to generate an island like map.
	 * 
	 */
	private void generateGradient(final int width, final int height) {

		int midX = width / 2;
		int midY = height / 2;

		int distX;
		int distY;
		int gradient;
		int value;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				distX = midX - x;
				distY = midY - y;
				gradient = (int) (100 * (Math.sqrt(distX * distX + distY
						* distY) * 2));
				value = (this.noise.get(y).get(x)) / 2 - gradient * 4;
				this.noise.get(y).set(x, value);
			}
		}
	}

	/**
	 * Calculate the terrain/height levels of the map
	 * 
	 */
	private void calcLevels() {
		// Store all heights into an array
		int seaPercent = 100 - this.landPercent;
		List<Integer> altitudes = new ArrayList<Integer>();
		for (List<Integer> row : this.noise) {
			for (Integer element : row) {
				altitudes.add(element);

			}
		}

		// Sort the array
		Collections.sort(altitudes);

		// set the level of the sea
		int cellsLeft = altitudes.size();
		int seaIndex = cellsLeft * seaPercent / 100;
		this.seaLevel = altitudes.get(seaIndex);

		// set the level of the deep sea
		int deepSeaIndex = (int) (seaIndex * this.deepSeaPercent / 100);
		this.deepSeaLevel = altitudes.get(deepSeaIndex);
		// set the level of mountains
		cellsLeft -= seaIndex;
		int mountainIndex = altitudes.size() - cellsLeft * this.mountainPercent
				/ 100;
		this.mountainLevel = altitudes.get(mountainIndex);
		// set the level of forest
		cellsLeft = mountainIndex - seaIndex;
		int forestIndex = mountainIndex
				- (int) (cellsLeft * Math.random() * 0.5);
		this.forestLevel = altitudes.get(forestIndex);
		// set the level of field
		cellsLeft = forestIndex - seaIndex;
		int fieldIndex = forestIndex - (int) (cellsLeft * Math.random() * 0.8);
		this.fieldLevel = altitudes.get(fieldIndex);
	}
	
	/**
	 * 
	 * @param map
	 */
	private void placeEnergyResources(final List<List<TerrainType>> map) {

		int numERCells = (int)((this.height * this.width * this.landPercent / 100.0 - this.groundConvertedToWater)
				* this.energyPercent / 100.0);
		int xPos;
		int yPos;
		for (int i = 0; i < numERCells; i++) {
			xPos = (int) (this.width * Math.random());
			yPos = (int) (this.height * Math.random());
			// if the cell is not water make it an ER
			if (this.noise.get(yPos).get(xPos) > this.seaLevel && this.noise.get(yPos).get(xPos) < this.mountainLevel) {
				map.get(yPos).set(xPos,
						TerrainType.ENERGY_RESOURCE);
			}
			// if it was water try to find another location
			else {
				i--;
			}
		}
	}
}
