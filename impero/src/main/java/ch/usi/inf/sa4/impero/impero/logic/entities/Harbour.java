package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.BuildingAttributeMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class Harbour extends AbstractProductionBuilding {

	public Harbour(final Player owner, final Hexagon hexagon) {
		super(owner, hexagon, Harbour.class.getSimpleName(),
				BuildingAttributeMapper.getHealth(Harbour.class),
				BuildingAttributeMapper.getVisibleRange(Harbour.class),
				BuildingAttributeMapper.getUnitCapacity(Harbour.class),
				BuildingAttributeMapper.getHealth(Harbour.class));
	}

}
