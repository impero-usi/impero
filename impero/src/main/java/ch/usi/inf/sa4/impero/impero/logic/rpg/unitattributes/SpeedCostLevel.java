package ch.usi.inf.sa4.impero.impero.logic.rpg.unitattributes;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The available levels of the speedCost attribute of units.
 * @author Orestis Melkonian
 */
public enum SpeedCostLevel {
	INSANE_CHEAP(2),
	CRAZY_CHEAP(4),
	VERY_CHEAP(6),
	LITTLE_CHEAP(8),
	MEDIUM(10),
	LITTLE_EXPENSIVE(12),
	VERY_EXPENSIVE(14),
	CRAZY_EXPENSIVE(16),
	INSANE_EXPENSIVE(18);
	
	/**
	 * Map from integer values to level enum expressions.
	 */
	private static final Map<Integer, SpeedCostLevel> LOOKUP 
		= new HashMap<Integer, SpeedCostLevel>();

	static {
		for (SpeedCostLevel sl : EnumSet.allOf(SpeedCostLevel.class)) {
			LOOKUP.put(sl.getSpeedCost(), sl);
		}
	}
	/**
     * The speedCost of the current unit.
     */
	private int speedCost;
	/**
     * Constructor.
     * @param sc The integer associated with the current attribute level.
     */
	private SpeedCostLevel(final int sc) {
		this.speedCost = sc;
	}
	/**
	 * Getter for speedCost.
     * @return The current level's speedCost.
     */
	public int getSpeedCost() {
		return this.speedCost;
	}
}
