package ch.usi.inf.sa4.impero.impero.web.server;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName = "LoggedInFilter", urlPatterns = { "/dashboard/*",
		"/game/*" })
public class LoginFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		// Get the IP address of client machine.
		String ipAddress = request.getRemoteAddr();
		String id = "";
		if(req.getCookies()==null) {
			resp.sendRedirect("/index.jsp");
			return;
		}
		/*for(Cookie c :req.getCookies()) {
			if(c.getName().equals("id")) {
				id = c.getValue();
			}
		} 
		 if( req.getSession(false) != null && !req.getSession().getAttribute("id").equals(id)) {
			System.out.println(ipAddress + " tampered his cookie");
			Cookie cookie = new Cookie("id",id);
			resp.addCookie(cookie);
		} */
		if (req.getSession(false) == null || req.getSession(false).getAttribute("username") == null) {
			resp.sendRedirect("/index.jsp");
			System.out.println("Invalid acces IP " + ipAddress + ", Time "
					+ new Date().toString());
			return;
		} 
		chain.doFilter(request, response);

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
