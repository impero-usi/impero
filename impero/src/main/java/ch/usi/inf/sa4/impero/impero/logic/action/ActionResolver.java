package ch.usi.inf.sa4.impero.impero.logic.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.changes.AbstractChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.AttackChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.BattleChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.CreateEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.MapChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.MoveChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.RemoveEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.UpdateEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.VisibleEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.Battleship;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.EEF;
import ch.usi.inf.sa4.impero.impero.logic.entities.Harbour;
import ch.usi.inf.sa4.impero.impero.logic.entities.RadarTower;
import ch.usi.inf.sa4.impero.impero.logic.entities.Soldier;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.logic.rpg.DamageExecution;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Pair;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Utilities;

/**
 * This class is the logic action resolver. The players' actions are collected and sorted and then executed sequentially. 
 * @author Matteo Morisoli
 *
 */
public class ActionResolver {

	
	/**
	 * Reference to the map for the game.
	 */
	private final HexagonalMap map;
	/**
	 * the list of players in the game.
	 */
	private final List<Player> playerList;
	/**
	 * the list of units currently alive in the game.
	 */
	private final List<AbstractUnit> unitList;
	/**
	 * the list of buildings currently alive in the game.
	 */
	private final List<AbstractBuilding> buildingList;
	/**
	 * the list of production building currently alive in the game.
	 */
	private final List<AbstractProductionBuilding> productionBuildingList;
	/**
	 * the map of the action each player has scheduled this turn.
	 */
	private Map<Player, List<AbstractAction>> actionList;
	/**
	 * the list of action already resolved this turn.
	 */
	private final List<AbstractAction> resolvedActions;
	
	private List<AbstractChange> turnChanges;
	
	private final Utilities utility;

	
	/**
	 * Constructor for the actionResolver.
	 * @param playerList the player list.
	 * @param unitList the list of units.
	 * @param buildingList the list of buildings.
	 * @param productionBuildingList the list of production buildings.
	 * @param actionList the map of the actions of each player.
	 * @param battleCells the list of cells where a battle has happened.
	 * @param utilities reference to utilities.
	 */
	public ActionResolver(
			final List<Player> playerList, final List<AbstractUnit> unitList,
			final List<AbstractBuilding> buildingList,
			final List<AbstractProductionBuilding> productionBuildingList,
			final Map<Player, List<AbstractAction>> actionList, final HexagonalMap map, Utilities utility) {
		this.playerList = playerList;
		this.unitList = unitList;
		this.buildingList = buildingList;
		this.productionBuildingList = productionBuildingList;
		this.actionList = actionList;
		this.resolvedActions = new ArrayList<>();
		this.turnChanges = new ArrayList<>();
		this.map = map;
		this.utility = utility;
		}
	
	/**
	 * this method removes all the elements from the resolved action list.
	 */
	public void emptyResolvedAction(){
		getResolvedActions().clear();
	}
	
	public void emptyTurnChanges(){
		turnChanges = new ArrayList<>();
	}
	
	public boolean allDone(Map<Player, Pair<Integer, Integer>> map, Map<Player, List<AbstractAction>> actionList){
		for(Player player : map.keySet()){
			if(map.get(player).getFirstElement() < actionList.get(player).size()){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * this method is the main method of the actionResolver class, 
	 * it sorts by actionPoints the actions of the various players and then executes them sequentially invoking the right method for each action.
	 */
	public List<AbstractChange> resolveTurn(){
//		Integer maxLength = getLongestActionListLength();
//		for(int i = 0; i < maxLength; i++){
//			for(int j = 0; j < 100; j++){
//				for(Player player : playerList){
//					if(getActionList().get(player).size() > i){
//						if(getActionList().get(player).get(i).getActionPointCost() == j){
//							resolveAction(getActionList().get(player).get(i));
//						}
//					}
//				}
//			}
//		}
		Map<Player, Pair<Integer, Integer>> playerIndexes = new HashMap<Player, Pair<Integer, Integer>>();
		for(Player player : playerList){
			playerIndexes.put(player, new Pair<Integer, Integer>(0, 0));
		}
		Integer minCostIndex;
		Player p = null;
		while(! allDone(playerIndexes, actionList)){
			minCostIndex= Integer.MAX_VALUE;
			for(Player player : playerList){
				if(playerIndexes.get(player).getFirstElement() < actionList.get(player).size()){
					if(actionList.get(player).get(playerIndexes.get(player).getFirstElement()).getActionPointCost() + playerIndexes.get(player).getSecondElement() < minCostIndex){
						minCostIndex = actionList.get(player).get(playerIndexes.get(player).getFirstElement()).getActionPointCost() + playerIndexes.get(player).getSecondElement();
						p = player;
					}
				}
			}
			resolveAction(actionList.get(p).get(playerIndexes.get(p).getFirstElement()));
			playerIndexes.get(p).setSecondObject(playerIndexes.get(p).getSecondElement() + actionList.get(p).get(playerIndexes.get(p).getFirstElement()).getActionPointCost());
			playerIndexes.get(p).setFirstObject(playerIndexes.get(p).getFirstElement() + 1);
		}
		return turnChanges;
	}
	
	/**
	 * This methods checks the dynamic type of the object action and calls the dedicated method for each concrete action class.
	 * @param action a concrete action object.
	 */
	public void resolveAction(AbstractAction action){
		if(action instanceof MoveAction){
			resolveMoveAction((MoveAction) action);
		}else if
		(action instanceof SpawnUnitAction){
			resolveSpawnUnitAction((SpawnUnitAction) action);
		}else if
		(action instanceof SpawnBuildingAction){
			resolveSpawnBuildingAction((SpawnBuildingAction) action);
		}else if
		(action instanceof AttackAction){
			resolveAttackAction((AttackAction) action);
		}else if
		(action instanceof ConquerAction){
			//resolveConquerAction((ConquerAction) action);
		} 
	}
	
	/**
	 * This method implements in the logic a move action, effectively moving the unit and checking if battles happen during the movement.
	 * @param action a concrete MoveAction object.
	 */
	public void resolveMoveAction(MoveAction action){
		for(int x = 1; x < action.getPath().size(); x++){
			Hexagon hex = action.getPath().get(x);
			AbstractEntity entity = getEntityAt(hex);
			if(entity != null && !entity.getOwner().getTeamID().equals(action.getOwner().getTeamID())){
				if(x == action.getPath().size() - 1 && entity instanceof AbstractUnit){
					AbstractUnit unit = (AbstractUnit) entity;
					moveAndComputeChanges(action.getActor().getHexagon(), hex, action.getOwner(), action.getActor());
					Pair<Boolean, Integer> p = DamageExecution.battle(action.getActor(), unit);
					turnChanges.add(new BattleChange(hex));
					if(p.getFirstElement()){
						unitList.remove(unit);
						unit.getOwner().incUnitsLost();
						action.getActor().getOwner().incUnitDestroyed();
						turnChanges.add(new RemoveEntityChange(hex, unit.getOwner()));
						utility.updateVisibleMap(action.getOwner());
						turnChanges.add(new MapChange(getPlayerMap(action.getOwner()), action.getOwner()));
						action.getActor().setHealth(action.getActor().getHealth() - p.getSecondElement());
						turnChanges.add(new UpdateEntityChange(action.getOwner(), hex, action.getActor().getClass(), action.getActor().getHealth()));
					}
					if (!p.getFirstElement() || action.getActor().getHealth() <= 0){
						killAndComputeChanges(action.getActor(), unit);
						unit.setHealth(unit.getHealth() - p.getSecondElement());
						if(unit.getHealth() <= 0){
							unitList.remove(unit);
							turnChanges.add(new RemoveEntityChange(hex, unit.getOwner()));
							unit.getOwner().incUnitsLost();
							action.getActor().getOwner().incUnitDestroyed();
//							utility.updateVisibleMap(action.getOwner());
//							turnChanges.add(new MapChange(getPlayerMap(action.getOwner()), action.getOwner()));
						}else{
							turnChanges.add(new UpdateEntityChange(unit.getOwner(), hex, unit.getClass(), unit.getHealth()));
						}
					}
				}else if (x == action.getPath().size() - 1){
					break;
				}else{
					moveAndComputeChanges(action.getActor().getHexagon(), hex, action.getOwner(), action.getActor());
					if(entity == null || !utility.canAttack(action.getActor(), entity)){
						break;
					}
					Pair<Integer, Integer> damages = DamageExecution.movingAttack(action.getActor(), entity);
					turnChanges.add(new BattleChange(hex));
					action.getActor().setHealth(action.getActor().getHealth() - damages.getSecondElement());
					entity.setHealth(entity.getHealth() - damages.getFirstElement());
					if ( action.getActor().getHealth() <= 0){
						killAndComputeChanges(action.getActor(), entity);
					}else{
						turnChanges.add(new UpdateEntityChange(action.getActor().getOwner(), hex, action.getActor().getClass(), action.getActor().getHealth()));
					}
					if(entity.getHealth() <= 0){
						if(entity instanceof AbstractUnit){
							unitList.remove(entity);
						}else if(entity instanceof AbstractProductionBuilding){
							productionBuildingList.remove(entity);
						}else{
							buildingList.remove(entity);
						}
						turnChanges.add(new RemoveEntityChange(hex, entity.getOwner()));
//						utility.updateVisibleMap(action.getOwner());
//						turnChanges.add(new MapChange(getPlayerMap(action.getOwner()), action.getOwner()));
					}else{
						turnChanges.add(new UpdateEntityChange(entity.getOwner(), hex, entity.getClass(), entity.getHealth()));
					}
				}
			} else{
				if(x == action.getPath().size() - 1 && entity != null){
					break;
				}
				moveAndComputeChanges(action.getActor().getHexagon(), hex, action.getOwner(), action.getActor());
			
			}
		}
		getResolvedActions().add(action);

	}
	
	/**
	 * This method implements in the logic a spawn unit action, effectively adding the new unit to the unit list.
	 * @param action a concrete SpawnUnitAction object.
	 */
	public void resolveSpawnUnitAction(SpawnUnitAction action){
		if(getUnitAt(action.getTarget())==null){
			if(!isCellOccupied(action.getTarget())){
				createAndComputeChanges(action);
			}
		}
	}
	
	/**
	 * This method implements in the logic a spawn building action, effectively adding the new building to the building list.
	 * @param action a concrete SpawnBuildingAction object.
	 */
	public void resolveSpawnBuildingAction(SpawnBuildingAction action){
		if(getUnitAt(action.getTarget()) == null){
			if(!isCellOccupied(action.getTarget())){
				createAndComputeChanges(action);
			}
		}
	}
	
	/**
	 * This method implements in the logic an attack action, effectively calculating damages and removing units if they die.
	 * @param action a concrete AttackAction object.
	 */
	public void resolveAttackAction(AttackAction action){
		AbstractEntity entity = getEntityAt(action.getTarget());
		if(entity != null){
			if(!entity.getOwner().equals(action.getOwner())){
				turnChanges.add(new AttackChange(action.getActor().getHexagon(), entity.getHexagon()));
				Integer damages = DamageExecution.attack(action.getActor());
				entity.setHealth(entity.getHealth() - damages);
				turnChanges.add(new UpdateEntityChange(entity.getOwner(), entity.getHexagon(), entity.getClass(), entity.getHealth() - damages));
				if(entity.getHealth() <= 0){
					if(entity instanceof AbstractUnit){
						unitList.remove(entity);
						turnChanges.add(new RemoveEntityChange(entity.getHexagon(), entity.getOwner()));
						utility.updateVisibleMap(action.getOwner());
						turnChanges.add(new MapChange(getPlayerMap(action.getOwner()), action.getOwner()));
						entity.getOwner().incUnitsLost();
						action.getOwner().incUnitDestroyed();
					}else if
					((entity instanceof AbstractBuilding) && ! (entity instanceof AbstractProductionBuilding)){
						buildingList.remove(entity);
						turnChanges.add(new RemoveEntityChange(entity.getHexagon(), entity.getOwner()));
						utility.updateVisibleMap(action.getOwner());
						turnChanges.add(new MapChange(getPlayerMap(action.getOwner()), action.getOwner()));
					}else if
					(entity instanceof AbstractProductionBuilding){
						productionBuildingList.remove(entity);
						turnChanges.add(new RemoveEntityChange(entity.getHexagon(), entity.getOwner()));
						utility.updateVisibleMap(action.getOwner());
						turnChanges.add(new MapChange(getPlayerMap(action.getOwner()), action.getOwner()));
					}
				}
				getResolvedActions().add(action);
			}
		}
	}
	
	/**
	 * This method implements in the logic a conquer action, effectively calculating if the unit succeds and the damage the building takes.
	 * @param action a concrete ConquerAction object.
	 */
//	public void resolveConquerAction(ConquerAction action){
//		AbstractBuilding building = getBuidingAt(action.getTarget());
//		if(building != null){
//			if(!building.getOwner().equals(action.getOwner())){
//				Pair<Boolean, Integer> p = DamageExecution.conquer(action.getActor(), building);
//				if(p.getFirstElement()){
//					building.setHealth(building.getHealth() - p.getSecondElement());
//					building.setOwner(action.getOwner());
//					turnChanges.add(new UpdateEntityChange(action.getOwner(), building.getHexagon(), building.getClass(), building.getHealth() - p.getSecondElement()));
//					if(building.getHealth() <= 0){
//						if(building instanceof AbstractProductionBuilding){
//							productionBuildingList.remove(building);
//							turnChanges.add(new RemoveEntityChange(building.getHexagon(), action.getOwner()));
//						}else {
//							buildingList.remove(building);
//							turnChanges.add(new RemoveEntityChange(building.getHexagon(), action.getOwner()));
//						}
//					}
//				}else {
//					building.setHealth(building.getHealth() - p.getSecondElement());
//					turnChanges.add(new UpdateEntityChange(building.getOwner(), building.getHexagon(), building.getClass(), building.getHealth() - p.getSecondElement()));
//					if(building.getHealth() <= 0){
//						if(building instanceof AbstractProductionBuilding){
//							productionBuildingList.remove(building);
//							turnChanges.add(new RemoveEntityChange(building.getHexagon(), building.getOwner()));
//						}else{
//							buildingList.remove(building);
//							turnChanges.add(new RemoveEntityChange(building.getHexagon(), action.getOwner()));
//						}
//					}
//				}
//				resolvedActions.add(action);
//			}
//		}
//	}
	
	/**
	 * this method computes the longest list size in the action list map.
	 * @return the maximum size of list in the action list map. 
	 */
	public Integer getLongestActionListLength(){
		Integer maxLength = 0;
		for(Player player : getActionList().keySet()){
			if(getActionList().get(player).size() > maxLength){
				maxLength = getActionList().get(player).size();
			}
		}
		return maxLength;
	}
	
	/**
	 * This method takes an hexagon and checks if it is already occupied by a building.
	 * @param hex the hexagon that will be checked.
	 * @return true if the cell is occupied by a building, false otherwise.
	 */
	private boolean isCellOccupied(Hexagon hex){
		for (AbstractBuilding building : buildingList) {
			if (building.getHexagon().equals(hex)) {
				return true;
			}
		}
		for (AbstractProductionBuilding productionBuilding : productionBuildingList) {
			if (productionBuilding.getHexagon().equals(hex)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method takes an hexagon and checks if it is already occupied by a unit, if it is, it returns it.
	 * @param hex the hexagon that will be checked.
	 * @return the unit object that is on the cell, or null if the cell is not occupied.
	 */
	private AbstractUnit getUnitAt(Hexagon hex) {
		for (AbstractUnit unit : unitList) {
			if (unit.getHexagon().equals(hex)) {
				return unit;
			}
		}
		return null;
	}
	
	/**
	 * This method takes an hexagon and checks if it is already occupied by a building, if it is, it returns it.
	 * @param hex the hexagon that will be checked.
	 * @return the building object that is on the cell, or null if the cell is not occupied.
	 */
	private AbstractBuilding getBuidingAt(Hexagon hex){
		for (AbstractBuilding building : buildingList) {
			if (building.getHexagon().equals(hex)) {
				return building;
			}
		}
		for (AbstractProductionBuilding productionBuilding : productionBuildingList) {
			if (productionBuilding.getHexagon().equals(hex)) {
				return productionBuilding;
			}
		}
		return null;
	}
	
	/**
	 * This method takes an hexagon and check if it is already occupied by an entity, if it is, it returns it.
	 * @param hex the hexagon that will be checked.
	 * @return the entity object that is on the cell, or null if the cell is not occupied.
	 */
	private AbstractEntity getEntityAt(Hexagon hex){
		for (AbstractUnit unit : unitList) {
			if (unit.getHexagon().equals(hex)) {
				return unit;
			}
		}
		for (AbstractBuilding building : buildingList) {
			if (building.getHexagon().equals(hex)) {
				return building;
			}
		}
		for (AbstractProductionBuilding productionBuilding : productionBuildingList) {
			if (productionBuilding.getHexagon().equals(hex)) {
				return productionBuilding;
			}
		}
		return null;
	}
	
	public Integer computeMovementOrientation(final Hexagon startHex, final Hexagon endHex){
		Map<Integer, Hexagon> neighbours = map.getNeighbour(startHex);
		for(Integer i : neighbours.keySet()){
			if(neighbours.get(i) != null){
				if(neighbours.get(i).equals(endHex)){
					return i;
				}
			}
		}
		return -1;
		
	}
		

	/**
	 * The getter method for the resolved action lists.
	 * @return the list of resolved actions.
	 */
	public List<AbstractAction> getResolvedActions() {
		return resolvedActions;
	}

	/**
	 * The getter method for the map of the list of actions.
	 * @return the map of the list of actions.
	 */
	public Map<Player, List<AbstractAction>> getActionList() {
		return actionList;
	}

	public List<AbstractChange> getTurnChanges() {
		return turnChanges;
	}
	
	/**
	 * Returns the custom map for the player.
	 * 
	 * @param p
	 *            the {@link Player}
	 * @return the map
	 */
	public List<Hexagon> getPlayerMap(Player p) {
		List<Hexagon> newPlayerMap = new ArrayList<>();
		for (Hexagon hex : this.map.get1DMap()) {
			if (p.getVisibleHexagons().contains(hex)) {
				//hex.setDiscovered(false);
				newPlayerMap.add(new Hexagon(hex,false));
			} else if (p.getDiscoveredHexagons().contains(hex)) {
				//hex.setDiscovered(true);
				newPlayerMap.add(new Hexagon(hex,true));
			} else {
				Hexagon tmp = new Hexagon(hex,false);
				tmp.setType(TerrainType.UNDISCOVERED);
				newPlayerMap.add(tmp);
			}
		}
		return newPlayerMap;
	}

	public Utilities getUtility() {
		return utility;
	}
	
	
	public void moveAndComputeChanges(final Hexagon hex1, final Hexagon hex2, final Player player, final AbstractUnit unit){
		Integer orient = computeMovementOrientation(hex1, hex2);
		turnChanges.add(new MoveChange(hex1, hex2, orient, unit.getName()));
		List<AbstractEntity> set1 = getVisibleEntities(player);
		List<AbstractEntity> set2 = getVisibleEntities(player);
		List<Hexagon> oldMap = getPlayerMap(player);
		unit.setOrientation(orient);
		unit.setHexagon(hex2);
		utility.updateVisibleMap(player);
		List<Hexagon> newMap = getPlayerMap(player);
		List<AbstractEntity> set3 = getVisibleEntities(player);
		List<AbstractEntity> set4 = getVisibleEntities(player);
		set4.removeAll(set2);
		set1.removeAll(set3);
		List<Hexagon> changedMap = new ArrayList<>();
		for (int i = 0; i < oldMap.size(); i++) {
			if(oldMap.get(i).isDiscovered() != newMap.get(i).isDiscovered() || oldMap.get(i).getType() != newMap.get(i).getType()){
				changedMap.add(newMap.get(i));
			}
			
		}
		System.out.println("set 1 dimension: " + set1.size());
		System.out.println("set 4 dimension: " + set4.size());
		turnChanges.add(new MapChange(changedMap, player));
		turnChanges.add(new VisibleEntityChange(set4, set1, player));
	}
	
	public void createAndComputeChanges(AbstractSpawnAction action){
		List<AbstractEntity> set1 = getVisibleEntities(action.getOwner());
		List<Hexagon> map1 = getPlayerMap(action.getOwner());
		if(action instanceof SpawnUnitAction){
			action.getOwner().incUnitsCreated();
			AbstractUnit unit = null;
			if(action.getSpawned().equals(Tank.class)){
			    unit = new Tank(action.getOwner(), action.getTarget());
				unitList.add(unit);
			}else if(action.getSpawned().equals(Bomber.class)){
				unit = new Bomber(action.getOwner(), action.getTarget());
				unitList.add(unit);
			}else if(action.getSpawned().equals(Battleship.class)){
				unit = new Battleship(action.getOwner(), action.getTarget());
				unitList.add(unit);
			}else if(action.getSpawned().equals(Soldier.class)){
				unit = new Soldier(action.getOwner(), action.getTarget());
				unitList.add(unit);
			}
			if(unit != null){
				turnChanges.add(new CreateEntityChange(unit));
			}
			getResolvedActions().add(action);
		} else if(action instanceof SpawnBuildingAction){
			AbstractBuilding building = null;
			if(action.getSpawned().getSimpleName().equals("Road")){
				action.getTarget().setType(TerrainType.ROAD);
				getResolvedActions().add(action);
			}else if(action.getSpawned().getSimpleName().equals("Harbour")){
				building = new Harbour(action.getOwner(), action.getTarget());
				productionBuildingList.add((AbstractProductionBuilding) building);
			}else if(action.getSpawned().getSimpleName().equals("RadarTower")){
				building = new RadarTower(action.getOwner(), action.getTarget());
				buildingList.add(building);
			}else if(action.getSpawned().getSimpleName().equals("EEF")){
				building = new EEF(action.getOwner(), action.getTarget());
				buildingList.add(building);	
			}
			if(building != null){
				turnChanges.add(new CreateEntityChange(building));
			}
			getResolvedActions().add(action);
		}
		
		utility.updateVisibleMap(action.getOwner());
		
		List<AbstractEntity> set2 = getVisibleEntities(action.getOwner());
		List<Hexagon> map2 = getPlayerMap(action.getOwner());
		System.out.println("DEBUG:" + set2.size());
		set2.removeAll(set1);
		for(AbstractEntity e : set2){
			if(e.getHexagon().equals(action.getTarget())){
				set2.remove(e);
				break;
			}
		}
		System.out.println("DEBUG2:" + set2.size());
		
		List<Hexagon> result = new ArrayList<>();
		for (int i = 0; i < map1.size(); i++) {
			if(map1.get(i).isDiscovered() != map2.get(i).isDiscovered() || map1.get(i).getType() != map2.get(i).getType()){
				result.add(map2.get(i));
			}
			
		}
		turnChanges.add(new MapChange(result, action.getOwner()));
		turnChanges.add(new VisibleEntityChange(set2, new ArrayList<AbstractEntity>(), action.getOwner()));
		getResolvedActions().add(action);
	}
	
	public void killAndComputeChanges(AbstractEntity entity, AbstractEntity enemy){
		List<AbstractEntity> set1 = getVisibleEntities(entity.getOwner());
		List<Hexagon> map1 = getPlayerMap(entity.getOwner());
		if(entity instanceof AbstractUnit){
			unitList.remove(entity);
		}else if(entity instanceof AbstractProductionBuilding){
			productionBuildingList.remove(entity);
		}else{
			buildingList.remove(entity);
		}
		entity.getOwner().incUnitsLost();
		enemy.getOwner().incUnitDestroyed();
		utility.updateVisibleMap(entity.getOwner());
		List<AbstractEntity> set2 = getVisibleEntities(entity.getOwner());
		List<Hexagon> map2 = getPlayerMap(entity.getOwner());
		set1.removeAll(set2);
		List<Hexagon> result = new ArrayList<>();
		for (int i = 0; i < map1.size(); i++) {
			if(map1.get(i).isDiscovered() != map2.get(i).isDiscovered() || map1.get(i).getType() != map2.get(i).getType()){
				result.add(map2.get(i));
			}
		}
		turnChanges.add(new RemoveEntityChange(entity.getHexagon(), entity.getOwner()));
		turnChanges.add(new MapChange(result, entity.getOwner()));
		turnChanges.add(new VisibleEntityChange(new ArrayList<AbstractEntity>(), set1, entity.getOwner()));
		
	}
	
	
	public List<AbstractEntity> getVisibleEntities(Player p)
	{
		List<AbstractEntity> ret = new ArrayList<>();
		
		for (AbstractUnit u : this.unitList) {
			if (p.getVisibleHexagons().contains(u.getHexagon())) {
				ret.add(u);
			}
		}
		
		for (AbstractBuilding u : this.buildingList) {
			if (p.getVisibleHexagons().contains(u.getHexagon())) {
				ret.add(u);
			}
		}
		
		for (AbstractProductionBuilding u : this.productionBuildingList) {
			if (p.getVisibleHexagons().contains(u.getHexagon())) {
				ret.add(u);
			}
		}
		
		
		return ret;
	}
	
}


