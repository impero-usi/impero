package ch.usi.inf.sa4.impero.impero.web.rest.game;

import java.util.List;

public class StartGameReqPOJO {
	private List<String> players;
	private int width;
	private int height;
	private int actionpoints;
	private int energy;
	private int water;
	private int land;
	private int mountain;
	private boolean lakes;
	private boolean rivers;
	private int resource;
	private String name; // name of the game
	private String owner; //Mongo playerID
	
	public StartGameReqPOJO(){
		
	}

	/**
	 * @return the players
	 */
	public List<String> getPlayers() {
		return players;
	}

	/**
	 * @param players the players to set
	 */
	public void setPlayers(List<String> players) {
		this.players = players;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the actionpoints
	 */
	public int getActionpoints() {
		return actionpoints;
	}

	/**
	 * @param actionpoints the actionpoints to set
	 */
	public void setActionpoints(int actionspoints) {
		this.actionpoints = actionspoints;
	}

	/**
	 * @return the energy
	 */
	public int getEnergy() {
		return energy;
	}

	/**
	 * @param energy the energy to set
	 */
	public void setEnergy(int energy) {
		this.energy = energy;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getWater() {
		if(this.water == 0){
			return 10;
		}else{
			return this.water;
		}
	}
	
	/**
	 * 
	 * @param water
	 */
	public void setWater(int water) {
		this.water = water;
	}


	/**
	 * 
	 * @return
	 */
	public int getLand() {
		if(this.land == 0 ){
			return 50;
		}else{
			return this.land;
		}
	}
	
	/**
	 * 
	 * @param land
	 */
	public void setLand(int land){
		this.land = land;
	}

	/**
	 * 
	 * @param mountain
	 */
	public void setMountain(int mountain) {
		this.mountain = mountain;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getMountain() {
		if(this.mountain == 0){
			return 10;
		}else{
			return this.mountain;
		}
	}

	/**
	 * 
	 * @param lakes
	 */
	public void setLakes(boolean lakes){
		this.lakes = lakes;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getLakes() {
		return this.lakes;
	}

	/**
	 * 
	 * @param rivers
	 */
	public void setRivers(boolean rivers){
		this.rivers = rivers;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getRivers() {
		return this.rivers;
	}

	/**
	 * 
	 * @param energyPerc
	 */
	public void setResource(int energyPerc){
		this.resource = energyPerc;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getResource() {
		if(this.resource==0){
			return 5;
		}else{
			return this.resource;
		}
	}
	

	

	
}
