package ch.usi.inf.sa4.impero.impero.db;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/***
 * 
 * @author Kasim Bordogna
 * 
 * Class to handle load/save data from/to MongoDB for Chat
 *
 */

public class MongoChat {
	private static DBCollection message;
	static{
		MongoChat.message = MongoConnection.getDB().getCollection("message");
	}
	
	public static List<MessagePOJO> getPlayerMessages(String playerID, String read, boolean inbox){
		List<MessagePOJO> ret = new ArrayList<MessagePOJO>();
		MessagePOJO entry = null;
		BasicDBObject query = null;
		if(read=="0"){
			 query = new BasicDBObject("to",playerID).append("read", read);
		}else{
			 query = new BasicDBObject("to",playerID);
		}
		
		BasicDBObject query2 = null;
		if(read=="0"){
			 query2 = new BasicDBObject("from",playerID).append("read", read);
		}else{
			 query2 = new BasicDBObject("from",playerID);
		}
		
		
	    BasicDBObject keys = new BasicDBObject("_id", 1).append("date",2).append("time",3).append("from",4).append("to",5).append("message",6).append("read",7);
	    DBCursor cur;
	    if(inbox){
		     cur = MongoChat.message.find(query,keys);
		     System.out.println(playerID + ": to messages:"+ cur.size());	    	
	    }else{
		     cur = MongoChat.message.find(query2,keys);
		     System.out.println(playerID + ": from messages:"+ cur.size());

	    }
	    
	    while(cur.hasNext()){
			DBObject curMessage = cur.next(); 
			entry = new MessagePOJO();
			entry.setDate(curMessage.get("date").toString());
			entry.setFrom(curMessage.get("from").toString());
			entry.setFromName(MongoPlayer.getPlayerByID(curMessage.get("from").toString()));
			entry.setId(curMessage.get("_id").toString());
			entry.setRead(curMessage.get("read").toString());
			entry.setTime(curMessage.get("time").toString());
			entry.setTo(curMessage.get("to").toString());
			entry.setToName(MongoPlayer.getPlayerByID(curMessage.get("to").toString()));
			entry.setMessage(curMessage.get("message").toString());
			ret.add(entry);
			
			// If the message was unread... set it to read
			if(curMessage.get("read").toString()=="0"){
				curMessage.put("read", "1");
				MongoChat.message.save(curMessage);
			}
	    } 
		return ret;
	}
	
	public static String insertMessage(String from, String to, String message){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		
		Date date = new Date();
		String curDate = dateFormat.format(date);
		String curTime = timeFormat.format(date);
		
		BasicDBObject doc = new BasicDBObject("date", curDate).
				append("time", curTime).append("from",from).
				append("to",to).append("message", message).
				append("read", "0");

		MongoChat.message.insert(doc);
		return ((ObjectId) doc.get("_id")).toString();	
	}

	public static int getPlayerMessagesCount(String playerID,String read) {
		BasicDBObject query = null;
		if(read=="0"){
			 query = new BasicDBObject("to",playerID).append("read", read);
		}else{
			 query = new BasicDBObject("to",playerID);
		}
		
		BasicDBObject keys = new BasicDBObject("_id", 1);
		DBCursor cur = MongoChat.message.find(query,keys);
		return cur.size();
	}
	
}
