/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.turnEnd;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * @author Kasim Bordogna
 *
 */
@Path("/action/turnend/{gameID}")
public class TurnEndRest {

	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean turnEnd(@PathParam("gameID")  String gameID,TurnEndPOJO request,@Context HttpServletRequest rq){
		String playerID = rq.getSession(false).getAttribute("id").toString();
		Game g = GameServer.getGame(gameID);
		boolean ret= true;
		System.out.println("received a TURN END request for game " + gameID + " from player "+ playerID);
		
		if(playerID.equals("5345b930036405f8f43ade4a")){
			System.out.println(playerID+" overrides turn end for all players");
			for(Player p : g.getPlayerList()){
				ret &= g.turnExecuted(p.getPlayerID());
			}
		}else{
			ret =  g.turnExecuted(playerID);
			System.out.println("logic says:"+ ret);
		}
		return ret;
	}
}
