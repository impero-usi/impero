package ch.usi.inf.sa4.impero.impero.web.rest.message;

public class MessageReqPOJO {
	private String to;
	private String message;

	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
