/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.logic.action.ActionVerifier;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;

/**
 * @author jesper
 *
 */
@Path("/action/pathRequest/{gameID}")
public class PathRequestRest {
	
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public PathRequestAnsPOJO pathRequest(@PathParam("gameID") String gameID, PathRequestReqPOJO request,@Context HttpServletRequest rq) {
		
		
		String playerID = rq.getSession(false).getAttribute("id").toString();
	
		
		System.out.println("received PATH request for game " + gameID);
		
		Game g = GameServer.getGame(gameID);
		
		ActionVerifier verifier = g.getVerifier();
		
		Map<Hexagon, Integer> cellsHex = verifier.getMovementCells(request.getRow(), request.getCol(), playerID);
		
		List<Hexagon> attackHex = verifier.getAttackCells(request.getRow(), request.getCol(), playerID);
		
		PathRequestAnsPOJO answer = new PathRequestAnsPOJO();
		
		if(!(cellsHex == null)){
			System.out.println("Movements cells:" + cellsHex.size());
			for (Hexagon hexagon : cellsHex.keySet()) {
				answer.addCoordinate(hexagon.getRow(), hexagon.getCol());
				answer.addCost(cellsHex.get(hexagon));
			}
		}else{
			System.out.println("Movements cells: empty");
		}
		
		if(!(attackHex == null)){
		for( Hexagon aHex : attackHex){
			answer.addAttackCordinate(aHex.getRow(), aHex.getCol());
		}
		}else{
			System.out.println("Attack cells: empty");
		}
		
		return answer;
	}

}
