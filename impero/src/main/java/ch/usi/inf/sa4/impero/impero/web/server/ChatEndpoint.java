package ch.usi.inf.sa4.impero.impero.web.server;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.websocket.CloseReason;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import ch.usi.inf.sa4.impero.impero.db.MongoChat;
import ch.usi.inf.sa4.impero.impero.db.MongoPlayer;
import ch.usi.inf.sa4.impero.impero.web.rest.message.MessageReqPOJO;

@ServerEndpoint(value = "/chat", configurator = GetHttpSessionConfigurator.class)
public class ChatEndpoint {

	private final static Map<String, Session> openSessions = new HashMap<>();
	private Session wsSession;
	private Integer count;
	private HttpSession httpSession;

	@OnOpen
	public void open(Session session, EndpointConfig config)
			throws IOException, EncodeException {
		this.wsSession = session;
		this.httpSession = (HttpSession) config.getUserProperties().get(
				HttpSession.class.getName());
		this.count = MongoChat.getPlayerMessagesCount(this.httpSession
				.getAttribute("id").toString(), "1");
		ChatEndpoint.openSessions.put(this.httpSession.getAttribute("id")
				.toString(), this.wsSession);
		System.out
				.println("New session " + this.httpSession.getAttribute("id"));
		session.getBasicRemote().sendText(Integer.toString(this.count));
	}

	@OnMessage
	public void onMessage(Session session, String message) {
		try {
			MessageReceivePOJO receive = new Gson().fromJson(message,
					MessageReceivePOJO.class);

			Session reciever = ChatEndpoint.openSessions.get(receive.getSession());
			
			if (reciever != null) {
				String from = MongoPlayer.getPlayerByID(receive.getSession());
				MessageSendPOJO respond = new MessageSendPOJO(from, receive.getMessage());
				reciever.getBasicRemote().sendText(new Gson().toJson(respond));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}

	@OnClose
	public void close(Session session, CloseReason reason) {
		System.err.println("Closing open session "
				+ this.httpSession.getAttribute("id").toString());
		ChatEndpoint.openSessions.remove(this.httpSession.getAttribute("id")
				.toString());
	}

	@OnError
	public void error(Session session, Throwable error) {
		/*
		 * ChatEndpoint.openSessions.remove(this.httpSession.getAttribute("id")
		 * .toString());
		 */
		System.err.println("Error in ChatEndpoint");
	}

	public class MessageReceivePOJO {
		private String session;
		private String message;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getSession() {
			return session;
		}

		public void setSession(String session) {
			this.session = session;
		}
	}
	
	public class MessageSendPOJO {
		private String from;
		private String message;

		public MessageSendPOJO(String from, String message) {
			this.from = from;
			this.message = message;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}
		
	}
}
