package ch.usi.inf.sa4.impero.impero.db;

public class PlayerEntryPOJO {

	private String actionPoints;
	private String energy;
	private String energyGathered;
	private String id;
	private String lostGames;
	private String status;
	private String totalEnergyGathered;
	private String totalUnitsCreated;
	private String totalUnitsDestroyed;
	private String totalUnitsLost;
	private String unitsCreated;
	private String unitsDestroyed;
	private String unitsLost;
	private String username;
	private String wonGames;

	public String getActionPoints() {
		return actionPoints;
	}

	public String getEnergy() {
		return energy;
	}

	public String getEnergyGathered() {
		return energyGathered;
	}

	public String getId() {
		return id;
	}

	/**
	 * @return the lostGames
	 */
	public String getLostGames() {
		return lostGames;
	}

	public String getName() {
		return username;
	}

	public String getStatus() {
		return status;
	}

	/**
	 * @return the totalEnergyGathered
	 */
	public String getTotalEnergyGathered() {
		return totalEnergyGathered;
	}
	/**
	 * @return the totalUnitsCreated
	 */
	public String getTotalUnitsCreated() {
		return totalUnitsCreated;
	}
	/**
	 * @return the totalUnitsDestroyed
	 */
	public String getTotalUnitsDestroyed() {
		return totalUnitsDestroyed;
	}
	/**
	 * @return the totalUnitsLost
	 */
	public String getTotalUnitsLost() {
		return totalUnitsLost;
	}
	public String getUnitsCreated() {
		return unitsCreated;
	}

	public String getUnitsDestroyed() {
		return unitsDestroyed;
	}

	public String getUnitsLost() {
		return unitsLost;
	}

	/**
	 * @return the wonGames
	 */
	public String getWonGames() {
		return wonGames;
	}

	public void setActionPoints(String actionPoints) {
		this.actionPoints = actionPoints;
	}

	public void setEnergy(String energy) {
		this.energy = energy;
	}

	public void setEnergyGathered(String energyGathered) {
		this.energyGathered = energyGathered;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param lostGames
	 *            the lostGames to set
	 */
	public void setLostGames(String lostGames) {
		this.lostGames = lostGames;
	}

	public void setName(String name) {
		this.username = name;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param totalEnergyGathered
	 *            the totalEnergyGathered to set
	 */
	public void setTotalEnergyGathered(String totalEnergyGathered) {
		this.totalEnergyGathered = totalEnergyGathered;
	}

	/**
	 * @param totalUnitsCreated
	 *            the totalUnitsCreated to set
	 */
	public void setTotalUnitsCreated(String totalUnitsCreated) {
		this.totalUnitsCreated = totalUnitsCreated;
	}

	/**
	 * @param totalUnitsDestroyed
	 *            the totalUnitsDestroyed to set
	 */
	public void setTotalUnitsDestroyed(String totalUnitsDestroyed) {
		this.totalUnitsDestroyed = totalUnitsDestroyed;
	}

	/**
	 * @param totalUnitsLost
	 *            the totalUnitsLost to set
	 */
	public void setTotalUnitsLost(String totalUnitsLost) {
		this.totalUnitsLost = totalUnitsLost;
	}

	public void setUnitsCreated(String unitsCreated) {
		this.unitsCreated = unitsCreated;
	}

	public void setUnitsDestroyed(String unitsDestroyed) {
		this.unitsDestroyed = unitsDestroyed;
	}

	public void setUnitsLost(String unitsLost) {
		this.unitsLost = unitsLost;
	}

	/**
	 * @param wonGames
	 *            the wonGames to set
	 */
	public void setWonGames(String wonGames) {
		this.wonGames = wonGames;
	}

}
