package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.UnitAttributeMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class Soldier extends AbstractGroundUnit {

	public Soldier(Player owner, Hexagon hexagon) {
		super(owner, hexagon, Soldier.class.getSimpleName(),
				UnitAttributeMapper.getHealth(Soldier.class),
				UnitAttributeMapper.getAttackCost(Soldier.class),
				UnitAttributeMapper.getAttackRange(Soldier.class),
				UnitAttributeMapper.getMovementRange(Soldier.class),
				UnitAttributeMapper.getOffensiveDamage(Soldier.class),
				UnitAttributeMapper.getDefensiveDamage(Soldier.class),
				UnitAttributeMapper.getVisibleRange(Soldier.class),
				UnitAttributeMapper.getAttackClass(Soldier.class),
				UnitAttributeMapper.getHealth(Soldier.class));
		// TODO Auto-generated constructor stub
	}

}
