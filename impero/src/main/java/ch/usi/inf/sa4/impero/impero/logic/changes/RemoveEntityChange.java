package ch.usi.inf.sa4.impero.impero.logic.changes;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * @author Luca
 *
 */
public class RemoveEntityChange implements AbstractChange {
	//the cell where the removed entity is
	private Hexagon cell;
	private Player owner;
	
	/**
	 * Constructor. It take the cell of the entity yo remove
	 * @param cell
	 */
	public RemoveEntityChange(Hexagon cell, Player owner) {
		this.cell = cell;
		this.owner = owner;
	}
	
	/**
	 * Getter for the field cell
	 * @return
	 */
	public Hexagon getCell() {
		return cell;
	}

	public Player getOwner() {
		return owner;
	}
}
