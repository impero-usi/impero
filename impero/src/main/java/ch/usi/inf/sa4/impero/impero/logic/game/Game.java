package ch.usi.inf.sa4.impero.impero.logic.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ch.usi.inf.sa4.impero.impero.db.GamePropertiesPOJO;
import ch.usi.inf.sa4.impero.impero.db.GameStatsPOJO;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.db.MongoPlayer;
import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.action.ActionResolver;
import ch.usi.inf.sa4.impero.impero.logic.action.ActionVerifier;
import ch.usi.inf.sa4.impero.impero.logic.changes.AbstractChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.DefeatedPlayerChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.EconomyChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.GameOverChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.MapChange;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractProductionBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.EEF;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.map.HexagonalMap;
import ch.usi.inf.sa4.impero.impero.logic.map.TerrainType;
import ch.usi.inf.sa4.impero.impero.logic.mapgenerator.MapGenerator;
import ch.usi.inf.sa4.impero.impero.logic.mappers.MovementCostMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.logic.player.PlayerState;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Utilities;

/**
 * This class is the main logic of the game, it store turns, players, entities
 * and action and resolves the actions and interaction between them
 * 
 * @author ChristianVuerich MatteoMorisoli
 * 
 */
public class Game {

	/**
	 * Reference to the map for the game.
	 */
	private final HexagonalMap map;
	/**
	 * the list of player present at the start the game.
	 */
	private final List<Player> playerList;
	/**
	 * the list of units currently alive in the game.
	 */
	private final List<AbstractUnit> unitList;
	/**
	 * the list of buildings currently alive in the game.
	 */
	private final List<AbstractBuilding> buildingList;
	/**
	 * the list of production buildings currently alive in the game.
	 */
	private final List<AbstractProductionBuilding> productionBuildingList;
	/**
	 * the map of actions that each player has already scheduled this turn.
	 */
	private final Map<Player, List<AbstractAction>> actionList;
	/**
	 * the number of action points the player gains each turn.
	 */
	private final Integer actionPointsPerTurn;
	/**
	 * production of EEF per turn.
	 */
	private final Integer EEFProductionPerTurn;
	/**
	 * the turn number.
	 */
	private Integer turnNumber;
	/**
	 * the gameId that identifies this instance of the game.
	 */
	private final String gameID;
	/**
	 * the number of player that have already declared that their turn is ended.
	 */
	private Integer turnEndPlayers;
	
	private Integer defeatedPlayers;
	/**
	 * this marks if the game has ended or not.
	 */
	private boolean gameOver;
	/**
	 * the reference to the action verifier that validates the various actions
	 * of the player.
	 */
	private final ActionVerifier verifier;
	/**
	 * the reference to the action resolver, that applies the effect of actions
	 * to the state of the game.
	 */
	private final ActionResolver resolver;
	/**
	 * a reference to an utilities object, who has some methods needed by the
	 * game.
	 */
	private final Utilities utility;
	/**
	 * the list of changes to the game.
	 */
	private final List<List<AbstractChange>> changeLog;

	public Game(final GamePropertiesPOJO g) {
		this.gameID = g.getGameID();
		this.actionPointsPerTurn = g.getActionPointsPerTurn();
		this.map = g.getMap();
		this.playerList = g.getPlayerList();
		this.actionList = g.getActionList();

		unitList = g.getUnitList();
		buildingList = g.getBuildingList();
		productionBuildingList = g.getProductionList();
		this.changeLog = new ArrayList<>();
		this.utility = new Utilities(map, unitList, buildingList,
				productionBuildingList, playerList);
		this.resolver = new ActionResolver(playerList, unitList, buildingList,
				productionBuildingList, actionList, map, utility);
		// spawnHQ(); Kasim's correct orders :)
		this.verifier = new ActionVerifier(utility, actionList);
		this.EEFProductionPerTurn = 50;
		//TODO change that according to turn save
		this.turnNumber = 1;
		this.turnEndPlayers = 0;
		this.defeatedPlayers = 0;
	}

	/**
	 * the constructor for the game class
	 * 
	 * @param mapWidth
	 *            the desired width of the map.
	 * @param mapHeight
	 *            the desired height of the map.
	 * @param actionPointsPerTurn
	 *            the number of action points per turn in this game.
	 * @param startEnergy
	 *            the number of starting energy.
	 * @param players
	 *            the list of player names in this game.
	 * @param gameID
	 *            the ID of the game on the server.
	 * @param waterPercent
	 *            the percentage of water "on land" on the map.
	 * @param landPercent
	 *            the percentage of land relative to the whole map.
	 * @param mountainPercent
	 *            the percentage of mountains.
	 * @param energyPercent
	 *            the percentage of energy cells where you can build EEF.
	 * @param lakes
	 *            if the map will have lakes.
	 * @param rivers
	 *            if the map will have rivers.
	 */
	public Game(final Integer mapWidth, final Integer mapHeight,
			final Integer actionPointsPerTurn, final Integer startEnergy,
			final List<String> players, final String gameID,
			final Integer waterPercent, final int landPercent,
			final int mountainPercent, final int energyPercent,
			final boolean lakes, final boolean rivers) {
		// Basic setup of the game.
		this.gameID = gameID;
		this.actionPointsPerTurn = actionPointsPerTurn;
		this.EEFProductionPerTurn = 50;
		this.turnNumber = 1;
		this.turnEndPlayers = 0;
		this.defeatedPlayers = 0;

		// Map setup.
		final MapGenerator mapGenerator = new MapGenerator();
		this.map = mapGenerator.generateMap(mapWidth, mapHeight, landPercent,
				waterPercent, mountainPercent, energyPercent, lakes, rivers);

		// Basic list setup.
		this.playerList = new ArrayList<>();
		this.actionList = new HashMap<>();
		Player newp;
		List<AbstractAction> newl;
		int i = 0;
		for (String player : players) {
			newp = new Player(player, i, this.getActionPointsPerTurn(),
					startEnergy);
			newp.setPlayerState(PlayerState.PLAYING);
			newl = new ArrayList<AbstractAction>();
			this.playerList.add(newp);
			this.actionList.put(newp, newl);
			i++;
		}
		unitList = new ArrayList<>();
		buildingList = new ArrayList<>();
		productionBuildingList = new ArrayList<>();
		this.changeLog = new ArrayList<>();

		// Utilities and logic setup.
		this.utility = new Utilities(map, unitList, buildingList,
				productionBuildingList, playerList);
		this.resolver = new ActionResolver(playerList, unitList, buildingList,
				productionBuildingList, actionList, map, utility);
		this.verifier = new ActionVerifier(utility, actionList);

		// HQ spawn and fog creation.

		
		spawnHQ((int)(Math.pow(landPercent/100.0 * Math.min(mapHeight, mapWidth), 2) / (2*players.size())));
		for (Player p : this.playerList) {
			this.utility.updateVisibleMap(p);
		}
	}

	/**
	 * this method handles the end turn command from the players
	 * 
	 * @param playerID
	 *            the ID that identifies the player
	 * @return returns true if the command of the player has been accepted or
	 *         was previously accepted, false otherwise
	 */
	public boolean turnExecuted(final String playerID) {
		boolean ret = false;
		for (Player player : playerList) {
			if (player.getPlayerID().equals(playerID)) {
				if (player.getPlayerState() == PlayerState.PLAYING) {
					turnEndPlayers++;
					player.setPlayerState(PlayerState.FINISHED);
					MongoGame.persistPlayers(this);
					ret = true;
				} else {
					ret = true;
				}
			}
		}

		if (turnEndPlayers == this.playerList.size()) {
			turnEndPlayers++;
			turnEnd();
		}
		return ret;
	}

	/**
	 * this method calls the action resolver and resolves all the actions
	 * scheduled for this turn, then player's HQ are checked for defeated
	 * players and, if they are still in the game, their action points and
	 * energy are updated.
	 */
	public void turnEnd() {
		getResolver().emptyResolvedAction();
		List<AbstractChange> changes = getResolver().resolveTurn();
		for (Player player : getPlayerList()) {
			if (hasHQ(player)) {
				player.setActionPoint(this.actionPointsPerTurn);
				player.setPlayerState(PlayerState.PLAYING);
				MongoGame.persistPlayers(this);
				Integer newEnergy = 0;
				for (AbstractBuilding building : buildingList) {
					if (building instanceof EEF && building.getOwner().equals(player)) {
						newEnergy = newEnergy + getEEFProductionPerTurn();
						player.incEnergyGathered(newEnergy);
					}
				}
				player.setEnergy(player.getEnergy() + newEnergy);
				changes.add(new EconomyChange(player, player.getEnergy()
						+ newEnergy, this.actionPointsPerTurn));
				//utility.updateVisibleMap(player);
				//changes.add( new MapChange(getPlayerMap(player), player));
			} else {
				player.setPlayerState(PlayerState.DEFEATED);
				augmentDefeatedPlayers();
				MongoGame.persistPlayers(this);
				
				GameStatsPOJO stats = new GameStatsPOJO();
				stats.setWon(false);
				stats.setTotalEnergyGathered(player.getEnergyGathered());
				stats.setTotalUnitDestroyed(player.getUnitsDestroyed());
				stats.setTotalUnitLost(player.getUnitsLost());
				stats.setTotalUnitsCreated(player.getUnitsCreated());
				
				MongoPlayer.updateStats(stats, player.getPlayerID());
				changes.add(new DefeatedPlayerChange(player));
				actionList.remove(player);
			}
		}

		// check for GameOver
		if (checkGameOver()) {
			this.gameOver = true;
			for (Player player : this.playerList) {
				if (player.getPlayerState() != PlayerState.DEFEATED) {
					player.setPlayerState(PlayerState.WON);
					
					GameStatsPOJO stats = new GameStatsPOJO();
					stats.setWon(true);
					stats.setTotalEnergyGathered(player.getEnergyGathered());
					stats.setTotalUnitDestroyed(player.getUnitsDestroyed());
					stats.setTotalUnitLost(player.getUnitsLost());
					stats.setTotalUnitsCreated(player.getUnitsCreated());
					
					MongoGame.persistPlayers(this);
					MongoPlayer.updateStats(stats, player.getPlayerID());
				}

			}
			changes.add(new GameOverChange(true));
		} else {
			changes.add(new GameOverChange(false));
		}

		changeLog.add(changes);
		emptyActionLists();
		getResolver().emptyTurnChanges();
		turnEndPlayers = 0 + getDefeatedPlayers();
		augmentTurnNumber();
		for (AbstractUnit unit : this.unitList) {
			unit.setActing(false);
		}
		
		// Persist the game
		MongoGame.persistGame(this);
	}

	public boolean checkGameOver() {
		List<Integer> foundTeams = new ArrayList<>();
		for (Player player : this.playerList) {
			if ((!foundTeams.contains(player.getTeamID()))
					&& (player.getPlayerState() != PlayerState.DEFEATED)) {
				foundTeams.add(player.getTeamID());
			}
		}
		if (foundTeams.size() <= 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Spawns HQ for every player in the game in random positions in the island.
	 */
	public final void spawnHQ(Integer minDistance) {
		int i = 0;
		int j = 0;
		int distance;
		Hexagon cell;
		for (Player p : this.playerList) {
			boolean exit = false;
			while (!exit) {
				i = new Random().nextInt(this.map.getWidth() - 1);
				j = new Random().nextInt(this.map.getHeight() - 1);
				cell = this.map.getCellAt(j, i);
				if (MovementCostMapper.isValidTerrainAndCost(HQ.class, cell.getType()) != -1) {
					exit = true;
//					for (AbstractUnit unit : this.unitList) {
//						if (unit.getHexagon().getRow() == i
//								&& unit.getHexagon().getCol() == j) {
//							exit = false;
//						}
//					}
//					for (AbstractBuilding building : this.buildingList) {
//						if (building.getHexagon().getRow() == i
//								&& building.getHexagon().getCol() == j) {
//							exit = false;
//						}
//					}
					for (AbstractProductionBuilding building : this.productionBuildingList) {
						if (building.getHexagon().equals(cell)) {
							exit = false;
						}
					}
					
					for (AbstractProductionBuilding building : this.productionBuildingList) {
						distance = (int) (Math.pow(building.getHexagon().getRow() - cell.getRow(), 2) + Math.pow(building.getHexagon().getCol() - cell.getCol(), 2));
						if (distance <minDistance){
							exit = false;
						}
					}
				}
			}
			this.productionBuildingList
					.add(new HQ(p, this.map.getCellAt(j, i)));
		}
	}

	/**
	 * This method checks if the HQ of a specific player is still alive.
	 * 
	 * @param player
	 *            the player object we want to check.
	 * @return true if the HQ is still in the list of production buildings,
	 *         false otherwise.
	 */
	public boolean hasHQ(Player player) {
		for (AbstractProductionBuilding building : productionBuildingList) {
			if (building.getOwner().equals(player)) {
				if (building.getName().equals(HQ.class.getSimpleName())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * this method return the list of changes occurred in the last game
	 * 
	 * @return the list of changes from turn 2, otherwise null
	 */
	public List<AbstractChange> getLastTurnChanges(Player player) {
		if (player.getLastUpdatedTurn() >= this.turnNumber) {
			return null;
		} else {
			if (this.turnNumber - 2 >= 0) {
				player.setLastUpdatedTurn(turnNumber);
				return utility.filterChanges(player,
						changeLog.get(turnNumber - 2));
			} else {
				return null;
			}
		}
	}

	/**
	 * Returns the custom map for the player.
	 * 
	 * @param p
	 *            the {@link Player}
	 * @return the map
	 */
	public List<Hexagon> getPlayerMap(Player p) {
		List<Hexagon> newPlayerMap = new ArrayList<>();
		for (Hexagon hex : this.map.get1DMap()) {
			if (p.getVisibleHexagons().contains(hex)) {
				//hex.setDiscovered(false);
				newPlayerMap.add(new Hexagon(hex,false));
			} else if (p.getDiscoveredHexagons().contains(hex)) {
				//hex.setDiscovered(true);
				newPlayerMap.add(new Hexagon(hex,true));
			} else {
				Hexagon tmp = new Hexagon(hex,false);
				tmp.setType(TerrainType.UNDISCOVERED);
				newPlayerMap.add(tmp);
			}
		}
		return newPlayerMap;
	}

	/**
	 * this method empties the action list of each player.
	 */
	public void emptyActionLists() {
		for (Player p : actionList.keySet()) {
			actionList.get(p).clear();
		}
	}

	/**
	 * the getter method for the map.
	 * 
	 * @return the map.
	 */
	public HexagonalMap getMap() {
		return map;
	}

	/**
	 * the getter method for the player list.
	 * 
	 * @return the playerList.
	 */
	public List<Player> getPlayerList() {
		return playerList;
	}

	/**
	 * the getter method for the action points per turn.
	 * 
	 * @return the actionPointsPerTurn.
	 */
	public Integer getActionPointsPerTurn() {
		return actionPointsPerTurn;
	}

	/**
	 * the getter method for the game ID.
	 * 
	 * @return the gameID
	 */
	public String getGameID() {
		return gameID;
	}

	/**
	 * the getter method for the unit list.
	 * 
	 * @return the unitList.
	 */
	public List<AbstractUnit> getUnitList() {
		return unitList;
	}

	/**
	 * The visible Units of the {@link Player} p
	 * 
	 * @param p
	 *            the player p
	 * @return the list
	 */
	public List<AbstractUnit> getVisibleUnitList(Player p) {
		List<AbstractUnit> ret = new ArrayList<>();
		for (AbstractUnit u : this.unitList) {
			if (p.getVisibleHexagons().contains(u.getHexagon())) {
				ret.add(u);
			}
		}
		return ret;
	}

	/**
	 * the getter method for the building list.
	 * 
	 * @return the buildingList.
	 */
	public List<AbstractBuilding> getBuildingList() {
		return buildingList;
	}

	/**
	 * The visible Buildings of the {@link Player} p
	 * 
	 * @param p
	 *            the player p
	 * @return the list
	 */
	public List<AbstractBuilding> getVisibleBuildingList(Player p) {
		List<AbstractBuilding> ret = new ArrayList<>();
		for (AbstractBuilding u : this.buildingList) {
			if (p.getVisibleHexagons().contains(u.getHexagon())) {
				ret.add(u);
			}
		}
		return ret;
	}

	/**
	 * the getter method for the production building list.
	 * 
	 * @return the productionBuildingList.
	 */
	public List<AbstractProductionBuilding> getProductionBuildingList() {
		return productionBuildingList;
	}

	/**
	 * The visible ProductionBuildings of the {@link Player} p
	 * 
	 * @param p
	 *            the player p
	 * @return the list
	 */
	public List<AbstractProductionBuilding> getVisibleProductionBuildingList(
			Player p) {
		List<AbstractProductionBuilding> ret = new ArrayList<>();
		for (AbstractProductionBuilding u : this.productionBuildingList) {
			if (p.getVisibleHexagons().contains(u.getHexagon())) {
				ret.add(u);
			}
		}
		return ret;
	}

	/**
	 * the getter method for the action verifier.
	 * 
	 * @return the actionVerifier.
	 */
	public ActionVerifier getVerifier() {
		return verifier;
	}

	/**
	 * the getter method for the action list map.
	 * 
	 * @return the map of lists of action of each player.
	 */
	public Map<Player, List<AbstractAction>> getActionList() {
		return actionList;
	}

	/**
	 * the getter method for the utilities.
	 * 
	 * @return the utility.
	 */
	public Utilities getUtility() {
		return this.utility;
	}

	/**
	 * the getter method for the action resolver.
	 * 
	 * @return the actionResolver.
	 */
	public ActionResolver getResolver() {
		return resolver;
	}

	public Integer getTurnNumber() {
		return turnNumber;
	}

	public void augmentTurnNumber() {
		this.turnNumber++;
	}

	public List<List<AbstractChange>> getChangeLog() {
		return this.changeLog;
	}

	public boolean getGameOver() {
		return this.gameOver;
	}

	public Integer getEEFProductionPerTurn() {
		return EEFProductionPerTurn;
	}

	/** 
	 * @param gameOver the gameOver to set
	 */
	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public Integer getDefeatedPlayers() {
		return defeatedPlayers;
	}

	public void augmentDefeatedPlayers() {
		this.defeatedPlayers++;
	}
}
