/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.moveUnit;

/**
 * @author jesper
 *
 */
public class PathRequestReqPOJO {
	
	private int row;
	private int col;
	private String type;
	private String playerID;
	
	public PathRequestReqPOJO(){}

	/**
	 * @return the x
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @param x the x to set
	 */
	public void setRow(int x) {
		this.row = x;
	}

	/**
	 * @return the y
	 */
	public int getCol() {
		return col;
	}

	/**
	 * @param y the y to set
	 */
	public void setCol(int y) {
		this.col = y;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the playerID
	 */
	public String getPlayerID() {
		return playerID;
	}

	/**
	 * @param playerID the playerID to set
	 */
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	
	
	
	

}
