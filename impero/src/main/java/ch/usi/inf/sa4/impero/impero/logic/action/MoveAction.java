package ch.usi.inf.sa4.impero.impero.logic.action;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * This class model the move action one general unit can do.
 * 
 * @author ChristianVuerich
 *
 */
public class MoveAction extends AbstractAction {
	/**
	 * this field represent the unit that moves;
	 */
	private final AbstractUnit actor;
	private final List<Hexagon> path;

	public MoveAction(Player owner, Integer actionPointCost,
			Hexagon target, final AbstractUnit actor, List<Hexagon> path) {
		super(owner, actionPointCost, target);
		this.actor = actor;
		this.path = path;
	}

	/**
	 * Method that return the unit that wants to move.
	 * 
	 * @return the actor
	 */
	public AbstractUnit getActor() {
		return actor;
	}

	public List<Hexagon> getPath() {
		return path;
	}

	public boolean isAttack() {
		// TODO Fill this method
		return false;
	}

}
