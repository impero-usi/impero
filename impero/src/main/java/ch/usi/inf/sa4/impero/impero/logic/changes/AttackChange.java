package ch.usi.inf.sa4.impero.impero.logic.changes;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;

/**
 * This class represent a change to the units involved in the long range attack
 * @author Luca
 *
 */
public class AttackChange implements AbstractChange{
	//the attacker unit
	private final Hexagon attacker;
	//the defender unit
	private final Hexagon defender;
	
	/**
	 * Constructor. It take the attacker and the defender unit
	 * @param attacker the cell where the attacker is
	 * @param defender the cell where the defender is
	 */
	public AttackChange(Hexagon attacker, Hexagon defender) {
		this.attacker = attacker;
		this.defender = defender;
	}
	
	/**
	 * Getter for the field attacker
	 * @return
	 */
	public Hexagon getAttacker() {
		return attacker;
	}
	
	/**
	 * Getter for the field defender
	 * @return
	 */
	public Hexagon getDefender() {
		return defender;
	}
}
