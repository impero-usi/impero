package ch.usi.inf.sa4.impero.impero.web.rest.player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.db.GameStatsPOJO;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.db.MongoPlayer;
import ch.usi.inf.sa4.impero.impero.db.PlayerEntryPOJO;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Pair;

/**
 * 
 * @author Kasim Bordogna
 *
 * Class that returns a list of users  (potential players) and their names
 */

@Path("/players")
public class PlayerListRest {

	@GET 
	@Produces(MediaType.APPLICATION_JSON)
	public List<PlayerEntryPOJO> getPlayers(@QueryParam("prefix") String prefix){
		if(prefix == null)
			prefix = "";
		return MongoPlayer.getPlayers(prefix);
	}

	@GET 
	@Path("stats/{playerID}")
	@Produces(MediaType.APPLICATION_JSON)
	public GameStatsPOJO getPlayerStats(@PathParam("playerID") String playerID){
		return MongoPlayer.getStats(playerID);
	}

	
	@GET
	@Path("{gameID}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PlayerEntryPOJO> getPlayersInGame(@PathParam("gameID") String gameID){
		List<PlayerEntryPOJO> ret = new ArrayList<PlayerEntryPOJO>();
		PlayerEntryPOJO entry;
		for(Player p : MongoGame.getPlayersInGame(gameID)){
			entry = new PlayerEntryPOJO();
			entry.setId(p.getPlayerID());
			entry.setEnergy(p.getEnergy().toString());
			entry.setName(MongoPlayer.getPlayerByID(p.getPlayerID()));
			entry.setStatus(p.getPlayerState().toString());
			entry.setActionPoints(p.getActionPoint().toString());
			entry.setUnitsCreated(p.getUnitsCreated().toString());
			entry.setUnitsDestroyed(p.getUnitsDestroyed().toString());
			entry.setUnitsLost(p.getUnitsLost().toString());
			entry.setEnergyGathered(p.getEnergyGathered().toString());
			ret.add(entry);
		}
		return ret;
	}

}
