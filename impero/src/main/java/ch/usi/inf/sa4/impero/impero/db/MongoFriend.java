package ch.usi.inf.sa4.impero.impero.db;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

/**
 * 
 * @author Kasim Bordogna
 *
 */
public class MongoFriend {
	private static DBCollection friends;
	static{
		MongoFriend.friends = MongoConnection.getDB().getCollection("friend");
	}
	
	public static List<FriendEntryPOJO> getFriends(String playerID){
		List<FriendEntryPOJO> ret = new ArrayList<FriendEntryPOJO>();		
		BasicDBObject q = new BasicDBObject("player1",playerID);
		DBCursor pList = MongoFriend.friends.find(q);
		while(pList.hasNext()){
			DBObject current = pList.next();
			FriendEntryPOJO entry = new FriendEntryPOJO();
			entry.setId(current.get("_id").toString());
			entry.setPlayerID(current.get("friendID").toString());
			entry.setName(MongoPlayer.getPlayer(current.get("friendID").toString()).getName().toString());
			ret.add(entry);
		}
		return ret;
	}
	
	public static boolean makeFriends(String playerID1, String playerID2){
		if(MongoPlayer.isPlayer(playerID1) && MongoPlayer.isPlayer(playerID2)){
			//If not already friends
			BasicDBObject query = new BasicDBObject("player1",playerID1).append("friendID", playerID2);
		    BasicDBObject keys = new BasicDBObject("_id", 1);
			DBCursor cur = MongoFriend.friends.find(query,keys);
			if(!cur.hasNext()){
				BasicDBObject q = new BasicDBObject("player1",playerID1).append("friendID", playerID2);
				WriteResult w = MongoFriend.friends.insert(q);				
				return w.getN() == 1 ? false : true;
			}else{
				return false;
			}
		}else{
			return false;
		}		
	}
	
	public static boolean unMakeFriend(String playerID1, String playerID2){
		BasicDBObject q = new BasicDBObject("player1",playerID1).append("friendID", playerID2);
		WriteResult w = MongoFriend.friends.remove(q);
		return w.getN() == 1 ? false : true;
	}
	
}
