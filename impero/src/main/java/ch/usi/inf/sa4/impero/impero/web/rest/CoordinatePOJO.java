package ch.usi.inf.sa4.impero.impero.web.rest;

public class CoordinatePOJO {
	private int row;
	private int col;
	
	
	/**
	 * @return the x
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @param row the x to set
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * @return the y
	 */
	public int getCol() {
		return col;
	}

	/**
	 * @param col the y to set
	 */
	public void setCol(int col) {
		this.col = col;
	}

	
}
