package ch.usi.inf.sa4.impero.impero.logic.rpg.unitattributes;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The available levels of the attackRange attribute of units.
 * @author Orestis Melkonian
 */
public enum AttackRangeLevel {
	INSANE_SHORT(1),
	CRAZY_SHORT(2),
	VERY_SHORT(3),
	LITTLE_SHORT(4),
	MEDIUM(5),
	LITTLE_LONG(6),
	VERY_LONG(7),
	CRAZY_LONG(9),
	INSANE_LONG(11);
	
	/**
	 * Map from integer values to level enum expressions.
	 */
     private static final Map<Integer, AttackRangeLevel> LOOKUP 
          = new HashMap<Integer, AttackRangeLevel>();

     static {
          for (AttackRangeLevel arl : EnumSet.allOf(AttackRangeLevel.class)) {
        	  LOOKUP.put(arl.getAttackRange(), arl);
          }
     }
     /**
      * The attackRange of the current unit.
      */
     private int attackRange;
     /**
      * Constructor.
      * @param ar The integer associated with the current attribute level.
      */
     private AttackRangeLevel(final int ar) {
          this.attackRange = ar;
     }
     /**
      * Getter for attackRange.
      * @return The current level's attack range.
      */
     public int getAttackRange() { 
    	 return this.attackRange; 
     }
}