package ch.usi.inf.sa4.impero.impero.web.rest.changes;


public class EconomyChangePOJO {
	
	private  Integer newEnergy;
	private  String playerID;
	private Integer actionPoints;
	
	public Integer getNewEnergy() {
		return newEnergy;
	}

	public void setNewEnergy(Integer newEnergy) {
		this.newEnergy = newEnergy;
	}

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	/**
	 * @return the actionPoints
	 */
	public Integer getActionPoints() {
		return actionPoints;
	}

	/**
	 * @param actionPoints the actionPoints to set
	 */
	public void setActionPoints(Integer actionPoints) {
		this.actionPoints = actionPoints;
	}
	
	
	
}
