package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.db.MongoPlayer;
import ch.usi.inf.sa4.impero.impero.db.PlayerEntryPOJO;
import ch.usi.inf.sa4.impero.impero.logic.changes.AbstractChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.AttackChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.BattleChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.CreateEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.DefeatedPlayerChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.EconomyChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.GameOverChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.MapChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.MoveChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.RemoveEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.UpdateEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.changes.VisibleEntityChange;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinateEntryPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.EntityPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.entities.EntitiesDataAndCostPOJO;
import ch.usi.inf.sa4.impero.impero.web.rest.player.PlayerPOJO;

@Path("/changes/{gameID}")
public class ChangeRest {
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON)
	public ChangeRestAnsPOJO getGame(@PathParam("gameID") String gameID,@Context HttpServletRequest ctx){
		

//		sequence : [ {type, index} ] // DONE
//		attackChange : [ { attacker, defender } ] // DONE
//		battleChange : [ HEX ] // DONE
//		createEntityChange : [ ENTITY ] // DONE
//		economyChange : [ { player, energy, actionPoints } ] // DONE
//		moveChange : [ { start, end, orientation } ] // DONE
//		removeEntityChange : [ { cell, owner } ] // DONE
//		updateEntityChange : [ { owner, cell, type, health } ] 
//		mapChange : [ hexagon ] list of changed hexagons
//		defeatedPlayerChange : [ player ] 
//		gamerOverChange : boolean
		
		
		ChangeRestAnsPOJO ret = new ChangeRestAnsPOJO();
		//Get the game from GameServer
		Game g;
		//TODO: remove this fake game sooner or later
		if(gameID.equals("123123") && !GameServer.containsGame("123123")){
			List<String> ll = new ArrayList<String>();
			ll.add("5346a7ed0364b9a1436f6f1f");
			ll.add("5346a81f03649f076148271d");
			ll.add("5346640303643dd80eb5ea8d");
			 g = new Game(80, 60,100,100, ll, "FAKE",50,10,10,5,false,false);
			 GameServer.putGame(gameID, g);
		}else{
			 g = GameServer.getGame(gameID);
		}
		
		//Get the latest changes
		List<SequencePOJO> turnSequence = new ArrayList<>();
		List<AttackChangePOJO> attackChanges = new ArrayList<>();
		List<BattleChangePOJO> battleChanges = new ArrayList<>();
		List<EconomyChangePOJO> eclist = new ArrayList<EconomyChangePOJO>();
		List<EntityPOJO> celist = new ArrayList<EntityPOJO>();
		List<MoveChangePOJO> mclist = new ArrayList<MoveChangePOJO>();
		List<RemoveEntityChangePOJO> reclist = new ArrayList<RemoveEntityChangePOJO>();
		List<UpdateEntityChangePOJO> ueclist = new ArrayList<UpdateEntityChangePOJO>();
		List<MapChangesPOJO> mapChanges = new ArrayList<>();
		List<PlayerEntryPOJO> defeatedPlayerChanges = new ArrayList<>();
		List<VisibilityEntityChangePOJO> visibleEntityChanges = new ArrayList<>();
		
		List<AbstractChange> lal = g.getLastTurnChanges(g.getUtility().getPlayerFromID(ctx.getSession().getAttribute("id").toString()));
			if(lal != null && lal.size() >0){
				
				for(AbstractChange c: lal ){
					
					SequencePOJO sequence = new SequencePOJO();
					sequence.setType(c.getClass().getSimpleName());
					turnSequence.add(sequence);
					
					if(c.getClass().equals(EconomyChange.class)){
						EconomyChangePOJO ecoc = new EconomyChangePOJO();
						ecoc.setPlayerID( ((EconomyChange)c).getPlayer().getPlayerID().toString() );
						ecoc.setNewEnergy( ((EconomyChange)c).getNewEnergy());
						ecoc.setActionPoints(((EconomyChange)c).getActionPoints());
						eclist.add(ecoc);
						sequence.setIndex(eclist.lastIndexOf(ecoc));
						
					} else if(c.getClass().equals(AttackChange.class)) {
						AttackChangePOJO acPOJO = new AttackChangePOJO();
						
						CoordinatePOJO attacker = new CoordinatePOJO();
						Hexagon hex = ((AttackChange)c).getAttacker();
						attacker.setCol(hex.getCol());
						attacker.setRow(hex.getRow());
						acPOJO.setAttacker(attacker);
						
						CoordinatePOJO defender = new CoordinatePOJO();
						hex = ((AttackChange)c).getDefender();
						defender.setCol(hex.getCol());
						defender.setRow(hex.getRow());
						acPOJO.setDefender(defender);
						
						attackChanges.add(acPOJO);
						sequence.setIndex(attackChanges.lastIndexOf(acPOJO));
						
					} else if (c.getClass().equals(BattleChange.class)) {
						
						BattleChangePOJO bcPOJO = new BattleChangePOJO();
						
						CoordinatePOJO position = new CoordinatePOJO();
						Hexagon hex = ((BattleChange)c).getPosition();
						position.setCol(hex.getCol());
						position.setRow(hex.getRow());
						
						bcPOJO.setPosition(position);
						battleChanges.add(bcPOJO);
						sequence.setIndex(battleChanges.lastIndexOf(bcPOJO));
						
					} else if(c.getClass().equals(CreateEntityChange.class)){
						EntityPOJO cec = new EntityPOJO();
						AbstractEntity entity = ((CreateEntityChange)c).getEntity();
						cec.setCol(entity.getHexagon().getCol());
						cec.setHealth(entity.getHealth());
						cec.setOwner(entity.getOwner().getPlayerID());
						cec.setRow(entity.getHexagon().getRow());
						cec.setType(entity.getName());
						cec.setMaxHealth(entity.getMaxHealth());

						// get attack class and put in list
						if (AbstractUnit.class.isAssignableFrom(entity.getClass())) {
							cec.setOrientation(((AbstractUnit)entity).getOrientation());
							List<Class<? extends AbstractEntity>> attacks = ((AbstractUnit)entity).getAttackClass();
							List<String> attackClasses = new ArrayList<>();
							for (Class<? extends AbstractEntity> entityClass : attacks) {
								attackClasses.add(entityClass.getSimpleName());
							}
							cec.setAttackClass(attackClasses);
						}
						
						celist.add(cec);
						sequence.setIndex(celist.lastIndexOf(cec));
						
					} else if(c.getClass().equals(MoveChange.class)){
						
						MoveChangePOJO mc = new MoveChangePOJO();
						Hexagon endhex = ((MoveChange)c).getEndCell();
						Hexagon starthex = ((MoveChange)c).getStartCell();
						
						CoordinatePOJO coordinateEnd = new CoordinatePOJO();
						coordinateEnd.setCol(endhex.getCol());
						coordinateEnd.setRow(endhex.getRow());
						
						CoordinatePOJO coordinateStart = new CoordinatePOJO();
						coordinateStart.setCol(starthex.getCol());
						coordinateStart.setRow(starthex.getRow());
						
						mc.setEndCell(coordinateEnd);
						mc.setStartCell(coordinateStart);
						mc.setMovementOrientation(((MoveChange)c).getMovementOrientation());
						mc.setEntityType(((MoveChange)c).getType());
						
						mclist.add(mc);
						sequence.setIndex(mclist.lastIndexOf(mc));
						
					} else if(c.getClass().equals(RemoveEntityChange.class)){
						
						RemoveEntityChangePOJO rec = new RemoveEntityChangePOJO();
						Hexagon hex = ((RemoveEntityChange)c).getCell();
						CoordinatePOJO coordinate = new CoordinatePOJO();
						coordinate.setCol(hex.getCol());
						coordinate.setRow(hex.getRow());
						rec.setCell(coordinate);
						rec.setOwner( ((RemoveEntityChange)c).getOwner().getPlayerID().toString());
						reclist.add(rec);
						sequence.setIndex(reclist.lastIndexOf(rec));
						
					} else if(c.getClass().equals(UpdateEntityChange.class)){
						
						UpdateEntityChangePOJO uec = new UpdateEntityChangePOJO();
						Hexagon hex = ((UpdateEntityChange)c).getCell();
						CoordinatePOJO coordinate = new CoordinatePOJO();
						coordinate.setCol(hex.getCol());
						coordinate.setRow(hex.getRow());
						uec.setCell(coordinate);
						uec.setHealth( ((UpdateEntityChange)c).getHealth());
						uec.setOwner( ((UpdateEntityChange)c).getOwner().getPlayerID().toString());
						uec.setType( ((UpdateEntityChange)c).getType().getSimpleName());
						ueclist.add(uec);
						sequence.setIndex(ueclist.lastIndexOf(uec));
					} else if(c.getClass().equals(MapChange.class)) {
						
						List<CoordinateEntryPOJO> changes = new ArrayList<>();
						List<Hexagon> hex = ((MapChange)c).getNewMap();
						
						for (Hexagon h : hex) {
							CoordinateEntryPOJO change = new CoordinateEntryPOJO();
							change.setCol(h.getCol());
							change.setRow(h.getRow());
							change.setDiscovered(h.isDiscovered());
							change.setType(h.getType().ordinal());
							changes.add(change);
						}
						
						MapChangesPOJO mapChange = new MapChangesPOJO();
						mapChange.setChange(changes);
						mapChanges.add(mapChange);
						sequence.setIndex(mapChanges.lastIndexOf(mapChange));
						
					} else if(c.getClass().equals(DefeatedPlayerChange.class)) {

						Player p = ((DefeatedPlayerChange)c).getPlayer();
						PlayerEntryPOJO pPOJO = MongoPlayer.getPlayer(p.getPlayerID());
						pPOJO.setActionPoints(p.getActionPoint().toString());
						pPOJO.setEnergy(p.getEnergy().toString());
						defeatedPlayerChanges.add(pPOJO);
							
						
					} else if(c.getClass().equals(GameOverChange.class)) {
						ret.setGameOverChange(((GameOverChange)c).isGameOver());
						sequence.setIndex(0);
					} else if(c.getClass().equals(VisibleEntityChange.class)){
		
						List<AbstractEntity> newVisibleList = ((VisibleEntityChange)c).getNewVisibleEntities();
						List<AbstractEntity> removedVisibleList = ((VisibleEntityChange)c).getRemovedVisibleEntities();
						
						List<EntityPOJO> newVisibleListRet = new ArrayList<EntityPOJO>();
						List<EntityPOJO> removedVisibleListRet = new ArrayList<EntityPOJO>();
						
						EntityPOJO entry;
						
						for(AbstractEntity a : newVisibleList){
							entry = new EntityPOJO();
							entry.setCol(a.getHexagon().getCol());
							entry.setRow(a.getHexagon().getRow());
							entry.setHealth(a.getHealth());
							entry.setOwner(a.getOwner().getPlayerID());
							entry.setType(a.getName());
							newVisibleListRet.add(entry);
						}
						
						for(AbstractEntity a : removedVisibleList){
							entry = new EntityPOJO();
							entry.setCol(a.getHexagon().getCol());
							entry.setRow(a.getHexagon().getRow());
							entry.setHealth(a.getHealth());
							entry.setOwner(a.getOwner().getPlayerID());
							entry.setType(a.getName());
							removedVisibleListRet.add(entry);
						}

						VisibilityEntityChangePOJO cc = new VisibilityEntityChangePOJO();
						cc.setNewVisibleEntities(newVisibleListRet);
						cc.setRemovedVisibleEntities(removedVisibleListRet);
						visibleEntityChanges.add(cc);
						sequence.setIndex(visibleEntityChanges.lastIndexOf(cc));
					}
										
				}
								
				ret.setSequence(turnSequence);
				ret.setAttackChanges(attackChanges);
				ret.setBattleChanges(battleChanges);
				ret.setCreateEntityChanges(celist);
				ret.setEconomyChanges(eclist);
				ret.setMoveChanges(mclist);
				ret.setRemoveEntityChanges(reclist);
				ret.setUpdateEntityChanges(ueclist);
				ret.setMapChange(mapChanges);
				ret.setDefeatedPlayerChange(defeatedPlayerChanges);
				ret.setVisibleEntityChanges(visibleEntityChanges);
				// game over change already set previously
				
			}else{
				System.out.println("Empty list of changes");
			}
			
	return ret;
	
	}
}
