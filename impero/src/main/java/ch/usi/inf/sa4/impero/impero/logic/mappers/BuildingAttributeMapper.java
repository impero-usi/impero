package ch.usi.inf.sa4.impero.impero.logic.mappers;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.EEF;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.entities.Harbour;
import ch.usi.inf.sa4.impero.impero.logic.entities.RadarTower;

public class BuildingAttributeMapper {

	private static Map<Class<? extends AbstractBuilding>, Integer> healthMap = new HashMap<>();
	private static Map<Class<? extends AbstractBuilding>, Integer> unitCapacityMap = new HashMap<>();
	private static Map<Class<? extends AbstractBuilding>, Integer> viewRangeMap = new HashMap<>();
	
	static {
		healthMap.put(EEF.class, 50);
		unitCapacityMap.put(EEF.class, 0);
		viewRangeMap.put(EEF.class, 1);
		
		healthMap.put(HQ.class, 300);
		unitCapacityMap.put(HQ.class, 5);
		viewRangeMap.put(HQ.class, 3);
		
		healthMap.put(RadarTower.class, 30);
		unitCapacityMap.put(RadarTower.class, 0);
		viewRangeMap.put(RadarTower.class, 6);
		
		healthMap.put(Harbour.class, 20);
		unitCapacityMap.put(Harbour.class, 0);
		viewRangeMap.put(Harbour.class, 2);
	}
	
	public static Integer getHealth(final Class< ? extends AbstractBuilding> building){
		return healthMap.get(building);
	}
	
	public static Integer getUnitCapacity(final Class< ? extends AbstractBuilding> building){
		return unitCapacityMap.get(building);
	}
	
	public static Integer getVisibleRange(final Class< ? extends AbstractBuilding> building){
		return viewRangeMap.get(building);
	}
}
