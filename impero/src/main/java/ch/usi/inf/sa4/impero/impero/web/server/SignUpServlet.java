package ch.usi.inf.sa4.impero.impero.web.server;

import java.io.IOException;
import java.net.HttpCookie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.usi.inf.sa4.impero.impero.db.MongoPlayer;

import com.mongodb.Mongo;

@WebServlet(value="/SignUpServlet", name="SignUpServlet")
public class SignUpServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String username = req.getParameter("username");
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		String password2 = req.getParameter("password2");
		String errorFields = "";
		//Username must not be zero or non-existant
		if(username.length() == 0 || username== null) {
			errorFields += "username";
		}
		//username must be unique
		if(MongoPlayer.getPlayers(username).size() != 0) {
			errorFields += ",username";
		}
		//email must be non zero or non existant
		if(email.length() == 0 || email== null) {
			errorFields += ",email";
		}
		//passwords must exist and match
		if(password.length() == 0 || password== null || !password.equals(password2)) {
			errorFields += ",password";
		}
		//if ther error string exists then redirect to signup page with errors
		if(errorFields.length() != 0) {
			resp.sendRedirect("/signup.jsp?error=" + errorFields);
			return;
		}
		Login.insUser(username, password, email);
		//set a new session
		HttpSession session = req.getSession(true);
		String id = Login.verify(username, password);
		Cookie cookie = new Cookie("id", id);
		resp.addCookie(cookie);
		session.setAttribute("username", username);
		session.setAttribute("id", id);
		//redirect to dashboard
		resp.sendRedirect(req.getContextPath() + "/index.jsp");
	}
	
	public void has_error(String where) {

	}

}
