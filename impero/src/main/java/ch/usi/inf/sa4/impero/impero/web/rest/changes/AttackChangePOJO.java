package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

public class AttackChangePOJO {
	
	//the attacker unit
	private CoordinatePOJO attacker;
	//the defender unit
	private CoordinatePOJO defender;
	
	public AttackChangePOJO() {}
	
	/**
	 * @return the attacker
	 */
	public CoordinatePOJO getAttacker() {
		return attacker;
	}
	/**
	 * @param attacker the attacker to set
	 */
	public void setAttacker(CoordinatePOJO attacker) {
		this.attacker = attacker;
	}
	/**
	 * @return the defender
	 */
	public CoordinatePOJO getDefender() {
		return defender;
	}
	/**
	 * @param defender the defender to set
	 */
	public void setDefender(CoordinatePOJO defender) {
		this.defender = defender;
	}

}
