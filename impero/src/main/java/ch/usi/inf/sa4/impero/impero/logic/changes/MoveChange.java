package ch.usi.inf.sa4.impero.impero.logic.changes;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;

/**
 * This class represent a change cause by a MoveAction
 * @author Luca
 *
 */
public class MoveChange implements AbstractChange {
	//the path of the move
	private final Hexagon startCell;
	private final Hexagon endCell;
	private final Integer movementOrientation;
	private final String type;
	
	/**
	 * Constructor. It take the path of the move
	 * @param path
	 */
	public MoveChange(final Hexagon startCell, final Hexagon endCell, final Integer movementOrientation, final String type) {
		this.startCell = startCell;
		this.endCell = endCell;
		this.movementOrientation = movementOrientation;
		this.type = type;
	}

	public Hexagon getStartCell() {
		return startCell;
	}
	
	public Hexagon getEndCell() {
		return endCell;
	}

	public Integer getMovementOrientation() {
		return movementOrientation;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
}
