/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.swap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.logic.action.ActionVerifier;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;

/**
 * @author jesper
 *
 */
@Path("/action/swap/{gameID}")
public class SwapRest {
	
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean swapAction(@PathParam("gameID")  String gameID,SwapReqPOJO request,@Context HttpServletRequest rq){
		
		String playerID = rq.getSession(false).getAttribute("id").toString();
	
		System.out.println("received a SWAP ACTION request for game " + gameID);
		
		Game g = GameServer.getGame(gameID);
		ActionVerifier verifier = g.getVerifier();
		boolean answer = verifier.popInstert(request.getFrom(), request.getTo(), playerID);
		System.out.println("ACTION SWAP: " + answer);
		MongoGame.persistPlayers(g);
		return answer;
	}

}
