package ch.usi.inf.sa4.impero.impero.logic.action;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public abstract class AbstractSpawnAction extends AbstractAction {

	private final Integer cost;
	
	private final Class<?extends AbstractEntity> spawned;
	

	
	public AbstractSpawnAction(Player owner, Integer actionPointCost,
			Hexagon target, final Integer cost, final Class<?extends AbstractEntity> spawned) {
		super(owner, actionPointCost, target);
		this.spawned = spawned;
		this.cost = cost;
		// TODO Auto-generated constructor stub
	}



	public Integer getCost() {
		return cost;
	}



	public Class<?extends AbstractEntity> getSpawned() {
		return spawned;
	}




	

}
