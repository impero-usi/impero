package ch.usi.inf.sa4.impero.impero.web.rest.changes;

public class SequencePOJO {
	
	private String type;
	private int index;
	
	public SequencePOJO(){}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	

}
