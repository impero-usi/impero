package ch.usi.inf.sa4.impero.impero.web.rest.entities;

public class EntitiesDataAndCostPOJO {
	
	
	private String name;
	private Integer energyCost;
	private Integer actionPointCost;
	public String getName() {
		return name;
	}
	
	public EntitiesDataAndCostPOJO() {
	}


	public void setName(String name) {
		this.name = name;
	}
	public Integer getEnergyCost() {
		return energyCost;
	}
	public void setEnergyCost(Integer energyCost) {
		this.energyCost = energyCost;
	}
	public Integer getActionPointCost() {
		return actionPointCost;
	}
	public void setActionPointCost(Integer actionPointCost) {
		this.actionPointCost = actionPointCost;
	}
	
	
	
}
