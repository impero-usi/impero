package ch.usi.inf.sa4.impero.impero.web.rest.player;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.db.MongoPlayer;
import ch.usi.inf.sa4.impero.impero.db.PlayerEntryPOJO;

/**
 * 
 * @author Kasim Bordogna
 *
 */

@Path("/player")
public class PlayerRest {
		@GET 
		@Produces(MediaType.TEXT_PLAIN)
		public String getPlayer(@QueryParam("id") String id, @QueryParam("name") String name){
			if(id != null){
				return MongoPlayer.getPlayerByID(id);
			}else{
				return MongoPlayer.getPlayerByName(name);
			}
		}
		
	
}
