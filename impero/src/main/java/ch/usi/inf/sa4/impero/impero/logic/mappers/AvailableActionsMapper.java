package ch.usi.inf.sa4.impero.impero.logic.mappers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.action.AbstractAction;
import ch.usi.inf.sa4.impero.impero.logic.action.AttackAction;
import ch.usi.inf.sa4.impero.impero.logic.action.MoveAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnBuildingAction;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnUnitAction;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.EmptyEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.entities.Harbour;
import ch.usi.inf.sa4.impero.impero.logic.entities.Battleship;
import ch.usi.inf.sa4.impero.impero.logic.entities.RadarTower;
import ch.usi.inf.sa4.impero.impero.logic.entities.Soldier;
import ch.usi.inf.sa4.impero.impero.logic.entities.UndefinedEntity;

/**
 * 
 * @author ChristianVuerich Class which is used for it's static method
 *         getActions which given a class of an entity returns a list of Actions
 *         classes the units of the given class can do
 * 
 */
public class AvailableActionsMapper {
	
	/**
	 * from jenkins Utility classes should not have a public constructor
	 */
	private AvailableActionsMapper(){}
	
	/**
	 * map to store which actions an entity can do.
	 */
	private static Map<Class<? extends AbstractEntity>, List<Class<? extends AbstractAction>>> map = new HashMap<>();

	/**
	 * initialize the map
	 */
	static {

		List<Class<? extends AbstractAction>> list = new ArrayList<>();

		// Add what an Battleship can do
		list = new ArrayList<>();
		list.add(MoveAction.class);
		list.add(AttackAction.class);
		map.put(Battleship.class, list);

		// Add what an Tank can do
		list = new ArrayList<>();
		list.add(MoveAction.class);
		list.add(AttackAction.class);
		map.put(Tank.class, list);
		
		//Add what a Soldier can do
		list = new ArrayList<>();
		list.add(MoveAction.class);
		list.add(AttackAction.class);
		map.put(Soldier.class, list);

		// Add what an Bomber can do
		list = new ArrayList<>();
		list.add(MoveAction.class);
		list.add(AttackAction.class);
		map.put(Bomber.class, list);

		// Add what an EmptyEntity can do
		list = new ArrayList<>();
		list.add(SpawnBuildingAction.class);
		map.put(EmptyEntity.class, list);
		
		// Add what an HQ can do
		list = new ArrayList<>();
		list.add(SpawnUnitAction.class);
		map.put(HQ.class, list);
		
		// Add what an Undefined entity can do
		map.put(UndefinedEntity.class, new ArrayList<Class<? extends AbstractAction>>());
		
		// Add what an RadarTower entity can do
		map.put(RadarTower.class, new ArrayList<Class<? extends AbstractAction>>());
		
		// Add what an Harbour can do
		list = new ArrayList<>();
		list.add(SpawnUnitAction.class);
		map.put(Harbour.class, list);

	}

	/**
	 * 
	 * @param c
	 *            class of the object class you want to know what it can do
	 * @return list of actions (classes) the object class can do
	 */
	public static List<Class<? extends AbstractAction>> getActions(
			final Class<? extends AbstractEntity> c) {
		return map.get(c);
	}

}
