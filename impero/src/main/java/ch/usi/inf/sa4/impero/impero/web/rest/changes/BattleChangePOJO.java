package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

public class BattleChangePOJO {
	
	//the attacker unit
	private CoordinatePOJO position;
	
	public BattleChangePOJO() {}

	/**
	 * @return the position
	 */
	public CoordinatePOJO getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(CoordinatePOJO position) {
		this.position = position;
	}
	
	
}
