package ch.usi.inf.sa4.impero.impero.logic.mappers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractAirUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractBuilding;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractGroundUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractNavalUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractUnit;
import ch.usi.inf.sa4.impero.impero.logic.entities.Battleship;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.Soldier;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;

public class UnitAttributeMapper {

	private static Map<Class<? extends AbstractUnit>, Integer> defensiveDamageMap = new HashMap<>();
	private static Map<Class<? extends AbstractUnit>, Integer> offensiveDamageMap = new HashMap<>();
	private static Map<Class<? extends AbstractUnit>, Integer> healthMap = new HashMap<>();
	private static Map<Class<? extends AbstractUnit>, Integer> attackCostMap = new HashMap<>();
	private static Map<Class<? extends AbstractUnit>, Integer> attackRangeMap = new HashMap<>();
	private static Map<Class<? extends AbstractUnit>, Integer> movementRangeMap = new HashMap<>();
	private static Map<Class<? extends AbstractUnit>, Integer> viewRangeMap = new HashMap<>();
	private static Map<Class<? extends AbstractUnit>, List<Class<? extends AbstractEntity>>> attackClassMap = new HashMap<>();

	static {
		List<Class<? extends AbstractEntity>> tempList;
	
		
		tempList = new ArrayList<>();
		tempList.add(AbstractGroundUnit.class);
		tempList.add(AbstractNavalUnit.class);
		tempList.add(AbstractBuilding.class);
		attackClassMap.put(Bomber.class, tempList);
		
		healthMap.put(Bomber.class, 30);
		attackCostMap.put(Bomber.class, 8);
		attackRangeMap.put(Bomber.class, 1);
		movementRangeMap.put(Bomber.class, 4);
		offensiveDamageMap.put(Bomber.class, 40);
		defensiveDamageMap.put(Bomber.class, 30);
		viewRangeMap.put(Bomber.class, 4);
		
		
		
		
		tempList = new ArrayList<>();
		tempList.add(AbstractGroundUnit.class);
		tempList.add(AbstractNavalUnit.class);
		tempList.add(AbstractBuilding.class);
		attackClassMap.put(Tank.class, tempList);
		
		healthMap.put(Tank.class, 60);
		attackCostMap.put(Tank.class, 6);
		attackRangeMap.put(Tank.class, 2);
		movementRangeMap.put(Tank.class, 3);
		offensiveDamageMap.put(Tank.class, 50);
		defensiveDamageMap.put(Tank.class, 20);
		viewRangeMap.put(Tank.class, 3);

		
		
		
		tempList = new ArrayList<>();
		tempList.add(AbstractGroundUnit.class);
		tempList.add(AbstractNavalUnit.class);
		tempList.add(AbstractAirUnit.class);
		tempList.add(AbstractBuilding.class);
		attackClassMap.put(Battleship.class, tempList);

		healthMap.put(Battleship.class, 100);
		attackCostMap.put(Battleship.class, 10);
		attackRangeMap.put(Battleship.class, 4);
		movementRangeMap.put(Battleship.class, 3);
		offensiveDamageMap.put(Battleship.class, 80);
		defensiveDamageMap.put(Battleship.class, 50);
		viewRangeMap.put(Battleship.class, 4);
		
		
		
		tempList = new ArrayList<>();
		tempList.add(AbstractGroundUnit.class);
		tempList.add(AbstractNavalUnit.class);
		tempList.add(AbstractAirUnit.class);
		attackClassMap.put(Soldier.class, tempList);
		
		healthMap.put(Soldier.class, 30);
		attackCostMap.put(Soldier.class, 4);
		attackRangeMap.put(Soldier.class, 2);
		movementRangeMap.put(Soldier.class, 2);
		offensiveDamageMap.put(Soldier.class, 30);
		defensiveDamageMap.put(Soldier.class, 20);
		viewRangeMap.put(Soldier.class, 2);
	}

	public static Integer getHealth(final Class<? extends AbstractUnit> unit) {
		return healthMap.get(unit);
	}

	public static Integer getAttackCost(final Class<? extends AbstractUnit> unit) {
		return attackCostMap.get(unit);
	}

	public static Integer getAttackRange(
			final Class<? extends AbstractUnit> unit) {
		return attackRangeMap.get(unit);
	}

	public static Integer getMovementRange(
			final Class<? extends AbstractUnit> unit) {
		return movementRangeMap.get(unit);
	}

	public static Integer getOffensiveDamage(
			final Class<? extends AbstractUnit> unit) {
		return offensiveDamageMap.get(unit);
	}

	public static Integer getDefensiveDamage(
			final Class<? extends AbstractUnit> unit) {
		return defensiveDamageMap.get(unit);
	}

	public static Integer getVisibleRange(
			final Class<? extends AbstractUnit> unit) {
		return viewRangeMap.get(unit);
	}
	
	public static List<Class<? extends AbstractEntity>> getAttackClass(
			final Class<? extends AbstractUnit> unit) {
		return attackClassMap.get(unit);
	}

}
