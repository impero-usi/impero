/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.swap;

/**
 * @author jesper
 *
 */
public class SwapReqPOJO {
	
	private int from;
	private int to;
	private String playerID;
	
	/**
	 * @return the from
	 */
	public int getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(int from) {
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public int getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(int to) {
		this.to = to;
	}
	/**
	 * @return the playerID
	 */
	public String getPlayerID() {
		return playerID;
	}
	/**
	 * @param playerID the playerID to set
	 */
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	
	

}
