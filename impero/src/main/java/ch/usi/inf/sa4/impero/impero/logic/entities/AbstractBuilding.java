package ch.usi.inf.sa4.impero.impero.logic.entities;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * The AbstractBuilding class is the superclass of every Building in the game
 * and defines some Unit-specific attributes.
 * 
 * @author Matteo Morisoli
 * 
 */
public abstract class AbstractBuilding extends AbstractEntity {
	/**
	 * The units that are being stored inside the building.
	 */
	public List<AbstractUnit> unitsInside = new ArrayList<AbstractUnit>();

	/**
	 * The maximum amount of units this building can contain.
	 */
	private int unitCapacity;

	/**
	 * Constructor for the AbstractBuilding class.
	 * 
	 * @param health
	 *            the initial health of the entity.
	 * @param owner
	 *            the player who creates the entity.
	 * @param hexagon
	 *            the cell where the entity is created.
	 * @param name
	 *            the name of the type of the entity.
	 */
	public AbstractBuilding(final Player owner, final Hexagon hexagon,
			final String name, final Integer health,
			final Integer visibleRange, final Integer buildingCapacity, final Integer maxHealth) {
		super(health, owner, hexagon, name, visibleRange, maxHealth); 
															
		this.unitCapacity = buildingCapacity;
	}

	/**
	 * the setter method for the player field.
	 * 
	 * @param health
	 *            the new owner of the entity.
	 */
	public void setOwner(Player owner) {
		this.owner = owner;
	}

	/**
	 * Getter for unitCapacity.
	 * 
	 * @return the unit capacity of the building.
	 */
	public int getUnitCapacity() {
		return unitCapacity;
	}

	/**
	 * Sets the unit capacity of the building.
	 * 
	 * @param unitCapacity
	 *            the desired unit capacity.
	 */
	public void setUnitCapacity(int unitCapacity) {
		this.unitCapacity = unitCapacity;
	}

	/**
	 * Store a unit inside the building.
	 * 
	 * @param unit
	 *            The unit to be stored.
	 */
	public void addUnit(AbstractUnit unit) {
		this.unitsInside.add(unit);
	}

	/**
	 * Remove a unit from the building.
	 * 
	 * @param unit
	 *            The unit to be removed.
	 */
	public void removeUnit(AbstractUnit unit) {
		if (this.unitsInside.contains(unit))
			this.unitsInside.remove(unit);
	}

	/**
	 * @return The list of unit inside the building.
	 */
	public ArrayList<AbstractUnit> getUnitsInside() {
		return (ArrayList<AbstractUnit>) this.unitsInside;
	}

	/**
	 * @return The number of units stored inside the building.
	 */
	public int getNumberOfUnitsInside() {
		return this.unitsInside.size();
	}
}
