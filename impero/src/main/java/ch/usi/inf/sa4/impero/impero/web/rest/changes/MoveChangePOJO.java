package ch.usi.inf.sa4.impero.impero.web.rest.changes;

import ch.usi.inf.sa4.impero.impero.web.rest.CoordinatePOJO;

public class MoveChangePOJO {
	private CoordinatePOJO startCell;
	private CoordinatePOJO endCell;
	private Integer movementOrientation;
	private String entityType;
	
	public MoveChangePOJO(){}
	
	/**
	 * @return the movementOrientation
	 */
	public Integer getMovementOrientation() {
		return movementOrientation;
	}

	/**
	 * @param movementOrientation the movementOrientation to set
	 */
	public void setMovementOrientation(Integer movementOrientation) {
		this.movementOrientation = movementOrientation;
	}

	public CoordinatePOJO getStartCell() {
		return startCell;
	}
	public void setStartCell(CoordinatePOJO startCell) {
		this.startCell = startCell;
	}
	public CoordinatePOJO getEndCell() {
		return endCell;
	}
	public void setEndCell(CoordinatePOJO endCell) {
		this.endCell = endCell;
	}

	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	
}
