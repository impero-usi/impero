package ch.usi.inf.sa4.impero.impero.web.rest.message;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;


import ch.usi.inf.sa4.impero.impero.db.MessagePOJO;
import ch.usi.inf.sa4.impero.impero.db.MongoChat;

/***
 * 
 * @author Kasim Bordogna
 *
 */

@Path("/message/")
public class MessageRest {
	
	@GET
	@Path("/{read}")
	@Produces({"application/json"})
	public List<MessagePOJO> getMessage(@Context HttpServletRequest request,@PathParam("read") String read){
		return MongoChat.getPlayerMessages(request.getSession(false).getAttribute("id").toString(), read,true);
	}
	

	@POST 
	@Consumes({"application/json"})
	public String createMessage(@Context HttpServletRequest request, MessageReqPOJO req){
		return MongoChat.insertMessage(request.getSession(false).getAttribute("id").toString(), req.getTo(), req.getMessage());
	}
	
	@GET
	@Path("/inbox/{read}")
	@Produces({"application/json"})
	public List<MessagePOJO> getInMessage(@Context HttpServletRequest request,@PathParam("read") String read){
		return MongoChat.getPlayerMessages(request.getSession(false).getAttribute("id").toString(), read,true);
	}
	
	@GET
	@Path("/outbox/{read}")
	@Produces({"application/json"})
	public List<MessagePOJO> getOutMessage(@Context HttpServletRequest request,@PathParam("read") String read){
		return MongoChat.getPlayerMessages(request.getSession(false).getAttribute("id").toString(), read,false);
	}

	@GET
	@Path("/unread")
	@Produces(MediaType.TEXT_PLAIN)
	public int getInboxMessageUnreadCound(@Context HttpServletRequest request){
		return MongoChat.getPlayerMessagesCount(request.getSession(false).getAttribute("id").toString(), "0");
	}

}
