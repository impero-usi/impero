package ch.usi.inf.sa4.impero.impero.db;

public class GameStatsPOJO {
	private boolean won;
	private int totalUnitsCreated;
	private int totalUnitsDestroyed;
	private int totalUnitsLost;
	private int totalEnergyGathered;
	private int wonGames;
	private int lostGames;
	
	public int getTotalUnitsCreated() {
		return totalUnitsCreated;
	}
	public void setTotalUnitsCreated(int totalUnitCreated) {
		this.totalUnitsCreated = totalUnitCreated;
	}
	public int getTotalUnitsDestroyed() {
		return totalUnitsDestroyed;
	}
	public void setTotalUnitDestroyed(int totalUnitDestroyed) {
		this.totalUnitsDestroyed = totalUnitDestroyed;
	}
	public int getTotalUnitLost() {
		return totalUnitsLost;
	}
	public void setTotalUnitLost(int totalUnitLost) {
		this.totalUnitsLost = totalUnitLost;
	}
	public int getTotalEnergyGathered() {
		return totalEnergyGathered;
	}
	public void setTotalEnergyGathered(int totalEnergyGathered) {
		this.totalEnergyGathered = totalEnergyGathered;
	}
	public boolean isWon() {
		return won;
	}
	public void setWon(boolean won) {
		this.won = won;
	}
	public int getWonGames() {
		return wonGames;
	}
	public void setWonGames(int wonGames) {
		this.wonGames = wonGames;
	}
	public int getLostGames() {
		return lostGames;
	}
	public void setLostGames(int lostGames) {
		this.lostGames = lostGames;
	}
}
