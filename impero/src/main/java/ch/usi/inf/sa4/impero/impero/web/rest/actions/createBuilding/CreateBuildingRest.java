/**
 * 
 */
package ch.usi.inf.sa4.impero.impero.web.rest.actions.createBuilding;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.usi.inf.sa4.impero.impero.GameServer;
import ch.usi.inf.sa4.impero.impero.db.MongoGame;
import ch.usi.inf.sa4.impero.impero.logic.action.SpawnBuildingAction;
import ch.usi.inf.sa4.impero.impero.logic.game.Game;

/**
 * @author jesper
 *
 */

@Path("/action/createBuilding/{gameID}")
public class CreateBuildingRest {

	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreateBuildingAnsPOJO createBuilding(@PathParam("gameID") String gameID,
			CreateBuildingReqPOJO request,@Context HttpServletRequest rq) {
		String playerID = rq.getSession(false).getAttribute("id").toString();
		
		System.out.println("received CREATE BUILDING request for game " + gameID);

		Game g = GameServer.getGame(gameID);	
	
		SpawnBuildingAction a = (g.getVerifier()).verifySpawnBuildingAction(request.getRow(), request.getCol(), request.getType(), playerID);
				
		CreateBuildingAnsPOJO response = new CreateBuildingAnsPOJO();
		
		if(a != null){
			response.setValid(true);
			response.setCost(a.getActionPointCost());
			response.setEnergyCost(a.getCost());
		}
		else {
			response.setValid(false);
			response.setCost(-1);
			
		}
		
		MongoGame.persistPlayers(g);
		return response;
	}
}
