package ch.usi.inf.sa4.impero.impero.logic.changes;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class MapChange implements AbstractChange {
	
	private final List<Hexagon> newMap;
	private final Player player;
	
	public MapChange(final List<Hexagon> newMap, final Player player){
		this.newMap = newMap;
		this.player = player;
	}

	public List<Hexagon> getNewMap() {
		return newMap;
	}

	public Player getPlayer() {
		return player;
	}

}
