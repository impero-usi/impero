package ch.usi.inf.sa4.impero.impero.web.rest.actions.createBuilding;

public class CreateBuildingAnsPOJO {
	private int cost;
	private boolean valid;
	private int energyCost;
	
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	/**
	 * @return the energyCost
	 */
	public int getEnergyCost() {
		return energyCost;
	}
	/**
	 * @param energyCost the energyCost to set
	 */
	public void setEnergyCost(int energyCost) {
		this.energyCost = energyCost;
	}
	
	
}
