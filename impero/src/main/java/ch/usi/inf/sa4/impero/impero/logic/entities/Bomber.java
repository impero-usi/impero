package ch.usi.inf.sa4.impero.impero.logic.entities;

import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.mappers.UnitAttributeMapper;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

public class Bomber extends AbstractAirUnit {
	/**
	 * This is the constructor for the GroundUnit01 class.
	 * 
	 * @param owner
	 *            the player who creates the entity.
	 * @param hexagon
	 *            the cell where the entity is created.
	 */
	public Bomber(Player owner, Hexagon hexagon) {
		super(owner, hexagon, Bomber.class.getSimpleName(),
				UnitAttributeMapper.getHealth(Bomber.class),
				UnitAttributeMapper.getAttackCost(Bomber.class),
				UnitAttributeMapper.getAttackRange(Bomber.class),
				UnitAttributeMapper.getMovementRange(Bomber.class),
				UnitAttributeMapper.getOffensiveDamage(Bomber.class),
				UnitAttributeMapper.getDefensiveDamage(Bomber.class),
				UnitAttributeMapper.getVisibleRange(Bomber.class),
				UnitAttributeMapper.getAttackClass(Bomber.class),
				UnitAttributeMapper.getHealth(Bomber.class));
	}
}
