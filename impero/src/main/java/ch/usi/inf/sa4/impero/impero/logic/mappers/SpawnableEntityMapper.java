package ch.usi.inf.sa4.impero.impero.logic.mappers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.Battleship;
import ch.usi.inf.sa4.impero.impero.logic.entities.Bomber;
import ch.usi.inf.sa4.impero.impero.logic.entities.EEF;
import ch.usi.inf.sa4.impero.impero.logic.entities.EmptyEntity;
import ch.usi.inf.sa4.impero.impero.logic.entities.HQ;
import ch.usi.inf.sa4.impero.impero.logic.entities.Harbour;
import ch.usi.inf.sa4.impero.impero.logic.entities.RadarTower;
import ch.usi.inf.sa4.impero.impero.logic.entities.Road;
import ch.usi.inf.sa4.impero.impero.logic.entities.Soldier;
import ch.usi.inf.sa4.impero.impero.logic.entities.Tank;
import ch.usi.inf.sa4.impero.impero.logic.map.Hexagon;
import ch.usi.inf.sa4.impero.impero.logic.utilities.Pair;

/**
 * 
 * @author ChristianVuerich 
 *		   Class which is used for it's static method
 *         getSpawnableUnits which given a class of an entity returns a list of
 *         classes the units of the given class can spawn
 */

public class SpawnableEntityMapper {
	/**
	 * To remove default public constructor
	 */
	private SpawnableEntityMapper(){}

	/**
	 * Map for entity to list of entity to store what an entity can spawn
	 */
	private static Map<Class<? extends AbstractEntity>, List<Class<? extends AbstractEntity>>> map = new HashMap<>();

	/**
	 * Initialize the map
	 */
	static {
		// Add what an EmptyEntity can spawn
		List<Class<? extends AbstractEntity>> list = new ArrayList<>();
		list.add(Road.class);
		list.add(RadarTower.class);
		list.add(Harbour.class);
		list.add(EEF.class);
		map.put(EmptyEntity.class, list);
		

		// Add what an HQ can spawn
		list = new ArrayList<>();
		list.add(Bomber.class);
		list.add(Tank.class);
		list.add(Soldier.class);
		map.put(HQ.class, list);
		
		// Add what an Harbor can spawn
		list = new ArrayList<>();
		list.add(Battleship.class);
		map.put(Harbour.class, list);

	}


	
	/**
	 * 
	 * @param c
	 *            class of the object you want to know what it can spawn
	 * @return a list of classes representing the object it can spawn
	 */
	public static Map<Class<? extends AbstractEntity>, Pair<Integer, Integer>> getSpawnableUnits(
			final Class<? extends AbstractEntity> c, Hexagon hex) {
		if(c.equals(EmptyEntity.class)){
			return getSpawnableUnitsAtCell(c, hex);
		}
		Map<Class<? extends AbstractEntity>, Pair<Integer, Integer>> result = new HashMap<>();
		for(Class<? extends AbstractEntity> e : map.get(c)){
			result.put(e, new Pair<Integer, Integer>(SpawnActionPointAndCostMapper.getSpawnActionCost(e), SpawnActionPointAndCostMapper.getSpawnEnergyCost(e)));
		}
		
		return result;
	}
	
	public static Map<Class<? extends AbstractEntity>, Pair<Integer, Integer>> getSpawnableUnitsAtCell(
			final Class<? extends AbstractEntity> c, Hexagon hex) {
		
		if(hex == null){
			return new HashMap<Class<? extends AbstractEntity>, Pair<Integer, Integer>>();
		}
		
		List<Class<? extends AbstractEntity>> actions = new ArrayList<>();
		List<Class<? extends AbstractEntity>> ref =  map.get(c);
		
		for(Class<? extends AbstractEntity> action : ref){
			if(MovementCostMapper.isValidTerrainAndCost(action, hex.getType()) != -1){
				actions.add(action);
			}
		}
		
		Map<Class<? extends AbstractEntity>, Pair<Integer, Integer>> result = new HashMap<>();
		for(Class<? extends AbstractEntity> e : actions){
			result.put(e, new Pair<Integer, Integer>(SpawnActionPointAndCostMapper.getSpawnActionCost(e), SpawnActionPointAndCostMapper.getSpawnEnergyCost(e)));
		}
		
		return result;
	}

}
