package ch.usi.inf.sa4.impero.impero.logic.changes;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;

/**
 * This class represent a change caused by a CreateUnitAction
 * @author Luca
 *
 */
public class CreateEntityChange implements AbstractChange {
	
	private AbstractEntity entity;
	
	/**
	 * Constructor. It take the player that want create the unit, the cell where spawn the new unit and the
	 * type of the new unit
	 * @param entity
	 */
	public CreateEntityChange(final AbstractEntity entity) {
		this.entity = entity;
	}
	
	/**
	 * Getter for the field owner
	 * @return
	 */
	public AbstractEntity getEntity(){
		return this.entity;
	}
}
