package ch.usi.inf.sa4.impero.impero.logic.changes;

import java.util.List;

import ch.usi.inf.sa4.impero.impero.logic.entities.AbstractEntity;
import ch.usi.inf.sa4.impero.impero.logic.player.Player;

/**
 * 
 * @author Matteo Morisoli
 *
 */
public class VisibleEntityChange implements AbstractChange {
	
	private final List<AbstractEntity> newVisibleEntities;
	private final List<AbstractEntity> removedVisibleEntities;
	private final Player player;
	
	public VisibleEntityChange(final List<AbstractEntity> newVisibleEntities, final List<AbstractEntity> removedVisibleEntities, final Player player){
		this.newVisibleEntities = newVisibleEntities;
		this.removedVisibleEntities = removedVisibleEntities;
		this.player = player;
	}

	public List<AbstractEntity> getNewVisibleEntities() {
		return newVisibleEntities;
	}

	public List<AbstractEntity> getRemovedVisibleEntities() {
		return removedVisibleEntities;
	}

	public Player getPlayer() {
		return player;
	}
	
	

}
